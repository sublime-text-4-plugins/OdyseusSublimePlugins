# -*- coding: utf-8 -*-
"""
Based on: https://github.com/VonHeikemen/sublime-pro-key-bindings
"""
from __future__ import annotations

import importlib.util
import json
import os

import sublime
import sublime_plugin

from . import sphinx_building_docs
from python_utils.lazy_import import lazy_module

if sphinx_building_docs:
    from python_utils import misc_utils
    from python_utils.sublime_text_utils import utils
else:
    misc_utils = lazy_module("python_utils.misc_utils")
    utils = lazy_module("python_utils.sublime_text_utils.utils")


__all__: list[str] = ["OdyseusProgrammaticResourcesCompilerCommand"]


def get_user_module(module_name, source, **kwargs):
    """Summary

    Parameters
    ----------
    module_name : TYPE
        Description
    source : TYPE
        Description
    **kwargs
        Description

    Returns
    -------
    TYPE
        Description
    """
    spec = importlib.util.spec_from_file_location(module_name, source)
    user_module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(user_module)

    return user_module


class OdyseusProgrammaticResourcesCompilerCommand(sublime_plugin.ApplicationCommand):
    """Summary

    Attributes
    ----------
    default_resources : TYPE
        Description
    destination : TYPE
        Description
    module : str
        Description
    res_types : tuple
        Description
    resources : list
        Description
    source : TYPE
        Description
    """

    def __init__(self):
        """Summary"""
        self.module = "odyseus_programmatic_resources"
        self.res_types = ("keymap", "commands")
        self.default_resources = {
            "keymap": {
                "source": "$packages/OdyseusSublimePlugins/scripts/programmatic_keybindings.py",
                "destination": "$packages/User/Default ($platform).sublime-keymap",
            },
            "commands": {
                "source": "$packages/OdyseusSublimePlugins/scripts/programmatic_commands.py",
                "destination": "$packages/User/OdyseusSublimePlugins.sublime-commands",
            },
        }

    def run(
        self,
        res_type="",
        source="",
        destination="",
    ):
        """Summary

        Parameters
        ----------
        res_type : str, optional
            Description
        source : str, optional
            Description
        destination : str, optional
            Description

        Returns
        -------
        TYPE
            Description

        Raises
        ------
        err
            Description
        Exception
            Description
        """
        if not res_type or res_type not in self.res_types:
            raise Exception("The first argument needs to be a string.")

        self.resources = []

        view_context = utils.get_view_context(None)
        self.source = utils.substitute_variables(
            view_context, source or self.default_resources[res_type]["source"]
        )
        self.destination = utils.substitute_variables(
            view_context, destination or self.default_resources[res_type]["destination"]
        )

        if not os.path.isfile(self.source):
            sublime.error_message("Couldn't find %s\n\nMake sure the file exists." % self.source)
            return

        try:
            user_module = get_user_module(self.module, self.source)

            if res_type == "keymap":
                user_module.keymap(self.key, self.command)
            elif res_type == "commands":
                user_module.commands(self.caption, self.command)
        except Exception as err:
            sublime.error_message(
                "Something went wrong.\nCheck out sublime console to see more information."
            )
            self.resources = []
            raise err

        if len(self.resources) == 0:
            sublime.error_message("Couldn't find any resource.")
            return

        self.save_resources()

    def save_resources(self):
        """Summary"""
        destination_exist = os.path.exists(self.destination)
        response = None

        if destination_exist:
            response = sublime.yes_no_cancel_dialog(
                "Resource file exists.", "Overwrite", "Backup and create"
            )
        else:
            self._save_resources()

        if response == sublime.DIALOG_YES:
            self._save_resources()
        elif response == sublime.DIALOG_NO:
            os.rename(
                self.destination,
                self.destination + misc_utils.get_date_time("filename") + ".bak",
            )
            self._save_resources()
        elif response == sublime.DIALOG_CANCEL:
            sublime.status_message("Operation canceled.")

    def _save_resources(self):
        """Summary"""
        destination_folder = os.path.dirname(self.destination)
        os.makedirs(destination_folder, exist_ok=True)

        with open(self.destination, "w") as file:
            json.dump(self.resources, file, indent=None)
            self.resources = []
            sublime.message_dialog("Resources updated.")

    def caption(self, spr_caption, spr_cmd, **kwargs):
        """Summary

        Parameters
        ----------
        spr_caption : TYPE
            Description
        spr_cmd : TYPE
            Description
        **kwargs
            Description

        Raises
        ------
        Exception
            Description
        """
        if not isinstance(spr_caption, str) or len(spr_caption) < 1:
            raise Exception("The first argument needs to be a string.")

        if not isinstance(spr_cmd, (str, list)) or len(spr_cmd) < 1:
            raise Exception("The command needs to be a string or an array.")

        data = {}

        data["caption"] = spr_caption

        if isinstance(spr_cmd, list):
            data["command"] = "odyseus_run_multiple_cmds"
            data["args"] = {"commands": spr_cmd}
        else:
            data["command"] = spr_cmd
            if len(kwargs) > 0:
                data["args"] = self.set_args(kwargs)

        self.resources.append(data)

    def key(self, spr_keys, spr_cmd, *spr_ctx, **kwargs):
        """Summary

        Parameters
        ----------
        spr_keys : TYPE
            Description
        spr_cmd : TYPE
            Description
        *spr_ctx
            Description
        **kwargs
            Description

        Raises
        ------
        Exception
            Description
        """
        if not isinstance(spr_keys, list) or len(spr_keys) < 1:
            raise Exception("The first argument needs to be an array of keys.")

        if not isinstance(spr_cmd, (str, list)) or len(spr_cmd) < 1:
            raise Exception("The command needs to be a string or an array.")

        data = {}

        data["keys"] = spr_keys

        if isinstance(spr_cmd, list):
            data["command"] = "odyseus_run_multiple_cmds"
            data["args"] = {"commands": spr_cmd}
        else:
            data["command"] = spr_cmd
            if len(kwargs) > 0:
                data["args"] = self.set_args(kwargs)

        if len(spr_ctx) > 0:
            data["context"] = self.context(spr_ctx)

        self.resources.append(data)

    def set_args(self, args):
        """Summary

        Parameters
        ----------
        args : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        data = {}
        for arg in args:
            data[arg] = args.get(arg)

        return data

    def context(self, args):
        """Summary

        Parameters
        ----------
        args : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        res = []
        for a in args:
            if isinstance(a, list):
                res.extend(a)
            else:
                res.append(a)

        return res

    def command(self, spr_name, **kwargs):
        """Summary

        Parameters
        ----------
        spr_name : TYPE
            Description
        **kwargs
            Description

        Returns
        -------
        TYPE
            Description
        """
        data = {}
        data["command"] = spr_name

        if len(kwargs) > 0:
            data["args"] = {}

            for k in kwargs:
                data["args"][k] = kwargs.get(k)

        return data


if __name__ == "__main__":
    pass
