# -*- coding: utf-8 -*-
"""
Attributes
----------
events : events.Events
    Events manager.
keybindings_help_key_list_html : str
    Markup for HTML list items.
keybindings_help_popup_html : str
    Markup for HTML popups.
logger : logging_system.Logger
    The logger.
plugin_name : str
    The plugin name.
queue : queue.Queue
    Queue manager.
root_folder : str
    The plugin root folder.
settings : str
    Settings object.
sphinx_building_docs : bool
    It the plugin module is imported by Sphinx when building documentation.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from python_utils.sublime_text_utils.settings import SettingsManager
    from typing import Any

import os

import sublime
import sublime_plugin

# NOTE: The SPHINX_BUILDING_DOCS environment variable is meant to be set inside the
# conf.py file of a Sphinx project.
sphinx_building_docs: bool = "SPHINX_BUILDING_DOCS" in os.environ

root_folder: str = os.path.realpath(
    os.path.abspath(
        os.path.join(
            os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir))
        )
    )
)

from python_utils import logging_system
from python_utils.sublime_text_utils import settings as settings_utils
from python_utils.sublime_text_utils import utils
from python_utils.sublime_text_utils.events import Events
from python_utils.sublime_text_utils.queue import Queue

queue: Queue = Queue()
events: Events = Events()
plugin_name: str = "OdyseusSublimePlugins"
logger: logging_system.Logger = logging_system.Logger(
    logger_name=plugin_name,
    use_file_handler=os.path.join(root_folder, "tmp", "plugin_logs"),
)
settings: settings_utils.SettingsManager = settings_utils.SettingsManager(
    settings_file=plugin_name,
    events=events,
    logger=logger,
)


__all__: list[str] = [
    "display_message_in_panel",
    "events",
    "keybindings_help_key_list_html",
    "keybindings_help_popup_html",
    "logger",
    "OdyseusPluginsToggleConsolePersistenceCommand",
    "OdyseusPluginsToggleLoggingLevelCommand",
    "plugin_name",
    "root_folder",
    "settings",
]


def set_logging_level() -> None:
    """Set logger logging level."""
    try:
        logger.set_logging_level(
            logging_level=settings.get("general.logging_level", "INFO")
        )
    except Exception as err:
        print(__file__, err)


@events.on("plugin_loaded")
def on_plugin_loaded() -> None:
    """Called on plugin loaded."""
    queue.debounce(
        settings.load,
        delay=100,
        key=f"{plugin_name}-settings.load",
    )
    queue.debounce(
        set_logging_level,
        delay=200,
        key=f"{plugin_name}-set_logging_level",
    )


@events.on("plugin_unloaded")
def on_plugin_unloaded() -> None:
    """Called on plugin unloaded."""
    settings.unobserve()
    queue.unload()
    events.destroy()


@events.on("settings_changed")
def on_settings_changed(settings_obj: SettingsManager) -> None:
    """Called on settings changed.

    Parameters
    ----------
    settings_obj : object
        An instance of :any:`python_utils.sublime_text_utils.settings.SettingsManager`.
    """
    if settings_obj.has_changed("general.logging_level"):
        queue.debounce(
            set_logging_level,
            delay=500,
            key=f"{plugin_name}-debounce-settings-changed",
        )


class OdyseusPluginsToggleLoggingLevelCommand(
    settings_utils.SettingsToggleList, sublime_plugin.ApplicationCommand
):
    """Toggle logging level command."""

    def __init__(self, *args, **kwargs) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        sublime_plugin.ApplicationCommand.__init__(self, *args, **kwargs)
        settings_utils.SettingsToggleList.__init__(
            self,
            key="general.logging_level",
            settings=settings,
            description="OdyseusPlugins - Logging level - {}",
            values_list=["DEBUG", "INFO"],
        )


class OdyseusPluginsToggleConsolePersistenceCommand(
    settings_utils.SettingsToggleBoolean, sublime_plugin.ApplicationCommand
):
    """Toggle console persistence command."""

    def __init__(self, *args, **kwargs) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        sublime_plugin.ApplicationCommand.__init__(self, *args, **kwargs)
        settings_utils.SettingsToggleBoolean.__init__(
            self,
            key="general.persist_console",
            settings=settings,
            description="OdyseusPlugins - Persist console - {}",
        )


def display_message_in_panel(
    view_or_window: sublime.View | sublime.Window | None = None,
    title: str = "",
    body: Any = "",
    file_path: str = "",
    debug: bool = False,
    log_level: str = "error",
) -> None:
    """Display message panel.

    Parameters
    ----------
    view_or_window : sublime.View | sublime.Window | None, optional
        A Sublime Text ``View`` or ``Window`` object.
    title : str, optional
        String representing a title for the message displayed in the message panel.
    body : str, optional
        The body of the message displayed in the message panel.
    file_path : str, optional
        A path to the file.
    debug : bool, optional
        If the logger's logging level is set to ``logging.DEBUG``, and this parameter is
        set to :py:class:`True`, display the message panel.
    log_level : str, optional
        ``logger`` logging level.

    Returns
    -------
    None
        Halt execution.
    """
    if debug and settings.get("general.logging_level", "ERROR").lower() != "debug":
        return

    msg: list[str] = []
    window: sublime.View | sublime.Window | None = None

    if view_or_window is None:
        window = sublime.active_window()
    elif isinstance(view_or_window, sublime.View):
        window = view_or_window.window()
        file_path = utils.get_file_path(view_or_window)
    else:
        window = view_or_window

    if title:
        msg.append("# %s%s" % ("DEBUG::" if debug else "", str(title)))

    if file_path:
        msg.append("[File](%s)" % file_path)

    # Merge title and file path into the same item.
    # https://stackoverflow.com/a/1142879 <3
    if title and file_path:
        msg[:] = [" - ".join(msg[:])]

    if body:
        msg.append(f"\n{body}\n")

    if title and body:
        log_level = "debug" if debug else log_level
        log_level = log_level.lower()
        getattr(logger, log_level)(f"{str(title)}\n{str(body)}")

    if msg:
        message = "\n".join(msg)

        if window:
            window.run_command(
                "odyseus_display_message_in_panel", {"msg": f"{message}\n***"}
            )
        else:
            sublime.error_message(message)


def get_user_storage_root() -> str:
    """Get the root folder in **Packages/User** that's used by all plugins for storage purposes.

    Returns
    -------
    str
        Path to the storage folder.
    """
    return os.path.join(sublime.packages_path(), "User", plugin_name)


keybindings_help_key_list_html: str = (
    """<li><code class="shortcut-key">{key}</code>{description}</li>"""
)
keybindings_help_popup_html: str = """<html>
<style>
    h2 {{
        margin-top: 0;
        color: var(--orangish);
        font-size: 1.5rem;
    }}
    ul {{
        margin: 0;
        padding: 5px 20px 5px;
    }}
    .shortcut-key {{
        font-size: 1rem;
        font-weight: bold;
        color: var(--greenish);
    }}
</style>
<body>
    <h2>{plugin_name} Keyboard Shortcuts</h2>
    <ul>
{keys_list}
    </ul>
</body>
</html>
"""

if __name__ == "__main__":
    pass
