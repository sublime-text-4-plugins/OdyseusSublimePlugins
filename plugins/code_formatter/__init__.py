# -*- coding: utf-8 -*-
"""Code formatter plugin.

This plugin is meant to replace any Sublime Text code formatter that uses external commands.

.. contextual-admonition::
    :title: Execution map registration logic

    The execution maps of a command definition (``exec_map`` option) can have multiple commands
    per platform. To choose the command that should be executed, the first valid command that either
    is executable or can be found in ``PATH`` is chosen. To avoid command lookups every single time
    that the ``odyseus_code_formatter`` command is executed, a valid execution map is always
    stored in :py:attr:`Storage.registered_exec_maps` mapped to a window ID.

    The :py:class:`OdyseusCodeFormatterEventListener` clears the registered execution maps
    for a window every time a window is closed and every time a project is loaded or closed.
    This clear operation is done to force a commands lookup taking into account the settings
    of a possibly new project with its own defined settings.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from collections.abc import Callable
    from typing import Any

import sublime
import sublime_plugin

from .. import display_message_in_panel
from .threads import CodeFormatterMultipleCommandsThread  # noqa
from .threads import CodeFormatterSingleCommandThread  # noqa
from .utils import Storage
from .utils import get_line_number
from .utils import get_setting
from .utils import handle_diff
from .utils import log_error
from .utils import log_scope
from .utils import log_warning
from .utils import replace_file
from .utils import replace_selections
from .utils import status_message
from .utils import to_new_file
from python_utils import cmd_utils
from python_utils import misc_utils
from python_utils.sublime_text_utils import merge_utils
from python_utils.sublime_text_utils import utils

# WARNING: Do not use lazy loading inside this module.

__all__: list[str] = [
    "OdyseusCodeFormatterOverlayCommand",
    "OdyseusCodeFormatterCommand",
    "OdyseusUpdateContentCommand",
    "OdyseusCodeFormatterEventListener",
]

_formatter_args: tuple = ("save", "ignore_selection", "diff")


class OdyseusCodeFormatterCommand(sublime_plugin.TextCommand):
    """Code formatter command."""

    def run(self, edit: sublime.Edit, cmd_id: str | list = "None", **kwargs) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        cmd_id : str | list, optional
            The command ID as defined in the plugins settings.
        **kwargs
            save : bool, optional
                If the formatted file should be saved after formatting.
            ignore_selection : bool, optional
                Ignore selections and always format the whole file.
            diff : bool, optional
                Display a diff after formatting without actually making any changes.

        Returns
        -------
        None
            Halt execution.
        """
        is_chain: bool = False
        command_handler: Callable[..., Any] = None

        if isinstance(cmd_id, list):
            is_chain = True
            command_handler = self._handle_chain_of_commands
        elif isinstance(cmd_id, str):
            is_chain = False
            command_handler = self._handle_single_command
        else:
            title: str = f"{self.__class__.__name__} Error:"
            msg: str = f"``cmd_id`` parameter should be of type ``str`` or ``list``; type detected: {type(cmd_id)}"
            display_message_in_panel(self.view, title=title, body=msg)
            log_error(msg)
            return

        command_handler_response: tuple[dict, str, bool, bool | dict] | None = command_handler(
            cmd_id
        )

        # NOTE: Logging was already done at this point. And None is returned only when the command
        # cannot or should not be executed.
        if command_handler_response is None:
            return

        (
            exec_map,
            thread_class,
            format_whole_file_allowed,
            to_new_file_def,
        ) = command_handler_response

        cmd_args: dict = self._handle_cmd_args(is_chain, cmd_id, kwargs)

        all_selections: list = utils.get_selections(
            self.view, return_whole_file=False, extract_words=False
        )

        if (cmd_args["ignore_selection"] or len(all_selections) == 0) and format_whole_file_allowed:
            text_content: str = self.view.substr(sublime.Region(0, self.view.size()))
            thread: CodeFormatterSingleCommandThread = globals()[thread_class](
                self.view, text_content, exec_map=exec_map
            )
            thread.start()

            if to_new_file_def:
                self._handle_single_thread(
                    thread,
                    lambda error: to_new_file(self.view, to_new_file_def, [thread], error),
                )
            else:
                self._handle_single_thread(
                    thread,
                    lambda error: replace_file(self.view, thread, cmd_args, error),
                )
        else:
            # Format each and every selection block
            threads: list[
                CodeFormatterSingleCommandThread | CodeFormatterMultipleCommandsThread
            ] = []
            for region in all_selections:
                # Take only the user selection
                text_content: str = self.view.substr(region)

                if text_content:
                    thread: CodeFormatterSingleCommandThread | CodeFormatterMultipleCommandsThread = globals()[
                        thread_class
                    ](
                        self.view,
                        text_content,
                        region=region,
                        exec_map=exec_map,
                    )
                    threads.append(thread)
                    thread.start()

            if to_new_file_def:
                self._handle_selection_threads(
                    threads,
                    lambda *args: to_new_file(*args),
                    data=to_new_file_def,
                )
            else:
                self._handle_selection_threads(
                    threads,
                    lambda *args: replace_selections(*args),
                    data=cmd_args,
                )

    def _handle_cmd_args(self, is_chain: bool, cmd_id: str | list, passed_cmd_args: dict) -> dict:
        """Handle command arguments.

        Parameters
        ----------
        is_chain : bool
            If ``cmd_id`` is a list of command IDs.
        cmd_id : str | list
            A command definition ID or a list of command IDs.
        passed_cmd_args : dict
            The actual keyword arguments passed to the command.

        Returns
        -------
        dict
            The result of the combination of the keyword arguments and the options
            of one or more command definitions. The truthy values win.
        """
        if is_chain:
            cmd_args: dict = {}

            for a in _formatter_args:
                for id in cmd_id:
                    cmd_args[a] = any(
                        (
                            passed_cmd_args.get(a, False),
                            Storage.command_definitions[id].get(a, False),
                        )
                    )

                    if cmd_args[a]:
                        break

            return cmd_args
        else:
            cmd_def: dict = Storage.command_definitions[cmd_id]
            return {
                a: any((passed_cmd_args.get(a, False), cmd_def.get(a, False)))
                for a in _formatter_args
            }

    def _handle_chain_of_commands(
        self, cmd_id: list[str]
    ) -> tuple[list[dict], str, bool, bool | dict] | None:
        """Handle chain of commands.

        Parameters
        ----------
        cmd_id : list[str]
            A list of command IDs as defined in the settings file.

        Returns
        -------
        tuple[list[dict], str, bool, bool | dict] | None
            Commands execution options.
        """
        exec_map: list[dict] = [self._get_exec_map(id) for id in cmd_id]

        if not all(exec_map):
            title: str = (
                "%s: Not all commands were found. Check your commands definitions."
                % self.__class__.__name__
            )
            display_message_in_panel(self.view, title=title, body=f"Commands chain: {repr(cmd_id)}")
            return None

        if not all([self.can_execute(exec_map=c) for c in exec_map]):
            log_warning(
                (
                    "One of or more of the command definitions on the chain of commands is set "
                    + "to not run on current view:\nCommand IDs:\n"
                    + "\n".join(cmd_id)
                )
            )
            log_scope(self.view)
            status_message("Execution not allowed")
            return None

        thread_class: str = "CodeFormatterMultipleCommandsThread"
        all_defs: list[dict] = [Storage.command_definitions.get(c["cmd_id"]) for c in exec_map]
        # NOTE: All definitions need to allow whole file formatting.
        format_whole_file_allowed: bool = all(
            [self._format_whole_file_allowed(c["cmd_id"]) for c in exec_map]
        )
        to_new_file_defs: list = [d.get("to_new_file") for d in all_defs if "to_new_file" in d]
        # NOTE: Only the to_new_file setting from the last command in the chain
        # will be accepted.
        to_new_file_def: Any = to_new_file_defs[-1:] if to_new_file_defs else False

        return (
            exec_map,
            thread_class,
            format_whole_file_allowed,
            to_new_file_def,
        )

    def _handle_single_command(self, cmd_id: str) -> tuple[dict, str, bool, bool | dict] | None:
        """Handle single command.

        Parameters
        ----------
        cmd_id : str
            Command ID as defined in the settings file.

        Returns
        -------
        tuple[dict, str, bool, bool | dict] | None
            Command execution options.
        """
        exec_map: dict = self._get_exec_map(cmd_id)

        if not exec_map or not exec_map["cmd"]:
            title: str = (
                "%s: No command set or found. Check your command definition."
                % self.__class__.__name__
            )
            display_message_in_panel(self.view, title=title, body=f"cmd_id: {cmd_id}")
            return None

        if not self.can_execute(exec_map=exec_map):
            log_warning(f"Command definition set to not run on current view:\nCommand ID: {cmd_id}")
            log_scope(self.view)
            status_message("Execution not allowed")
            return None

        thread_class: str = "CodeFormatterSingleCommandThread"
        format_whole_file_allowed: bool = self._format_whole_file_allowed(exec_map["cmd_id"])
        to_new_file_def: dict | bool = Storage.command_definitions.get(exec_map["cmd_id"]).get(
            "to_new_file", {}
        )

        return (
            exec_map,
            thread_class,
            format_whole_file_allowed,
            to_new_file_def,
        )

    def _handle_single_thread(
        self, thread: CodeFormatterSingleCommandThread, callback: Callable[..., None]
    ) -> None:
        """Handle thread.

        Parameters
        ----------
        thread : CodeFormatterSingleCommandThread
            Thread.
        callback : Callable[..., None]
            Method to call if a thread execution was successful.
        """
        if thread and thread.is_alive():
            sublime.set_timeout(lambda: self._handle_single_thread(thread, callback), 250)
        elif thread and thread.error:
            callback(thread.error)
        elif thread and thread.formatted_content is not False:
            callback(None)
        else:
            title: str = "%s Error:" % self.__class__.__name__
            display_message_in_panel(self.view, title=title, body=thread.error)

    def _handle_selection_threads(
        self,
        threads: list,
        callback: Callable[[sublime.View, bool | dict, list, str], None],
        process: list | None = None,
        last_error: str | None = None,
        data: dict | bool = None,
    ) -> None:
        """Handle threads.

        Parameters
        ----------
        threads : list
            List of threads.
        callback : Callable[[sublime.View, bool | dict, list, str], None]
            Method to call if a thread execution was successful.
        process : list | None, optional
            List of successfully executed threads ready to process.
        last_error : str | None, optional
            Error message.
        data : dict | bool, optional
            Data passed to the callback function.
        """
        next_threads: list[
            CodeFormatterSingleCommandThread | CodeFormatterMultipleCommandsThread
        ] = []
        if process is None:
            process = []

        for thread in threads:
            if thread.is_alive():
                next_threads.append(thread)
                continue

            if thread.formatted_content is False:
                # This thread failed
                last_error = thread.error
                continue

            # Thread completed correctly
            process.append(thread)

        if len(next_threads):
            # Some more threads to wait
            sublime.set_timeout(
                lambda: self._handle_selection_threads(
                    next_threads,
                    callback,
                    process=process,
                    last_error=last_error,
                    data=data,
                ),
                250,
            )
        else:
            callback(self.view, data, process, last_error)

    def _get_exec_map_defaults(self) -> dict:
        """Get default settings.

        Returns
        -------
        dict
            Default settings.
        """
        return {"cmd": "", "args": []}

    def _get_exec_map(self, cmd_id: str) -> dict:
        """Set settings.

        Parameters
        ----------
        cmd_id : str
            Command ID as defined in the settings file.

        Returns
        -------
        dict
            Halt execution.
        dict
            Command definition of an existent command.
        """
        try:
            return Storage.registered_exec_maps[self.view.window().id()][cmd_id]
        except KeyError:
            pass

        cmd_def: dict = Storage.command_definitions.get(cmd_id, {})
        cmd_is_enabled: bool | None = bool(cmd_def.get("enabled", True)) if cmd_def else None

        if cmd_id is None or not cmd_def or not cmd_is_enabled:
            # NOTE: This is logged only when the command definition exists and its enabled option
            # is set to false. This is a special case because if a disabled command definition
            # has been requested to be executed, either the files where the commands are
            # defined (.sublime-commands, .sublime-menu, etc.) should be revised,
            # or the command definition enabled state should be modified.
            if cmd_def and cmd_is_enabled is False:
                log_warning(
                    "Attempting to execute a command from a disabled command definition:\n"
                    f"Command definition: {repr(cmd_def)}"
                )

            return {}

        settings_exec_map: list[dict] = utils.substitute_variables(
            utils.get_view_context(self.view),
            cmd_def.get("exec_map", {}).get(sublime.platform(), []),
        )

        for _exec_map in settings_exec_map:
            if "cmd" not in _exec_map or not _exec_map["cmd"] or not _exec_map.get("enabled", True):
                continue

            # NOTE: The call to cmd_utils.can_exec is to not require the command to exist in PATH.
            # Let's see how it goes.
            if cmd_utils.can_exec(_exec_map["cmd"]) or cmd_utils.which(_exec_map["cmd"]):
                # NOTE: Store some extra data into exec_map so I don't have to pass
                # a million parameters down the road.
                _exec_map["cmd_id"] = cmd_id
                return Storage.register_exec_map(
                    self.view,
                    cmd_id,
                    misc_utils.merge_dict(self._get_exec_map_defaults(), _exec_map),
                )

        return {}

    def _format_whole_file_allowed(self, cmd_id: str):
        """Format whole file allowed.

        Parameters
        ----------
        cmd_id : str
            Command ID as defined in the settings file.

        Returns
        -------
        bool
            If it is allowed to format the whole content of a file.
        """
        return not utils.evaluate_scope_selector(
            self.view,
            Storage.command_definitions.get(cmd_id).get("whole_file_not_allowed", False),
        )

    def can_execute(self, exec_map: dict) -> bool:
        """Check if the command can be executed.

        Parameters
        ----------
        exec_map : dict
            Command definition of an existent command.

        Returns
        -------
        bool
            If the command can be executed.
        """
        return self.is_visible(exec_map=exec_map)

    def is_visible(self, **kwargs) -> bool:
        """Set command visibility.

        Parameters
        ----------
        **kwargs
            Keyword arguments.

        Returns
        -------
        bool
            If the command should be visible.
        """
        exec_map: dict = kwargs.get("exec_map") or self._get_exec_map(kwargs.get("cmd_id"))

        if not exec_map:  # It might have "enabled" set to false.
            return False

        return utils.evaluate_scope_selector(
            self.view,
            Storage.command_definitions.get(exec_map.get("cmd_id"), {}).get("is_visible", True),
        )


class OdyseusUpdateContentCommand(sublime_plugin.TextCommand):
    """Update content."""

    def run(
        self,
        edit: sublime.Edit,
        formatted_data: list | str = None,
        save: bool = False,
        diff: bool = False,
    ) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        formatted_data : list | str, optional
            It could be the actual formatted text which would be used to replace the whole file content.
            Or it could also be a list of three elements :py:class:`tuple`.
        save : bool, optional
            If the file should be saved after being formatted.
        diff : bool, optional
            Do not modify the file content, display a diff instead.
        """
        self._formatted_data: list | str = formatted_data
        self._save: bool = save
        self._diff: bool = diff

        if isinstance(formatted_data, list):
            diff_data: list[tuple[int | None, str, str]] = []

            for region_begin, region_end, formatted_text in formatted_data:
                region: sublime.Region = sublime.Region(region_begin, region_end)

                if diff:
                    diff_data.append(
                        (
                            get_line_number(self.view, region),
                            self.view.substr(region),
                            formatted_text,
                        )
                    )
                else:
                    self.view.replace(edit, region, formatted_text)

            if diff and diff_data:
                handle_diff(self.view, diff_data)
        elif isinstance(formatted_data, str):
            self._replace_whole_view(edit)

    def _replace_whole_view(self, edit: sublime.Edit) -> None:
        """Replace whole view.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.

        Returns
        -------
        None
            Halt execution.
        """
        view_settings: dict = self.view.settings()
        code: str = self.view.substr(sublime.Region(0, self.view.size()))

        if isinstance(self._formatted_data, str):
            if view_settings.get(
                "ensure_newline_at_eof_on_save"
            ) and not self._formatted_data.endswith("\n"):
                self._formatted_data = self._formatted_data + "\n"

            if self._diff:
                handle_diff(self.view, [(None, code, self._formatted_data)])
                return

            _, err = merge_utils.merge_code(self.view, edit, code, self._formatted_data)

            if err:
                title: str = "# %s Error: Merge failure:" % self.__class__.__name__
                display_message_in_panel(self.view, title=title, body=err)
            else:
                status_message("File formatted")

                if self._save:
                    self.view.run_command("save")


class OdyseusCodeFormatterOverlayCommand(sublime_plugin.TextCommand):
    """Code formatter overlay command."""

    def run(self, edit: sublime.Edit, cmd_id: str = None) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        cmd_id : str, optional
            Command ID or a list of command IDs as defined in the settings file.
        """
        view: sublime.View = self.view

        if cmd_id and view:
            view.run_command("odyseus_code_formatter", {"cmd_id": cmd_id})

    def input_description(self) -> str:
        """Allows a custom name to be show to the left of the cursor in the input box, instead of
        the default one generated from the command name.

        Returns
        -------
        str
            Input description.
        """
        return "Code Formatter"

    def input(self, args: dict) -> sublime_plugin.ListInputHandler | None:
        """If this returns something other than None, the user will be prompted for an input before
        the command is run in the **Command Palette**.

        Parameters
        ----------
        args : dict
            Arguments.

        Returns
        -------
        sublime_plugin.ListInputHandler | None
        sublime_plugin.ListInputHandler
            The input handler for the command.
        """
        if args.get("cmd_id") is None:
            return OdyseusCodeFormatterOverlayCommandIdListInputHandler(self.view)

        return None


class OdyseusCodeFormatterOverlayCommandIdListInputHandler(sublime_plugin.ListInputHandler):
    """List input handler for the ``cmd_id`` parameter.

    Attributes
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    """

    def __init__(self, view: sublime.View) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        self.view: sublime.View = view

    def name(self) -> str:
        """The command argument name this input handler is editing.

        Returns
        -------
        str
            Parameter name.
        """
        return "cmd_id"

    def placeholder(self) -> str:
        """Placeholder text is shown in the text entry box before the user has entered anything.

        Returns
        -------
        str
            The placeholder text.
        """
        return "Filter commands"

    def list_items(self) -> list:
        """This method should return the items to show in the list.

        Returns
        -------
        list
            List of :any:`CustomListInputItem` items.
        """
        if get_setting("contextual_overlay", True):
            return [
                item
                for item in Storage.command_definitions_list_items
                if item.can_execute(self.view)
            ] or [("No commands can be executed on current view", None)]
        else:
            return Storage.command_definitions_list_items or ["No commands defined?"]

    def preview(self, value: object) -> sublime.Html | None:
        """Called whenever the user changes the selected item.

        Parameters
        ----------
        value : object
            :any:`CustomListInputItem` item value.

        Returns
        -------
        sublime.Html | None
            HTML content to be displayed in an overlay's preview section.
        """
        if value is None:
            return None

        is_chain: bool = False
        can_execute: bool = False

        # NOTE: can_execute is not evaluated when contextual_overlay is enabled because the items
        # themselves were already evaluated.
        if isinstance(value, list):
            is_chain = True
            can_execute = (
                True
                if get_setting("contextual_overlay", True)
                else all(
                    [
                        utils.evaluate_scope_selector(
                            self.view,
                            Storage.command_definitions.get(cmd_id, {}).get("is_visible", True),
                        )
                        for cmd_id in value
                    ]
                )
            )
            value = ", ".join(value)
        else:
            is_chain = False
            can_execute = (
                True
                if get_setting("contextual_overlay", True)
                else utils.evaluate_scope_selector(
                    self.view,
                    Storage.command_definitions.get(value, {}).get("is_visible", True),
                )
            )
            value = value

        return sublime.Html(
            f"""<code title="Can execute"
                      style="font-weight: bold;color:var(--{"greenish" if can_execute else "orangish"});"
                >⬛</code>&nbsp;
            <span style="color:var(--foreground);">
                {f"Chain IDs: <code>{value}" if is_chain else f"Command ID: <code>{value}"}
            </code></span>"""
        )


class OdyseusCodeFormatterEventListener(sublime_plugin.EventListener):
    """Code formatter listener."""

    def on_pre_close_window(self, window: sublime.Window) -> None:
        """Called right before a window is closed, passed the Window object.

        Parameters
        ----------
        window : sublime.Window
            A Sublime Text ``Window`` object.
        """
        Storage.clear_registered_exec_maps(window)

    def on_pre_close_project(self, window: sublime.Window) -> None:
        """Called right before a project is closed, passed the Window object.

        Parameters
        ----------
        window : sublime.Window
            A Sublime Text ``Window`` object.
        """
        Storage.clear_registered_exec_maps(window)

    def on_load_project_async(self, window: sublime.Window) -> None:
        """Called right after a project is loaded, passed the Window object.

        Parameters
        ----------
        window : sublime.Window
            A Sublime Text ``Window`` object.
        """
        Storage.clear_registered_exec_maps(window)


if __name__ == "__main__":
    pass
