# -*- coding: utf-8 -*-
"""Various utilities.

Attributes
----------
error_template : str
    Error message template.
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

import difflib
import html
import io
import os

from collections import defaultdict

import sublime

from .. import display_message_in_panel
from .. import events
from .. import logger
from .. import plugin_name
from .. import settings

from python_utils import css_props_case_correction
from python_utils import misc_utils
from python_utils.sublime_text_utils import sublime_lib
from python_utils.sublime_text_utils import utils
from python_utils.sublime_text_utils.custom_classes import CustomListInputItem
from python_utils.sublime_text_utils.queue import Queue

queue: Queue = Queue()

_css_scopes: str = "source.scss, source.sass, source.css"
_status_message_template: str = "Code Formatter: {}"
_log_prefix: str = "Code Formatter:\n"

error_template: str = """Working directory: `{cwd}`
Command: `{cmd}`
```
{stderr}
```"""

_plugin_id: str = f"{plugin_name}CodeFormatter-{{}}"
_command_kind: tuple = (sublime.KIND_ID_COLOR_BLUISH, "S", "Command")
_chain_kind: tuple = (sublime.KIND_ID_COLOR_ORANGISH, "M", "Commands chain")


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"code_formatter.{s}", default)


class Storage:
    """Storage class.

    Attributes
    ----------
    command_definitions : dict
        All commands definitions as defined in the settings file.
    command_definitions_list_items : list
        All command definitions list items.
    registered_exec_maps : defaultdict
        Registered execution maps.
    """

    registered_exec_maps: defaultdict = defaultdict(dict)
    command_definitions: dict = {}
    command_definitions_list_items: list = []

    @classmethod
    def _get_window_id(cls, view_or_window: sublime.View | sublime.Window) -> int:
        """Get window ID.

        Parameters
        ----------
        view_or_window : sublime.View | sublime.Window
            A Sublime Text ``View`` or ``Window`` object.

        Returns
        -------
        int
            A window ID.
        """
        return (
            view_or_window.window().id()
            if isinstance(view_or_window, sublime.View)
            else view_or_window.id()
        )

    @classmethod
    def clear_registered_exec_maps(cls, view_or_window: sublime.View | sublime.Window) -> None:
        """Clear registered commands execution maps.

        Parameters
        ----------
        view_or_window : sublime.View | sublime.Window
            A Sublime Text ``View`` or ``Window`` object.
        """
        win_id: int = cls._get_window_id(view_or_window)

        if win_id in cls.registered_exec_maps:
            cls.registered_exec_maps[win_id].clear()

    @classmethod
    def register_exec_map(
        cls, view_or_window: sublime.View | sublime.Window, cmd_id: str, exec_map: dict
    ) -> dict:
        """Update storage.

        Parameters
        ----------
        view_or_window : sublime.View | sublime.Window
            A Sublime Text ``View`` or ``Window`` object.
        cmd_id : str
            The command ID as defined in the plugins settings.
        exec_map : dict
            Command definition of an existent command.

        Returns
        -------
        dict
            Return back the registered definition.
        """
        win_id: int = cls._get_window_id(view_or_window)

        cls.registered_exec_maps[win_id][cmd_id] = exec_map

        return cls.registered_exec_maps[win_id][cmd_id]

    @classmethod
    def update(cls) -> None:
        """Update storage."""
        cls.registered_exec_maps.clear()
        cls.command_definitions.clear()
        cls.command_definitions_list_items.clear()

        command_definitions_settings: dict = misc_utils.merge_dict(
            get_setting("command_definitions", {}),
            get_setting("command_definitions_user", {}),
            logger=logger,
        )

        chain_definitions_settings: list = get_setting("chain_definitions_user", []) + get_setting(
            "chain_definitions", []
        )

        for cmd_id, cmd_def in command_definitions_settings.items():
            if "base" in cmd_def:
                if cmd_def["base"] in command_definitions_settings:
                    if "base" in cmd_def["base"]:
                        log_warning(
                            "A command used as a base command shouldn't be based on another command:\n"
                            f"Command definition:{repr(cmd_def)}"
                        )
                        status_message("Something requires your attention")
                        continue

                    # NOTE: A command definition used as a base for another definition can have
                    # enabled set to false.
                    if "enabled" in cmd_def:
                        del cmd_def["enabled"]

                    cls.command_definitions[cmd_id] = misc_utils.merge_dict(
                        cmd_def, command_definitions_settings.get(cmd_def["base"], {})
                    )
                    continue
                else:
                    log_warning(
                        "Command definition contains a non-defined base definition:\n"
                        f"Command definition:{repr(cmd_def)}"
                    )
                    status_message("Something requires your attention")
                    continue

            cls.command_definitions[cmd_id] = cmd_def

        for cmd_id, cmd_def in cls.command_definitions.items():
            if cmd_def.get("enabled", True):
                cls.command_definitions_list_items.append(
                    CustomListInputItemWithCanExecute(
                        text=cmd_def.get("name", cmd_id),
                        value=cmd_id,
                        details=f"Command ID: <code>{html.escape(cmd_id)}</code>",
                        kind=_command_kind,
                    )
                )

        for chain_def in chain_definitions_settings:
            if chain_def.get("enabled", True):
                chain: list = chain_def.get("chain", [])
                chain_ids: str = ", ".join(chain)
                cls.command_definitions_list_items.append(
                    CustomListInputItemWithCanExecute(
                        text=chain_def.get("name", chain_ids),
                        value=chain,
                        details=f"Chain IDs: <code>{html.escape(chain_ids)}</code>",
                        kind=_chain_kind,
                    )
                )

        cls.command_definitions_list_items.sort()
        command_definitions_settings.clear()
        chain_definitions_settings.clear()


@events.on("plugin_loaded")
def on_code_formatter_plugin_loaded() -> None:
    """Called on plugin loaded."""
    queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-storage-update"))


@events.on("settings_changed")
def on_code_formatter_settings_changed(
    settings_obj: object,
) -> None:
    """Called on settings changed.

    Parameters
    ----------
    settings_obj : object
        An instance of :any:`python_utils.sublime_text_utils.settings.SettingsManager`.
    """
    if any(
        (
            settings_obj.has_changed("code_formatter.command_definitions"),
            settings_obj.has_changed("code_formatter.command_definitions_user"),
            settings_obj.has_changed("code_formatter.chain_definitions"),
            settings_obj.has_changed("code_formatter.chain_definitions_user"),
        )
    ):
        queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage"))


class CustomListInputItemWithCanExecute(CustomListInputItem):
    """An instance of ``CustomListInputItem`` with a method to check if the item
    should be displayed in the command palette.
    """

    def can_execute(self, view: sublime.View) -> bool:
        """Check if a command can be executed on a view.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.

        Returns
        -------
        bool
            If command can be executed on view.
        """
        value: object = self.value

        if isinstance(value, list):
            return all(
                [
                    utils.evaluate_scope_selector(
                        view,
                        Storage.command_definitions.get(cmd_id, {}).get("is_visible", True),
                    )
                    for cmd_id in value
                ]
            )
        else:
            return utils.evaluate_scope_selector(
                view, Storage.command_definitions.get(value, {}).get("is_visible", True)
            )


def log_scope(view: sublime.View) -> None:
    """Log view scope.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    """
    logger.info(f"Detected scope: {view.scope_name(0)}")


def log_error(msg: str) -> None:
    """Log error with a prefix.

    Parameters
    ----------
    msg : str
        Message.
    """
    logger.error(_log_prefix + msg)


def log_warning(msg: str) -> None:
    """Log warning with a prefix.

    Parameters
    ----------
    msg : str
        Message.
    """
    logger.warning(_log_prefix + msg)


def status_message(msg: str) -> None:
    """Display a status message.

    Parameters
    ----------
    msg : str
        Message to display.
    """
    sublime.status_message(_status_message_template.format(msg))


def to_new_file(
    view: sublime.View, to_new_file_def: dict | bool, threads: list, last_error: str
) -> None:
    """Save formatted data to a new file.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    to_new_file_def : dict | bool
        The ``to_new_file`` definition setting.
    threads : list
        A list of threads storing the formatted data.
    last_error : str
        Error message.

    Returns
    -------
    None
        Halt execution.
    """
    if last_error:
        title: str = "Code formatter Error:"
        display_message_in_panel(view, title=title, body=last_error)
        return

    file_path: str = utils.get_file_path(view)

    if not file_path:
        return

    output_suffix: str = "-" + misc_utils.get_date_time("filename")
    output_extension: str = ""
    overwrite_existing: str | bool = "confirm"

    if isinstance(to_new_file_def, dict):
        output_suffix = to_new_file_def.get("output_suffix", output_suffix)
        output_extension = to_new_file_def.get("output_extension", output_extension)
        overwrite_existing = to_new_file_def.get("overwrite_existing", overwrite_existing)

    name, ext = os.path.splitext(utils.get_filename(view))

    if output_extension:
        ext: str = output_extension

    new_file_path: str = os.path.join(os.path.dirname(file_path), f"{name}{output_suffix}{ext}")
    new_file_exist: bool = os.path.exists(new_file_path)
    overwrite_response: int | None = None

    formatted_data: list[str] = []
    # NOTE: Death to web technologies!!!
    is_css: bool = view.score_selector(0, _css_scopes) > 0

    if getattr(threads[0], "region") is not None and len(threads) > 1:
        threads = sorted(threads, key=lambda t: t.region.begin())

    for thread in threads:
        if isinstance(thread.formatted_content, bool):
            status_message("Something went wrong")
            log_error(thread.error)
            continue

        formatted_content: str = thread.formatted_content

        if is_css:
            formatted_content = css_props_case_correction.correct_property_case(formatted_content)

        # NOTE: thread.text_content is bytes.
        if thread.text_content == formatted_content.encode("utf-8"):
            continue

        formatted_data.append(formatted_content)

    formatted_data = "\n".join(formatted_data)

    if formatted_data:
        if new_file_exist and overwrite_existing == "confirm":
            overwrite_response = sublime.yes_no_cancel_dialog(
                "File exists.", "Overwrite", "Backup and create"
            )
        elif not new_file_exist or (new_file_exist and overwrite_existing):
            save_to_new_file(formatted_data, new_file_path)
        elif new_file_exist and not overwrite_existing:
            status_message("Operation canceled!")
            return

        if overwrite_response == sublime.DIALOG_YES:
            save_to_new_file(formatted_data, new_file_path)
        elif overwrite_response == sublime.DIALOG_NO:
            os.rename(
                new_file_path,
                new_file_path + misc_utils.get_date_time("filename") + ".bak",
            )
            save_to_new_file(formatted_data, new_file_path)
        elif overwrite_response == sublime.DIALOG_CANCEL:
            status_message("Operation canceled!")
    else:
        status_message("Nothing to format")


def save_to_new_file(formatted_data: str, new_file_path: str) -> None:
    """Save to file.

    Parameters
    ----------
    formatted_data : str
        New file content.
    new_file_path : str
        New file path.
    """
    with open(new_file_path, "w") as file:
        file.write(formatted_data)
        status_message("File saved")


def replace_file(
    view: sublime.View, thread: object, cmd_args: dict = {}, error: str | None = None
) -> None:
    """Replace the entire file content with the formatted text.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    thread : object
        Thread.
    cmd_args : dict, optional
        Arguments that were passed to the ``odyseus_code_formatter`` command.
    error : str | None, optional
        Error message.

    Returns
    -------
    None
        Halt execution. Do not make modifications if there are no changes.
    """
    if isinstance(thread.formatted_content, bool):
        status_message("Something went wrong")
        log_error(thread.error)
        return

    if (
        thread.formatted_content
        and thread.text_content.strip() == thread.formatted_content.encode("utf-8").strip()
    ):
        status_message("No formatting required")
        return

    formatted_content: str = thread.formatted_content

    # NOTE: Death to web technologies!!!
    if view.score_selector(0, _css_scopes) > 0:
        formatted_content = css_props_case_correction.correct_property_case(formatted_content)

    view.run_command(
        "odyseus_update_content",
        {
            "formatted_data": formatted_content,
            "save": cmd_args.get("save"),
            "diff": cmd_args.get("diff"),
        },
    )


def replace_selections(
    view: sublime.View, cmd_args: dict, threads: list[object], last_error: str
) -> None:
    """Replace the content of a list of selections.

    This is called when there are multiple cursors or a selection of text

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    cmd_args : dict
        Arguments that were passed to the ``odyseus_code_formatter`` command.
    threads : list[object]
        List of threads.
    last_error : str
        Error message.

    Returns
    -------
    None
        Halt execution.
    """
    if last_error:
        title: str = "Code formatter Error:"
        display_message_in_panel(view, title=title, body=last_error)
        return

    # Modify the selections from top to bottom to account for different text length
    offset: int = 0
    formatted_data: list[str] = []
    # NOTE: Death to web technologies!!!
    is_css: bool = view.score_selector(0, _css_scopes) > 0

    for thread in sorted(threads, key=lambda t: t.region.begin()):
        if isinstance(thread.formatted_content, bool):
            status_message("Something went wrong")
            log_error(thread.error)
            continue

        formatted_content: str = thread.formatted_content

        if is_css:
            formatted_content = css_props_case_correction.correct_property_case(formatted_content)

        if thread.text_content == formatted_content.encode("utf-8"):
            continue

        data: tuple = (
            thread.region.begin(),
            thread.region.end(),
            thread.formatted_content,
        )

        if offset:
            data = (
                data[0] + offset,
                data[1] + offset,
                data[2],
            )

        offset += len(thread.formatted_content) - len(thread.text_content)
        formatted_data.append(data)

    if formatted_data:
        view.run_command(
            "odyseus_update_content",
            {"formatted_data": formatted_data, "diff": cmd_args.get("diff")},
        )
        status_message("Selections formatted")
    else:
        status_message("Nothing to format")


def handle_diff(view: sublime.View, diff_data: list[tuple[int | None, str, str]]) -> None:
    """Handle diff.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    diff_data : list[tuple[int | None, str, str]]
        List of three elements :py:class:`tuple` ``(line_number, unformatted_text, formated_text)``.

    Todo
    ----
    - Figure out how to display the correct line numbers when handling selections' diffs. \
    Right now I'm just jamming the line number at the beginning of each individual selection diff.
    - Without fixing/implementing the previous issue, I can't implement patching (using the \
    diff result to apply the formatted code to the unformatted one).
    """
    diff_content: list[str] = []

    for line_number, original_text, formatted_text in diff_data:
        original_text_stream: io.StringIO = io.StringIO(original_text)
        formatted_text_stream: io.StringIO = io.StringIO(formatted_text)
        diff_stream: io.StringIO = io.StringIO()
        diff_stream.writelines(
            difflib.unified_diff(
                original_text_stream.readlines(),
                formatted_text_stream.readlines(),
                fromfile="",
                tofile="",
            )
        )

        # NOTE: Temporary workaround. See docstring's TODO.
        if line_number:
            diff_content.append(f"Line number: {line_number}")

        diff_content.append(diff_stream.getvalue())

    sublime_lib.new_view(
        view.window(),
        content="\n".join(diff_content),
        name=f"{utils.get_filename(view)}.diff",
        read_only=True,
        scratch=True,
        scope="source.diff",
    )


def get_line_number(view: sublime.View, region: sublime.Region) -> int:
    """Get line number.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    region : sublime.Region
        The region from which to extract the line number.

    Returns
    -------
    int
        Line number.
    """
    return view.rowcol(region.a)[0]


if __name__ == "__main__":
    pass
