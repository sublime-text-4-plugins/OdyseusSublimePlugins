# -*- coding: utf-8 -*-
"""Threads.
"""
from __future__ import annotations

import os
import re
import threading

import sublime

from .. import display_message_in_panel
from .. import logger
from .utils import error_template
from .utils import status_message
from python_utils import cmd_utils
from python_utils.sublime_text_utils import sublime_lib
from python_utils.sublime_text_utils import utils


class CodeFormatterSingleCommandThread(threading.Thread):
    """Thread.

    Attributes
    ----------
    error : str
        Error message.
    formatted_content : str | bool | None
        Formatted content.
    region : sublime.Region
        A ``sublime.Region`` object.
    text_content : bytes
        Text to pass to command for formatting.
    """

    def __init__(
        self,
        view: sublime.View,
        text_content: str,
        region: sublime.Region | None = None,
        exec_map: dict = {},
    ) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        text_content : str
            Text to pass to command for formatting.
        region : sublime.Region | None, optional
            A ``sublime.Region`` object.
        exec_map : dict, optional
            Command settings.
        """
        self._view: sublime.View = view
        self._exec_map: dict = exec_map

        self._file_path: str = utils.get_file_path(view)

        self._cmd: list = [exec_map.get("cmd", "")]
        self._cmd.extend(exec_map.get("args", []))
        self._cwd: str | None = exec_map.get("cwd") or (
            os.path.dirname(self._file_path) if self._file_path else None
        )

        self.text_content: bytes = text_content.encode("utf-8")
        self.region: sublime.Region = region
        self.formatted_content: str | bool | None = None
        self.error: str = ""
        threading.Thread.__init__(self)

    def read_output(self, output: bytes) -> str:
        """Read output.

        Parameters
        ----------
        output : bytes
            stdout/stderr returned by subprocess.Popen.

        Returns
        -------
        str
            Encoded output,
        """
        return str(output, encoding="utf-8")

    def run(self) -> None:
        """Run thread.

        Raises
        ------
        RuntimeError
            Missing command.
        """
        try:
            if not self._cmd:
                msg: str = "No command defined!"
                status_message(msg)
                raise RuntimeError(msg)

            # NOTE: Some CLI tools will throw errors when passing empty STDINs (mostly will print
            # usage). But since the errors are displayed in a popup, not executing the command
            # will avoid annoying popups display.
            if not self.text_content.strip():
                msg: str = "Empty STDIN!"
                status_message(msg)
                raise RuntimeError(msg)

            title: str = "OdyseusCodeFormatterCommand::CodeFormatterSingleCommandThread"
            debug_msg: str = "\n".join(
                [
                    "Command ID: `%s`" % self._exec_map.get("cmd_id", "None"),
                    "Working directory: `%s`" % self._cwd,
                    "Command: `%s`" % " ".join(self._cmd),
                ]
            )
            display_message_in_panel(
                self._view,
                title=title,
                body=debug_msg,
                file_path=self._file_path,
                debug=True,
            )
            logger.debug("%s\n%s\n%s" % (title, self._file_path, debug_msg))

            sublime_lib.ActivityIndicator.width = 5
            with sublime_lib.ActivityIndicator(
                self._view, "Formatting: %s" % self._exec_map.get("cmd_id", "None")
            ):
                with cmd_utils.popen(
                    self._cmd,
                    logger=logger,
                    bufsize=160 * len(self.text_content),
                    cwd=self._cwd,
                ) as proc:
                    stdout, stderr = proc.communicate(self.text_content)

                    if stderr:
                        self.formatted_content = False
                        self.error = error_template.format(
                            cwd=self._cwd,
                            cmd=" ".join(self._cmd),
                            stderr=self.read_output(stderr),
                        )
                    else:
                        self.formatted_content = self.read_output(stdout)

                        if self.region:
                            self.formatted_content: str = re.sub(
                                r"(\r|\r\n|\n)\Z", "", self.formatted_content
                            )
        except Exception as err:
            self.formatted_content = False
            self.error = error_template.format(
                cwd=self._cwd, cmd=" ".join(self._cmd), stderr=str(err)
            )


class CodeFormatterMultipleCommandsThread(threading.Thread):
    """Thread.

    Attributes
    ----------
    error : str
        Error message.
    formatted_content : str | bool | None
        Formatted content.
    region : sublime.Region
        A ``sublime.Region`` object.
    text_content : bytes
        Text to pass to command for formatting.
    """

    def __init__(
        self,
        view: sublime.View,
        text_content: str,
        region: sublime.Region = None,
        exec_map: dict = [],
    ) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        text_content : str
            Text to pass to command for formatting.
        region : sublime.Region, optional
            A ``sublime.Region`` object.
        exec_map : dict, optional
            Command settings list.
        """
        self._view: sublime.View = view
        self._exec_map: dict = exec_map
        self._file_path: str = utils.get_file_path(view)
        self.text_content: bytes = text_content.encode("utf-8")
        self.region: sublime.Region = region
        self.formatted_content: str | bool | None = None
        self.error: str = ""
        threading.Thread.__init__(self)

    def read_output(self, output: bytes) -> str:
        """Read output.

        Parameters
        ----------
        output : bytes
            stdout/stderr returned by subprocess.Popen.

        Returns
        -------
        str
            Encoded output,
        """
        return str(output, encoding="utf-8")

    def run(self) -> None:
        """Run thread.

        Raises
        ------
        RuntimeError
            Missing command.
        """
        try:
            if not self._exec_map:
                msg: str = "No commands defined!"
                status_message(msg)
                raise RuntimeError(msg)

            # NOTE: Some CLI tools will throw errors when passing empty STDINs (mostly will print
            # usage). But since the errors are displayed in a popup, not executing the command
            # will avoid annoying popups display.
            if not self.text_content.strip():
                msg: str = "Empty STDIN!"
                status_message(msg)
                raise RuntimeError(msg)

            title = "OdyseusCodeFormatterCommand::CodeFormatterMultipleCommandsThread"

            for cmd_setting in self._exec_map:
                cwd: str | None = cmd_setting.get("cwd") or (
                    os.path.dirname(self._file_path) if self._file_path else None
                )
                cmd: list = [cmd_setting.get("cmd", "")]
                cmd.extend(cmd_setting.get("args", []))

                debug_msg: str = "\n".join(
                    [
                        "Command IDs: `%s`" % cmd_setting.get("cmd_id", "None"),
                        "Working directory: `%s`" % cwd,
                        "Command: `%s`" % " ".join(cmd),
                    ]
                )
                display_message_in_panel(
                    self._view,
                    title=title,
                    body=debug_msg,
                    file_path=self._file_path,
                    debug=True,
                )
                logger.debug("%s\n%s\n%s" % (title, self._file_path, debug_msg))

                sublime_lib.ActivityIndicator.width = 5
                with sublime_lib.ActivityIndicator(
                    self._view, "Formatting: %s" % cmd_setting.get("cmd_id", "None")
                ):
                    text_content: bytes | str = (
                        self.formatted_content.encode("utf-8")
                        if self.formatted_content
                        else self.text_content
                    )

                    with cmd_utils.popen(
                        cmd,
                        logger=logger,
                        bufsize=160 * len(text_content),
                        cwd=cwd,
                    ) as proc:
                        stdout, stderr = proc.communicate(text_content)

                        if stderr:
                            self.formatted_content = False
                            self.error = error_template.format(
                                cwd=cwd,
                                cmd=" ".join(cmd),
                                stderr=self.read_output(stderr),
                            )
                            break
                        else:
                            self.formatted_content = self.read_output(stdout)

                            if self.region:
                                self.formatted_content = re.sub(
                                    r"(\r|\r\n|\n)\Z", "", self.formatted_content
                                )
        except Exception as err:
            self.formatted_content = False
            self.error = error_template.format(cwd="", cmd="", stderr=str(err))


if __name__ == "__main__":
    pass
