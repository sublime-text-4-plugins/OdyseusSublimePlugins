# -*- coding: utf-8 -*-
"""Case conversion plugin.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

from collections.abc import Callable

import sublime
import sublime_plugin

from . import display_message_in_panel
from . import settings
from . import sphinx_building_docs
from python_utils.lazy_import import lazy_module

if sphinx_building_docs:
    from python_utils import case_conversion
    from python_utils.sublime_text_utils import utils
else:
    case_conversion = lazy_module("python_utils.case_conversion")
    utils = lazy_module("python_utils.sublime_text_utils.utils")


__all__: list[str] = [
    "OdyseusConvertCaseCommand",
    "OdyseusToggleSnakeCamelPascalCommand",
]


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"case_conversion.{s}", default)


def toggle_case(text: str, acronyms: list) -> str:
    """Toggle text case.

    Parameters
    ----------
    text : str
        Text to convert.
    acronyms : list
        List of acronyms.

    Returns
    -------
    str
        Converted case text.
    """
    words, case, sep = case_conversion.parse_case(text, acronyms)

    if case == case_conversion.Case.PASCAL and not sep:
        return case_conversion.snake(text, acronyms)
    elif case == case_conversion.Case.LOWER and sep == "_":
        return case_conversion.camel(text, acronyms)
    elif case == case_conversion.Case.CAMEL and not sep:
        return case_conversion.pascal(text, acronyms)
    else:
        return text


def run_on_selections(
    view: sublime.View,
    edit: sublime.Edit,
    func: str | Callable[[str, list[str]], str],
    acronyms: list[str] | None = None,
) -> None:
    """Run conversion function on selections.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    edit : sublime.Edit
        A Sublime Text ``Edit`` object.
    func : str | Callable[[str, list[str]], str]
        Function or function name that will convert the text case. If ``func`` is a string, it must
        be the name of an existent function on the ``case_conversion`` module.
    acronyms : list[str] | None, optional
        List of acronyms. If :py:class:`None` or :py:class:`False`, don't use acronnyms.
        If True, use the value of the ``case_conversion.acronyms`` setting.
        If :py:class:`list`, use the specified acronyms.

    Raises
    ------
    AttributeError
        No valid case function defined.
    RuntimeError
        No case function defined.
    """
    if isinstance(func, str):
        try:
            case_func = getattr(case_conversion, func)
        except AttributeError:
            raise AttributeError("No valid case function defined. <%s>" % func)
    else:
        case_func = func

    if not case_func:
        raise RuntimeError("No case function defined.")

    if acronyms is None or isinstance(acronyms, list):
        acronyms = acronyms
    else:
        acronyms = get_setting("acronyms", [])

    replacement_data = []

    for region in utils.get_selections(view, return_whole_file=False, extract_words=True):
        try:
            text = view.substr(region)
            # Preserve leading and trailing whitespace
            leading = text[: len(text) - len(text.lstrip())]
            trailing = text[len(text.rstrip()):]
            new_text = leading + case_func(text.strip(), acronyms) + trailing

            if new_text != text:
                replacement_data.append((region, new_text))
        except Exception as err:
            title = "run_on_selections: Error"
            display_message_in_panel(title=title, body=str(err))

    if replacement_data:
        utils.replace_all_selections(view, edit, replacement_data)


class OdyseusToggleSnakeCamelPascalCommand(sublime_plugin.TextCommand):
    """Toggle selections case between snake/camel/pascal case."""

    def run(self, edit: sublime.Edit, acronyms: list[str] | None = None) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        acronyms : list[str] | None, optional
            List of acronyms. See :py:meth:`run_on_selections`.
        """
        run_on_selections(self.view, edit, toggle_case, acronyms=acronyms)


class OdyseusConvertCaseCommand(sublime_plugin.TextCommand):
    """Convert case of selections."""

    def run(
        self,
        edit: sublime.Edit,
        case_func: str = None,
        acronyms: list[str] | None = None,
    ) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        case_func : str, optional
            Function name. See :py:meth:`run_on_selections`.
        acronyms : list[str] | None, optional
            List of acronyms. See :py:meth:`run_on_selections`.
        """
        try:
            run_on_selections(self.view, edit, case_func, acronyms=acronyms)
        except Exception as err:
            title: str = "%s: Error" % self.__class__.__name__
            display_message_in_panel(title=title, body=str(err))


if __name__ == "__main__":
    pass
