# -*- coding: utf-8 -*-
"""Center comment plugin.

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

import re

import sublime
import sublime_plugin

from . import logger
from . import plugin_name
from . import settings
from python_utils.sublime_text_utils.queue import Queue

queue: Queue = Queue()

__all__: list[str] = ["OdyseusCenterCommentCommand"]

_plugin_id: str = f"{plugin_name}PluginName-{{}}"


def build_comment_data(
    view: sublime.View, pt: int
) -> tuple[list[tuple[str, bool]], list[tuple[str, str, bool]]]:
    """Build comment data.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    pt : int
        An int that represents the offset from the beginning of the editor buffer.

    Returns
    -------
    tuple[list[tuple[str, bool]], list[tuple[str, str, bool]]]
        Comment data.
    """
    shell_vars: list[dict[str, str]] = view.meta_info("shellVariables", pt)
    logger.debug(shell_vars)
    print("shell_vars", type(shell_vars))
    if not shell_vars:
        return ([], [])

    # transform the list of dicts into a single dict
    all_vars: dict[str, str] = {}
    for v in shell_vars:
        if "name" in v and "value" in v:
            all_vars[v["name"]] = v["value"]

    line_comments: list[tuple[str, bool]] = []
    block_comments: list[tuple[str, str, bool]] = []

    # transform the dict into a single array of valid comments
    suffixes: list[str] = [""] + ["_" + str(i) for i in range(1, 10)]
    for suffix in suffixes:
        start: str = all_vars.setdefault("TM_COMMENT_START" + suffix, "")
        end: str = all_vars.setdefault("TM_COMMENT_END" + suffix, "")
        disable_indent: str = all_vars.setdefault("TM_COMMENT_DISABLE_INDENT" + suffix, "")

        if start and end:
            block_comments.append((start.strip(), end.strip(), disable_indent == "yes"))
            block_comments.append((start, end, disable_indent == "yes"))
        elif start:
            line_comments.append((start.strip(), disable_indent == "yes"))
            line_comments.append((start, disable_indent == "yes"))

    return (line_comments, block_comments)


class OdyseusCenterCommentCommand(sublime_plugin.TextCommand):
    """Center comment command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        wrap_width: str | int | None = settings.get("center_comment.line_width", "wrap_width")
        line_width: int = 79

        if wrap_width == "wrap_width":
            line_width = self.view.settings().get("wrap_width")

        if not line_width:
            line_width = 79

        for region in self.view.sel():
            lines: list[sublime.Region] = self.view.lines(region)
            for line in lines:
                line_comments, block_comments = build_comment_data(self.view, line.begin())

                text: str = self.view.substr(line)
                # Line comments
                m: re.Match | None = None
                for line_comment in line_comments:
                    m = re.match(
                        r"^(.*?)"
                        + "("
                        + re.escape(line_comment[0])
                        + ")"
                        + r"(\s?)\s*([\-=*]*)\s*?(\s?)(\w.*?)?([ \-=*]*?)([ ]?)()$",
                        text,
                    )
                    if m:
                        break

                # Single-line block comments
                if not m:
                    for block_comment in block_comments:
                        restr: str = (
                            r"^(.*?)"
                            + "("
                            + re.escape(block_comment[0])
                            + ")"
                            + r"(\s?)\s*([\-=*]*)\s*?(\s?)(\w.*?)?([ \-=*]*?)([ ]?)"
                            + "("
                            + re.escape(block_comment[1])
                            + ")"
                            + r"$"
                        )
                        # print(repr(restr))
                        m = re.match(restr, text)
                        if m:
                            break

                if m:
                    # print(m.groups())
                    title_text: str | None = m.group(6)
                    if not title_text:
                        title_text = ""
                    title_space: str = m.group(5)  # Space on either side of the title
                    pad_char: str = ""

                    if len(m.group(4)) > 0:
                        pad_char = m.group(4)[0]
                    else:
                        pad_char = " "
                        title_space = ""

                    trailing_comment_mark: str = m.group(9)
                    trailing_space: str = ""

                    if trailing_comment_mark != "":
                        trailing_space = m.group(3)

                    length_excl_pad: int = (
                        len(m.group(1))
                        + len(m.group(2))
                        + len(m.group(3))
                        + len(title_space) * 2
                        + len(title_text)
                        + len(trailing_space)
                        + len(trailing_comment_mark)
                    )

                    pads_needed: int = line_width - length_excl_pad
                    num_to_add: int = int(pads_needed / 2)
                    num_to_add2: int = pads_needed - num_to_add

                    if num_to_add < 0:
                        num_to_add = 0
                        num_to_add2 = 0

                    new_text: str = (
                        m.group(1)
                        + m.group(2)
                        + m.group(3)
                        + (pad_char * num_to_add)
                        + title_space
                        + title_text
                        + title_space
                        + (pad_char * num_to_add2)
                        + trailing_space
                        + trailing_comment_mark
                    )

                    # Remove trailing spaces, for the case that the padding char is itself a space
                    new_text = re.sub(r"\s+$", "", new_text)

                    self.view.replace(edit, line, new_text)


if __name__ == "__main__":
    pass
