# -*- coding: utf-8 -*-
"""Plugin which lets you easily toggle quote marks in your code.

Based on: https://github.com/spadgos/sublime-ToggleQuotes
"""
from __future__ import annotations

import re

import sublime
import sublime_plugin


__all__: list[str] = ["OdyseusToggleQuotesCommand"]

_re_quotes: re.Pattern = re.compile("^(['\"`])(.*)\\1$")
_quote_list: list[str] = ["'", '"', "`"]


class OdyseusToggleQuotesCommand(sublime_plugin.TextCommand):
    """Toggle quotes command."""

    def run(self, edit: sublime.Edit, force_quote: str | None = None) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        force_quote : str | None, optional
            Description

        Raises
        ------
        err
            Description
        """
        if len(self.view.sel()) and self.view.sel()[0].size() == 0:
            self.view.run_command("expand_selection", {"to": "scope"})

        for sel in self.view.sel():
            try:
                text: str = self.view.substr(sel)
                res: re.Match | None = _re_quotes.match(text)

                if not res:
                    #  the current selection doesn't begin and end with a quote.
                    #  let's expand the selection one character each direction and try again
                    sel = sublime.Region(sel.begin() - 1, sel.end() + 1)
                    text = self.view.substr(sel)
                    res = _re_quotes.match(text)
                    if not res:
                        #  still no match... skip it!
                        continue

                old_quotes: str = res.group(1)

                new_quotes = (
                    force_quote
                    or _quote_list[(_quote_list.index(old_quotes) + 1) % len(_quote_list)]
                )

                text = res.group(2)
                text = text.replace(new_quotes, "\\" + new_quotes)
                text = text.replace("\\" + old_quotes, old_quotes)
                text = new_quotes + text + new_quotes
                self.view.replace(edit, sel, text)
            except Exception as err:
                raise err


if __name__ == "__main__":
    pass


# 'te\'st'
# 'te"st'
# 'test'
# 'te\'st'
# 'te"st'
# 'te"st'
# "te'st"
# `te"s't`
