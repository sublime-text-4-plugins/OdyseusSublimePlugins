# -*- coding: utf-8 -*-
"""Better find results.

Based on `BetterFindBuffer for SublimeText 3 <https://packagecontrol.io/packages/BetterFindBuffer>`__.

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

import os
import re

import sublime
import sublime_plugin

from .. import events
from .. import keybindings_help_key_list_html
from .. import keybindings_help_popup_html
from .. import plugin_name
from .. import settings
from .. import sphinx_building_docs
from python_utils.lazy_import import lazy_module
from python_utils.sublime_text_utils.queue import Queue

if sphinx_building_docs:
    from python_utils import misc_utils
    from python_utils.sublime_text_utils import utils
else:
    misc_utils: object = lazy_module("python_utils.misc_utils")
    utils: object = lazy_module("python_utils.sublime_text_utils.utils")


__all__: list[str] = [
    "OdyseusBfrFindInFilesOpenFileCommand",
    "OdyseusBfrFindInFilesOpenAllFilesCommand",
    "OdyseusBfrFindInFilesJumpFileCommand",
    "OdyseusBfrFindInFilesJumpMatchCommand",
    "OdyseusBfrOpenPopupHelpCommand",
    "OdyseusBfrFoldAndMoveToFileCommand",
    "OdyseusBfrCopyFromFindResultsCommand",
    "OdyseusBfrToggleCustomSyntaxCommand",
]

queue: Queue = Queue()

_plugin_id: str = f"{plugin_name}BetterFindResults-{{}}"
# _clip_cleaner_re = re.compile(r'^\s*\d+(\:\s|\s{2})', re.MULTILINE)
_clip_cleaner_re: re.Pattern = re.compile(r"^\s*(\d+(\:\s|\s{2})|\.+)", re.MULTILINE)
_help_data: dict[str, dict[str, object]] = {
    "Open the file under the cursor": {
        "command": "odyseus_bfr_find_in_files_open_file",
    },
    "Open all files": {
        "command": "odyseus_bfr_find_in_files_open_all_files",
    },
    "Jump to next search result": {
        "command": "odyseus_bfr_find_in_files_jump_file",
    },
    "Jump to previous search result": {
        "command": "odyseus_bfr_find_in_files_jump_file",
        "args": {"forward": False},
    },
    "Scroll to next file": {
        "command": "odyseus_bfr_find_in_files_jump_match",
    },
    "Scroll to previous file": {
        "command": "odyseus_bfr_find_in_files_jump_match",
        "args": {"forward": False},
    },
    "Fold current file and move to the first match on next file": {
        "command": "odyseus_bfr_fold_and_move_to_file",
    },
    "Fold current file and move to the first match on previous file": {
        "command": "odyseus_bfr_fold_and_move_to_file",
        "args": {"forward": False},
    },
    "Show this shortcuts help": {
        "command": "odyseus_bfr_open_popup_help",
    },
}


@events.on("plugin_loaded")
def on_better_find_results_plugin_loaded() -> None:
    """Called on plugin loaded."""
    queue.debounce(
        lambda: sublime.run_command("odyseus_bfr_toggle_custom_syntax", {"initialize": True}),
        delay=500,
        key=_plugin_id.format("debounce-characters-table-plugin-loaded"),
    )


class OdyseusBfrFindInFilesOpenFileCommand(sublime_plugin.TextCommand):
    """Open the file under the cursors in the Find Results view."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        view: sublime.View = self.view
        for sel in view.sel():
            line_no: str | None = self.get_line_no(sel)
            file_name: str | None = self.get_file(sel)
            if line_no and file_name:
                file_loc: str = "%s:%s" % (file_name, line_no)
                view.window().open_file(file_loc, sublime.ENCODED_POSITION)
            elif file_name:
                view.window().open_file(file_name)

    def get_line_no(self, sel: sublime.Region) -> str | None:
        """Get line number.

        Parameters
        ----------
        sel : sublime.Region
            A Sublime Text ``Region`` object.

        Returns
        -------
        str | None
            The line number where the path to a file is located.
        """
        view: sublime.View = self.view
        line_text: str = view.substr(view.line(sel))
        match: re.Match | None = re.match(r"\s*(\d+).+", line_text)
        if match:
            return match.group(1)
        return None

    def get_file(self, sel: sublime.Region) -> str | None:
        """Get file path.

        Parameters
        ----------
        sel : sublime.Region
            A Sublime Text ``Region`` object.

        Returns
        -------
        str | None
            The path to a file.
        """
        view: sublime.View = self.view
        line: sublime.Region = view.line(sel)
        while line.begin() > 0:
            line_text: str = view.substr(line)
            match: re.Match | None = re.match(r"(.+):$", line_text)
            if match:
                if os.path.exists(match.group(1)):
                    return match.group(1)
            line = view.line(line.begin() - 1)
        return None


class OdyseusBfrFindInFilesOpenAllFilesCommand(sublime_plugin.TextCommand):
    """Open all files in the Find Results view."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        view: sublime.View = self.view
        if is_find_results(view):
            for file_name in self.get_files():
                view.window().open_file(file_name, sublime.ENCODED_POSITION)

    def get_files(self) -> list[str]:
        """Get all file paths.

        Returns
        -------
        list[str]
            The paths to all files.
        """
        view: sublime.View = self.view
        content = view.substr(sublime.Region(0, view.size()))
        return [match.group(1) for match in re.finditer(r"^([^\s].+):$", content, re.MULTILINE)]


class OdyseusBfrFindInFilesJumpCommand(sublime_plugin.TextCommand):
    """Base command to jump to a search term match or file in a Find Results view."""

    def run(self, edit: sublime.Edit, forward: bool = True, cycle: bool = True) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        forward : bool, optional
            Jump to next match.
        cycle : bool, optional
            Jump to first/last match/file when reaching the end/start in the view.
        """
        caret = self.view.sel()[0]
        matches = self.filter_matches(caret, self.find_matches())
        if forward:
            match = self.find_next_match(caret, matches, cycle)
        else:
            match = self.find_prev_match(caret, matches, cycle)
        if match:
            self.goto_match(match)

    def find_matches(self) -> Any:
        """Summary"""
        pass

    def find_next_match(
        self, caret: sublime.Region, matches: list[sublime.Region], cycle: bool
    ) -> sublime.Region:
        """Summary

        Parameters
        ----------
        caret : sublime.Region
            Description
        matches : list[sublime.Region]
            Description
        cycle : bool
            Description

        Returns
        -------
        sublime.Region
            Description
        """
        print("matches", matches)
        """Find next match.

        Parameters
        ----------
        caret : sublime.Region
            A Sublime Text ``Region`` object.
        matches : list[sublime.Region]
            List of matches.
        cycle : bool
            Jump to first/last match/file when reaching the end/start in the view.

        Returns
        -------
        sublime.Region
            A Sublime Text ``Region`` object.
        """
        default: sublime.Region | None = matches[0] if cycle and len(matches) else None
        return next((m for m in matches if caret.begin() < m.begin()), default)

    def filter_matches(
        self, caret: sublime.Region, matches: list[sublime.Region]
    ) -> list[sublime.Region]:
        """Filter matches.

        Parameters
        ----------
        caret : sublime.Region
            A Sublime Text ``Region`` object.
        matches : list[sublime.Region]
            List of matches.

        Returns
        -------
        list[sublime.Region]
            Filtered list of matches.
        """
        footers: list[sublime.Region] = self.view.find_by_selector("footer.find-in-files")
        lower_bound: int = next((f.end() for f in reversed(footers) if f.end() < caret.begin()), 0)
        upper_bound: int = next(
            (f.end() for f in footers if f.end() > caret.begin()), self.view.size()
        )
        return [m for m in matches if m.begin() > lower_bound and m.begin() < upper_bound]

    def find_prev_match(
        self, caret: sublime.Region, matches: list[sublime.Region], cycle: bool
    ) -> sublime.Region:
        """Find previous match.

        Parameters
        ----------
        caret : sublime.Region
            A Sublime Text ``Region`` object.
        matches : list[sublime.Region]
            List of matches.
        cycle : bool
            Jump to first/last match/file when reaching the end/start in the view.

        Returns
        -------
        sublime.Region
            A Sublime Text ``Region`` object.
        """
        default = matches[-1] if cycle and len(matches) else None
        return next((m for m in reversed(matches) if caret.begin() > m.begin()), default)

    def goto_match(self, match: sublime.Region) -> None:
        """Go to match.

        Parameters
        ----------
        match : sublime.Region
            A Sublime Text ``Region`` object.
        """
        self.view.sel().clear()
        self.view.sel().add(match)
        if self.view.is_folded(self.view.sel()[0]):
            self.view.unfold(self.view.sel()[0])


class OdyseusBfrFindInFilesJumpFileCommand(OdyseusBfrFindInFilesJumpCommand):
    """Jump to a file in the Find Results view."""

    def find_matches(self) -> list[sublime.Region]:
        """Find matched files.

        Returns
        -------
        list[sublime.Region]
            A list of Sublime Text ``Region`` objects.
        """
        return self.view.find_by_selector("entity.name.filename.find-in-files")

    def goto_match(self, match: sublime.Region) -> None:
        """Go to matched file.

        Parameters
        ----------
        match : sublime.Region
            A Sublime Text ``Region`` object.
        """
        v: sublime.View = self.view
        region: sublime.Region = sublime.Region(match.begin(), match.begin())
        super().goto_match(region)
        top_offset: int = v.text_to_layout(region.begin())[1] - v.line_height()
        v.set_viewport_position((0, top_offset), True)


class OdyseusBfrFindInFilesJumpMatchCommand(OdyseusBfrFindInFilesJumpCommand):
    """Jump to a search term match in the Find Results view."""

    def find_matches(self) -> list[sublime.Region]:
        """Find search term matches.

        Returns
        -------
        list[sublime.Region]
            A list of Sublime Text ``Region`` objects.
        """
        return self.view.get_regions("match")

    def goto_match(self, match: sublime.Region) -> None:
        """Go to search term matche.

        Parameters
        ----------
        match : sublime.Region
            A Sublime Text ``Region`` object.
        """
        v: sublime.View = self.view
        super().goto_match(match)
        vx, vy = v.viewport_position()
        vw, vh = v.viewport_extent()
        x, y = v.text_to_layout(match.begin())
        h: int = v.line_height()
        if y < vy or y + h > vy + vh:
            v.show_at_center(match)


class OdyseusBfrOpenPopupHelpCommand(sublime_plugin.TextCommand):
    """Open keyboard shortcuts help popup."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        self.view.show_popup(
            content=keybindings_help_popup_html.format(
                plugin_name="Better Find Results",
                keys_list=utils.generate_keybindings_help_data(
                    _help_data, keybindings_help_key_list_html
                ),
            ),
            flags=0,
            location=-1,
            max_width=1024,
            max_height=768,
        )


class OdyseusBfrFoldAndMoveToFileCommand(sublime_plugin.TextCommand):
    """Move to next/previous file in the Find Result view and fold the current one."""

    def run(self, edit: sublime.Edit, forward: bool = True) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        forward : bool, optional
            If the move should be forward.
        """
        begin: sublime.Region = self.get_begin()
        end: sublime.Region = self.get_end()

        self.view.fold(sublime.Region(begin.b + 1, end.a - 1))

        if forward:
            sublime.set_timeout_async(lambda: self.move_to_file(end.a, forward), 10)
        else:
            sublime.set_timeout_async(lambda: self.move_to_file(begin.a, forward), 10)

    def move_to_file(self, location: int, forward: bool) -> None:
        """Move to file.

        Parameters
        ----------
        location : int
            An :py:class:`int` that represents the offset from the beginning of the editor buffer.
        forward : bool
            If the move should be forward.
        """
        self.view.run_command("odyseus_bfr_find_in_files_jump_file", {"forward": forward})
        self.view.run_command("odyseus_bfr_find_in_files_jump_match", {"forward": forward})
        sublime.set_timeout_async(lambda: self.view.show(location), 10)

    def get_begin(self) -> sublime.Region | None:
        """Get fold region begin.

        Returns
        -------
        sublime.Region | None
            A Sublime Text ``Region`` object.
        """
        view: sublime.View = self.view
        if len(view.sel()) == 1:
            line: sublime.Region = view.line(view.sel()[0])
            while line.begin() > 0:
                line_text: str = view.substr(line)
                match: re.Match | None = re.match(r"\S(.+):$", line_text)
                if match:
                    return line
                line = view.line(line.begin() - 1)
        return None

    def get_end(self) -> sublime.Region | None:
        """Get fold region end.

        Returns
        -------
        sublime.Region | None
            A Sublime Text ``Region`` object.
        """
        view: sublime.View = self.view
        if len(view.sel()) == 1:
            line: sublime.Region = view.line(view.sel()[0])
            while line.end() <= view.size():
                line_text: str = view.substr(line)
                if len(line_text) == 0:
                    return line
                line = view.line(line.end() + 1)
        return None


class OdyseusBfrCopyFromFindResultsCommand(sublime_plugin.TextCommand):
    """Copy from the Find Results view without line numbers.

    Based on `Sublime Copy from Find Results <https://github.com/nicosantangelo/sublime-copy-from-find-results>`__.
    """

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.

        Returns
        -------
        None
            Halt execution.
        """
        self.view.run_command("copy")

        if not is_find_results(self.view):
            return

        sublime.get_clipboard_async(self._copy_to_clipboard)

    def _copy_to_clipboard(self, clipboard_contents: str) -> None:
        """Summary

        Parameters
        ----------
        clipboard_contents : str
            Description
        """
        if clipboard_contents:
            new_clipboard: str = _clip_cleaner_re.sub("", clipboard_contents)
            sublime.set_clipboard(new_clipboard)


class OdyseusBfrToggleCustomSyntaxCommand(sublime_plugin.ApplicationCommand):
    """Toggle custom syntax command.

    Attributes
    ----------
    custom_syntax_path : str
        Description
    default_package_path : str
        Description
    default_syntax_override_path : str
        Description
    """

    def run(self, initialize: bool = False) -> None:
        """Called when the command is run.

        Parameters
        ----------
        initialize : bool, optional
            Description

        Returns
        -------
        None
            Description
        """
        self.default_package_path: str = os.path.join(sublime.packages_path(), "Default")
        self.custom_syntax_path: str = os.path.join(
            sublime.packages_path(),
            plugin_name,
            "plugins",
            "better_find_results",
            "Find Results.hidden-tmLanguage",
        )
        self.default_syntax_override_path: str = os.path.join(
            sublime.packages_path(), "Default", "Find Results.hidden-tmLanguage"
        )

        if initialize:
            if not settings.get(
                "better_find_results.syntax_override_applied", False
            ) and not os.path.exists(self.default_syntax_override_path):
                self._override_find_result_syntax()
                settings.set("better_find_results.syntax_override_applied", True)

            return

        if os.path.exists(self.default_syntax_override_path):
            response: int = sublime.yes_no_cancel_dialog(
                "Syntax file exists!\n"
                "Choose what to do with the existent Find Results.hidden-tmLanguage file.",
                "Backup and overwrite with custom syntax",
                "Delete syntax file",
            )

            if response == sublime.DIALOG_YES:
                os.rename(
                    self.default_syntax_override_path,
                    self.default_syntax_override_path
                    + misc_utils.get_date_time("filename")
                    + ".bak",
                )
                self._override_find_result_syntax()
            elif response == sublime.DIALOG_NO:
                os.remove(self.default_syntax_override_path)
                sublime.status_message("Better Find Results syntax file deleted.")
            elif response == sublime.DIALOG_CANCEL:
                sublime.status_message("Operation canceled.")
        else:
            self._override_find_result_syntax()

    def _override_find_result_syntax(self):
        """Copy custom syntax."""
        os.makedirs(self.default_package_path, exist_ok=True)

        with open(self.custom_syntax_path, "r") as source_file:
            with open(self.default_syntax_override_path, "w") as dst_file:
                dst_file.write(source_file.read())
                sublime.status_message("Better Find Results syntax file created.")


def is_find_results(view: sublime.View) -> bool:
    """Check if a view is the Find Results view.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.

    Returns
    -------
    bool
        If the view is the Find Results view.
    """
    view_settings: sublime.Settings = view.settings()
    view_syntax: str | None = view_settings.get("syntax") if view_settings else None
    return all(
        (
            view,
            view_settings,
            view_settings and view_syntax,
            view_syntax and "Find Results" in view_syntax,
        )
    )


if __name__ == "__main__":
    pass
