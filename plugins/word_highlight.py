# -*- coding: utf-8 -*-
"""Summary

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

import re

from functools import partial

import sublime
import sublime_plugin

from . import plugin_name
from . import settings
from python_utils.sublime_text_utils import settings as settings_utils
from python_utils.sublime_text_utils.queue import Queue


__all__: list[str] = [
    "OdyseusWhSelectHighlightedNextWordCommand",
    "OdyseusWhSelectHighlightedSkipLastWordCommand",
    "OdyseusWhSelectHighlightedWordsCommand",
    "OdyseusWhToggleLiveHighlightCommand",
    "OdyseusWhWordHighlightClickCommand",
    "OdyseusWhWordHighlightListener",
]

queue: Queue = Queue()

_plugin_id: str = f"{plugin_name}WordHighlight-{{}}"


class Storage:
    """Storage class.

    Attributes
    ----------
    prev_regions : list[sublime.Region]
        All previous regions.
    prev_selections : str
        All view's previous selections.
    select_next_word_skiped : int
        Description
    """

    prev_selections: str = ""
    prev_regions: list[sublime.Region] = []
    select_next_word_skiped: int = 0

    @classmethod
    def reset(cls):
        """Summary"""
        cls.prev_regions.clear()
        cls.prev_selections = ""


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"word_highlight.{s}", default)


def escape_regex(string: str) -> str:
    """Escape regular expresion.

    Parameters
    ----------
    string : str
        The string to escape.

    Returns
    -------
    str
        The escaped string.
    """
    # Sublime text chokes when regexes contain \', \<, \>, or \`.
    # Call re.escape to escape everything, and then unescape these four.
    string = re.escape(string)
    for c in "'<>`":
        string = string.replace("\\" + c, c)
    return string


def find_regions(
    view: sublime.View, regions: list[sublime.Region], string: str, limited_size: bool
) -> list[sublime.Region]:
    """Find regions.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    regions : list[sublime.Region]
        A list of Sublime Text regions.
    string : str
        The string that should be highlighted.
    limited_size : bool
        If the search region should be limited.

    Returns
    -------
    list[sublime.Region]
        List of regions.
    """
    search: str = escape_regex(string)
    # It seems as if \b doesn't pay attention to word_separators, but
    # \w does. Hence we use lookaround assertions instead of \b.
    if not get_setting("highlight_non_word_characters", False):
        search = r"(?<!\w)" + search + r"(?!\w)"

    if not limited_size:
        regions += view.find_all(
            search, (not get_setting("case_sensitive", True)) * sublime.IGNORECASE
        )
    else:
        chars: int = get_setting("when_file_size_limit_search_this_num_of_characters", 20000)
        visible_region: sublime.Region = view.visible_region()
        begin: int = 0 if visible_region.begin() - chars < 0 else visible_region.begin() - chars
        end: int = visible_region.end() + chars
        from_point: int = begin
        while True:
            region: sublime.Region = view.find(search, from_point)
            if region:
                regions.append(region)
                if region.end() > end:
                    break
                else:
                    from_point = region.end()
            else:
                break
    return regions


def delayed_highlight(
    view: sublime.View,
    regions: list[sublime.Region],
    occurrences_message: str,
    limited_size: bool,
) -> None:
    """Delayed highlight.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    regions : list[sublime.Region]
        List of regions.
    occurrences_message : str
        List of ocurrences messages.
    limited_size : bool
        If the search region should be limited.
    """
    view.add_regions(
        key=_plugin_id.format("regions"),
        regions=regions,
        scope=get_setting("color_scope_name", "comment"),
        icon=get_setting("icon_type_on_gutter", "dot")
        if get_setting("mark_occurrences_on_gutter", False)
        else "",
        flags=get_setting("hide_on_minimap", True) * sublime.HIDE_ON_MINIMAP
        | get_setting("draw_no_fill", False) * sublime.DRAW_NO_FILL
        | get_setting("draw_no_outline", True) * sublime.DRAW_NO_OUTLINE
        | get_setting("draw_solid_underline", True) * sublime.DRAW_SOLID_UNDERLINE,
    )

    if get_setting("show_word_highlight_status_bar_message", False):
        view.set_status(
            _plugin_id.format("status"),
            ", ".join(list(set(occurrences_message)))
            + (" found on a limited portion of the document " if limited_size else ""),
        )


def highlight_occurences(view: sublime.View) -> None:
    """Summary

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    """
    sublime.set_timeout_async(lambda: _highlight_occurences(view), 10)


def _highlight_occurences(view: sublime.View) -> None:
    """Highlight occurrences.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.

    Returns
    -------
    None
        Halt execution.
    """
    if (
        not get_setting("highlight_when_selection_is_empty", False)
        and not view.has_non_empty_selection_region()
    ):
        view.erase_status(_plugin_id.format("status"))
        view.erase_regions(_plugin_id.format("regions"))
        Storage.reset()
        return

    prev_selections: str = str(view.sel())

    if Storage.prev_selections == prev_selections:
        return
    else:
        Storage.prev_selections = prev_selections

    limited_size: bool = view.size() > get_setting("file_size_limit", 4194304)

    regions: list[sublime.Region] = []
    processed_words: list[str] = []
    occurrences_message: list[str] = []
    occurrences_count: int = 0
    word_separators: str = view.settings().get("word_separators", "")
    string: str = ""

    for sel in view.sel():
        if get_setting("highlight_when_selection_is_empty", False) and sel.empty():
            string = view.substr(view.word(sel)).strip()
            if string not in processed_words:
                processed_words.append(string)
                if string and all([c not in word_separators for c in string]):
                    regions = find_regions(view, regions, string, limited_size)
                if not get_setting("highlight_word_under_cursor_when_selection_is_empty", False):
                    for s in view.sel():
                        regions = [r for r in regions if not r.contains(s)]
        elif not sel.empty() and get_setting("highlight_non_word_characters", False):
            string = view.substr(sel)
            if string and string not in processed_words:
                processed_words.append(string)
                regions = find_regions(view, regions, string, limited_size)
        elif not sel.empty():
            word: sublime.Region = view.word(sel)
            if word.end() == sel.end() and word.begin() == sel.begin():
                string = view.substr(word).strip()
                if string not in processed_words:
                    processed_words.append(string)
                    if string and all([c not in word_separators for c in string]):
                        regions = find_regions(view, regions, string, limited_size)

        occurrences: int = len(regions) - occurrences_count
        if occurrences > 0:
            occurrences_message.append('"' + string + '" ' + str(occurrences) + " ")
            occurrences_count = occurrences_count + occurrences

    if Storage.prev_regions != regions:
        view.erase_regions(_plugin_id.format("regions"))
        if regions:
            queue.debounce(
                partial(delayed_highlight, view, regions, occurrences_message, limited_size),
                delay=get_setting("highlight_delay", 100),
                key=_plugin_id.format("debounce-delayed_highlight"),
            )
        else:
            view.erase_status(_plugin_id.format("status"))

        Storage.prev_regions = regions


class OdyseusWhSelectHighlightedWordsCommand(sublime_plugin.TextCommand):
    """Select highlighted words command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        wh: sublime.Region = self.view.get_regions(_plugin_id.format("regions"))
        for w in wh:
            self.view.sel().add(w)


class OdyseusWhSelectHighlightedNextWordCommand(sublime_plugin.TextCommand):
    """Select highlighted next word command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        sel: list[sublime.Region] = [s for s in self.view.sel()]
        sel.reverse()
        if sel:
            word: sublime.Region = sel[0]
            wh: sublime.Region = self.view.get_regions(_plugin_id.format("regions"))
            for w in wh:
                if w.end() > word.end() and w.end() > Storage.select_next_word_skiped:
                    self.view.sel().add(w)
                    self.view.show(w)
                    Storage.select_next_word_skiped = w.end()
                    break


class OdyseusWhSelectHighlightedSkipLastWordCommand(sublime_plugin.TextCommand):
    """Select highlighted, skip last word command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        sel: list[sublime.Region] = [s for s in self.view.sel()]
        sel.reverse()
        if sel and len(sel) > 1:
            self.view.sel().subtract(sel[0])
            Storage.select_next_word_skiped = sel[0].end()


class OdyseusWhWordHighlightClickCommand(sublime_plugin.TextCommand):
    """Summary"""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        Storage.select_next_word_skiped = 0
        view: sublime.View = self.view
        if get_setting("live_highlight", True) and not view.settings().get("is_widget"):
            highlight_occurences(view)


class OdyseusWhWordHighlightListener(sublime_plugin.EventListener):
    """Summary"""

    def on_activated_async(self, view: sublime.View) -> None:
        """Called when the view gains input focus.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        Storage.prev_selections = ""
        Storage.select_next_word_skiped = 0

        if not view.is_loading() and not get_setting("live_highlight", True):
            view.erase_regions(_plugin_id.format("regions"))

    def on_selection_modified_async(self, view: sublime.View) -> None:
        """Called after the selection has been modified in the view.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if (
            view
            and len(view.sel())
            and get_setting("live_highlight", True)
            and not view.settings().get("is_widget")
        ):
            queue.debounce(
                partial(highlight_occurences, view),
                delay=100,
                key=_plugin_id.format("debounce-highlight_occurences"),
            )


class OdyseusWhToggleLiveHighlightCommand(
    settings_utils.SettingsToggleBoolean, sublime_plugin.TextCommand
):
    """Summary"""

    def __init__(self, *args, **kwargs) -> None:
        """Summary

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        sublime_plugin.TextCommand.__init__(self, *args, **kwargs)
        settings_utils.SettingsToggleBoolean.__init__(
            self,
            key="word_highlight.live_highlight",
            settings=settings,
            description="Word Highlight - Live Highlight - {}",
        )

    def run(self, *args, **kwargs) -> None:
        """Called when the command is run.

        Parameters
        ----------
        *args
            Description
        **kwargs
            Description

        Returns
        -------
        None
            Description
        """
        settings_utils.SettingsToggleBoolean.run(self)

        view: sublime.View = self.view

        if not view:
            return

        if get_setting("live_highlight", True):
            highlight_occurences(view)
        else:
            view.erase_regions(_plugin_id.format("regions"))


if __name__ == "__main__":
    pass
