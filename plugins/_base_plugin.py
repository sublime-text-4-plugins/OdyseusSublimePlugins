# -*- coding: utf-8 -*-
"""Case conversion plugin.

Attributes
----------
queue : queue.Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from collections.abc import Callable
    from typing import Any

import sublime
import sublime_plugin

from . import display_message_in_panel
from . import events
from . import logger
from . import plugin_name
from . import settings
from python_utils.sublime_text_utils.queue import Queue

queue: Queue = Queue()


__all__: list[str] = []

_plugin_id: str = f"{plugin_name}PluginName-{{}}"


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : dict, any, optional
        Default value in case the key storing a value isn't found.

    Returns
    -------
    any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"plugin_name.{s}", default)


class Storage:
    """Storage class.

    Attributes
    ----------
    storage_key : list
        Description
    """
    storage_key: list[object] = []

    @classmethod
    def update(cls) -> None:
        """Summary
        """
        pass


@events.on("plugin_loaded")
def on_plugin_name_plugin_loaded() -> None:
    """Called on plugin loaded.
    """
    queue.debounce(
        Storage.update, delay=500, key=_plugin_id.format("debounce-storage-update")
    )


@events.on("settings_changed")
def on_plugin_name_settings_changed(settings_obj: SettingsManager) -> None:
    """Called on settings changed.

    Parameters
    ----------
    settings_obj : python_utils.sublime_text_utils.settings.SettingsManager
        An instance of :any:`python_utils.sublime_text_utils.settings.SettingsManager`.
    """
    if any((settings_obj.has_changed("preference_key"),)):
        queue.debounce(
            Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage")
        )


if __name__ == "__main__":
    pass
