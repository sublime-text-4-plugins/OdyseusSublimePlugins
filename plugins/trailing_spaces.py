# -*- coding: utf-8 -*-
"""Provides both a trailing spaces highlighter and a deletion command.

@author: Jean-Denis Vauguet <jd@vauguet.fr>, Oktay Acikalin <ok@ryotic.de>
@license: MIT (http://www.opensource.org/licenses/mit-license.php)
@since: 2011-02-25

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

from functools import partial

import sublime
import sublime_plugin

from . import plugin_name
from . import settings
from python_utils.sublime_text_utils import settings as settings_utils
from python_utils.sublime_text_utils.queue import Queue


__all__: list[str] = [
    "OdyseusTsDeleteTrailingSpacesCommand",
    "OdyseusTsHighlightTrailingSpacesCommand",
    "OdyseusTsToggleLiveHighlightCommand",
    "OdyseusTsTrailingSpacesListener",
]

queue: Queue = Queue()

_plugin_id: str = f"{plugin_name}TrailingSpaces-{{}}"


class Storage:
    """Storage class.

    Attributes
    ----------
    prev_highlightable : list[sublime.Region]
        Description
    """

    prev_highlightable: list[sublime.Region] = []


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"trailing_spaces.{s}", default)


def find_trailing_spaces(
    view: sublime.View,
) -> list[list[sublime.Region]]:
    """Get the regions matching trailing spaces.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.

    Returns
    -------
    list[list[sublime.Region]]
        The list of regions which map to trailing spaces and the list of
        regions which are to be highlighted, as a list [matched, highlightable].
    """
    if not view or len(view.sel()) == 0:
        return [None, None]

    include_empty_lines: bool = get_setting("include_empty_lines", True)
    include_current_line: bool = get_setting("include_current_line", False)
    ignored_scopes: str = "|".join(get_setting("scope_ignore", []))
    regexp: str = get_setting("regexp", "[ \t]+") + "$"
    no_empty_lines_regexp: str = r"(?<=\S)%s$" % regexp

    sel: sublime.Region = view.sel()[0]
    line: sublime.Region = view.line(sel.b)

    # trailing_regions = view.find_all(regexp if include_empty_lines else no_empty_lines_regexp)

    # filter out ignored scopes
    trailing_regions: list[sublime.Region] = [
        region
        for region in view.find_all(regexp if include_empty_lines else no_empty_lines_regexp)
        if not ignored_scopes or not view.match_selector(region.begin(), ignored_scopes)
    ]

    if include_current_line:
        return [trailing_regions, trailing_regions]
    else:
        current_offender: sublime.Region = view.find(
            regexp if include_empty_lines else no_empty_lines_regexp, line.a
        )
        removal: bool = False if current_offender is None else line.intersects(current_offender)
        highlightable: list[sublime.Region] = (
            [i for i in trailing_regions if i != current_offender] if removal else trailing_regions
        )
        return [trailing_regions, highlightable]


def match_trailing_spaces(view: sublime.View) -> None:
    """Find the trailing spaces in the view and flags them as such.

    It will refresh highlighted regions as well. Does not execute if the
    document's size exceeds the file_max_size setting, or if the fired in a view
    which is not a legacy document (helper/build views and so on).

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.

    Returns
    -------
    None
        Halt execution.
    """
    # Silently pass ignored views.
    if ignore_view(view):
        return

    # Silently pass if file is too big.
    if max_size_exceeded(view):
        sublime.status_message("File is too big, trailing spaces handling disabled.")
        return

    (matched, highlightable) = find_trailing_spaces(view)

    if matched is None or highlightable is None:
        return

    highlight_trailing_spaces_regions(view, highlightable)


def ignore_view(view: sublime.View) -> bool:
    """Checks if the view should be ignored based on a list of syntaxes.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.

    Returns
    -------
    bool
        True if the view should be ignored, False otherwise.
    """
    if not view or view.is_scratch() or view.settings().get("is_widget"):
        return True

    view_syntax: str = (
        view.settings().get("syntax").split("/")[-1].lower()
        if view.settings().get("syntax")
        else ""
    )

    if not view_syntax:
        return False

    for syntax_ignore in get_setting("syntax_ignore", []):
        if syntax_ignore.lower() in view_syntax:
            return True

    return False


def max_size_exceeded(view: sublime.View) -> bool:
    """Checks whether the document is bigger than the max_size setting.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.

    Returns
    -------
    bool
        If file size is too big.
    """
    return view.size() > get_setting("file_max_size", 1048576)


def highlight_trailing_spaces_regions(view: sublime.View, regions: list[sublime.Region]) -> None:
    """Highlights specified regions as trailing spaces.

    It will use the scope enforced by the state of the toggable highlighting.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    regions : list[sublime.Region]
        Regions qualified as trailing spaces.

    Returns
    -------
    None
        Description
    """
    # NOTE: Prevent unnecessary regions removal/addition. First, because it's a total waste of
    # resources and second because it causes "blinking" when it's constantly removing/adding regions
    # while typing.
    if Storage.prev_highlightable == regions:
        return

    Storage.prev_highlightable = regions
    view.erase_regions(_plugin_id.format("highlighted-regions"))
    view.add_regions(
        _plugin_id.format("highlighted-regions"),
        regions,
        get_setting("highlight_color", "region.redish") or "",
        "",
        sublime.HIDE_ON_MINIMAP | sublime.DRAW_NO_OUTLINE,
    )


def delete_trailing_regions(view: sublime.View, edit: sublime.Edit) -> int:
    """Deletes the trailing spaces regions.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    edit : sublime.Edit
        A Sublime Text edit.

    Returns
    -------
    int
        The number of deleted regions.
    """
    regions, highlightable = find_trailing_spaces(view)

    if regions:
        # Trick: reversing the regions takes care of the growing offset while
        # deleting the successive regions.
        regions.reverse()
        for r in regions:
            view.erase(edit, r)
        return len(regions)
    else:
        return 0


class OdyseusTsTrailingSpacesListener(sublime_plugin.EventListener):
    """Matches and highlights trailing spaces on key events, according to the current settings."""

    def _ody_match(self, view: sublime.View) -> None:
        """Match trailing spaces.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if get_setting("live_highlight", True):
            queue.debounce(
                partial(match_trailing_spaces, view),
                delay=get_setting("highlight_delay", 0),
                key=_plugin_id.format("debounce"),
            )

    def on_modified_async(self, view: sublime.View) -> None:
        """Called after changes have been made to the view.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        self._ody_match(view)

    def on_selection_modified_async(self, view: sublime.View) -> None:
        """Called after the selection has been modified in the view.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        self._ody_match(view)

    def on_activated_async(self, view: sublime.View) -> None:
        """Called when the view gains input focus.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        self._ody_match(view)


class OdyseusTsDeleteTrailingSpacesCommand(sublime_plugin.TextCommand):
    """Deletes the trailing spaces."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text edit.

        Returns
        -------
        None
            Halt execution.
        """
        if max_size_exceeded(self.view):
            sublime.status_message("File is too big, trailing spaces handling disabled.")
            return

        deleted: int = delete_trailing_regions(self.view, edit)
        message: str = "No trailing spaces to delete!"

        if deleted:
            message = "Deleted {regions} trailing spaces region{plural}".format(
                regions=deleted, plural="s" if deleted > 1 else ""
            )

        sublime.status_message(message)


class OdyseusTsHighlightTrailingSpacesCommand(sublime_plugin.TextCommand):
    """Highlights trailing spaces."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text edit.
        """
        queue.debounce(
            partial(match_trailing_spaces, self.view),
            delay=get_setting("highlight_delay", 0),
            key=_plugin_id.format("debounce"),
        )


class OdyseusTsToggleLiveHighlightCommand(
    settings_utils.SettingsToggleBoolean, sublime_plugin.TextCommand
):
    """Summary"""

    def __init__(self, *args, **kwargs) -> None:
        """Summary

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        sublime_plugin.TextCommand.__init__(self, *args, **kwargs)
        settings_utils.SettingsToggleBoolean.__init__(
            self,
            key="trailing_spaces.live_highlight",
            settings=settings,
            description="Trailing Spaces - Live Highlight - {}",
        )

    def run(self, *args, **kwargs) -> None:
        """Called when the command is run.

        Parameters
        ----------
        *args
            Description
        **kwargs
            Description

        Returns
        -------
        None
            Description

        Deleted Parameters
        ------------------
        edit : sublime.Edit
            Description
        """
        settings_utils.SettingsToggleBoolean.run(self)

        view: sublime.View = self.view

        if not view:
            return

        if get_setting("live_highlight", True):
            match_trailing_spaces(view)
        else:
            view.erase_regions(_plugin_id.format("highlighted-regions"))


if __name__ == "__main__":
    pass
