# -*- coding: utf-8 -*-
"""Utilities to execute Sublime Text commands.

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from python_utils.sublime_text_utils.settings import SettingsManager
    from types import ModuleType
    from typing import Any

import html

import sublime
import sublime_plugin

from . import display_message_in_panel
from . import events
from . import logger
from . import plugin_name
from . import settings
from . import sphinx_building_docs
from python_utils import misc_utils
from python_utils.lazy_import import lazy_module
from python_utils.sublime_text_utils.custom_classes import CustomListInputItem
from python_utils.sublime_text_utils.queue import Queue

if sphinx_building_docs:
    from python_utils.sublime_text_utils import utils
else:
    utils: object = lazy_module("python_utils.sublime_text_utils.utils")


__all__: list[str] = [
    "OdyseusRunMultipleCmdsOverlayCommand",
    "OdyseusRunCmdInAllViewsCommand",
    "OdyseusRunMultipleCmdsCommand",
]

queue: Queue = Queue()

_plugin_id: str = f"{plugin_name}RunCommands-{{}}"
_error_msg: str = """{clsname}
Failed to run command: <{cmd}>
Arguments: <{cmd_args}>
View: <{view}>
{err}"""


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"run_commands.{s}", default)


class Storage:
    """Storage class.

    Attributes
    ----------
    multiple_commands_definitions : dict
        Description
    multiple_commands_definitions_list_items : list[CustomListInputItem]
        Description

    Deleted Attributes
    ------------------
    storage_key : list
        Description
    """

    multiple_commands_definitions: dict = {}
    multiple_commands_definitions_list_items: list[CustomListInputItem] = []

    @classmethod
    def update(cls) -> None:
        """Summary"""
        cls.multiple_commands_definitions.clear()
        cls.multiple_commands_definitions_list_items.clear()

        cls.multiple_commands_definitions = misc_utils.merge_dict(
            get_setting("multiple_commands_definitions", {}),
            get_setting("multiple_commands_definitions_user", {}),
            logger=logger,
        )

        for cmd_id, cmd_def in cls.multiple_commands_definitions.items():
            if cmd_def.get("enabled", True):
                cls.multiple_commands_definitions_list_items.append(
                    CustomListInputItem(
                        text=cmd_def.get("name", cmd_id),
                        value=cmd_id,
                        details=f"Command ID: <code>{html.escape(cmd_id)}</code>",
                    )
                )

        cls.multiple_commands_definitions_list_items.sort()


@events.on("plugin_loaded")
def on_run_commands_plugin_loaded() -> None:
    """Called on plugin loaded."""
    queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-storage-update"))


@events.on("settings_changed")
def on_run_commands_settings_changed(settings_obj: SettingsManager) -> None:
    """Called on settings changed.

    Parameters
    ----------
    settings_obj : SettingsManager
        An instance of :any:`python_utils.sublime_text_utils.settings.SettingsManager`.
    """
    if any(
        (
            settings_obj.has_changed("code_formatter.multiple_commands_definitions"),
            settings_obj.has_changed("code_formatter.multiple_commands_definitions_user"),
        )
    ):
        queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage"))


class OdyseusRunCmdInAllViewsCommand(sublime_plugin.WindowCommand):
    """Execute any command (from Sublime text or from other plugins)
    in all currently opened files.

    Parameters
    ----------
    cmd : str
        The command to execute.
    cmd_args : dict, optional
        The arguments to pass to the command **cmd**.
    file_extensions : list, optional
        File extensions on which to run the command **cmd**.

    Extends
    -------
    sublime_plugin.WindowCommand
    """

    def run(
        self,
        cmd: str = "",
        cmd_args: dict = {},
        file_extensions: list[str] | str = [],
        view_syntaxes: list[str] | str = [],
        view_scopes: list[str] | str = [],
        strict_syntax: bool = False,
    ) -> None:
        """Called when the command is run.

        Parameters
        ----------
        cmd : str, optional
            A command string, whether it being a Sublime Text command or a Sublime Text plugin command.
        cmd_args : dict, optional
            Arguments to pass to the command to be executed.
        file_extensions : list[str] | str, optional
            List of file extensions.
        view_syntaxes : list[str] | str, optional
            List of syntaxes.
        view_scopes : list[str] | str, optional
            Description
        strict_syntax : bool, optional
            Perform equality checks instead of membership checks when checking file syntaxes.

        Returns
        -------
        None
            Description
        """
        msg_title: str = "%s Error:" % self.__class__.__name__

        if not cmd:
            display_message_in_panel(self.window, title=msg_title, body="No command was specified")
            return

        for view in self.window.views():
            allow_run: bool = False

            if file_extensions:
                allow_run = utils.has_right_extension(view, file_extensions)
            elif view_syntaxes:
                allow_run = utils.has_right_syntax(view, view_syntaxes, strict_syntax)
            else:
                # If neither "file_extensions" nor "view_syntaxes" are specified,
                # always run the command on all views.
                allow_run = True

            if allow_run:
                try:
                    view.run_command(cmd, args=cmd_args or None)
                except Exception as err:
                    display_message_in_panel(
                        self.window,
                        title=msg_title,
                        body=_error_msg.format(
                            clsname=self.__class__.__name__,
                            cmd=cmd,
                            cmd_args=repr(cmd_args),
                            view=utils.get_file_path(view),
                            err=str(err),
                        ),
                    )


class OdyseusRunMultipleCmdsCommand(sublime_plugin.TextCommand):
    """Summary"""

    def run(self, edit: sublime.Edit, commands: list[dict] = []) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        commands : list[dict], optional
            Description

        Returns
        -------
        None
            Description
        """
        if not commands:
            return

        for cmd in commands:
            self.exec_command(cmd)

    def exec_command(self, command: dict) -> None:
        """Summary

        Parameters
        ----------
        command : dict
            Description

        Returns
        -------
        None
            Description
        """
        name: str = command.get("command", "")
        args: dict[str, object] = command.get("args", None)

        if not name:
            return

        context: sublime.View | sublime.Window | ModuleType = self.view.window()

        if "context" in command:
            context_name = command["context"]

            if context_name == "view":
                context = self.view
            elif context_name == "app":
                context = sublime

        if args is None:
            sublime.set_timeout(lambda: context.run_command(name), 100)
        else:
            sublime.set_timeout(lambda: context.run_command(name, args), 100)


class OdyseusRunMultipleCmdsOverlayCommand(sublime_plugin.TextCommand):
    """Code formatter overlay command."""

    def run(self, edit: sublime.Edit, cmd_id: str | None = None) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            Description
        cmd_id : str | None, optional
            Description
        """
        view: sublime.View = self.view

        if cmd_id and view:
            if cmd_id in Storage.multiple_commands_definitions:
                commands: list[dict[str, object]] = Storage.multiple_commands_definitions[
                    cmd_id
                ].get("commands")

                if commands:
                    view.run_command("odyseus_run_multiple_cmds", {"commands": commands})
                else:
                    logger.warning(f"Command definition <{cmd_id}> has no commands defined.")
            else:
                logger.warning(f"Missing command definition: {cmd_id}")

    def input_description(self) -> str:
        """Allows a custom name to be show to the left of the cursor in the input box, instead of
        the default one generated from the command name.

        Returns
        -------
        str
            Input description.
        """
        return "Run Multiple Commands"

    def input(self, args: dict[str, object]) -> sublime_plugin.ListInputHandler | None:
        """If this returns something other than None, the user will be prompted for an input before
        the command is run in the **Command Palette**.

        Parameters
        ----------
        args : dict[str, object]
            Arguments.

        Returns
        -------
        sublime_plugin.ListInputHandler | None
        sublime_plugin.ListInputHandler
            The input handler for the command.
        """
        if args.get("cmd_id") is None:
            return OdyseusRunMultipleCmdsOverlayCommandIdListInputHandler()

        return None


class OdyseusRunMultipleCmdsOverlayCommandIdListInputHandler(sublime_plugin.ListInputHandler):
    """List input handler for the ``cmd_id`` parameter."""

    def name(self) -> str:
        """The command argument name this input handler is editing.

        Returns
        -------
        str
            Parameter name.
        """
        return "cmd_id"

    def placeholder(self) -> str:
        """Placeholder text is shown in the text entry box before the user has entered anything.

        Returns
        -------
        str
            The placeholder text.
        """
        return "Filter commands"

    def list_items(self) -> list[CustomListInputItem] | list[str]:
        """This method should return the items to show in the list.

        Returns
        -------
        list[CustomListInputItem] | list[str]
        """
        if Storage.multiple_commands_definitions_list_items:
            return Storage.multiple_commands_definitions_list_items

        return ["No commands defined"]


if __name__ == "__main__":
    pass
