# -*- coding: utf-8 -*-
"""Message panel plugin.
"""
from __future__ import annotations

import os

import sublime
import sublime_plugin

from .. import events
from .. import plugin_name
from python_utils import cmd_utils
from python_utils import misc_utils


__all__: list[str] = [
    "OdyseusClearMessagePanelCommand",
    "OdyseusDisplayMessageInPanelCommand",
    "OdyseusOpenFileFromPanelListener",
    "OdyseusRemoveMessagePanelCommand",
    "OdyseusToggleMessagePanelCommand",
]


_panel_name: str = plugin_name + "MessagePanel"
_output_panel: str = "output." + _panel_name
_output_panel_paths: str = _output_panel + "-paths"
_output_panel_icons: str = _output_panel + "-icons"
# NOTE: The regular expression matches a markdown link. Either on its own
# line or at the end of a heading.
_path_region_regex: str = r"^(\[File\]\(|#.*?\]\()(.*?)\)$"
_date_region_regex: str = r"^\*\*\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}\.\d{6}\*\*$"
_panel_syntax: str = (
    "Packages/%s/plugins/message_panel/OSP-Message Panel.sublime-syntax" % plugin_name
)
_tooltip_template: str = """
<body id="odyseus-message-view-tooltip">
    <div><a href="edit-file">Edit file</a> - <a href="launch-file">Launch file</a></div>
</body>
"""


@events.on("plugin_unloaded")
def on_message_panel_plugin_unloaded() -> None:
    """Called on plugin unloaded."""
    for window in sublime.windows():
        window.destroy_output_panel(_panel_name)


class OdyseusDisplayMessageInPanelCommand(sublime_plugin.WindowCommand):
    """Display message panel."""

    def run(self, msg: str = "") -> None:
        """Called when the command is run.

        Parameters
        ----------
        msg : str, optional
            Message to append to the panel view.

        Returns
        -------
        None
            Halt execution.
        """
        msg = "**%s**\n%s" % (misc_utils.get_date_time(), msg)
        panel: sublime.View = ensure_panel(self.window)

        if not panel:
            return

        scroll_to: int = panel.size()

        panel.set_read_only(False)
        # I don't know if these deletions are needed, but just in case.
        # Plus, these regions aren't visible, so there is no flashing to worry about.
        panel.erase_regions(_output_panel_paths)
        panel.erase_regions(_output_panel_icons)
        panel.run_command("append", {"characters": msg.strip() + "\n"})
        path_regions: list[sublime.Region] = self._ody_get_modified_paths_regions(panel)
        panel.add_regions(
            key=_output_panel_paths,
            regions=path_regions,
            flags=sublime.HIDDEN,
        )
        panel.add_regions(
            key=_output_panel_icons,
            regions=panel.find_all(_date_region_regex),
            scope="region.bluish",
            icon="bookmark",
            flags=sublime.HIDDEN,
        )
        panel.set_read_only(True)
        panel.show(scroll_to)
        self.window.run_command("show_panel", {"panel": _output_panel})

    def _ody_get_modified_paths_regions(self, panel: sublime.View) -> list[sublime.Region]:
        """Get modified regions.

        Parameters
        ----------
        panel : sublime.View
            A sublime.View object.

        Returns
        -------
        list[sublime.Region]
            List  of modified regions.
        """
        paths: list[str] = []
        mod_regions: list[sublime.Region] = []
        regions: list[sublime.Region] = panel.find_all(
            _path_region_regex, fmt=r"\2", extractions=paths
        )

        for i, r in enumerate(regions):
            mod_regions.append(sublime.Region(r.end() - len(paths[i]) - 1, r.end() - 1))

        return mod_regions


class OdyseusRemoveMessagePanelCommand(sublime_plugin.WindowCommand):
    """Remove message panel."""

    def run(self) -> None:
        """Called when the command is run."""
        self.window.destroy_output_panel(_panel_name)


class OdyseusClearMessagePanelCommand(sublime_plugin.WindowCommand):
    """Remove message panel."""

    def run(self) -> None:
        """Called when the command is run."""
        panel: sublime.View = ensure_panel(self.window)
        panel.run_command("OdyseusClearMessagePanelCommand")
        _, y = panel.viewport_position()
        panel.set_read_only(False)
        panel.erase_regions(_output_panel_paths)
        panel.erase_regions(_output_panel_icons)
        panel.run_command("select_all")
        panel.run_command("right_delete")
        # Avoid https://github.com/SublimeTextIssues/Core/issues/2560
        # Force setting the viewport synchronous by setting it twice.
        panel.set_viewport_position((0, 0), False)
        panel.set_viewport_position((0, y), False)
        panel.set_read_only(True)


class OdyseusToggleMessagePanelCommand(sublime_plugin.WindowCommand):
    """Toggle message panel."""

    def run(self) -> None:
        """Called when the command is run."""
        ensure_panel(self.window)

        if panel_is_active(self.window):
            self.window.run_command("hide_panel", {"panel": _output_panel})
        else:
            self.window.run_command("show_panel", {"panel": _output_panel})


class OdyseusOpenFileFromPanelListener(sublime_plugin.EventListener):
    """Open file view.

    The panel messages will sometimes have specified the file that generated a message. I wanted
    to be able to directly open those files in Sublime Text by simply double clicking on them.
    I wanted to achieve something similar to what SublimeLinter does (when double clicking on
    an error line on SublimeLinter's panel view, it will move to that file's line), but I
    couldn't figure out how it is done there (it's a very complex plugin!). So, like always, I did
    it in the most naive way that I could come up with. I "hijacked" `on_text_command`
    with a debounce, if a double click is performed inside the assigned region for a path, the
    file will be opened in Sublime Text. This is just a very "hacky" way, but it does what I
    want it to do.

    NOTE
    ----
    I already figured out how SublimeLinter does it. SublimeLinter seems to use the absolutely
    obscure view settings called ``result_file_regex`` and ``result_line_regex`` which are of no
    use to me for what I want to do.
    """

    def on_hover(self, view: sublime.View, point: int, hover_zone: int) -> None:
        """Called when the user's mouse hovers over the view for a short period.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        point : int
            The offset from the beginning of the editor buffer.
        hover_zone : int
            The hover zone flag.
        """
        if view and view.name() == _panel_name and hover_zone == sublime.HOVER_TEXT:
            for region in view.get_regions(_output_panel_paths):
                if region.intersects(view.line(point)):
                    self._ody_show_tooltip(view, view.substr(region).strip(), mouse_location=point)
                    break

    def on_text_command(self, view: sublime.View, command_name: str, args: dict) -> None:
        """Called when a text command is issued.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        command_name : str
            Command name.
        args : dict
            Command arguments.
        """
        if view and view.name() == _panel_name:
            # Detect double click.
            double_click: bool = (
                command_name == "drag_select"
                and "by" in args
                and args["by"] == "words"
                and view.sel()
            )

            if double_click:
                self._ody_on_double_click(view)

    def _ody_show_tooltip(
        self, view: sublime.View, file_path: str, mouse_location: int | None = None
    ) -> None:
        """Show tooltip.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        file_path : str
            File path.
        mouse_location : int | None, optional
            Mouse location.
        """
        if mouse_location is None:
            mouse_location = view.sel()[0].begin()

        def on_anchor_clicked(href):
            """On anchor clicked.

            Parameters
            ----------
            href : str
                Anchor reference.
            """
            if href == "edit-file" or href == "launch-file":
                self._ody_open_file(view, file_path, open_type=href)
                view.hide_popup()

        view.show_popup(
            _tooltip_template,
            flags=sublime.HIDE_ON_MOUSE_MOVE_AWAY,
            location=mouse_location,
            max_width=512,
            on_navigate=on_anchor_clicked,
        )

    def _ody_on_double_click(self, view: sublime.View) -> None:
        """On "double click".

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        selection: sublime.Region = view.sel()[0]
        cursor: int = get_cursor_pos(view)

        for region in view.get_regions(_output_panel_paths):
            # The cursor should be inside the region...
            # ...and the selection begin and end should be inside the region.
            if (
                region.begin() <= cursor <= region.end()
                and selection.begin() >= region.begin()
                and selection.end() <= region.end()
            ):
                self._ody_open_file(view, view.substr(region).strip(), open_type="edit-file")
                break

    def _ody_open_file(
        self, view: sublime.View, file_path: str, open_type: str = "edit-file"
    ) -> None:
        """Open a file either in Sublime Text or with the system's file handler.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        file_path : str
            Path to a file.
        open_type : str, optional
            How to open the file.
        """
        if os.path.exists(file_path):
            window = view.window() or sublime.active_window()

            try:
                if open_type == "edit-file":
                    window.open_file(file_path)
                elif open_type == "launch-file":
                    cmd_utils.launch_default_for_file(file_path)
            except Exception as err:
                sublime.error_message(str(err))


def get_cursor_pos(view: sublime.View) -> int:
    """Get cursor position.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.

    Returns
    -------
    int
        Offset from the beginning of the editor buffer.
    """
    return next((s.begin() for s in view.sel()), -1)


def panel_is_active(window: sublime.Window) -> bool:
    """Check if panel is active.

    Parameters
    ----------
    window : sublime.Window
        A Sublime Text ``Window`` object.

    Returns
    -------
    bool
        If panel is active.
    """
    return False if not window else window.active_panel() == _output_panel


def ensure_panel(window: sublime.Window) -> sublime.View:
    """Ensure panel existence.

    Parameters
    ----------
    window : sublime.Window
        A Sublime Text ``Window`` object.

    Returns
    -------
    sublime.View
        Found or created panel.
    """
    return get_panel(window) or create_panel(window)


def get_panel(window: sublime.Window) -> sublime.View:
    """
    Parameters
    ----------
    window : sublime.Window
        A Sublime Text ``Window`` object.

    Returns
    -------
    sublime.View
    None
        Found panel.
    """
    return window.find_output_panel(_panel_name)


def create_panel(window: sublime.Window) -> sublime.View:
    """Create panel.

    Parameters
    ----------
    window : sublime.Window
        A Sublime Text ``Window`` object.

    Returns
    -------
    sublime.View
        Created panel.
    """
    panel: sublime.View = window.create_output_panel(_panel_name)
    # NOTE: I set the panel name so I don't have to keep track of its ID.
    # I don't know why CREATING A PANEL WITH A NAME would not set the name to be able to
    # get the name with the panel.name() function. JEESH!!!
    panel.set_name(_panel_name)

    panel.assign_syntax(sublime.find_syntax_by_scope("output.odyseus_plugins_message_panel")[0])
    # Call create_output_panel a second time after assigning the above
    # settings, so that it'll be picked up as a result buffer
    # see: Packages/Default/exec.py#L228-L230
    return window.create_output_panel(_panel_name)


if __name__ == "__main__":
    pass
