# -*- coding: utf-8 -*-
"""Console wrap.

Based `Console Wrap <https://packagecontrol.io/packages/Console Wrap>`__.

Attributes
----------
queue : Queue
    Queue manager.

Deleted Attributes
------------------
go : TYPE
    Description
js : TYPE
    Description
php : TYPE
    Description
py : TYPE
    Description
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

import sublime
import sublime_plugin

from .. import events
from .. import plugin_name
from .. import settings
from .. import sphinx_building_docs

from python_utils.lazy_import import lazy_module
from python_utils.misc_utils import merge_dict
from python_utils.sublime_text_utils.queue import Queue

if sphinx_building_docs:
    from .wrappers import go
    from .wrappers import js
    from .wrappers import php
    from .wrappers import py
else:
    go: object = lazy_module(f"{plugin_name}.plugins.console_wrap.wrappers.go")
    js: object = lazy_module(f"{plugin_name}.plugins.console_wrap.wrappers.js")
    php: object = lazy_module(f"{plugin_name}.plugins.console_wrap.wrappers.php")
    py: object = lazy_module(f"{plugin_name}.plugins.console_wrap.wrappers.py")


__all__: list[str] = [
    "OdyseusConsoleWrapCommand",
    "OdyseusConsoleWrapActionCommand",
]

queue: Queue = Queue()

_plugin_id: str = f"{plugin_name}ConsoleWrap-{{}}"
_valid_wrappers: tuple[str] = (
    "go",
    "js",
    "php",
    "py",
)


class Storage:
    """Storage class.

    Attributes
    ----------
    scope_to_wrapper_map : dict[str, object]
        Scopes to wrappers map.
    wrap_connector : dict[str, object]
        Wrapper instances storage.
    """

    wrap_connector: dict[str, object] = {}
    scope_to_wrapper_map: dict[str, object] = {}

    @classmethod
    def get_wrapper(cls, wrapper: str) -> object | None:
        """Get wrapper.

        Parameters
        ----------
        wrapper : str
            A wrapper name.

        Returns
        -------
        object | None
            An instance of a wrapper.
        """
        if wrapper not in _valid_wrappers:
            return None

        try:
            if wrapper not in cls.wrap_connector:
                cls.wrap_connector[wrapper] = getattr(
                    globals()[wrapper], wrapper.title() + "Wrapp"
                )()

            return cls.wrap_connector[wrapper]
        except Exception:
            return None

    @classmethod
    def update(cls) -> None:
        """Update storage."""
        cls.scope_to_wrapper_map.clear()
        cls.scope_to_wrapper_map = merge_dict(
            get_setting("scope_to_wrapper_map", {}),
            get_setting("scope_to_wrapper_map_user", {}),
        )


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"console_wrap.{s}", default)


@events.on("plugin_loaded")
def on_console_wrap_plugin_loaded() -> None:
    """Called on plugin loaded."""
    queue.debounce(
        Storage.update,
        delay=500,
        key=_plugin_id.format("debounce-update-storage"),
    )


@events.on("settings_changed")
def on_console_wrap_settings_changed(settings_obj: SettingsManager) -> None:
    """Called on settings changed.

    Parameters
    ----------
    settings_obj : SettingsManager
        An instance of :any:`python_utils.sublime_text_utils.settings.SettingsManager`.
    """
    if any(
        (
            settings_obj.has_changed("console_wrap.js"),
            settings_obj.has_changed("console_wrap.py"),
            settings_obj.has_changed("console_wrap.php"),
            settings_obj.has_changed("console_wrap.go"),
            settings_obj.has_changed("console_wrap.scope_to_wrapper_map"),
        )
    ):
        queue.debounce(
            Storage.update,
            delay=500,
            key=_plugin_id.format("debounce-update-storage"),
        )


def is_supported_file(view: sublime.View) -> bool:
    """Is supperted file.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.

    Returns
    -------
    bool
        If the view main scope is one of the scopes that this plugin can handle.
    """
    supported: bool = False

    cursors: list[sublime.Region] = view.sel()

    if not list(cursors):
        view.sel().add(0)

    for cursor in cursors:
        scope_name: str = view.scope_name(cursor.begin())
        file_type_intersect: list[str] = list(
            set(scope_name.split(" ")).intersection(Storage.scope_to_wrapper_map)
        )

        if file_type_intersect:
            supported = True
            break

    return supported


def run_command(
    view: sublime.View, edit: sublime.Edit, action: str, insert_before: bool = False
) -> None:
    """Run command.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    edit : sublime.Edit
        A Sublime Text ``Edit`` object.
    action : str
        Action to perform.
    insert_before : bool, optional
        If the insertion should be placed before the selection.

    Returns
    -------
    None
        Last cursor position.
    """
    cursors: list[sublime_plugin.Region] = view.sel()

    last_pos: float = float("inf")
    end: float = float("inf")

    if not list(cursors):
        view.sel().add(0)

    for cursor in cursors:
        scope_name: str = view.scope_name(cursor.begin())

        file_type_intersect: list[str] = list(
            set(scope_name.split(" ")).intersection(Storage.scope_to_wrapper_map)
        )[::-1]

        if not file_type_intersect:
            file_type: str = scope_name.split(" ")[0]
            msg: str = "Console Wrap: Not work in this file type ( {} )".format(file_type)
            sublime.status_message(msg)
            continue

        wrapper_type: str = Storage.scope_to_wrapper_map.get(file_type_intersect[0], False)

        if view.match_selector(cursor.begin(), "source.php"):
            wrapper_type = "php"

        if view.match_selector(cursor.begin(), "source.js"):
            wrapper_type = "js"

        wrapper: object | None = Storage.get_wrapper(wrapper_type)

        if wrapper:
            if action == "create":
                end = getattr(wrapper, action)(view, edit, cursor, insert_before)
            else:
                end = getattr(wrapper, action)(view, edit, cursor)

        if end:
            last_pos = end if end < last_pos else last_pos

    return last_pos


class OdyseusConsoleWrapCommand(sublime_plugin.TextCommand):
    """Console warp command."""

    def run(self, edit: sublime.Edit, insert_before: bool = False) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        insert_before : bool, optional
            If the insertion should be placed before the selection.
        """
        view: sublime.View = self.view
        last_pos: float = run_command(view, edit, "create", insert_before)

        if last_pos > 0 and last_pos < float("inf"):
            view.sel().clear()
            view.sel().add(sublime.Region(last_pos, last_pos))
            self.view.run_command("move_to", {"to": "eol"})


class OdyseusConsoleWrapActionCommand(sublime_plugin.TextCommand):
    """Console wrap action command."""

    def is_visible(self) -> bool:
        """Returns ``True`` if the command should be shown in the menu at this time.

        Returns
        -------
        bool
            If the command should be displayed.
        """
        return is_supported_file(self.view)

    def is_enabled(self) -> bool:
        """Returns ``True`` if the command is able to be run at this time.

        Returns
        -------
        bool
            If the command should be able to be executed.
        """
        return is_supported_file(self.view)

    def run(self, edit: sublime.Edit, action: bool = False) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        action : bool, optional
            Action to perform.
        """
        run_command(self.view, edit, action)


if __name__ == "__main__":
    pass
