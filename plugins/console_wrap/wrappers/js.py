# -*- coding: utf-8 -*-
"""Summary
"""
from __future__ import annotations

import re
import sublime
import sublime_plugin  # noqa

from . import get_selections
from . import get_setting
from . import quotes_map


class JsSettings:
    """Summary
    """

    def get_console_func(self):
        """Summary

        Returns
        -------
        TYPE
            Description
        """
        return get_setting("js", {}).get("console_func", ["console", "log"])

    def get_console_log_types(self):
        """Summary

        Returns
        -------
        TYPE
            Description
        """
        return get_setting("js", {}).get("log_types", ["log", "info", "warn", "error"])

    def get_console_str(self):
        """Summary

        Returns
        -------
        TYPE
            Description
        """
        return get_setting("js", {}).get("console_str", "{title}, {variable}")

    def get_console_quotes(self):
        """Summary

        Returns
        -------
        TYPE
            Description
        """
        return quotes_map.get(get_setting("js", {}).get("quotes", "`"), "`")

    def get_console_separator(self):
        """Summary

        Returns
        -------
        TYPE
            Description
        """
        return get_setting("js", {}).get("console_separator", ", ")

    def get_semicolon_setting(self):
        """Summary

        Returns
        -------
        TYPE
            Description
        """
        return get_setting("js", {}).get("semicolon", False)


class JsWrapp(JsSettings):
    """Summary
    """

    def create(self, view, edit, cursor, insert_before):
        """Summary

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        cursor : TYPE
            Description
        insert_before : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        line_region = view.line(cursor)
        string = view.substr(line_region)
        match = re.search(r"(\s*)", string)
        end = 0

        if self.is_log_string(string):
            self.change_log_type(view, edit, line_region, string)
            return end

        if match:
            # check if cursor is on the word and trying to get that word
            if cursor.begin() == cursor.end():
                word = view.word(cursor)
            else:
                word = cursor

            var_text = view.substr(word).strip()

            # if selection is empty and there is no word under cursor use clipboard
            if not var_text:
                var_text = sublime.get_clipboard()

            if var_text[-1:] == ";":
                var_text = var_text[:-1]

            if len(var_text) == 0:
                return sublime.status_message(
                    "Console Wrap: Please make a selection or copy something."
                )
            else:
                indent_str = self.get_indent(view, line_region, insert_before)
                text = self.get_wrapper(view, var_text, indent_str, insert_before)
                # msg('text', text)
                if insert_before:
                    line_reg = line_region.begin()
                else:
                    line_reg = line_region.end()
                view.insert(edit, line_reg, text)
                end = view.line(line_reg + 1).end()

        return end

    def is_log_string(self, line):
        """Summary

        Parameters
        ----------
        line : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        log_func = self.get_console_func()[0]
        return re.match(
            r"(\/\/\s)?" + log_func + "(\\.?)(\\w+)?\\((.+)?\\);?", line.strip()
        )

    def change_log_type(self, view, edit, line_region, line):
        """Summary

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        line_region : TYPE
            Description
        line : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        log_types = self.get_console_log_types()
        log_func = self.get_console_func()[0]
        matches = re.findall(r"(" + log_func + ")(\\.?)(\\w+)?", line)
        if not matches:
            return
        func, dot, method = matches[0]

        if dot:
            if method not in log_types:
                return

            next_type = log_types[(log_types.index(method) + 1) % len(log_types)]
            new_line = line.replace(log_func + "." + method, log_func + "." + next_type)
            view.replace(edit, line_region, new_line)

    def get_indent(self, view, region, insert_before):
        """Summary

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        region : TYPE
            Description
        insert_before : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        matches = re.findall(r"^(\s*)[^\s]", view.substr(region))
        indent_str = matches and len(matches) and matches[0] or ""
        if insert_before:
            return indent_str
        indent_line = view.substr(self.find_next_line(view, region)).strip()
        need_indent = [
            True for i in ["{", "=", ":", "->", "=>"] if indent_line.endswith(i)
        ]
        indent_line.lstrip("{}[]() \t")
        if need_indent:
            indent_str += "\t"
        return indent_str

    def find_next_line(self, view, region):
        """Summary

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        region : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        while (
            0 < region.a
            and region.b < view.size()
            and view.classify(region.a) is sublime.CLASS_EMPTY_LINE
        ):
            region = view.line(region.a - 1)
        return region

    def get_wrapper(self, view, var, indent_str, insert_before):
        """Summary

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        var : TYPE
            Description
        indent_str : TYPE
            Description
        insert_before : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        console_str = self.get_console_str()
        quotes, quotes_escaped = self.get_console_quotes()
        insert_semicolon = self.get_semicolon_setting()
        console_func = self.get_console_func()
        separator = self.get_console_separator()
        text = var.replace(quotes, quotes_escaped)

        if insert_semicolon:
            semicolon = ";"
        else:
            semicolon = ""

        console_arr = console_str.split(separator)

        t = console_arr[0]

        if len(console_arr) >= 2:
            v = separator.join(console_arr[1:])
        else:
            v = t

        tmpl = indent_str if insert_before else ("\n" + indent_str)

        a = f"{'.'.join(console_func)}({quotes}{t}{quotes}{separator}{v}){semicolon}"
        a = a.format(title=text, variable=var)

        tmpl += a

        tmpl += "\n" if insert_before else ""

        return tmpl

    def comment(self, view, edit, cursor):
        """Summary

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        cursor : TYPE
            Description
        """
        log_func = self.get_console_func()[0]
        get_selections(view)
        cursor = view.sel()[0]
        line_region = view.line(cursor)
        string = view.substr(line_region)
        matches = re.finditer(
            r"(?<!\/\/\s)" + log_func + "(\\.?)(\\w+)?\\((.+)?\\);?",
            string,
            re.MULTILINE,
        )

        for match in matches:
            string = string.replace(match.group(0), "// " + match.group(0))

        # remove duplicate
        for match in re.finditer(
            r"((\/\/\s?){2,})(" + log_func + "(\\.?)(\\w+)?\\((.+)?\\);?)",
            string,
            re.MULTILINE,
        ):
            string = string.replace(match.group(1), "// ")

        view.replace(edit, line_region, string)
        view.sel().clear()

    def remove(self, view, edit, cursor):
        """Summary

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        cursor : TYPE
            Description
        """
        log_func = self.get_console_func()[0]
        get_selections(view)
        cursor = view.sel()[0]
        line_region = view.line(cursor)
        string = view.substr(line_region)
        newstring = re.sub(
            r"(\/\/\s)?" + log_func + "(\\.?)(\\w+)?\\((.+)?\\);?", "", string
        )
        view.replace(edit, line_region, newstring)
        view.sel().clear()

    def quick_nav_done(self, view, index, regions, showOnly=False):
        """Summary

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        index : TYPE
            Description
        regions : TYPE
            Description
        showOnly : bool, optional
            Description
        """
        view.sel().clear()
        region = sublime.Region(regions[index].b)
        if not showOnly:
            view.sel().add(region)
        view.show_at_center(region)

    def show_quick_nav(self, view, edit, cursor):
        """Summary

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        cursor : TYPE
            Description
        """
        tags = []
        regions = []

        log_func = self.get_console_func()[0]
        get_selections(view)
        counter = 1
        regex = re.compile(
            r"(\/\/\s)?(" + log_func + ")(\\.?)(\\w+)?\\((.+)?\\);?",
            re.UNICODE | re.DOTALL,
        )
        for comment_region in view.sel():
            for splited_region in view.split_by_newlines(comment_region):
                m = regex.search(view.substr(splited_region))
                if m:
                    # Add a counter for faster selection
                    tag = m.group(0)
                    tags.append(str(counter) + ". " + tag)
                    regions.append(splited_region)
                    counter += 1

        if len(tags):
            view.window().show_quick_panel(
                tags,
                lambda index: self.quick_nav_done(view, index, regions),
                0,
                0,
                lambda index: self.quick_nav_done(view, index, regions),
            )
        else:
            sublime.status_message("Console Wrap: No 'logs' found")

        view.sel().clear()

    def remove_commented(self, view, edit, cursor):
        """Summary

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        cursor : TYPE
            Description
        """
        log_func = self.get_console_func()[0]
        get_selections(view)
        cursor = view.sel()[0]
        line_region = view.line(cursor)
        string = view.substr(line_region)
        newstring = re.sub(
            r"(\/\/\s)" + log_func + "(\\.?)(\\w+)?\\((.+)?\\);?", "", string
        )
        view.replace(edit, line_region, newstring)
        view.sel().clear()
