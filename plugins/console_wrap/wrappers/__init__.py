# -*- coding: utf-8 -*-
"""Summary

Attributes
----------
quotes_map : TYPE
    Description
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

import sublime

from .. import settings

quotes_map: dict[str, list[str]] = {
    '"': ['"', r"\""],
    "'": ["'", r"\'"],
    "`": ["`", r"\`"],
}


def get_selections(view: sublime.View) -> list[sublime.Region]:
    """Summary

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    sublime : TYPE
        Description

    Returns
    -------
    TYPE
        Description
    """
    selections: list[sublime.Region] = view.sel()
    has_selections: bool = False
    for sel in selections:
        if sel.empty() is False:
            has_selections = True
    if not has_selections:
        full_region: sublime.Region = sublime.Region(0, view.size())
        selections.add(full_region)
    return selections


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : dict, any, optional
        Default value in case the key storing a value isn't found.

    Returns
    -------
    any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"console_wrap.{s}", default)
