# -*- coding: utf-8 -*-
"""Smart keystrokes.
"""
from __future__ import annotations

import re

import sublime
import sublime_plugin

__all__: list[str] = [
    "OdyseusDeselectCommand",
    "OdyseusNormalBackspaceCommand",
    "OdyseusSmartBackspaceCommand",
    "OdyseusSmartDeleteCommand",
]


class OdyseusDeselectCommand(sublime_plugin.TextCommand):
    """Summary

    Based on: https://github.com/glutanimate/sublime-deselect
    """

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        end: int = self.view.sel()[0].b
        pt: sublime.Region = sublime.Region(end, end)
        self.view.sel().clear()
        self.view.sel().add(pt)


class OdyseusNormalBackspaceCommand(sublime_plugin.TextCommand):
    """Summary

    Based on: https://github.com/awhite/sublime-smart-backspace
    """

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        if len(self.view.sel()) == 1:
            region: sublime.Region = self.view.sel()[0]
            if not (region.empty()):
                self.view.run_command("left_delete")
            else:
                caret: int = region.begin()
                left_char: str = self.view.substr(sublime.Region(caret - 1, caret))
                right_char: str = self.view.substr(sublime.Region(caret, caret + 1))
                if is_pair(left_char, right_char):
                    self.view.run_command(
                        "run_macro_file",
                        {"file": "res://Packages/Default/Delete Left Right.sublime-macro"},
                    )
                else:
                    self.view.run_command("left_delete")
        else:
            self.view.run_command("left_delete")


class OdyseusSmartBackspaceCommand(sublime_plugin.TextCommand):
    """Summary

    Based on: https://github.com/awhite/sublime-smart-backspace
    """

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.

        Returns
        -------
        None
            Description
        """
        if self.view.settings().get("is_widget"):
            self.view.run_command("odyseus_normal_backspace")
            return

        for region in self.view.sel():
            if not (region.empty()):  # if there is a selection
                self.view.run_command("left_delete")
            else:
                caret: int = region.begin()
                full_line: sublime.Region = self.view.full_line(caret)
                line_contents: str = self.view.substr(full_line)
                left_region: sublime.Region = sublime.Region(full_line.begin(), caret)
                left_contents: str = self.view.substr(left_region)
                line_above: sublime.Region = self.view.full_line(left_region.begin() - 1)
                above_contents: str = self.view.substr(line_above).strip()

                if len(line_contents.strip()) == 0:  # if trimmed line empty
                    if len(above_contents) == 0:  # if the above line is empty
                        # delete above line
                        self.view.erase(edit, line_above)
                    else:
                        point_after_delete: int = left_region.begin() - 1
                        # delete line
                        self.view.run_command(
                            "run_macro_file",
                            {"file": "res://Packages/Default/Delete Line.sublime-macro"},
                        )
                        self.view.sel().clear()
                        self.view.sel().add(sublime.Region(point_after_delete))
                elif len(left_contents) == 0:  # if untrimmed region left is 0
                    self.view.run_command("odyseus_normal_backspace")
                elif len(left_contents.strip()) == 0:  # if trimmed region left of cursor empty
                    if full_line.begin() != 0:  # if there is a line above
                        if len(above_contents) == 0:  # if the above line is empty
                            # delete above line
                            self.view.erase(edit, line_above)
                        else:
                            last_char_point: int = line_above.end() - 1
                            while len(self.view.substr(last_char_point).strip()) == 0:
                                last_char_point = last_char_point - 1
                            last_char: str = self.view.substr(last_char_point)
                            last_char_point: int = last_char_point + 1
                            region_to_delete: sublime.Region = sublime.Region(
                                caret, last_char_point
                            )
                            self.view.erase(edit, region_to_delete)
                            pattern: re.Pattern = re.compile("\\w")
                            if pattern.match(last_char):
                                self.view.insert(edit, last_char_point, " ")
                    else:
                        self.view.run_command(
                            "run_macro_file",
                            {"file": "res://Packages/Default/Delete to Hard BOL.sublime-macro"},
                        )
                else:
                    self.view.run_command("odyseus_normal_backspace")


def is_pair(a: str, b: str) -> bool:
    """Summary

    Parameters
    ----------
    a : str
        Description
    b : str
        Description

    Returns
    -------
    bool
        Description
    """
    if a == "{":
        return b == "}"
    elif a == "'" or a == "`" or a == '"':
        return b == a
    elif a == "<":
        return b == ">"
    elif a == "[":
        return b == "]"
    elif a == "(":
        return b == ")"
    else:
        return False


class OdyseusSmartDeleteCommand(sublime_plugin.TextCommand):
    """Summary

    Based on: https://github.com/mac2000/sublime-smart-delete
    """

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.

        Returns
        -------
        None
            Description
        """
        if len(self.view.sel()) != 1 or self.view.settings().get("is_widget"):
            # Default Delete if there are multiple selections.
            self.view.run_command("right_delete")
            return

        # Has only 1 sel
        for s in self.view.sel():
            if s.empty():
                line: sublime.Region = self.view.line(s)
                after: str = self.view.substr(line)[s.end() - line.end():]
                next_line: sublime.Region = self.view.line(
                    sublime.Region(line.end() + 1, line.end() + 1)
                )
                next_line_is_not_empty: bool = (
                    re.match(r"^\s*$", self.view.substr(next_line)) is None
                )

                if (
                    len(line) > 0
                    and next_line_is_not_empty
                    and (after.isspace() or line.end() == s.end())
                ):
                    b: int = s.begin()
                    e: int = s.end()
                    spaces_before: int = 0

                    while self.view.substr(b - 1).isspace():
                        spaces_before = spaces_before + 1
                        b = b - 1

                    # If there is only one space before - leave it
                    if spaces_before == 1:
                        b = b + 1

                    while self.view.substr(e).isspace():
                        e = e + 1

                    self.view.sel().clear()
                    self.view.sel().add(sublime.Region(b, e))
                else:
                    self.view.run_command("right_delete")

        for s in self.view.sel():
            self.view.erase(edit, s)


if __name__ == "__main__":
    pass
