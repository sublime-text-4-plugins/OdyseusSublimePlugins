# -*- coding: utf-8 -*-
"""Various data generation plugins.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from collections.abc import Generator

import re

import sublime
import sublime_plugin

from . import display_message_in_panel
from . import sphinx_building_docs
from python_utils.lazy_import import lazy_module

if sphinx_building_docs:
    import random
    import string
    import uuid
else:
    random: object = lazy_module("random")
    string: object = lazy_module("string")
    uuid: object = lazy_module("uuid")


__all__: list[str] = [
    "OdyseusGenerateUuidCommand",
    "OdyseusGenerateUuidListener",
    "OdyseusPasswordGeneratorCommand",
    "OdyseusPasswordGeneratorOverlayCommand",
]


class OdyseusGenerateUuidCommand(sublime_plugin.TextCommand):
    """Generate a UUID version 4.

    Plugin logic for the 'generate_uuid' command.
    Searches for "uuid_uppercase" setting in user preferences, capitalizes
    UUID if true.
    """

    def run(
        self,
        edit: sublime.Edit,
        short: bool = False,
        single: bool = False,
        uppercase: bool = False,
    ) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        short : bool, optional
            Remove dashes from UUID.
        single : bool, optional
            Generate a unique UUID for all selections instead of one per cursor.
        uppercase : bool, optional
            Generate upper cased UUID.
        """
        for r, value in zip(self.view.sel(), self.generate_uuids(short, single, uppercase)):
            self.view.replace(edit, r, value)

    def generate_uuids(
        self, short: bool, single: bool, uppercase: bool
    ) -> Generator[str, None, None]:
        """Generate UUIDs.

        Parameters
        ----------
        short : bool
            Remove dashes from UUID.
        single : bool
            Generate a unique UUID for all selections instead of one per cursor.
        uppercase : bool
            Generate upper cased UUID.

        Yields
        ------
        Generator[str, None, None]
            Generated UUID/s.
        """
        if single:
            value = self.new_uuid(uppercase, short)
            while True:
                yield value
        else:
            while True:
                yield self.new_uuid(uppercase, short)

    def new_uuid(self, uppercase: bool, short: bool) -> str:
        """Generate new UUID:

        Parameters
        ----------
        uppercase : bool
            Generate upper cased UUID.
        short : bool
            Remove dashes from UUID.

        Returns
        -------
        str
            Generated UUID.
        """
        value: str = str(uuid.uuid4())
        if uppercase:
            value = value.upper()
        if short:
            value = value.replace("-", "")
        return value


class OdyseusGenerateUuidListener(sublime_plugin.EventListener):
    """Expand 'uuid' and 'uuid4' to a random uuid (uuid4) and
    'uuid1' to a uuid based on host and current time (uuid1).
    Searches for "uuid_uppercase" setting in user preferences, capitalizes
    UUID if true. - author Matt Morrison mattdmo@pigimal.com

    Author: Rob Cowie
    Seealso: https://github.com/SublimeText/GenerateUUID/issues/1
    """

    def on_query_completions(
        self, view: sublime.View, prefix: str, locations: int
    ) -> list[tuple[str, str, str]]:
        """Called whenever completions are to be presented to the user.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        prefix : str
            String of the text to complete.
        locations : int
            List of points.

        Returns
        -------
        list[tuple[str, str, str]]
           List of completion values.
        """
        uuid_prefix: str = ""
        _prefix: str = prefix
        val: str = ""

        if prefix[:2] == "0x":
            uuid_prefix = "0x"
            prefix = prefix[2:]

        if prefix in ("uuid", "uuid4", "UUID", "UUID4"):  # random
            val = str(uuid.uuid4())
        elif prefix in ("uuid1", "UUID1"):  # host and current time
            val = str(uuid.uuid1())
        elif prefix in ("uuid1s", "UUID1S"):
            val = re.sub("-", "", str(uuid.uuid1()))
        elif prefix in ("uuids", "UUIDS"):
            val = re.sub("-", "", str(uuid.uuid4()))
        else:
            return []

        if prefix[0] == "U":
            _prefix = _prefix.upper()
            val = val.upper()

        return [(_prefix, _prefix, uuid_prefix + val)] if val else []


class OdyseusPasswordGeneratorCommand(sublime_plugin.TextCommand):
    """Password generator command.

    Attributes
    ----------
    chars : str
        Characters to use in the generated passwords.
    chars_plus_symbols : str
        Characters and symbols to use in the generated passwords.
    """

    chars = string.ascii_letters + string.digits
    chars_plus_symbols = chars + "!@#$%^&*_-+=|/?:;<>~"

    def run(self, edit: sublime.Edit, length: int = 30, symbols: bool = True) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        length : int, optional
            Password length.
        symbols : bool, optional
            If symbols should be included in the generated passwords.

        Raises
        ------
        ValueError
            If the chosen password length is surpasses the length limit.
        """
        msg_title: str = "%s Error:" % self.__class__.__name__
        length: int = int(length)
        population: str = self.chars_plus_symbols if symbols else self.chars

        try:
            if length > len(population):
                raise ValueError(f"Passwords cannot be longer than {len(population)} characters.")

            for region in self.view.sel():
                p: str = "".join(random.sample(population, length))
                self.view.replace(edit, region, p)
        except ValueError as err:
            display_message_in_panel(self.view, title=msg_title, body=str(err))


class OdyseusPasswordGeneratorOverlayCommand(sublime_plugin.WindowCommand):
    """Password generator overlay command."""

    def run(self, symbols: bool = False) -> None:
        """Called when the command is run.

        Parameters
        ----------
        symbols : bool, optional
            If symbols should be included in the generated passwords.
        """
        self._symbols: bool = symbols

        self.window.show_input_panel(
            "Set secure password length" if symbols else "Set alphanumeric password length",
            "20",
            self._on_done,
            None,
            None,
        )

    def _on_done(self, length: int) -> None:
        """Generate password.

        Parameters
        ----------
        length : int
            Password length.

        Returns
        -------
        None
            Halt execution.
        """
        view: sublime.View = self.window.active_view()

        if not view:
            return

        sublime.set_timeout_async(
            lambda: view.run_command(
                "odyseus_password_generator",
                {"length": length, "symbols": self._symbols},
            ),
            100,
        )


if __name__ == "__main__":
    pass
