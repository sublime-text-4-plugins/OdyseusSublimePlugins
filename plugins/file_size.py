# -*- coding: utf-8 -*-
"""Case conversion plugin.

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

import os

import sublime
import sublime_plugin

from . import plugin_name
from . import settings
from python_utils.sublime_text_utils.queue import Queue

queue: Queue = Queue()


__all__: list[str] = ["OdyseusUpdateFileSizeStatusCommand", "OdyseusStatusBarFileSize"]

_plugin_id: str = f"{plugin_name}FileSize-{{}}"
_status_key: str = _plugin_id.format("Status")


def file_size_str(size: int) -> str | None:
    """Generate file size string.

    Parameters
    ----------
    size : int
        File size.

    Returns
    -------
    str | None
        The file size string.
    """
    if size is None:
        return None

    divisor: int = 1024
    sizes: list[str] = ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"]

    if size < divisor:
        return "%d %s" % (size, "Bytes" if size != 1 else "Byte")
    else:
        size_val: int = size / divisor

        for unit in sizes:
            if size_val >= divisor:
                size_val /= divisor
            else:
                return "%.2f %s" % (size_val, unit)

        return "%.2f %s" % (size_val * divisor, sizes[-1])


def _update_file_size(view: sublime.View) -> None:
    """Update file size.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    """
    size = None

    if view and view.file_name() and not view.is_dirty():
        try:
            size = os.path.getsize(view.file_name())
        except OSError:
            pass

    if size is not None:
        status_text = file_size_str(size)
        view.set_status(_status_key, status_text)
    else:
        view.erase_status(_status_key)


class OdyseusUpdateFileSizeStatusCommand(sublime_plugin.TextCommand):
    """Update file size status command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        queue.debounce(
            lambda: _update_file_size(self.view),
            delay=100,
            key=_plugin_id.format("debounce-update-file_size"),
        )


class OdyseusStatusBarFileSize(sublime_plugin.EventListener):
    """Statusbar file size listener."""

    def update_file_size(self, view: sublime.View) -> None:
        """Update file size.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if settings.get("file_size.live_status_update", False):
            queue.debounce(
                lambda: _update_file_size(view),
                delay=100,
                key=_plugin_id.format("debounce-update-file_size"),
            )

    def on_deactivated_async(self, view: sublime.View) -> None:
        """Called when a view loses input focus.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        view.erase_status(_status_key)

    def on_post_save_async(self, view: sublime.View) -> None:
        """Called after a view has been saved.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        self.update_file_size(view)

    def on_activated_async(self, view: sublime.View) -> None:
        """Called when the view gains input focus.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        self.update_file_size(view)


if __name__ == "__main__":
    pass
