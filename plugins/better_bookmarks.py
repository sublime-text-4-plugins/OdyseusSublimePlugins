# -*- coding: utf-8 -*-
"""Better bookmarks.

Based on `Better Bookmarks <https://packagecontrol.io/packages/Better Bookmarks>`__.

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from python_utils.sublime_text_utils.settings import SettingsManager
    from typing import Any

import collections
import hashlib
import json
import os

import sublime
import sublime_plugin

from . import events
from . import logger
from . import plugin_name
from . import settings
from python_utils.sublime_text_utils import utils
from python_utils.sublime_text_utils.queue import Queue

queue: Queue = Queue()

__all__: list[str] = [
    "OdyseusBetterBookmarksCommand",
    "OdyseusBetterBookmarksEventListener",
]

_plugin_id: str = f"{plugin_name}BetterBookmarks-{{}}"
_status_name: str = f"{plugin_name}better_bookmarks_layer_status"


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"better_bookmarks.{s}", default)


class Storage:
    """Storage class.

    Attributes
    ----------
    cache_dir : str
        Path to where the bookmarks cache are stored.
    layer : str
        Name of the current bookmarks layer.
    layers : collections.deque
        All previous regions.
    """

    cache_dir: str = ""
    layer: str = ""
    layers: collections.deque = None

    @classmethod
    def update(cls):
        """Update storage."""
        cls.cache_dir = f"{sublime.packages_path()}/User/{plugin_name}/BetterBookmarks"
        cls.layers = collections.deque(get_setting("layer_icons", {}))
        cls.layer = get_setting("default_layer", "bookmarks")

        while not cls.layers[0] == cls.layer:
            cls.layers.rotate(1)

        if not os.path.exists(Storage.cache_dir):
            os.makedirs(Storage.cache_dir)


@events.on("plugin_loaded")
def on_better_bookmarks_plugin_loaded() -> None:
    """Called on plugin loaded."""
    queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage"))


@events.on("settings_changed")
def on_better_bookmarks_settings_changed(settings_obj: SettingsManager) -> None:
    """Called on settings changed.

    Parameters
    ----------
    settings_obj : SettingsManager
        An instance of :any:`python_utils.sublime_text_utils.settings.SettingsManager`.
    """
    if settings_obj.has_changed("better_bookmarks.layer_icons"):
        queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage"))


def fix_region(mark: sublime.Region) -> list[int]:
    """Takes a region and converts it to a list taking into consideration if
    the user wants us to care about the order of the selection.

    Parameters
    ----------
    mark : sublime.Region
        A Sublime Text ``Region`` object.

    Returns
    -------
    list[int]
        Fixed region.
    """
    if get_setting("ignore_cursor", True):
        return [mark.begin(), mark.end()]
    return [mark.a, mark.b]


class OdyseusBetterBookmarksCommand(sublime_plugin.TextCommand):
    """Better bookmarks command.

    Attributes
    ----------
    filename : str
        View's file name.
    """

    def run(self, edit: sublime.Edit, **kwargs) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        **kwargs
            Keyword arguments.

        Returns
        -------
        None
            Halt execution.
        """
        view: sublime.View = self.view
        subcommand: str = kwargs.get("subcommand")
        self.filename: str = utils.get_filename(view)
        layer: str = kwargs.get("layer", Storage.layer)
        direction = kwargs.get("direction", "next")

        if not subcommand or not view:
            return

        if subcommand == "mark_line":
            mode: str = get_setting("marking_mode", "selection")

            if mode == "line":
                selection = view.lines(view.sel()[0])
            elif mode == "selection":
                selection = view.sel()
            else:
                sublime.error_message(
                    f"Invalid BetterBookmarks setting: '{mode}' is invalid for 'marking_mode'"
                )

            line: list[sublime.Region] = kwargs.get("line", selection)

            self._add_marks(line, layer)
        elif subcommand == "cycle_mark":
            self.view.run_command(
                f"{direction}_bookmark",
                {"name": "odyseus_better_bookmarks"},
            )
        elif subcommand == "clear_marks":
            self.view.erase_regions("odyseus_better_bookmarks")
            self.view.erase_regions(self._get_region_name(layer))
        elif subcommand == "clear_all":
            self.view.erase_regions("odyseus_better_bookmarks")
            for lay in Storage.layers:
                self.view.erase_regions(self._get_region_name(lay))
        elif subcommand == "layer_swap":
            if direction == "prev":
                Storage.layers.rotate(-1)
            elif direction == "next":
                Storage.layers.rotate(1)
            else:
                sublime.error_message("Invalid layer swap direction.")

            self._change_to_layer(Storage.layers[0])
        elif subcommand == "on_load":
            logger.debug("Loading BBFile for " + self.filename)
            try:
                with open(self._get_cache_filename(), "r") as fp:
                    data = json.load(fp)

                    for name, marks in data["bookmarks"].items():
                        self._add_marks([sublime.Region(mark[0], mark[1]) for mark in marks], name)
            except Exception:
                pass
            self._change_to_layer(get_setting("default_layer", "bookmark"))
        elif subcommand == "on_save":
            self._save_marks()
        elif subcommand == "on_close":
            if get_setting("cache_marks_on_close", False):
                self._save_marks()

            if get_setting("cleanup_empty_cache_on_close", False) and self._is_empty():
                logger.debug("Removing BBFile for " + self.filename)
                try:
                    os.remove(self._get_cache_filename())
                except FileNotFoundError:
                    pass

    def _is_empty(self) -> bool:
        """View has bookmarks.

        Returns
        -------
        bool
            If the view has bookmarks.
        """
        for layer in Storage.layers:
            if self.view.get_regions(self._get_region_name(layer)):
                return False

        return True

    def _get_cache_filename(self) -> str:
        """Get the path to the cache file.

        Returns
        -------
        str
            Path to a cache file.
        """
        h: hashlib._Hash = hashlib.md5()
        h.update(self.view.file_name().encode())
        filename: str = str(h.hexdigest())

        return f"{Storage.cache_dir}/{filename}.bb_cache"

    def _get_region_name(self, layer: str | None = None) -> str:
        """Get region name.

        Parameters
        ----------
        layer : str | None, optional
            Bookmark layer name.

        Returns
        -------
        str
            Region name.
        """
        return "better_bookmarks_{}".format(layer if layer else Storage.layer)

    def _render(self) -> None:
        """Renders the current layers marks to the view."""
        marks: list[sublime.Region] = self.view.get_regions(self._get_region_name())
        icon: str = utils.substitute_variables(
            {"package": f"Packages/{plugin_name}"},
            get_setting("layer_icons", {})[Storage.layer]["icon"],
        )
        scope: str = get_setting("layer_icons", {})[Storage.layer]["scope"]

        self.view.add_regions(
            "odyseus_better_bookmarks",
            marks,
            scope=scope,
            icon=icon,
            flags=sublime.PERSISTENT | sublime.HIDDEN,
        )

    def _add_marks(self, new_marks: list[sublime.Region], layer: str | None = None) -> None:
        """Add a list of marks to the existing ones.

        Any marks that exist in both lists will be removed as this case is when the user is
        attempting to remove a mark.

        Parameters
        ----------
        new_marks : list[sublime.Region]
            New marks to add.
        layer : str | None, optional
            Name of bookmarks layer.
        """
        region: str = self._get_region_name(layer)
        marks: list[sublime.Region] = self.view.get_regions(region)

        for mark in new_marks:
            if mark in marks:
                marks.remove(mark)
            else:
                marks.append(mark)

        self.view.add_regions(region, marks, scope="", icon="", flags=0)

        if layer == Storage.layer:
            self._render()

    def _change_to_layer(self, layer: str) -> None:
        """Change the layer to the given one and update any and all of the status indicators.

        Parameters
        ----------
        layer : str
            Bookmarks layer name.
        """
        Storage.layer = layer

        status = get_setting("layer_status_location", ["permanent"])

        if "temporary" in status:
            sublime.status_message(Storage.layer)
        if "permanent" in status:
            self.view.set_status(_status_name, f"Bookmark Layer: {Storage.layer}")
        else:
            self.view.erase_status(_status_name)

        if "popup" in status:
            if self.view.is_popup_visible():
                self.view.update_popup(Storage.layer)
            else:
                self.view.show_popup(
                    Storage.layer,
                    flags=0,
                    location=-1,
                    max_width=1024,
                    max_height=768,
                    on_navigate=None,
                    on_hide=None,
                )

        self._render()

    def _save_marks(self) -> None:
        """Save marks."""
        if not self._is_empty():
            logger.debug("Saving BBFile for " + self.filename)
            with open(self._get_cache_filename(), "w") as fp:
                marks: dict[str, Any] = {
                    "filename": self.view.file_name(),
                    "bookmarks": {},
                }
                for layer in Storage.layers:
                    marks["bookmarks"][layer] = [
                        fix_region(mark)
                        for mark in self.view.get_regions(self._get_region_name(layer))
                    ]
                json.dump(marks, fp)


class OdyseusBetterBookmarksEventListener(sublime_plugin.EventListener):
    """Better bookmarks event listener."""

    def _contact(self, view: sublime.View, subcommand: str) -> None:
        """Called when the file is finished loading.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        subcommand : str
            Name of a sub-command.
        """
        view.run_command("odyseus_better_bookmarks", {"subcommand": subcommand})

    def on_load_async(self, view: sublime.View) -> None:
        """Called when the file is finished loading.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if get_setting("uncache_marks_on_load", True):
            # NOTE: Debounced because the Storage might not be updated at this stage.
            queue.debounce(
                lambda: self._contact(view, "on_load"),
                delay=500,
                key=_plugin_id.format("debounce-on-load"),
            )

    def on_pre_save(self, view: sublime.View) -> None:
        """Called just before a view is saved.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if get_setting("cache_marks_on_save", True):
            self._contact(view, "on_save")

    def on_close(self, view: sublime.View) -> None:
        """Called when a view is closed (note, there may still be other views into the same buffer).

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if view.file_name():
            self._contact(view, "on_close")
