# -*- coding: utf-8 -*-
"""Convert HTML to text.

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

import re

import sublime
import sublime_plugin

from . import display_message_in_panel
from . import logger
from . import plugin_name
from . import settings
from . import sphinx_building_docs

from python_utils.lazy_import import lazy_module
from python_utils.sublime_text_utils.queue import Queue

if sphinx_building_docs:
    from python_utils import html2text
    from python_utils.sublime_text_utils import utils
else:
    html2text: object = lazy_module("python_utils.html2text")
    utils: object = lazy_module("python_utils.sublime_text_utils.utils")


__all__: list[str] = ["OdyseusConvertHtmlToTextCommand"]

queue: Queue = Queue()

_plugin_id: str = f"{plugin_name}HTMLToText-{{}}"
_trailing_spaces_re: re.Pattern = re.compile(r"[ \t]+$", flags=re.M)
_surplus_blank_lines_re: re.Pattern = re.compile(r"\n{3,}", flags=re.M)


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    # NOTE:
    return settings.get(f"html_to_text.{s}", default)


class OdyseusConvertHtmlToTextCommand(sublime_plugin.TextCommand):
    """HTML to text command."""

    def run(
        self,
        edit: sublime.Edit,
        whole_file: bool = False,
        parser_options: str | None = None,
    ) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        whole_file : bool, optional
            Convert the whole file content.
        parser_options : str | None, optional
            Parser options as defined in the plugin settings.

        Returns
        -------
        None
            Halt execution.
        """
        view: sublime.View = self.view

        if not view:
            return

        all_selections: list[sublime.Region] = []

        if whole_file:
            all_selections = [sublime.Region(0, view.size())]
        else:
            all_selections = utils.get_selections(
                view, return_whole_file=False, extract_words=False
            )

        if not all_selections:
            return

        replacement_data: list[tuple[sublime.Region, str]] = []
        parser_options: dict[str, object] = (
            get_setting(parser_options, {}) if isinstance(parser_options, str) else {}
        )

        with html2text.HTML2Text(**parser_options) as parser:
            for region in all_selections:
                try:
                    text: str = view.substr(region)
                    parsed_text: str = parser.handle(text)
                    parsed_text: str = _trailing_spaces_re.sub("", parsed_text)
                    parsed_text: str = _surplus_blank_lines_re.sub("\n\n", parsed_text)

                    if parsed_text != text:
                        replacement_data.append((region, parsed_text))
                except Exception as err:
                    title: str = "OdyseusConvertHtmlToTextCommand: Error"
                    display_message_in_panel(title=title, body=err)
                    logger.exception(err)
                    break

        if replacement_data:
            utils.replace_all_selections(view, edit, replacement_data)


if __name__ == "__main__":
    pass
