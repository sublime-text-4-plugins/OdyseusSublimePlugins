# -*- coding: utf-8 -*-
"""Replace selected text with the regex escaped equivalent.

Based on: https://github.com/Jonnymcc/sublime_regex_escape
"""
from __future__ import annotations

import re

import sublime
import sublime_plugin

from . import sphinx_building_docs
from python_utils.lazy_import import lazy_module

if sphinx_building_docs:
    from python_utils.sublime_text_utils import utils
else:
    utils: object = lazy_module("python_utils.sublime_text_utils.utils")


__all__: list[str] = ["OdyseusRegexEscapeCommand"]


class OdyseusRegexEscapeCommand(sublime_plugin.TextCommand):
    """Regex escape command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.

        Returns
        -------
        None
            Halt execution.
        """
        if not self.view:
            return

        all_selections: list[sublime.Region] = utils.get_selections(
            self.view, return_whole_file=False, extract_words=False
        )

        if not len(all_selections):
            return

        replacement_data: list[tuple[sublime.Region, str]] = []

        for region in all_selections:
            replacement_data.append((region, re.escape(self.view.substr(region))))

        if replacement_data:
            utils.replace_all_selections(self.view, edit, replacement_data)


if __name__ == "__main__":
    pass
