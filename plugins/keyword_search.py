# -*- coding: utf-8 -*-
"""Keyword search.

Based on: https://github.com/bsoun/bang-search

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from python_utils.sublime_text_utils.settings import SettingsManager
    from typing import Any

import copy
import webbrowser

from threading import Thread
from urllib.parse import urlencode
from urllib.parse import urlparse

import sublime
import sublime_plugin

from . import events
from . import logger
from . import plugin_name
from . import settings
from . import sphinx_building_docs
from python_utils.lazy_import import lazy_module
from python_utils.sublime_text_utils.custom_classes import CustomListInputItem
from python_utils.sublime_text_utils.queue import Queue

if sphinx_building_docs:
    from python_utils import misc_utils
    from python_utils.sublime_text_utils import utils
else:
    misc_utils = lazy_module("python_utils.misc_utils")
    utils = lazy_module("python_utils.sublime_text_utils.utils")


__all__: list[str] = [
    "OdyseusKeywordSearchCommand",
    "OdyseusKeywordSearchInputCommand",
]

queue: Queue = Queue()

_plugin_id: str = f"{plugin_name}KeywordSearch-{{}}"
_default_query_param: dict[str, str] = {
    "q": "${search_term}",
}
_default_engines: dict[str, dict[str, object]] = {
    "ddg_keyword": {
        "url": "https://duckduckgo.com/",
        "params": _default_query_param,
    },
    "ddg": {
        "url": "https://duckduckgo.com/",
        "params": _default_query_param,
    },
    "qwant": {
        "url": "https://qwant.com/",
        "params": _default_query_param,
    },
    "qwant_keyword": {
        "url": "https://qwant.com/",
        "params": _default_query_param,
    },
    "google": {
        "url": "https://google.com/search",
        "params": _default_query_param,
    },
}
_default_extra_params: dict[str, dict[str, str]] = {
    "ddg_extra_params": {
        "kw": "s",
        "k1": "-1",
        "kam": "google-maps",
        "kap": "-1",
        "kz": "-1",
        "kav": "1",
        "kak": "-1",
        "kau": "-1",
        "kn": "1",
        "kaq": "-1",
        "kaj": "m",
        "kp": "-2",
        "kax": "-1",
        "kao": "-1",
        "kg": "g",
        "kae": "c",
        "kt": "Open+Sans",
        "ko": "1",
        "ka": "Open+Sans",
        "k18": "1",
        "ia": "definition",
    },
    "qwant_extra_params": {
        "hc": "0",
        "vt": "1",
        "b": "1",
        "locale": "en_GB",
        "theme": "0",
        "s": "0",
        "l": "en",
    },
}


class Storage:
    """Storage class.

    Attributes
    ----------
    extra_params : dict
        Extra parameters for the various serach engines.
    global_search_term : str | None
        Storage for the used search term. FIXME: this is a workaround. Do it the right way.
    keyword_definitions : dict[str, dict[str, Any]]
        Description
    search_engines_list_input_items : list[CustomListInputItem]
        Description
    """

    global_search_term: str | None = None
    keyword_definitions: dict[str, dict[str, Any]] = {}
    search_engines_list_input_items: list[CustomListInputItem] = []
    extra_params: dict = {
        "ddg_keyword": {},
        "ddg": {},
        "qwant_keyword": {},
        "qwant": {},
        "google": {},
    }

    @classmethod
    def update(cls) -> None:
        """Update storage."""
        cls.global_search_term = None
        cls.keyword_definitions.clear()
        cls.search_engines_list_input_items.clear()

        for p in list(cls.extra_params.keys()):
            cls.extra_params[p].clear()

        cls.keyword_definitions = misc_utils.merge_dict(
            get_setting("keyword_definitions", {}),
            get_setting("keyword_definitions_user", {}),
            logger=logger,
        )
        cls.extra_params["ddg_keyword"] = cls.extra_params["ddg"] = misc_utils.merge_dict(
            copy.deepcopy(_default_query_param),
            get_extra_params("ddg_extra_params"),
            logger=logger,
        )
        cls.extra_params["qwant_keyword"] = cls.extra_params["qwant"] = misc_utils.merge_dict(
            copy.deepcopy(_default_query_param),
            get_extra_params("qwant_extra_params"),
            logger=logger,
        )
        cls.extra_params["google"] = misc_utils.merge_dict(
            copy.deepcopy(_default_query_param),
            get_setting("google_extra_params", {}),
            logger=logger,
        )

        for keyword, keyword_def in cls.keyword_definitions.items():
            keyword_type: str | None = keyword_def.get("type", None)
            keyword_caption: str | None = keyword_def.get("caption", None)

            if keyword_type is None or keyword_caption is None:
                logger.debug(f"Keyword definition with missing required parameters: {keyword}")
                continue

            cls.keyword_definitions[keyword]["id"] = keyword

            if keyword_type in _default_engines:
                cls.keyword_definitions[keyword]["url"] = _default_engines[keyword_type]["url"]

            if keyword_def.get("visible", True):
                cls.search_engines_list_input_items.append(
                    CustomListInputItem(
                        text=keyword_def["id"],
                        annotation=keyword_def["caption"],
                        value=keyword_def["id"],
                        kind=get_kind_tuple(keyword_def["type"]),
                    )
                )

        cls.search_engines_list_input_items.sort()


@events.on("plugin_loaded")
def on_keyword_search_plugin_loaded() -> None:
    """Called on plugin loaded."""
    queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage"))


@events.on("settings_changed")
def on_keyword_search_settings_changed(settings_obj: SettingsManager) -> None:
    """Called on settings changed.

    Parameters
    ----------
    settings_obj : SettingsManager
        An instance of :any:`python_utils.sublime_text_utils.settings.SettingsManager`.
    """
    if any(
        (
            settings_obj.has_changed("keyword_search.keyword_definitions"),
            settings_obj.has_changed("keyword_search.ddg_extra_params"),
            settings_obj.has_changed("keyword_search.qwant_extra_params"),
            settings_obj.has_changed("keyword_search.google_extra_params"),
        )
    ):
        queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage"))


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"keyword_search.{s}", default)


def get_kind_tuple(keyword_type: str) -> tuple[int, str, str]:
    """Summary

    Parameters
    ----------
    keyword_type : str
        Description

    Returns
    -------
    tuple[int, str, str]
        Description
    """
    if keyword_type == "ddg_keyword" or keyword_type == "ddg":
        return (sublime.KIND_ID_COLOR_REDISH, "D", "DuckDuckGo search engine")

    if keyword_type == "qwant_keyword" or keyword_type == "qwant":
        return (sublime.KIND_ID_COLOR_ORANGISH, "Q", "Qwant search engine")

    if keyword_type == "google":
        return (sublime.KIND_ID_COLOR_GREENISH, "G", "Google search engine")

    return (sublime.KIND_ID_COLOR_BLUISH, "C", "Custom search engine")


def keyword_warning(text: str) -> None:
    """Summary

    Parameters
    ----------
    text : str
        Description
    """
    msg: str = f"Keyword Search: {text}"

    if get_setting("discrete_warnings", True):
        sublime.status_message(msg)
    else:
        sublime.message_dialog(msg)


def process_keyword_definition(keyword_id: str) -> list[dict[str, object]]:
    """Summary

    Parameters
    ----------
    keyword_id : str
        Description

    Returns
    -------
    list[dict[str, object]]
        Description
    """
    keyword_def: dict[str, Any] = Storage.keyword_definitions[keyword_id]

    if Storage.keyword_definitions[keyword_id]["type"] == "group":
        return [
            Storage.keyword_definitions[id]
            for id in keyword_def["keywordlist"]
            if id in Storage.keyword_definitions
        ]
    else:
        return [keyword_def]


def search_by_keyword(search_term: str, keyword: str) -> None:
    """Summary

    Parameters
    ----------
    search_term : str
        Description
    keyword : str
        Description
    """
    keyword_definitions: list[dict[str, Any]] = process_keyword_definition(keyword)
    threads: list[Thread] = []

    for keyword_def in keyword_definitions:
        keyword_id: str = keyword_def["id"]
        keyword_type: str = keyword_def["type"]
        chosen_browser: str = get_setting("browsers_list", [None])[0]
        chosen_browser = chosen_browser if chosen_browser != "default" else None
        keyword_params: dict[str, Any] = copy.deepcopy(keyword_def.get("params", {}))

        if keyword_type in _default_engines:
            keyword_params = misc_utils.merge_dict(
                copy.deepcopy(Storage.extra_params[keyword_type]),
                keyword_params,
                logger=logger,
            )
        else:
            keyword_params = keyword_params if keyword_params else _default_query_param

        params: str = urlencode(
            utils.substitute_variables(
                {
                    "search_term": f"{keyword_id} {search_term}"
                    if keyword_type.endswith("_keyword")
                    else f"{search_term}"
                },
                keyword_params,
            )
        )

        search_url: str = urlparse("%s?%s" % (keyword_def["url"], params)).geturl()

        webbrowser_controller: webbrowser.BaseBrowser = webbrowser.get(using=chosen_browser)

        t: Thread = Thread(target=webbrowser_controller.open_new_tab, args=(search_url,))
        t.start()
        threads.append(t)

        for thread in threads:
            if thread is not None and thread.is_alive():
                thread.join()


def get_extra_params(param_key: str) -> dict[str, object]:
    """Summary

    Parameters
    ----------
    param_key : str
        Description

    Returns
    -------
    dict[str, object]
        Description
    """
    extra_params: dict[str, object] = get_setting(param_key, False)

    if extra_params is False:
        return {}
    elif extra_params is True:
        return _default_extra_params[param_key]
    elif isinstance(extra_params, dict):
        return extra_params

    return {}


class OdyseusKeywordSearchCommand(sublime_plugin.TextCommand):
    """Summary"""

    def run(
        self,
        edit: sublime.Edit,
        search_term: str | None = None,
        search_method: str | None = None,
    ) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        search_term : str | None, optional
            Description
        search_method : str | None, optional
            Description

        Returns
        -------
        None
            Description
        """
        querys: list[str] = []

        if search_term:
            querys = [search_term]
        else:
            for region in utils.get_selections(
                self.view, return_whole_file=False, extract_words=True
            ):
                querys.append(self.view.substr(region))

        if len(querys) != 0:
            search_term = " ".join(querys)

            if search_term is not None:
                Storage.global_search_term = search_term

            if search_method:
                if search_method in Storage.keyword_definitions:
                    search_by_keyword(search_term or Storage.global_search_term, search_method)
                else:
                    keyword_warning("%s is not defined!!!" % (search_method))
            else:
                search_method = self.resolve_namespace(None)

                if not search_method:
                    keyword_warning("No search method found.")
                    return
                elif isinstance(search_method, OdyseusKeywordSearchMethodInputHandler):
                    # Fallback if command was called directly through e.g. a key binding.
                    sublime.set_timeout(self.rerun_from_command_palette, 100)
        else:
            keyword_warning("Nothing to search!")

    def input_description(self) -> str:
        """Allows a custom name to be show to the left of the cursor in the input box, instead of
        the default one generated from the command name.

        Returns
        -------
        str
            Description
        """
        return "Keyword search"

    def input(self, args) -> sublime_plugin.ListInputHandler | None:
        """Summary

        Parameters
        ----------
        args : TYPE
            Description

        Returns
        -------
        sublime_plugin.ListInputHandler | None
        """
        if "search_method" not in args:
            return OdyseusKeywordSearchMethodInputHandler(
                args.get("search_term") or Storage.global_search_term
            )

        return None

    def resolve_namespace(self, search_term: str | None):
        """Summary

        Parameters
        ----------
        search_term : str | None
            Description

        Returns
        -------
        TYPE
            Description
        """
        return OdyseusKeywordSearchMethodInputHandler(search_term or Storage.global_search_term)

    def rerun_from_command_palette(self) -> None:
        """Summary"""
        self.view.window().run_command(
            "show_overlay",
            {"overlay": "command_palette", "command": "odyseus_keyword_search"},
        )


class OdyseusKeywordSearchInputCommand(sublime_plugin.TextCommand):
    """Summary"""

    def run(
        self,
        edit: sublime.Edit,
        search_term: str | None = None,
        search_method: str | None = None,
    ) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        search_term : str | None, optional
            Description
        search_method : str | None, optional
            Description
        """
        if search_method and search_term:
            if search_method in Storage.keyword_definitions:
                search_by_keyword(search_term, search_method)
            else:
                keyword_warning(f"{search_method} is not defined!!!")
        elif search_term and not search_method:
            Storage.global_search_term = search_term

            self.view.window().run_command(
                "odyseus_keyword_search",
                {
                    "search_term": search_term,
                },
            )

    def input_description(self) -> str:
        """Allows a custom name to be show to the left of the cursor in the input box, instead of
        the default one generated from the command name.

        Returns
        -------
        str
            Description
        """
        return "Keyword search"

    def input(self, args: dict[str, Any]) -> sublime_plugin.TextInputHandler | None:
        """If this returns something other than None, the user will be prompted for an input before
        the command is run in the Command Palette.

        Parameters
        ----------
        args : dict[str, Any]
            Description

        Returns
        -------
        sublime_plugin.TextInputHandler | None
        """
        if args.get("search_term") is None:
            return OdyseusKeywordSimpleTextInputHandler(
                "search_term", placeholder=args.get("search_method", "search term")
            )

        return None


class OdyseusKeywordSimpleTextInputHandler(sublime_plugin.TextInputHandler):
    """Summary

    Attributes
    ----------
    param_name : str
        Description
    """

    def __init__(self, param_name: str, *, placeholder: str = "") -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        param_name : str
            Description
        placeholder : str, optional
            Description
        """
        self.param_name: str = param_name
        self._placeholder: str = placeholder

    def name(self) -> str:
        """Summary

        Returns
        -------
        str
            Description
        """
        return self.param_name

    def placeholder(self) -> str:
        """Placeholder text is shown in the text entry box before the user has entered anything.
        Empty by default.

        Returns
        -------
        str
            Description
        """
        return self._placeholder


class OdyseusKeywordSearchMethodInputHandler(sublime_plugin.ListInputHandler):
    """Summary

    Attributes
    ----------
    search_term : str
        Description
    """

    def __init__(self, search_term: str) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        search_term : str
            Description
        """
        self.search_term: str = search_term

    def name(self) -> str:
        """Summary

        Returns
        -------
        str
            Description
        """
        return "search_method"

    def placeholder(self) -> str:
        """Placeholder text is shown in the text entry box before the user has entered anything.
        Empty by default.

        Returns
        -------
        str
            Description
        """
        return "Select engine"

    def list_items(self) -> list[CustomListInputItem] | list[str]:
        """This method should return the items to show in the list.

        Returns
        -------
        list[CustomListInputItem] | list[str]
        """
        if Storage.search_engines_list_input_items:
            return Storage.search_engines_list_input_items

        return ["Something went wrong!"]

    def preview(self, value: Any) -> sublime.Html | None:
        """Called whenever the user changes the selected item. The returned value (either plain
        text or HTML) will be shown in the preview area of the Command Palette.

        Parameters
        ----------
        value : Any
            Description

        Returns
        -------
        sublime.Html | None
        """
        if value is None:
            return None

        query: str = self.search_term

        if Storage.keyword_definitions[value]["type"].endswith("_keyword"):
            query = f"{value} {query}"

        return sublime.Html(
            '<span style="color:var(--foreground);">Query: <code>{}</code></span>'.format(query)
        )


if __name__ == "__main__":
    pass
