# -*- coding: utf-8 -*-
"""Fold/Unfold Python docstrings.

Fold/Unfold Python docstrings based on: `SublimeFoldPythonDocstrings <https://github.com/alecthomas/SublimeFoldPythonDocstrings>`__.

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from python_utils.sublime_text_utils.settings import SettingsManager
    from typing import Any

import re

import sublime
import sublime_plugin

from . import display_message_in_panel
from . import events
from . import logger
from . import plugin_name
from . import settings
from . import sphinx_building_docs
from python_utils.lazy_import import lazy_module
from python_utils.sublime_text_utils.queue import Queue
from python_utils.sublime_text_utils.utils import is_editable_view

if sphinx_building_docs:
    from python_utils import fix_imports
    from python_utils.sublime_text_utils import merge_utils
    from python_utils.sublime_text_utils import utils
else:
    fix_imports: object = lazy_module("python_utils.fix_imports")
    merge_utils: object = lazy_module("python_utils.sublime_text_utils.merge_utils")
    utils: object = lazy_module("python_utils.sublime_text_utils.utils")

__all__: list[str] = [
    "OdyseusFixPythonImportsCommand",
    "OdyseusFoldFilePythonDocstringsListener",
    "OdyseusFoldPythonDocstringsCommand",
    "OdyseusUnfoldPythonDocstringsCommand",
    "OdyseusTogglePythonDocstringsAutoFoldCommand",
]

queue: Queue = Queue()

_plugin_id: str = f"{plugin_name}PythonUtilities-{{}}"
_docstring_startswith_regex: str = r"""^(?i)(ur|ru|u|r)?(\"\"\"|''')"""
_docstring_selectors: list[str] = [
    "string.quoted.double.block",
    "string.quoted.single.block",
    "string.quoted.docstring",
    "comment.block",
]


class Storage:
    """Storage class.

    Attributes
    ----------
    docstring_startswith_re : re.Pattern | None
        Compiled regular expression to match start of Python docstrings.
    """

    docstring_startswith_re: re.Pattern | None = None

    @classmethod
    def update(cls) -> None:
        """Update storage."""
        cls.docstring_startswith_re = re.compile(
            get_setting("docstring_startswith_regex", _docstring_startswith_regex)
        )


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"python_utilities.{s}", default)


@events.on("plugin_loaded")
def on_python_utilities_plugin_loaded() -> None:
    """Called on plugin loaded."""
    queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-storage-update"))


@events.on("settings_changed")
def on_python_utilities_settings_changed(settings_obj: SettingsManager) -> None:
    """Called on settings changed.

    Parameters
    ----------
    settings_obj : SettingsManager
        An instance of :any:`python_utils.sublime_text_utils.settings.SettingsManager`.
    """
    if any((settings_obj.has_changed("python_utilities.docstring_startswith_regex"),)):
        queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage"))


def fold_comments(view: sublime.View) -> None:
    """Fold comments.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    """
    selections: sublime.Selection = view.sel()
    number_lines_to_fold: int = get_setting("number_of_lines", 1)
    for region in view.find_by_selector(
        ", ".join(get_setting("docstring_selectors", _docstring_selectors))
    ):
        lines = view.lines(region)

        if len(lines) <= 1:
            continue

        if get_setting("folding_ignores_cursor", True) and any(
            (sel.intersects(region) for sel in selections)
        ):
            view.unfold(region)
            continue

        region: sublime.Region = sublime.Region(lines[0].begin(), lines[-1].end())
        text: str = view.substr(region).strip()

        if not Storage.docstring_startswith_re.match(text):
            continue

        start: int = 0

        # # Handle docstrings with just quotes on the first line.
        # if text[3:4] in "\n\r":
        #     leading = len(text[4:]) - len(text[4:].lstrip())
        #     region = sublime.Region(lines[0].end(), lines[1].begin() + leading)
        #     view.fold(region)
        #     start = 1

        fold_region: sublime.Region = sublime.Region(
            lines[start + number_lines_to_fold - 1].end(), lines[-1].end() - 3
        )
        view.fold(fold_region)


def debounced_fold_comments(view: sublime.View) -> None:
    """Debounced call to fold_comments.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.

    Returns
    -------
    None
        Halt execution.
    """
    if not get_setting("auto_fold", False) and not is_auto_fold_scope(view):
        return

    queue.debounce(
        lambda: fold_comments(view), delay=100, key=_plugin_id.format("debounce-fold-comments")
    )


def is_auto_fold_scope(view: sublime.View) -> bool:
    """Check if the current view's scope is the desired scrope.

    Parameters
    ----------
    view : sublime.View
        Description

    Returns
    -------
    bool
        If it is the desired scrope.
    """
    auto_fold_scope: str | list[str] = settings.get("auto_fold_scope", "source.python")

    if auto_fold_scope:
        return all(
            (
                is_editable_view(view),
                view.score_selector(0, auto_fold_scope) > 0,
            )
        )

    return False


class OdyseusFoldFilePythonDocstringsListener(sublime_plugin.EventListener):
    """Fold Python doctrings listener."""

    def on_load_async(self, view: sublime.View) -> None:
        """Called when the file is finished loading.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if get_setting("fold_on_load", False):
            debounced_fold_comments(view)

    def on_activated_async(self, view: sublime.View) -> None:
        """Called when the view gains input focus.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if get_setting("fold_on_activated", False):
            debounced_fold_comments(view)

    def on_pre_save_async(self, view: sublime.View) -> None:
        """Called just before a view is saved.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if get_setting("fold_on_save", False):
            debounced_fold_comments(view)

    def on_modified_async(self, view: sublime.View) -> None:
        """Called after changes have been made to the view.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if get_setting("fold_on_modified", False):
            debounced_fold_comments(view)

    def on_selection_modified_async(self, view: sublime.View) -> None:
        """Called after the selection has been modified in the view.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if get_setting("fold_on_selection_modified", False):
            debounced_fold_comments(view)


class OdyseusFoldPythonDocstringsCommand(sublime_plugin.TextCommand):
    """Fold Python doctrings command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        fold_comments(self.view)


class OdyseusUnfoldPythonDocstringsCommand(sublime_plugin.TextCommand):
    """Unfold Python doctrings command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        self.view.unfold(self.view.find_by_selector("string, comment.block"))


class OdyseusTogglePythonDocstringsAutoFoldCommand(sublime_plugin.TextCommand):
    """Unfold Python doctrings command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        settings.set("python_utilities.auto_fold", not get_setting("auto_fold", False))
        sublime.status_message(
            f'Python docstrings auto fold {"ON" if get_setting("auto_fold", False) else "OFF"}'
        )


class OdyseusFixPythonImportsCommand(sublime_plugin.TextCommand):
    """Fix Python imports command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.

        Returns
        -------
        None
            Halt execution.
        """
        if not utils.has_right_syntax(self.view, view_syntaxes="python"):
            return

        data = self.view.substr(sublime.Region(0, self.view.size()))

        logger.debug("Reorganizing file")
        split_import_statements: bool = get_setting("split_import_statements", True)
        sort_import_statements: bool = get_setting("sort_import_statements", True)
        logger.debug("Options")
        logger.debug("split_import_statements = {!r}".format(split_import_statements))
        logger.debug("sort_import_statements = {!r}".format(sort_import_statements))

        _modified, fixed_data = fix_imports.FixImports().sort_import_groups(
            data=data,
            split_import_statements=split_import_statements,
            sort_import_statements=sort_import_statements,
        )
        _is_dirty, err = merge_utils.merge_code(self.view, edit, data, fixed_data)

        if err:
            title: str = "# %s Error: Merge failure:" % self.__class__.__name__
            display_message_in_panel(self.view, title=title, body=err)
            raise


if __name__ == "__main__":
    pass
