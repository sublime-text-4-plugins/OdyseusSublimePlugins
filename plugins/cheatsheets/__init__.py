# -*- coding: utf-8 -*-
"""Cheatsheets.

Based on `Cheatsheet <https://packagecontrol.io/packages/Cheatsheet>`__.

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from python_utils.sublime_text_utils.settings import SettingsManager
    from typing import Any

import html
import os

import sublime
import sublime_plugin

from .. import events
from .. import plugin_name
from .. import settings
from python_utils.file_utils import recursive_dirlist
from python_utils.sublime_text_utils.custom_classes import CustomListInputItem
from python_utils.sublime_text_utils.queue import Queue

__all__: list[str] = ["OdyseusCheatsheetsOverlayCommand"]

queue: Queue = Queue()

_plugin_id: str = f"{plugin_name}Cheatsheet-{{}}"
_plugin_home: str = os.path.dirname(os.path.realpath(__file__))
_default_database_data: dict[str, Any] = {
    "name": "Default",
    "path": os.path.join(_plugin_home, "database"),
    "extensions": [".cheatsheet"],
}


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"cheatsheets.{s}", default)


class Storage:
    """Storage class.

    Attributes
    ----------
    all_cheatsheet_list_items : list[CustomListInputItem]
        All cheatsheets list items.
    categorized_cheatsheet_list_items : dict[str, dict[str, Any]]
        All cheatsheets list items categorized.
    category_list_items : list[CustomListInputItem]
        All categories cheatsheets list items.
    database_paths : list[tuple[Any, Any, tuple[Any]]]
        All paths to folders containing cheatsheets.
    """

    all_cheatsheet_list_items: list[CustomListInputItem] = []
    categorized_cheatsheet_list_items: dict[str, dict[str, Any]] = {}
    category_list_items: list[CustomListInputItem] = []
    database_paths: list[tuple[Any, Any, tuple[Any]]] = []

    @classmethod
    def _obfuscate_path(cls, path: str) -> str:
        """Obfuscate a path to keep it short.

        Parameters
        ----------
        path : str
            Path to a file/folder.

        Returns
        -------
        str
            Obfuscated path.
        """
        path = path.replace(_default_database_data["path"], _default_database_data["name"])
        path = path.replace(os.path.expanduser("~"), "~")

        return path

    @classmethod
    def _get_cat_name(cls, db_name: str, cat_path: str, db_path: str) -> str:
        """Get category name.

        Parameters
        ----------
        db_name : str
            A database name as defined in an entry in ``database_paths`` setting.
        cat_path : str
            Path to a folder inside a cheatsheets database folder.
        db_path : str
            Path to a cheatsheets database.

        Returns
        -------
        str
            A category name.
        """
        return ((db_name + os.sep if db_name else "") + os.path.relpath(cat_path, db_path)).replace(
            os.sep, " » "
        )

    @classmethod
    def initialize_list_items(cls) -> None:
        """Initialize list items."""
        cls.database_paths.clear()
        cls.category_list_items.clear()
        cls.categorized_cheatsheet_list_items.clear()
        cls.all_cheatsheet_list_items.clear()

        database_paths_setting: list[dict] = get_setting("database_paths", {}).get(
            sublime.platform(), []
        )

        if isinstance(database_paths_setting, str):
            database_paths_setting = [database_paths_setting]

        cls.database_paths = (
            [
                (
                    _default_database_data["name"],
                    _default_database_data["path"],
                    _default_database_data["extensions"],
                )
            ]
            if get_setting("display_default_cheatsheets", True)
            else []
        ) + [
            (
                database_def.get("name", ""),
                os.path.expandvars(os.path.expanduser(database_def.get("path"))),
                tuple(set(database_def.get("extensions", _default_database_data["extensions"]))),
            )
            for database_def in database_paths_setting
            if database_def
        ]

        # NOTE: Filter out non-existent paths.
        cls.database_paths = [
            (name, path, extensions)
            for name, path, extensions in cls.database_paths
            if os.path.exists(path)
        ]

        for db_name, db_path, db_extensions in cls.database_paths:
            all_cats: list[tuple[str, str]] = [
                (cls._get_cat_name(db_name, cat_path, db_path), cat_path)
                for cat_path in recursive_dirlist(db_path)
            ]
            # NOTE: Add the root database directory as a category.
            all_cats.append(
                (
                    (db_name + " » " if db_name else "") + os.path.basename(db_path.rstrip(os.sep)),
                    db_path,
                )
            )

            for cat, cat_path in all_cats:
                cls.categorized_cheatsheet_list_items[cat_path] = {
                    "name": cat,
                    "items": [],
                }

                cheatsheets: list[tuple[str, str]] = [
                    (entry.name, entry.path)
                    for entry in os.scandir(cat_path)
                    if all(
                        (
                            entry.is_file(),
                            entry.name.endswith(db_extensions),
                        )
                    )
                ]

                for cheat, cheat_path in cheatsheets:
                    cls.categorized_cheatsheet_list_items[cat_path]["items"].append(
                        CustomListInputItem(
                            text=cheat,
                            value=cheat_path,
                            details=html.escape(cls._obfuscate_path(cheat_path)),
                        )
                    )

                # NOTE: Only add the categoty list item if the category has cheatsheets to show.
                # Also delete the empty list from cls.categorized_cheatsheet_list_items.
                if cls.categorized_cheatsheet_list_items[cat_path]["items"]:
                    cls.category_list_items.append(
                        CustomListInputItem(
                            text=cat,
                            value=cat_path,
                            details=html.escape(cls._obfuscate_path(cat_path)),
                        )
                    )

                    cls.categorized_cheatsheet_list_items[cat_path]["items"].sort()
                else:
                    del cls.categorized_cheatsheet_list_items[cat_path]

        cls.category_list_items.sort()

        for cheatsheet_items in cls.categorized_cheatsheet_list_items.values():
            cls.all_cheatsheet_list_items.extend(cheatsheet_items["items"])

        cls.all_cheatsheet_list_items.sort()


@events.on("settings_changed")
def on_cheatsheets_settings_changed(settings_obj: SettingsManager) -> None:
    """Called on settings changed.

    Parameters
    ----------
    settings_obj : SettingsManager
        An instance of :any:`python_utils.sublime_text_utils.settings.SettingsManager`.
    """
    if any(
        (
            settings_obj.has_changed("cheatsheets.database_paths"),
            settings_obj.has_changed("cheatsheets.display_default_cheatsheets"),
        )
    ):
        queue.debounce(
            Storage.initialize_list_items,
            delay=500,
            key=_plugin_id.format("debounce-initialize-list-items"),
        )


class OdyseusCheatsheetsOverlayCommand(sublime_plugin.WindowCommand):
    """Cheatsheets overlay command."""

    def __init__(self, *args, **kwargs) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        sublime_plugin.WindowCommand.__init__(self, *args, **kwargs)
        self._input_description_text: str = "Cheatsheets"

    def run(self, category: str = None, cheatsheet: str = None) -> None:
        """Called when the command is run.

        Parameters
        ----------
        category : str, optional
            A cheatsheet category.
        cheatsheet : str, optional
            Path to a cheatsheet file.

        Returns
        -------
        None
            Halt execution.
        """
        self._input_description_text = "Cheatsheets"

        if not Storage.category_list_items:
            Storage.initialize_list_items()
            sublime.set_timeout_async(
                lambda: self.rerun_from_command_palette({"category": category}), 500
            )
            return

        if category and not cheatsheet:
            self._input_description_text = Storage.categorized_cheatsheet_list_items[category][
                "name"
            ]
            self.rerun_from_command_palette({"category": category})
            return

        if category and cheatsheet:
            view: sublime.View = self.window.open_file(cheatsheet)

            if get_setting("open_cheatsheets_as_read_only", True):
                view.set_read_only(True)

    def rerun_from_command_palette(self, args: dict[str, object]) -> None:
        """Re-run command from palette.

        Parameters
        ----------
        args : dict[str, object]
            Arguments.
        """
        self.window.run_command(
            "show_overlay",
            {
                "overlay": "command_palette",
                "command": "odyseus_cheatsheets_overlay",
                "args": args,
            },
        )

    def input_description(self) -> str:
        """Allows a custom name to be show to the left of the cursor in the input box, instead of
        the default one generated from the command name.

        Returns
        -------
        str
            Input description.
        """
        return self._input_description_text

    def input(self, args: dict) -> sublime_plugin.ListInputHandler | None:
        """If this returns something other than None, the user will be prompted for an input before
        the command is run in the **Command Palette**.

        Parameters
        ----------
        args : dict
            Arguments.

        Returns
        -------
        sublime_plugin.ListInputHandler | None
        """
        self._input_description_text = "Cheatsheets"

        if args.get("category") is None:
            return OdyseusCheatsheetsOverlayCategoryListInputHandler()

        if args.get("cheatsheet") is None:
            return OdyseusCheatsheetsOverlayCheatsheetListInputHandler(
                self.window, args.get("category")
            )

        return None


class OdyseusCheatsheetsOverlayCategoryListInputHandler(sublime_plugin.ListInputHandler):
    """List input handler for the ``category`` parameter.

    Attributes
    ----------
    categories_count : int
        Keep categories count to be displayed in the overlay's preview section.
    """

    def __init__(self) -> None:
        """See :py:meth:`object.__init__`."""
        self.categories_count: int = 0

    def name(self) -> str:
        """The command argument name this input handler is editing.

        Returns
        -------
        str
            Parameter name.
        """
        return "category"

    def placeholder(self) -> str:
        """Placeholder text is shown in the text entry box before the user has entered anything.

        Returns
        -------
        str
            The placeholder text.
        """
        return "Filter categories"

    def list_items(self) -> list[CustomListInputItem] | list[str]:
        """This method should return the items to show in the list.

        Returns
        -------
        list[CustomListInputItem] | list[str]
            List of :any:`CustomListInputItem` items.
        """
        self.categories_count = len(Storage.category_list_items)

        if Storage.category_list_items:
            return Storage.category_list_items

        return ["Something went wrong!"]

    def preview(self, value: object) -> sublime.Html | None:
        """Called whenever the user changes the selected item.

        Parameters
        ----------
        value : object
            :any:`CustomListInputItem` item value.

        Returns
        -------
        sublime.Html | None
            Html content to be displayed in an overlay's preview section.
        """
        if value is None:
            return None

        return sublime.Html(
            f'<span style="color:var(--foreground);">{self.categories_count} categories</span>'
        )


class OdyseusCheatsheetsOverlayCheatsheetListInputHandler(sublime_plugin.ListInputHandler):
    """List input handler for the ``cheatsheet`` parameter.

    Attributes
    ----------
    category : str
        A category name.
    cheatcheets_count : int
        Keep cheatcheets count to be displayed in the overlay's preview section.
    window : sublime.Window
        A Sublime Text ``Window`` object.
    """

    def __init__(self, window: sublime.Window, category: str) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        window : sublime.Window
            A Sublime Text ``Window`` object.
        category : str
            A category name.
        """
        self.window: sublime.Window = window
        self.category: str = category
        self.cheatcheets_count: int = 0

    def name(self) -> str:
        """The command argument name this input handler is editing.

        Returns
        -------
        str
            Parameter name.
        """
        return "cheatsheet"

    def placeholder(self) -> str:
        """Placeholder text is shown in the text entry box before the user has entered anything.

        Returns
        -------
        str
            The placeholder text.
        """
        return "Filter cheatsheets"

    def list_items(self) -> list[CustomListInputItem] | list[str]:
        """This method should return the items to show in the list.

        Returns
        -------
        list[CustomListInputItem] | list[str]
            List of :any:`CustomListInputItem` items.
        """
        if self.category == "*":
            self.cheatcheets_count = len(Storage.all_cheatsheet_list_items)

            if Storage.all_cheatsheet_list_items:
                return Storage.all_cheatsheet_list_items

            return ["Something went wrong!"]

        self.cheatcheets_count = len(
            Storage.categorized_cheatsheet_list_items[self.category]["items"]
        )
        return Storage.categorized_cheatsheet_list_items[self.category]["items"] or [
            "Something went wrong!"
        ]

    def _preview_cheatsheet(self, cheatsheet: str) -> None:
        """Display a preview of the content of a cheatsheet file.

        Parameters
        ----------
        cheatsheet : str
            Path to a cheatsheet file.
        """
        view: sublime.View = self.window.open_file(cheatsheet, flags=sublime.TRANSIENT)
        view.set_read_only(True)

    def preview(self, value: Any) -> sublime.Html | None:
        """Called whenever the user changes the selected item.

        Parameters
        ----------
        value : Any
            :any:`CustomListInputItem` item value.

        Returns
        -------
        sublime.Html | None
            Html content to be displayed in an overlay's preview section.
        """
        if value is None:
            return None

        if get_setting("preview_cheatsheets", True):
            queue.debounce(
                lambda: sublime.set_timeout_async(
                    lambda: self._preview_cheatsheet(value),
                    10,
                ),
                delay=200,
                key=_plugin_id.format("debounce-preview-cheatsheet"),
            )

        return sublime.Html(
            f'<span style="color:var(--foreground);">{self.cheatcheets_count} cheatsheets</span>'
        )


if __name__ == "__main__":
    pass
