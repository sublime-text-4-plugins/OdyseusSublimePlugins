# -*- coding: utf-8 -*-
"""Based on https://github.com/NicholasBuse/sublime_DeleteBlankLines
"""
from __future__ import annotations

import sublime
import sublime_plugin

__all__: list[str] = ["OdyseusDeleteBlankLinesCommand"]


class OdyseusDeleteBlankLinesCommand(sublime_plugin.TextCommand):
    """Delete blank lines commands."""

    def run(self, edit: sublime.Edit, surplus: bool = False, whole_file: bool = False) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        surplus : bool, optional
            Delete only surplus blank lines.
        whole_file : bool, optional
            Delete blank lines from the whole file.
        """
        new_selections: list[sublime.Region] = []

        if whole_file:
            self._ody_strip(edit, sublime.Region(0, self.view.size()), surplus)
        else:
            for region in self.view.sel():
                # Strip blank lines
                new_selections.append(self._ody_strip(edit, region, surplus))

            # Clear selections since they've been modified.
            self.view.sel().clear()

            for new_sel in new_selections:
                self.view.sel().add(new_sel)

    def _ody_strip(
        self, edit: sublime.Edit, region: sublime.Region, surplus: bool
    ) -> sublime.Region:
        """Strip blank lines.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        region : sublime.Region
            The selected region.
        surplus : bool
            Delete only surplus blank lines.

        Returns
        -------
        sublime.Region
            A new region.
        """
        # Convert the input range to a string, this represents the original region.
        orig: str = self.view.substr(region)
        lines: list[str] = orig.splitlines()

        i: int = 0
        have_blank: bool = False

        while i < len(lines) - 1:
            if lines[i].rstrip() == "":
                if not surplus or have_blank:
                    del lines[i]
                else:
                    i += 1
                have_blank = True
            else:
                have_blank = False
                i += 1

        output: str = "\n".join(lines)

        self.view.replace(edit, region, output)

        return sublime.Region(region.begin(), region.begin() + len(output))


if __name__ == "__main__":
    pass
