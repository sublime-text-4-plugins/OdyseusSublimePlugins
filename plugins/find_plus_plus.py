# -*- coding: utf-8 -*-
"""Find++.

Based on `Find++ <https://packagecontrol.io/packages/Find%2B%2B>`__.

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

import html
import os
import re

from collections.abc import Callable

import sublime
import sublime_plugin

from . import events
from . import plugin_name
from . import settings
from python_utils.sublime_text_utils.custom_classes import CustomListInputItem
from python_utils.sublime_text_utils.queue import Queue


__all__: list[str] = [
    "OdyseusFppFindInPathsCommand",
    "OdyseusFppFindInCurrentFileCommand",
    "OdyseusFppFindInCurrentFolderCommand",
    "OdyseusFppFindInOpenFilesCommand",
    "OdyseusFppFindInProjectCommand",
    "OdyseusFppFindInPanelCommand",
    "OdyseusFppFindQueryOverlayCommand",
]


queue: Queue = Queue()

_plugin_id: str = f"{plugin_name}FindPlusPlus-{{}}"
_sublime_panels: tuple[str] = (
    "find",
    "replace",
    "find_in_files",
)
_valid_panel_options: tuple[str] = (
    "regex",
    "case_sensitive",
    "wrap",
    "highlight",
    "in_selection",
    "whole_word",
    "preserve_case",
    "use_gitignore",
)


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"find_plus_plus.{s}", default)


class Storage:
    """Storage class.

    Attributes
    ----------
    query_def_list_items : list[CustomListInputItem]
        Query definition list items.
    query_definitions : dict[str, object]
        Query definitions.
    search_type_list_items : list[tuple[str, str]]
        Search type list items.
    """

    query_def_list_items: list[CustomListInputItem] = []
    query_definitions: dict[str, object] = {}
    search_type_list_items: list[tuple[str, str]] = [
        ("Find…", "find"),
        ("Replace…", "replace"),
        ("Find in Files…", "find_in_files"),
        ("Find++ In Current File", "odyseus_fpp_find_in_current_file"),
        ("Find++ In Current Folder", "odyseus_fpp_find_in_current_folder"),
        ("Find++ In Open Files", "odyseus_fpp_find_in_open_files"),
        ("Find++ In Project", "odyseus_fpp_find_in_project"),
    ]

    @classmethod
    def validate_panel_options(cls, query_def: dict[str, object]) -> dict[str, object]:
        """Validate panel options.

        Parameters
        ----------
        query_def : dict[str, object]
            Query definition.

        Returns
        -------
        dict[str, object]
            Validated query definitions.
        """
        if "options" in query_def:
            query_def["options"] = {
                k: v for k, v in list(query_def["options"].items()) if k in _valid_panel_options
            }

        return query_def

    @classmethod
    def update(cls) -> None:
        """Update storage."""
        cls.query_def_list_items.clear()
        cls.query_definitions.clear()

        definition_dicts: list[dict[str, object]] = get_setting(
            "query_definitions_user", []
        ) + get_setting("query_definitions", [])

        cls.query_definitions = {
            query_def["caption"]: cls.validate_panel_options(query_def)
            for query_def in definition_dicts
        }

        for caption, query_def in cls.query_definitions.items():
            if query_def.get("visible", True):
                cls.query_def_list_items.append(
                    CustomListInputItem(
                        text=caption,
                        value=caption,
                        details=f'<code>{html.escape(query_def.get("query"))}</code>',
                    )
                )

        cls.query_def_list_items.sort()


@events.on("plugin_loaded")
def on_find_plus_plus_plugin_loaded() -> None:
    """Called on plugin loaded."""
    queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage"))


@events.on("settings_changed")
def on_find_plus_plus_settings_changed(settings_obj: SettingsManager) -> None:
    """Called on settings changed.

    Parameters
    ----------
    settings_obj : SettingsManager
        An instance of :any:`python_utils.sublime_text_utils.settings.SettingsManager`.
    """
    if any(
        (
            settings_obj.has_changed("find_plus_plus.query_definitions"),
            settings_obj.has_changed("find_plus_plus.query_definitions_user"),
        )
    ):
        queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage"))


class DirectoryPanel(sublime_plugin.WindowCommand):
    """Directory panel command-

    Attributes
    ----------
    cb : Callable[..., object]
        Callback function.
    excluded : TYPE
        Description
    full_torelative_paths : list[str]
        Description
    rel_path_start : int
        Description
    relative_paths : list[str]
        Description

    Deleted Attributes
    ------------------
    selected_dir : TYPE
        Description
    """

    relative_paths: list[str] = []
    full_torelative_paths: list[str] = {}
    rel_path_start: int = 0

    def complete(self) -> None:
        """Summary"""
        # If there ia selected directory, callback with it
        selected_dir = self.selected_dir
        if selected_dir:
            self.cb(selected_dir)

    def open_panel(self, cb: Callable[..., object]) -> None:
        """Summary

        Parameters
        ----------
        cb : Callable[..., object]
            Description
        """
        # Build out exclude pattern, paths, and save cb
        self.construct_excluded_pattern()
        self.build_relative_paths()
        self.cb: Callable[..., object] = cb

        # If there is only one directory, return early with it
        if len(self.relative_paths) == 1:
            self.selected_dir = self.relative_paths[0]
            self.selected_dir = self.full_torelative_paths[self.selected_dir]
            self.complete()

        # Otherwise, if there are multiple directories, open a panel to search on
        elif len(self.relative_paths) > 1:
            self.move_current_directory_to_top()
            self.window.show_quick_panel(self.relative_paths, self.dir_selected)

        # Otherwise, attempt to resolve the directory of the current file
        else:
            view: sublime.View = self.window.active_view()
            self.selected_dir = os.path.dirname(view.file_name())
            self.complete()

    def construct_excluded_pattern(self) -> None:
        """Summary"""
        patterns: list[str] = [
            pat.replace("|", "\\") for pat in get_setting("excluded_dir_patterns", [])
        ]
        self.excluded = re.compile("|".join(patterns))

    def build_relative_paths(self) -> None:
        """Summary"""
        folders: list[str] = self.window.folders()
        self.relative_paths = []
        self.full_torelative_paths = {}
        for path in folders:
            rootfolders = os.path.split(path)[-1]
            self.rel_path_start = len(os.path.split(path)[0]) + 1
            if not self.excluded.search(rootfolders):
                self.full_torelative_paths[rootfolders] = path
                self.relative_paths.append(rootfolders)

            for base, dirs, files in os.walk(path):
                for dir in dirs:
                    relative_path = os.path.join(base, dir)[self.rel_path_start:]
                    if not self.excluded.search(relative_path):
                        self.full_torelative_paths[relative_path] = os.path.join(base, dir)
                        self.relative_paths.append(relative_path)

    def move_current_directory_to_top(self) -> None:
        """Summary

        Returns
        -------
        None
            Description
        """
        view: sublime.View = self.window.active_view()
        if view and view.file_name():
            cur_dir = os.path.dirname(view.file_name())[self.rel_path_start:]
            if cur_dir in self.full_torelative_paths:
                i = self.relative_paths.index(cur_dir)
                self.relative_paths.insert(0, self.relative_paths.pop(i))
            else:
                self.relative_paths.insert(0, os.path.dirname(view.file_name()))
        return

    def dir_selected(self, selected_index: int) -> None:
        """Summary

        Parameters
        ----------
        selected_index : int
            Description
        """
        if selected_index != -1:
            self.selected_dir = self.relative_paths[selected_index]
            self.selected_dir = self.full_torelative_paths[self.selected_dir]
            self.complete()


class OdyseusFppFindInPathsCommand(DirectoryPanel):
    """Summary"""

    def open_path(self, path: str = None) -> None:
        """Summary

        Parameters
        ----------
        path : str, optional
            Description
        """
        if path:
            self.open_paths([path])

    def open_paths(self, paths: list[str] = [], query_id: str | None = None) -> None:
        """Summary

        Parameters
        ----------
        paths : list[str], optional
            Description
        query_id : str | None, optional
            Description
        """
        # Hide the search panel
        panel_extra_options: dict[str, object] = {}
        window: sublime.Window = self.window
        window.run_command("hide_panel")

        if query_id:
            panel_extra_options = Storage.query_definitions[query_id].get("options", {})

        # Set up options for the current version
        options: dict[str, object] = {
            "panel": "find_in_files",
            "where": ",".join(paths),
            **panel_extra_options,
        }

        # Open the search path
        window.run_command("show_panel", options)

        if query_id:
            sublime.set_timeout_async(
                lambda: window.run_command(
                    "insert",
                    {"characters": Storage.query_definitions[query_id]["query"]},
                ),
                100,
            )


class OdyseusFppFindInCurrentFileCommand(OdyseusFppFindInPathsCommand):
    """Summary"""

    def run(self, query_id: str | None = None) -> None:
        """Called when the command is run.

        Parameters
        ----------
        query_id : str | None, optional
            Description
        """
        # Grab the file name
        window: sublime.Window = self.window
        file_name: str | None = window.active_view().file_name()

        # If there is one, run FppFindInPathsCommand on it
        if file_name:
            self.open_paths(**{"paths": [file_name]}, query_id=query_id)


class OdyseusFppFindInCurrentFolderCommand(OdyseusFppFindInPathsCommand):
    """Summary"""

    def run(self, query_id: str | None = None) -> None:
        """Called when the command is run.

        Parameters
        ----------
        query_id : str | None, optional
            Description
        """
        window: sublime.Window = self.window
        file_name: str | None = window.active_view().file_name()
        if file_name:
            self.open_paths(**{"paths": [os.path.dirname(file_name)]}, query_id=query_id)


class OdyseusFppFindInOpenFilesCommand(OdyseusFppFindInPathsCommand):
    """Summary"""

    def run(self, query_id: str | None = None) -> None:
        """Called when the command is run.

        Parameters
        ----------
        query_id : str | None, optional
            Description
        """
        self.open_paths(**{"paths": ["<open files>"]}, query_id=query_id)


class OdyseusFppFindInProjectCommand(OdyseusFppFindInPathsCommand):
    """Summary"""

    def run(self, query_id: str | None = None) -> None:
        """Called when the command is run.

        Parameters
        ----------
        query_id : str | None, optional
            Description
        """
        self.open_paths(**{"paths": ["<open files>", "<open folders>"]}, query_id=query_id)


class OdyseusFppFindInPanelCommand(OdyseusFppFindInPathsCommand):
    """Summary

    Attributes
    ----------
    INPUT_PANEL_CAPTION : str
        Description
    """

    INPUT_PANEL_CAPTION: str = "Find in:"

    def run(self) -> None:
        """Called when the command is run."""
        # Open a search panel which will open the respective path
        self.open_panel(lambda path: self.open_path(path))


class OdyseusFppFindQueryOverlayCommand(sublime_plugin.WindowCommand):
    """Summary"""

    def run(self, query_id: str | None = None, search_type: str | None = None) -> None:
        """Called when the command is run.

        Parameters
        ----------
        query_id : str | None, optional
            Description
        search_type : str | None, optional
            Description
        """
        if query_id and search_type:
            self.window.run_command("hide_panel")

            if search_type in _sublime_panels:
                self.window.run_command(
                    "show_panel",
                    {
                        "panel": search_type,
                        **Storage.query_definitions[query_id].get("options", {}),
                    },
                )
                self.window.run_command("insert", {"characters": ""})
                sublime.set_timeout_async(
                    lambda: self.window.run_command(
                        "insert",
                        {"characters": Storage.query_definitions[query_id]["query"]},
                    ),
                    100,
                )
            else:
                self.window.run_command(
                    search_type,
                    {"query_id": query_id},
                )
        elif query_id and not search_type:
            sublime.set_timeout_async(
                lambda: self.rerun_from_command_palette(query_id, search_type), 100
            )

    def rerun_from_command_palette(self, query_id: str | None, search_type: str | None) -> None:
        """Summary

        Parameters
        ----------
        query_id : str | None
            Description
        search_type : str | None
            Description
        """
        self.window.run_command(
            "show_overlay",
            {
                "overlay": "command_palette",
                "command": "odyseus_fpp_find_query_overlay",
                "args": {"query_id": query_id, "search_type": search_type},
            },
        )

    def input_description(self) -> str:
        """Allows a custom name to be show to the left of the cursor in the input box, instead of
        the default one generated from the command name.

        Returns
        -------
        str
            Input description.
        """
        return "Find++ query selector"

    def input(self, args: dict[str, object]) -> sublime_plugin.ListInputHandler | None:
        """If this returns something other than None, the user will be prompted for an input before
        the command is run in the **Command Palette**.

        Parameters
        ----------
        args : dict[str, object]
            Arguments.

        Returns
        -------
        sublime_plugin.ListInputHandler | None
        sublime_plugin.ListInputHandler
            The input handler for the command.
        """
        if args.get("query_id") is None:
            return FindQueryOverlayListInputHandler("query_id")

        if args.get("search_type") is None:
            return FindQueryOverlayListInputHandler("search_type", query_id=args.get("query_id"))

        return None


class FindQueryOverlayListInputHandler(sublime_plugin.ListInputHandler):
    """Find query overlay list input handler.

    Attributes
    ----------
    param_name : str
        Parameter name.
    query_id : str | None
        Query ID.
    """

    def __init__(self, param_name: str, query_id: str | None = None) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        param_name : str
            Parameter name.
        query_id : str | None, optional
            Query ID.
        """
        self.param_name: str = param_name
        self.query_id: str | None = query_id

    def name(self) -> str:
        """The command argument name this input handler is editing.

        Returns
        -------
        str
            Parameter name.
        """
        return self.param_name

    def placeholder(self) -> str:
        """Placeholder text is shown in the text entry box before the user has entered anything.

        Returns
        -------
        str
            The placeholder text.
        """
        if self.param_name == "query_id":
            return "Filter query"
        elif self.param_name == "search_type":
            return "Filter search type"

    def list_items(self) -> list[CustomListInputItem | str]:
        """This method should return the items to show in the list.

        Returns
        -------
        list[CustomListInputItem | str]
            List of :any:`CustomListInputItem` items.
        """
        if self.param_name == "query_id":
            return Storage.query_def_list_items or ["Something went wrong!"]
        elif self.param_name == "search_type":
            return Storage.search_type_list_items or ["Something went wrong!"]

    def preview(self, value: object) -> sublime.Html | None:
        """Called whenever the user changes the selected item.

        Parameters
        ----------
        value : object
            :any:`CustomListInputItem` item value.

        Returns
        -------
        sublime.Html | None
            HTML content to be displayed in an overlay's preview section.
        """
        if value is None or self.query_id is None:
            return None

        return sublime.Html(
            '<span style="color:var(--foreground);">Query: <code>{}</code></span>'.format(
                html.escape(Storage.query_definitions[self.query_id]["query"])
            )
        )
