# -*- coding: utf-8 -*-
"""Summary

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from python_utils.sublime_text_utils.settings import SettingsManager
    from typing import Any

import functools

import sublime
import sublime_plugin

from . import display_message_in_panel
from . import events
from . import plugin_name
from . import settings
from . import sphinx_building_docs
from python_utils.lazy_import import lazy_module
from python_utils.sublime_text_utils.queue import Queue

if sphinx_building_docs:
    from python_utils import cmd_utils
    from python_utils.sublime_text_utils import utils
else:
    cmd_utils = lazy_module("python_utils.cmd_utils")
    utils = lazy_module("python_utils.sublime_text_utils.utils")


__all__: list[str] = [
    "OdyseusZealSearchSelectionCommand",
    "OdyseusZealSearchCommand",
]

queue: Queue = Queue()

_plugin_id: str = f"{plugin_name}SearchWithZeal-{{}}"
_status_message_temp = "Zeal: {}"


class Storage:
    """Storage class.

    Attributes
    ----------
    docsets : set[Docset]
        Description
    """

    docsets: set[Docset] = set()

    @classmethod
    def update(cls) -> None:
        """Update storage."""
        docset_dicts: list[dict] = get_setting("docsets_user", []) + get_setting("docsets", [])
        cls.docsets = set(Docset(**d) for d in docset_dicts)


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"search_with_zeal.{s}", default)


@events.on("plugin_loaded")
def on_search_with_zeal_plugin_loaded() -> None:
    """Called on plugin loaded."""
    queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage"))


@events.on("settings_changed")
def on_search_with_zeal_settings_changed(settings_obj: SettingsManager) -> None:
    """Called on settings changed.

    Parameters
    ----------
    settings_obj : SettingsManager
        An instance of :any:`python_utils.sublime_text_utils.settings.SettingsManager`.
    """
    if any(
        (
            settings_obj.has_changed("search_with_zeal.docsets"),
            settings_obj.has_changed("search_with_zeal.docsets_user"),
        )
    ):
        queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage"))


@functools.total_ordering
class Docset:
    """A docset configuration item, computing defaults based on the given name.

    Comparison and hashing is reduced to the name attribute only.
    This is important when building sets, as later additions are discarded.

    Attributes
    ----------
    name : str
        Description
    namespace : str
        Description
    selector : str
        Description
    """

    def __init__(self, name: str, namespace: str = None, selector: str = None) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        name : str
            Description
        namespace : str, optional
            Description
        selector : str, optional
            Description
        """
        self.name: str = name
        self.namespace: str = namespace or name.lower().replace(" ", "-")
        self.selector: str = selector or "source.{}".format(self.namespace)

    def score(self, scope: str) -> bool:
        """Summary

        Parameters
        ----------
        scope : str
            Description

        Returns
        -------
        bool
            Description
        """
        return sublime.score_selector(scope, self.selector)

    def __repr__(self) -> str:
        """See :py:meth:`object.__repr__`.

        Returns
        -------
        str
            Description
        """
        return (
            "{self.__class__.__name__}"
            "(name={self.name!r}"
            ", namespace={self.namespace!r}"
            ", selector={self.selector!r}"
            ")".format(self=self)
        )

    def __gt__(self, other: object) -> bool:
        """See :py:meth:`object.__gt__`.

        Parameters
        ----------
        other : object
            Description

        Returns
        -------
        bool
            Description
        """
        return getattr(self, "name") > getattr(other, "name")

    def __eq__(self, other: object) -> bool:
        """See :py:meth:`object.__eq__`.

        Parameters
        ----------
        other : object
            Description

        Returns
        -------
        bool
            Description
        """
        return getattr(self, "name") == getattr(other, "name")

    def __hash__(self) -> int:
        """See :py:meth:`object.__hash__`.

        Returns
        -------
        int
            Description
        """
        return hash(self.name)


def get_word(view: sublime.View) -> tuple[str | None, str | None]:
    """Summary

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.

    Returns
    -------
    tuple[str | None, str | None]
    """
    for region in utils.get_selections(view, return_whole_file=False, extract_words=True):
        text: str = view.substr(region).strip()

        if text:
            scope: str = view.scope_name(region.begin())
            return text, scope

    return None, None


def query_string(namespace: str, text: str) -> str:
    """Summary

    Parameters
    ----------
    namespace : str
        Description
    text : str
        Description

    Returns
    -------
    str
        Description
    """
    return "{}:{}".format(namespace, text) if namespace else text


def status_message(msg: str) -> None:
    """Summary

    Parameters
    ----------
    msg : str
        Description
    """
    sublime.status_message(_status_message_temp.format(msg))


def open_zeal(view: sublime.View, query: str) -> None:
    """Summary

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    query : str
        Description

    Returns
    -------
    None
        Description
    """
    zeal_exec: str | None = utils.get_executable_from_settings(
        view, get_setting("exec_map", {}).get(sublime.platform(), [])
    )

    if not zeal_exec:
        display_message_in_panel(
            title="Could not find Zeal's executable"
            "\n\nEdit the `search_with_zeal.exec_map` setting."
        )
        return

    try:
        cmd_utils.popen([zeal_exec, query])
    except Exception as err:
        display_message_in_panel(title="Error trying to execute Zeal", body=err)


def matching_docsets(scope: str) -> list[Docset]:
    """Summary

    Parameters
    ----------
    scope : str
        Description

    Returns
    -------
    list[Docset]
        Description
    """
    matched: list[tuple[bool, Docset]] = [
        (docset.score(scope), docset) for docset in Storage.docsets
    ]
    return [docset for score, docset in sorted(matched) if score]


class OdyseusZealSearchSelectionCommand(sublime_plugin.TextCommand):
    """Summary"""

    def run(
        self,
        edit: sublime.Edit,
        namespace: str | OdyseusNamespaceInputHandler | None = None,
        **kwargs,
    ) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        namespace : str | OdyseusNamespaceInputHandler | None, optional
            Description
        **kwargs
            Description

        Returns
        -------
        None
            Description
        """
        text, scope = get_word(self.view)

        if not text:
            status_message("No word was selected.")
            return

        if namespace is None:
            namespace = self.resolve_namespace(text, scope)

            if not namespace:
                status_message("No namespace found.")
                return
            elif isinstance(namespace, OdyseusNamespaceInputHandler):
                # Fallback if command was called directly through e.g. a key binding.
                sublime.set_timeout(self.rerun_from_command_palette, 100)
                return

        if isinstance(namespace, str):
            open_zeal(self.view, query_string(namespace, text))

    def input_description(self) -> str:
        """Summary

        Returns
        -------
        str
            Description
        """
        return "Zeal search"

    def input(self, args: dict[str, object]) -> OdyseusNamespaceInputHandler | None:
        """Summary

        Parameters
        ----------
        args : dict[str, object]
            Description

        Returns
        -------
        OdyseusNamespaceInputHandler | None
        """
        if "namespace" not in args:
            text, scope = get_word(self.view)
            if not text:
                status_message("No text.")
                return None

            namespace = self.resolve_namespace(text, scope)
            if isinstance(namespace, OdyseusNamespaceInputHandler):
                return namespace
            # If a namespace could be resolved,
            # we need to run the same procedure in `run` again
            # because we can't directly pass values from here.
            # We could cache,
            # but we would need to consider the underlying settings changing.

        return None

    def rerun_from_command_palette(self) -> None:
        """Summary"""
        self.view.window().run_command(
            "show_overlay",
            {"overlay": "command_palette", "command": "odyseus_zeal_search_selection"},
        )

    def resolve_namespace(self, text: str, scope: str) -> OdyseusNamespaceInputHandler | str | None:
        """Summary

        Parameters
        ----------
        text : str
            Description
        scope : str
            Description

        Returns
        -------
        OdyseusNamespaceInputHandler | str | None
        """
        matched_docsets: list[Docset] = matching_docsets(scope)

        if len(matched_docsets) == 1:
            return matched_docsets[0].namespace
        elif matched_docsets:
            multi_match: str = get_setting("multi_match", "select")

            if multi_match == "select":
                return OdyseusNamespaceInputHandler(matched_docsets, text)
            elif multi_match == "join":
                return ",".join(ds.namespace for ds in matched_docsets)
        else:
            fallback: str = get_setting("fallback", "none")

            if fallback == "stop":
                status_message("No mapping found.")
                return None
            elif fallback == "none":
                return ""
            elif fallback == "guess":
                # Find innermost base scope
                # NOTE: I have no f*cking idea how to type hint this garbage.
                base_scopes: list[str] = list(
                    reversed(  # type: ignore[call-overload]
                        s for s in scope.split() if s.startswith("source.") or s.startswith("text.")
                    )
                )
                if not base_scopes:
                    status_message("No mapping found and could not guess from scope.")
                    return None
                base_scope: str = base_scopes[0]
                namespace: str = base_scope.split(".")[1]
                status_message(
                    "No docset matched {!r}, guessed {!r}.".format(base_scope, namespace)
                )
                return namespace
            else:
                status_message("Unrecognized 'fallback' setting.")
                return None

        return None


class OdyseusZealSearchCommand(sublime_plugin.TextCommand):
    """Summary"""

    def run(self, edit: sublime.Edit, text: str | None = None) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        text : str | None, optional
            Description
        """
        if text is not None:
            open_zeal(self.view, text)

    def input_description(self) -> str:
        """Summary

        Returns
        -------
        str
            Description
        """
        return "Zeal search"

    def input(self, args: dict[str, object]) -> sublime_plugin.TextInputHandler | None:
        """Summary

        Parameters
        ----------
        args : dict[str, object]
            Description

        Returns
        -------
        sublime_plugin.TextInputHandler | None
        """
        if "text" not in args:
            return OdyseusSimpleTextInputHandler("text", placeholder="query string")

        return None


class OdyseusSimpleTextInputHandler(sublime_plugin.TextInputHandler):
    """Summary

    Attributes
    ----------
    param_name : str
        Description
    """

    def __init__(self, param_name: str, *, placeholder: str = "") -> None:
        """Summary

        Parameters
        ----------
        param_name : str
            Description
        placeholder : str, optional
            Description
        """
        self.param_name: str = param_name
        self._placeholder: str = placeholder

    def name(self) -> str:
        """Summary

        Returns
        -------
        str
            Description
        """
        return self.param_name

    def placeholder(self) -> str:
        """Summary

        Returns
        -------
        str
            Description
        """
        return self._placeholder


class OdyseusNamespaceInputHandler(sublime_plugin.ListInputHandler):
    """Summary

    Attributes
    ----------
    docsets : list[Docset]
        Description
    text : str
        Description
    """

    def __init__(self, docsets: list[Docset], text: str) -> None:
        """Summary

        Parameters
        ----------
        docsets : list[Docset]
            Description
        text : str
            Description
        """
        self.docsets: list[Docset] = docsets
        self.text: str = text

    def name(self) -> str:
        """Summary

        Returns
        -------
        str
            Description
        """
        # NOTE: Since I changed the name of the ListInputHandler to OdyseusNamespaceInputHandler,
        # its auto-generated name changed to odyseus_namespace. To avoid changing all parameters.
        # I simply set the ListInputHandler name here.
        return "namespace"

    def placeholder(self) -> str:
        """Summary

        Returns
        -------
        str
            Description
        """
        return "Select docset"

    def list_items(self) -> list[tuple[str, str]]:
        """Summary

        Returns
        -------
        list[tuple[str, str]]
            Description
        """
        return sorted((lang.name, lang.namespace) for lang in self.docsets)

    def preview(self, value: object) -> sublime.Html:
        """Summary

        Parameters
        ----------
        value : object
            Description

        Returns
        -------
        sublime.Html
            Description
        """
        # NOTE: I'm using the query wrapped in a span with style because for some reason,
        # the preview text is rendered white on a light grey background.
        # So, I force to use the variable foreground that should be defined in all existent
        # color schemes.
        return sublime.Html(
            '<span style="color:var(--foreground);">Query: <code>{}:{}</code></span>'.format(
                value, self.text
            )
        )


if __name__ == "__main__":
    pass
