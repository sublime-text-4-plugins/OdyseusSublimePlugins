# -*- coding: utf-8 -*-
"""Convert JSON/YAML files or strings into YAML/JSON.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

import json
import os
import traceback

import sublime
import sublime_plugin

from . import display_message_in_panel
from . import settings
from . import sphinx_building_docs
from python_utils.lazy_import import lazy_module

if sphinx_building_docs:
    from python_utils import demjson3
    from python_utils import yaml_utils
    from python_utils.sublime_text_utils import sublime_lib
    from python_utils.sublime_text_utils import utils
else:
    demjson3: object = lazy_module("python_utils.demjson3")
    sublime_lib: object = lazy_module("python_utils.sublime_text_utils.sublime_lib")
    utils: object = lazy_module("python_utils.sublime_text_utils.utils")
    yaml_utils: object = lazy_module("python_utils.yaml_utils")


__all__: list[str] = ["OdyseusConvertJsonToFromYamlCommand"]

_error_template: str = """Working directory: `{cwd}`
Command `{cmd}`
```
{stderr}
```"""


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"json_to_from_yaml.{s}", default)


class OdyseusConvertJsonToFromYamlCommand(sublime_plugin.WindowCommand):
    """Convert to and from JSON/YAML command."""

    def run(self, target_format: str = "yaml", dumper_kwargs: dict[str, object] = {}) -> None:
        """Called when the command is run.

        Parameters
        ----------
        target_format : str, optional
            One of "yaml" or "json".
        dumper_kwargs : dict[str, object], optional
            Arguments to pass to the data dumpers. For YAML files, the ``yaml.dump`` method is used.
            For JSON files, the Python standard library method :any:`json.dumps` is used.

        Returns
        -------
        None
            Halt execution.
        """
        view: sublime.View = self.window.active_view()

        if not view:
            return

        dumper_kwargs: dict[str, object] = dumper_kwargs or get_setting(
            f"{target_format}_dumper_kwargs", {}
        )
        error_title: str = "%s Error:" % self.__class__.__name__
        warn_title: str = "%s Warning:" % self.__class__.__name__
        parse_warning_msg: str = (
            "Attempted to parse invalid JSON!\nAn alternate parsing method was used."
        )
        converted_string: str = ""
        file_name: str = utils.get_filename(view)

        for selection in utils.get_selections(view, return_whole_file=True, extract_words=False):
            try:
                if target_format == "yaml":
                    try:
                        converted_string += yaml_utils.dump(
                            json.loads(view.substr(selection)), **dumper_kwargs
                        )
                    except Exception:
                        display_message_in_panel(
                            view,
                            title=warn_title,
                            body=parse_warning_msg,
                            log_level="warning",
                        )
                        converted_string += yaml_utils.dump(
                            demjson3.decode(view.substr(selection)), **dumper_kwargs
                        )
                        traceback.print_exc()
                elif target_format == "json":
                    converted_string += json.dumps(
                        yaml_utils.load(view.substr(selection)), **dumper_kwargs
                    )
            except Exception as err:
                display_message_in_panel(view, title=error_title, body=str(err))
                return

        if converted_string:
            sublime_lib.new_view(
                self.window,
                content=converted_string,
                name=os.path.splitext(file_name)[0],
                scope=f"source.{target_format}",
            )


if __name__ == "__main__":
    pass
