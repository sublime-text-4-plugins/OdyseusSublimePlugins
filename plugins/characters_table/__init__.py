# -*- coding: utf-8 -*-
"""Unicode characters table.

Based on `Character Table <https://packagecontrol.io/packages/Character Table>`__.

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from python_utils.sublime_text_utils.settings import SettingsManager
    from typing import Any

import html
import json
import os
import threading

import sublime
import sublime_plugin

from .. import events
from .. import get_user_storage_root
from .. import logger
from .. import plugin_name
from .. import root_folder
from .. import settings
from .. import sphinx_building_docs
from python_utils.lazy_import import lazy_module
from python_utils.sublime_text_utils.custom_classes import CustomListInputItem
from python_utils.sublime_text_utils.queue import Queue
from python_utils.sublime_text_utils.sublime_lib import ActivityIndicator

if sphinx_building_docs:
    from python_utils.sublime_text_utils import utils
else:
    utils: object = lazy_module("python_utils.sublime_text_utils.utils")


__all__: list[str] = [
    "OdyseusCtCharactersTableMenuCommand",
    "OdyseusCtToggleMnemonicsKeymapCommand",
    "OdyseusCtToggleDigraphKeymapCommand",
    "OdyseusCtDigraphStatusListener",
]

ActivityIndicator.width = 5
queue: Queue = Queue()

_plugin_id: str = f"{plugin_name}CharacterTable-{{}}"
_digraph_status = _plugin_id.format("digraph")
_data_folder: str = os.path.join(root_folder, "plugins", "characters_table", "data")
_panel_name: str = plugin_name + "CharacterTable"
_output_panel: str = "output." + _panel_name
_preview_template: str = """<span style="color:var(--foreground);font-size:{preview_font_size};">
Character: <code style="color:var(--background);background-color:var(--foreground);font-weight:bold;">\
&nbsp;{value}&nbsp;</code>{mnemonic_markup}</span>"""
_preview_template_mnemonic: str = """&nbsp;&nbsp;&nbsp;Mnemonic: \
<code style="color:var(--background);background-color:var(--foreground);font-weight:bold;">\
&nbsp;{mnemonic}&nbsp;</code>"""
_help_data: dict[str, dict[str, object]] = {
    "to toggle": {
        "command": "odyseus_ct_toggle_digraph_keymap",
    },
}


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"characters_table.{s}", default)


class Storage:
    """Storage class.

    Attributes
    ----------
    character_to_mnemonic_map : dict[str, str]
        Unicode characters mnemonics map.
    code_to_character_map : dict[str, str]
        Unicode codes to characters map.
    data_loaded : bool
        Flag set after the characters data has been loaded.
    digraph_file : str
        Diagraph file path.
    digraph_file_exists : bool
        If the diagraph file exists.
    digraph_folder : str
        Diagraph folder path.
    help_data : str
        Help data generated from defined keybindings.
    mnemonic_file : str
        Mnemonic file path.
    mnemonic_folder : str
        Mnemonic folder path.
    unicode_characters_items : list[CustomListInputItem]
        List of :any:`CustomListInputItem` items to use in the command palette to represent Unicode
        characters.
    user_storage_path : str
        Path to user data for this plugin.
    windows_alt_codes_items : list[CustomListInputItem]
        List of :any:`CustomListInputItem` items to use in the command palette to represent Windows alt
        characters.
    windows_alt_codes_map : dict[str, dict[str, str]]
        Windows alt codes to Unicode codes/names map.
    """

    character_to_mnemonic_map: dict[str, str] = {}
    code_to_character_map: dict[str, str] = {}
    data_loaded: bool = False
    digraph_file: str = ""
    digraph_file_exists: bool = False
    digraph_folder: str = ""
    help_data: str = ""
    mnemonic_file: str = ""
    mnemonic_folder: str = ""
    unicode_characters_items: list[CustomListInputItem] = []
    user_storage_path: str = ""
    windows_alt_codes_items: list[CustomListInputItem] = []
    windows_alt_codes_map: dict[str, dict[str, str]] = {}

    @classmethod
    def update(cls) -> None:
        """Update storage."""
        cls.user_storage_path = os.path.join(get_user_storage_root(), "CharacterTable")
        cls.mnemonic_folder = os.path.join(cls.user_storage_path, "Default")
        cls.mnemonic_file = os.path.join(cls.mnemonic_folder, "Default.sublime-keymap")
        cls.digraph_folder = os.path.join(cls.user_storage_path, "Digraph")
        cls.digraph_file = os.path.join(cls.digraph_folder, "Default.sublime-keymap")

        cls.digraph_file_exists = os.path.exists(
            os.path.join(cls.user_storage_path, "Digraph", "Default.sublime-keymap")
        )
        try:
            cls.help_data = utils.generate_keybindings_help_data(
                _help_data, "{key} {description}", spacer=" ", logger=logger
            )
        except Exception:
            cls.help_data = ""
            pass


@events.on("plugin_loaded")
def on_characters_table_plugin_loaded() -> None:
    """Called on plugin loaded."""
    queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage"))


@events.on("settings_changed")
def on_characters_table_settings_changed(settings_obj: SettingsManager) -> None:
    """Called on settings changed.

    Parameters
    ----------
    settings_obj : SettingsManager
        The settings object.
    """
    if any(
        (
            settings_obj.has_changed("characters_table.two_char_mnemonics_reversed"),
            settings_obj.has_changed("characters_table.mnemonics_keys_trigger"),
        )
    ):
        queue.debounce(
            maybe_create_mnemonic_keymap,
            delay=500,
            key=_plugin_id.format("debounce-mnemonic-keymap-creation"),
        )


def load_characters_data() -> None:
    """Load characters data.

    Note
    ----
    I'm using a thread because I can't make ActivityIndicator work anywere but inside threads. ¬¬
    """
    t: threading.Thread = LoadCharacterTableThread()
    t.start()


def maybe_create_mnemonic_keymap() -> None:
    """If the mnemonic file exists, re-create it."""
    if os.path.exists(Storage.mnemonic_file):
        create_mnemonic_keymap()


def create_mnemonic_keymap() -> None:
    """Create mnemonic keymap.

    Returns
    -------
    None
        Halt execution.

    Note
    ----
    I'm using a thread because I can't make ActivityIndicator work anywere but inside threads. ¬¬
    """
    if not Storage.data_loaded:
        load_characters_data()
        sublime.set_timeout_async(create_mnemonic_keymap, 1000)
        return

    t: threading.Thread = CreateMnemonicKeymapThread()
    t.start()


def set_digraph_status(view: sublime.View) -> None:
    """Set diagraph status.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    """
    if Storage.digraph_file_exists:
        view.set_status(_digraph_status, f"Digraph ({Storage.help_data.strip()})")
    else:
        view.set_status(_digraph_status, "")
        view.erase_status(_digraph_status)


def make_key_definition(
    keys: list[str], mnemonic: str, unichr: str
) -> dict[str, list[str] | str | dict[str, Any]]:
    """Make key definition.

    Parameters
    ----------
    keys : list[str]
        Base keys to trigger mnemonics.
    mnemonic : str
        Mnemonic.
    unichr : str
        Unicode character code.

    Returns
    -------
    dict[str, list[str] | str | dict[str, Any]]
        A key definition as used in sublime-keymap files.
    """
    return {
        "keys": keys + [x for x in mnemonic],
        "command": "insert",
        "args": {"characters": unichr},
    }


class CreateMnemonicKeymapThread(threading.Thread):
    """Create mnemonic keymap command."""

    def run(self) -> None:
        """Called when the command is run."""
        with ActivityIndicator(sublime.active_window(), "Creating mnemonics keymap"):
            keys: list[str] = get_setting("mnemonics_keys_trigger", ["ctrl+k", "ctrl+k"])

            keymap: list[dict[str, list[str] | str | dict[str, Any]]] = []

            mnemonics: set[str] = set(Storage.character_to_mnemonic_map.values())

            for unichr, mnemonic in Storage.character_to_mnemonic_map.items():
                if get_setting("two_char_mnemonics_reversed", False):
                    if len(mnemonic) == 2:
                        rev: str = "".join([x for x in reversed(mnemonic)])
                        if rev[0] != " " and rev not in mnemonics:
                            keymap.append(make_key_definition(keys, rev, unichr))

                keymap.append(make_key_definition(keys, mnemonic, unichr))

            with open(Storage.mnemonic_file + ".tmp", "w") as f:
                json.dump(keymap, f, indent=4, ensure_ascii=True)

            os.rename(Storage.mnemonic_file + ".tmp", Storage.mnemonic_file)


class LoadCharacterTableThread(threading.Thread):
    """Load characters data thread."""

    def run(self) -> None:
        """Called when the command is run."""
        with ActivityIndicator(sublime.active_window(), "Loading characters table"):
            Storage.code_to_character_map.clear()
            Storage.character_to_mnemonic_map.clear()
            Storage.unicode_characters_items.clear()
            Storage.windows_alt_codes_map.clear()
            Storage.windows_alt_codes_items.clear()

            with open(os.path.join(_data_folder, "code_to_character_map.json"), "r") as f:
                Storage.code_to_character_map = json.load(f)

            with open(os.path.join(_data_folder, "character_to_mnemonic_map.json"), "r") as f:
                Storage.character_to_mnemonic_map = json.load(f)

            with open(os.path.join(_data_folder, "windows_alt_codes_map.json"), "r") as f:
                Storage.windows_alt_codes_map = json.load(f)

            with open(os.path.join(_data_folder, "full_unicode_map.json"), "r") as f:
                full_unicode_map: dict[str, dict[str, str]] = json.load(f)

            for code, char_data in full_unicode_map.items():
                try:
                    char: str = eval('u"\\U%s"' % code)
                    mnemonic: str = f' ({char_data["m"]})' if char_data["m"] else ""
                    Storage.unicode_characters_items.append(
                        CustomListInputItem(
                            text=f'U+{code} ({char_data["n"]}){mnemonic}',
                            value=char,
                            annotation=char,
                            prop_for_sort_val=code,
                            sort_prop="prop_for_sort",
                        )
                    )
                except Exception as err:
                    logger.error(
                        f"Error processing character code: {code}\n Character data: {repr(char_data)}\n Error: {err}"
                    )
                    continue

            for code, char_data in Storage.windows_alt_codes_map.items():
                try:
                    char: str = eval('u"\\U%s"' % char_data["u"])
                    Storage.windows_alt_codes_items.append(
                        CustomListInputItem(
                            text=f'{code} ({char_data["n"]})',
                            value=char,
                            prop_for_sort_val=code.rjust(4, "0"),
                            sort_prop="prop_for_sort",
                        )
                    )
                except Exception as err:
                    logger.error(
                        f"Error processing character code: {code}\n Character data: {repr(char_data)}\n Error: {err}"
                    )
                    continue

            if Storage.unicode_characters_items:
                Storage.unicode_characters_items.sort()

            if Storage.windows_alt_codes_items:
                Storage.windows_alt_codes_items.sort()

            full_unicode_map.clear()
            Storage.data_loaded = True


def insert_char(view: sublime.View, string: str) -> None:
    """Summary

    Parameters
    ----------
    view : sublime.View
        Description
    string : str
        Description
    """
    view.run_command("insert", {"characters": string})


class OdyseusCtCharactersTableMenuCommand(sublime_plugin.WindowCommand):
    """Characters table menu command."""

    def __init__(self, *args, **kwargs) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        sublime_plugin.WindowCommand.__init__(self, *args, **kwargs)
        self._input_description_text: str = "CharactersTable"

    def run(self, character: str = None, mode: str = "unicode_lookup") -> None:
        """Called when the command is run.

        Parameters
        ----------
        character : str, optional
            Selected/Inputed character or character code.
        mode : str, optional
            Command execution mode.

        Returns
        -------
        None
            Halt execution.
        """
        if not Storage.data_loaded:
            load_characters_data()
            sublime.set_timeout_async(lambda: self.rerun_from_command_palette(mode), 1000)
            return

        view: sublime.View = self.window.active_view()

        if mode and character and view:
            if mode == "unicode_lookup" or mode == "win_alt_code_lookup":
                insert_char(view, character)
            elif mode == "unicode_input":
                padded_code: str = str(character).rjust(8, "0")

                if view and padded_code in Storage.code_to_character_map:
                    insert_char(view, Storage.code_to_character_map[padded_code])
            elif mode == "win_alt_code_input":
                if view and character in Storage.windows_alt_codes_map:
                    character: str = eval(
                        'u"\\U%s"' % Storage.windows_alt_codes_map[character]["u"]
                    )
                    insert_char(view, character)
        elif mode:
            sublime.set_timeout_async(lambda: self.rerun_from_command_palette(mode), 100)

    def rerun_from_command_palette(self, mode: str) -> None:
        """Rerun from command palette.

        Parameters
        ----------
        mode : str
            Command execution mode.
        """
        if mode == "unicode_lookup":
            self._input_description_text = "Unicode characters lookup"
        elif mode == "unicode_input":
            self._input_description_text = "Unicode characters input"
        elif mode == "win_alt_code_lookup":
            self._input_description_text = "Windows alt codes lookup"
        elif mode == "win_alt_code_input":
            self._input_description_text = "Windows alt codes input"

        self.window.run_command(
            "show_overlay",
            {
                "overlay": "command_palette",
                "command": "odyseus_ct_characters_table_menu",
                "args": {"mode": mode},
            },
        )

    def input_description(self) -> str:
        """Custom name to be show to the left of the cursor in the input box.

        Returns
        -------
        str
            Custom input description.
        """
        return self._input_description_text

    def input(
        self, args: dict[str, Any]
    ) -> sublime_plugin.TextInputHandler | sublime_plugin.ListInputHandler | None:
        """Command input handler.

        Parameters
        ----------
        args : dict[str, Any]
            Command arguments.

        Returns
        -------
        sublime_plugin.TextInputHandler | sublime_plugin.ListInputHandler | None
        sublime.CommandInputHandler
            Command input handler.
        """
        if args.get("character") is None:
            mode: str | None = args.get("mode")

            if mode == "unicode_lookup" or mode == "win_alt_code_lookup":
                return OdyseusMenuListInputHandler(mode)
            elif mode == "unicode_input" or mode == "win_alt_code_input":
                return OdyseusMenuInputHandler(mode)

        return None


class OdyseusMenuInputHandler(sublime_plugin.TextInputHandler):
    """Menu text input handler.

    Attributes
    ----------
    mode : str
        Command mode.
    """

    def __init__(self, mode: str) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        mode : str
            Command mode.
        """
        self.mode: str = mode

    def name(self) -> str:
        """The command argument name this input handler is editing.

        Returns
        -------
        str
            Argument name.
        """
        return "character"

    def placeholder(self) -> str:
        """Placeholder text is shown in the text entry box before the user has entered anything.

        Returns
        -------
        str
            Placeholder.
        """
        if self.mode == "unicode_input":
            return "Type unicode character code"
        elif self.mode == "win_alt_code_input":
            return "Type alt code"

        return ""

    def preview(self, text: str) -> sublime.Html | None:
        """Called whenever the user changes the text in the entry box.

        Parameters
        ----------
        text : str
            The text that's being inputed.

        Returns
        -------
        sublime.Html | None
            Markup for the preview.
        """
        if text is None:
            return None

        char: str | None = None
        mnemonic: str | None = None

        if self.mode == "unicode_input":
            padded_code: str = str(text).rjust(8, "0")

            if padded_code in Storage.code_to_character_map:
                char = Storage.code_to_character_map[padded_code]
                mnemonic = Storage.character_to_mnemonic_map.get(char, "")
        elif self.mode == "win_alt_code_input":
            if text in Storage.windows_alt_codes_map:
                char = eval('u"\\U%s"' % Storage.windows_alt_codes_map[text]["u"])
                mnemonic = ""

        if char is not None and mnemonic is not None:
            return sublime.Html(
                _preview_template.format(
                    preview_font_size=get_setting("preview_font_size", "1.5em"),
                    value=html.escape(char).replace(" ", "&nbsp;"),
                    mnemonic_markup=_preview_template_mnemonic.format(
                        mnemonic=html.escape(mnemonic).replace(" ", "&nbsp;"),
                    )
                    if mnemonic
                    else "",
                )
            )

        return None


class OdyseusMenuListInputHandler(sublime_plugin.ListInputHandler):
    """Menu list input handler.

    Attributes
    ----------
    mode : str
        Command mode.
    """

    def __init__(self, mode: str) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        mode : str
            Command mode.
        """
        self.mode: str = mode

    def name(self) -> str:
        """The command argument name this input handler is editing.

        Returns
        -------
        str
            Argument name.
        """
        return "character"

    def placeholder(self) -> str:
        """Placeholder text is shown in the text entry box before the user has entered anything.

        Returns
        -------
        str
            Placeholder.
        """
        if self.mode == "unicode_lookup":
            return "Filter unicode character"
        elif self.mode == "win_alt_code_lookup":
            return "Filter alt code"

        return ""

    def list_items(self) -> list[CustomListInputItem] | list[str]:
        """This method should return the items to show in the list.

        Returns
        -------
        list[CustomListInputItem] | list[str]
            A list of ``sublime.ListInputItem`` items.
        """
        if self.mode == "unicode_lookup":
            if Storage.unicode_characters_items:
                return Storage.unicode_characters_items
        elif self.mode == "win_alt_code_lookup":
            if Storage.windows_alt_codes_items:
                return Storage.windows_alt_codes_items

        return ["Something went wrong!"]

    def preview(self, value: Any) -> sublime.Html | None:
        """Called whenever the user changes the selected item.

        Parameters
        ----------
        value : Any
            The value of the currently selected item.

        Returns
        -------
        sublime.Html | None
            Markup for the preview.
        """
        # NOTE: I don't know under what circumstances value would be None.
        if value is None:
            return None

        mnemonic: str | None = None

        if self.mode == "unicode_lookup":
            mnemonic = Storage.character_to_mnemonic_map.get(value, "")
        elif self.mode == "win_alt_code_lookup":
            mnemonic = ""

        if mnemonic is not None:
            return sublime.Html(
                _preview_template.format(
                    preview_font_size=get_setting("preview_font_size", "1.5em"),
                    value=html.escape(value).replace(" ", "&nbsp;"),
                    mnemonic_markup=_preview_template_mnemonic.format(
                        mnemonic=html.escape(mnemonic).replace(" ", "&nbsp;"),
                    )
                    if mnemonic
                    else "",
                )
            )

        return None


class OdyseusCtToggleMnemonicsKeymapCommand(sublime_plugin.WindowCommand):
    """Toggle mnemonics keymap command."""

    def run(self) -> None:
        """Called when the command is run."""
        if os.path.exists(Storage.mnemonic_file):
            os.remove(Storage.mnemonic_file)
            os.rmdir(Storage.mnemonic_folder)
        else:
            os.makedirs(Storage.mnemonic_folder, exist_ok=True)
            create_mnemonic_keymap()


class OdyseusCtToggleDigraphKeymapCommand(sublime_plugin.TextCommand):
    """Toggle diagraph keymap command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        if os.path.exists(Storage.digraph_file):
            os.remove(Storage.digraph_file)
            os.rmdir(Storage.digraph_folder)
        else:
            with open(os.path.join(_data_folder, "extreme_keymap.json"), "r") as f1:
                os.makedirs(Storage.digraph_folder, exist_ok=True)

                with open(Storage.digraph_file, "w") as f2:
                    f2.write(f1.read())

        sublime.set_timeout_async(self._update_states, 500)

    def _update_states(self) -> None:
        """Update states."""
        Storage.update()
        set_digraph_status(self.view)


class OdyseusCtDigraphStatusListener(sublime_plugin.EventListener):
    """Diagraph status listener."""

    def on_activated_async(self, view: sublime.View) -> None:
        """Called when the view gains input focus.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        set_digraph_status(view)

    def on_load_async(self, view: sublime.View) -> None:
        """Called when the file is finished loading.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        set_digraph_status(view)


if __name__ == "__main__":
    pass
