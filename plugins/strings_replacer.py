# -*- coding: utf-8 -*-
"""Toggle strings plugins.

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from python_utils.sublime_text_utils.settings import SettingsManager
    from typing import Any

import html

import sublime
import sublime_plugin

from . import events
from . import plugin_name
from . import settings
from . import sphinx_building_docs
from python_utils.lazy_import import lazy_module
from python_utils.sublime_text_utils.custom_classes import CustomListInputItem
from python_utils.sublime_text_utils.queue import Queue

if sphinx_building_docs:
    from python_utils.sublime_text_utils import utils
else:
    utils = lazy_module("python_utils.sublime_text_utils.utils")


__all__: list[str] = [
    "OdyseusStringsReplacerMenuCommand",
]

queue: Queue = Queue()

_plugin_id: str = f"{plugin_name}StringsReplacer-{{}}"
_status_message_temp: str = "Strings Replacer: {}"


class Storage:
    """Storage class.

    Attributes
    ----------
    input_items_list : list[CustomListInputItem]
        Description
    input_record : str
        Description
    last_operation : str
        Description
    """

    input_items_list: list[CustomListInputItem] = []
    input_record: str = ""
    last_operation: str = ""

    @classmethod
    def _join_operation(cls, operation: str | list[str]) -> str:
        """Summary

        Parameters
        ----------
        operation : str | list[str]
            Description

        Returns
        -------
        str
            Description
        """
        if isinstance(operation, list):
            return get_setting("chain_operator", ">>").join(operation)

        return operation

    @classmethod
    def update(cls) -> None:
        """Update storage."""
        cls.input_record = ""
        cls.input_items_list.clear()

        definition_dicts: list[dict] = get_setting("operations_definitions_user", []) + get_setting(
            "operations_definitions", []
        )

        for op_def in definition_dicts:
            if op_def.get("enabled", True) and "operation" in op_def and "caption" in op_def:
                operation: str = cls._join_operation(op_def.get("operation"))
                expanded_operation: str = expand_operation_variables(operation)
                caption: str = op_def.get("caption")
                cls.input_items_list.append(
                    CustomListInputItem(
                        text=(
                            expanded_operation
                            if get_setting("filter_by_operation", False)
                            else caption
                        ),
                        value=expanded_operation,
                        details=html.escape(
                            caption
                            if get_setting("filter_by_operation", False)
                            else expanded_operation
                        ),
                        kind=get_kind_tuple(operation),
                    )
                )

        cls.input_items_list.sort()


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"strings_replacer.{s}", default)


@events.on("plugin_loaded")
def on_strings_replacer_plugin_loaded() -> None:
    """Called on plugin loaded."""
    queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage"))


@events.on("plugin_unloaded")
def on_strings_replacer_plugin_unloaded() -> None:
    """Called on plugin unloaded."""
    queue.unload()


@events.on("settings_changed")
def on_strings_replacer_settings_changed(settings_obj: SettingsManager) -> None:
    """Called on settings changed.

    Parameters
    ----------
    settings_obj : SettingsManager
        An instance of :any:`python_utils.sublime_text_utils.settings.SettingsManager`.
    """
    if any(
        (
            settings_obj.has_changed("strings_replacer.filter_by_operation"),
            settings_obj.has_changed("strings_replacer.replace_operator"),
            settings_obj.has_changed("strings_replacer.swap_operator"),
            settings_obj.has_changed("strings_replacer.chain_operator"),
            settings_obj.has_changed("strings_replacer.operations_definitions"),
            settings_obj.has_changed("strings_replacer.operations_definitions_user"),
        )
    ):
        queue.debounce(Storage.update, delay=500, key=_plugin_id.format("debounce-update-storage"))


def status_message(msg: str) -> None:
    """Summary

    Parameters
    ----------
    msg : str
        Description
    """
    sublime.status_message(_status_message_temp.format(msg))


def get_kind_tuple(operation: str) -> tuple[int, str, str]:
    """Summary

    Parameters
    ----------
    operation : str
        Description

    Returns
    -------
    tuple[int, str, str]
        Description
    """
    if "${r" in operation:
        return (sublime.KIND_ID_COLOR_REDISH, "R", "Replace operation")

    if "${s" in operation:
        return (sublime.KIND_ID_COLOR_GREENISH, "S", "Swap operation")

    return (sublime.KIND_ID_COLOR_REDISH, "U", "Unrecognized operation")


def expand_operation_variables(operation: str) -> str:
    """Summary

    Parameters
    ----------
    operation : str
        Description

    Returns
    -------
    str
        Description
    """
    return utils.substitute_variables(
        {
            "r": get_setting("replace_operator", ">"),
            "s": get_setting("swap_operator", "<>"),
            "c": get_setting("chain_operator", ">>"),
            "replace": get_setting("replace_operator", ">"),
            "swap": get_setting("swap_operator", "<>"),
            "chain": get_setting("chain_operator", ">>"),
        },
        operation,
    )


def handle_operation(text: str, operation: str | list[str]) -> str:
    """Summary

    Parameters
    ----------
    text : str
        Description
    operation : str | list[str]
        Description

    Returns
    -------
    str
        Description

    Raises
    ------
    ValueError
        Description
    """
    operations: list[str] = []

    if isinstance(operation, str):
        if get_setting("chain_operator", ">>") in operation:
            operations.extend([*operation.split(get_setting("chain_operator", ">>"))])
        else:
            operations.append(operation)
    elif isinstance(operation, list):
        operations.extend(operation)
    else:
        raise ValueError(f"Operations should be of type `str` or `list`; not {type(operation)}.")

    variables: dict[str, str] = {}

    for i, op in enumerate(operations):
        first_var: str = f"first{i}"
        second_var: str = f"second{i}"

        if get_setting("swap_operator", "<>") in op:
            first, second = op.split(get_setting("swap_operator", "<>"))
            text = f"${{{first_var}}}".join(text.split(first))
            text = f"${{{second_var}}}".join(text.split(second))
            variables[first_var] = second
            variables[second_var] = first
        elif get_setting("replace_operator", ">") in op:
            first, second = op.split(get_setting("replace_operator", ">"))
            text = f"${{{first_var}}}".join(text.split(first))
            variables[first_var] = second

    return utils.substitute_variables(variables, text)


class OdyseusStringsReplacerMenuCommand(sublime_plugin.TextCommand):
    """Summary"""

    def run(
        self,
        edit: sublime.Edit,
        operation: str | list[str] | None = None,
        mode: str = "lookup",
    ) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        operation : str | list[str] | None, optional
            Description
        mode : str, optional
            Description

        Returns
        -------
        None
            Description
        """
        view: sublime.View = self.view

        if view and operation is not None and mode:
            all_selections: list[sublime.Region] = utils.get_selections(
                view, return_whole_file=False, extract_words=True
            )

            if not len(all_selections):
                status_message("No selections found!")
                return

            replacement_data: list[tuple[sublime.Region, str]] = []

            for region in all_selections:
                text: str = view.substr(region)
                new_text: str = handle_operation(text, operation)

                if text != new_text:
                    replacement_data.append((region, new_text))

            if replacement_data:
                utils.replace_all_selections(view, edit, replacement_data)
            else:
                status_message("No modifications needed!")
        elif mode:
            sublime.set_timeout_async(lambda: self.rerun_from_command_palette(mode), 100)

    def rerun_from_command_palette(self, mode: str) -> None:
        """Summary

        Parameters
        ----------
        mode : str
            Description
        """
        self.view.window().run_command(
            "show_overlay",
            {
                "overlay": "command_palette",
                "command": "odyseus_strings_replacer_menu",
                "args": {"mode": mode},
            },
        )

    def input_description(self) -> str:
        """Summary

        Returns
        -------
        str
            Description
        """
        return "Strings replacer"

    def input(
        self, args
    ) -> sublime_plugin.ListInputHandler | sublime_plugin.TextInputHandler | None:
        """Summary

        Parameters
        ----------
        args : TYPE
            Description

        Returns
        -------
        sublime_plugin.ListInputHandler | sublime_plugin.TextInputHandler | None
            Description
        """
        if args.get("operation") is None:
            mode: str | None = args.get("mode")

            if mode == "lookup":
                return OperationsListInputHandler()
            elif mode == "input":
                return OperationsTextInputHandler()

        return None


class OperationsTextInputHandler(sublime_plugin.TextInputHandler):
    """Summary"""

    def name(self) -> str:
        """Summary

        Returns
        -------
        str
            Description
        """
        return "operation"

    def initial_text(self) -> str:
        """Initial text shown in the filter box. Empty by default.

        Returns
        -------
        str
            Description
        """
        if get_setting("record_input", True):
            return Storage.input_record

        return ""

    def preview(self, text: str) -> str | None:
        """Called whenever the user changes the selected item. The returned value (either plain text or HTML) will be shown in the preview area of the Command Palette.

        Parameters
        ----------
        text : str
            Description

        Returns
        -------
        str | None
            Description
        """
        if get_setting("record_input", True):
            Storage.input_record = text

        return None

    def placeholder(self) -> str:
        """Summary

        Returns
        -------
        str
            Description
        """
        return "Type operation"


class OperationsListInputHandler(sublime_plugin.ListInputHandler):
    """Summary"""

    def name(self) -> str:
        """Summary

        Returns
        -------
        str
            Description
        """
        return "operation"

    def placeholder(self) -> str:
        """Summary

        Returns
        -------
        str
            Description
        """
        return "Select operation"

    def list_items(self) -> list[CustomListInputItem] | list[str]:
        """Summary

        Returns
        -------
        list[CustomListInputItem] | list[str]
            Description
        """
        if Storage.input_items_list:
            return Storage.input_items_list

        return ["Something went wrong!"]


if __name__ == "__main__":
    pass
