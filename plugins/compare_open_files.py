# -*- coding: utf-8 -*-
"""Compare open files plugin.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

import os

import sublime
import sublime_plugin

from . import display_message_in_panel
from . import settings
from . import sphinx_building_docs
from python_utils.lazy_import import lazy_module

if sphinx_building_docs:
    from python_utils import cmd_utils
    from python_utils.sublime_text_utils import utils
else:
    cmd_utils = lazy_module("python_utils.cmd_utils")
    utils = lazy_module("python_utils.sublime_text_utils.utils")


__all__: list[str] = [
    "OdyseusCompareFileListener",
    "OdyseusCompareThreeFilesCommand",
    "OdyseusCompareTwoFilesCommand",
]


_selected_files_base_error_msg: str = """You must have activated **{number}** files to compare.
Please select **{number}** tabs to compare and try again."""


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"compare_open_files.{s}", default)


class Storage:
    """Storage class.

    Attributes
    ----------
    file_a : str | None
        Path to a file.
    file_b : str | None
        Path to a file.
    file_c : str | None
        Path to a file.
    recording : bool
        Recording file storage flag.
    """

    recording: bool = False
    file_c: str | None = None
    file_b: str | None = None
    file_a: str | None = None

    @classmethod
    def update(cls, last_file: str, reset: bool = False) -> None:
        """Record active file.

        Parameters
        ----------
        last_file : str
            The path to the file from the last active view.
        reset : bool, optional
            Set all the stored file paths to None.

        Returns
        -------
        None
            Halt execution.
        """
        if cls.recording:
            return

        cls.recording = True

        cls.file_c = None if reset else cls.file_b
        cls.file_b = None if reset else cls.file_a
        cls.file_a = None if reset else last_file

        cls.recording = False


def run_comparison(three_way_comparison: bool, cls_name: str) -> None:
    """Run comparison.

    Parameters
    ----------
    three_way_comparison : bool
        Whether to compare three files or just two.
    cls_name : str
        Command class name.
    """
    diff_exec: str | None = utils.get_executable_from_settings(
        None, get_setting("exec_map", {}).get(sublime.platform(), [])
    )

    selected_files_error_msg: str = _selected_files_base_error_msg.format(
        number="THREE" if three_way_comparison else "TWO"
    )
    cwd: str = os.path.dirname(diff_exec)
    cwd = cwd if os.path.isdir(cwd) else os.path.expanduser("~")
    correct_files_lenght: bool = True

    if diff_exec:
        if three_way_comparison:
            if all(
                [
                    Storage.file_a is not None,
                    Storage.file_b is not None,
                    Storage.file_c is not None,
                ]
            ):
                cmd_utils.popen(
                    [diff_exec, Storage.file_a, Storage.file_b, Storage.file_c], cwd=cwd
                )
            else:
                correct_files_lenght = False
        else:
            if all([Storage.file_a is not None, Storage.file_b is not None]):
                cmd_utils.popen([diff_exec, Storage.file_a, Storage.file_b], cwd=cwd)
            else:
                correct_files_lenght = False

        if not correct_files_lenght:
            title = "%s Error:" % cls_name
            display_message_in_panel(title=title, body=selected_files_error_msg)
        else:
            Storage.update("", reset=True)
    else:
        title: str = "%s Error:" % cls_name
        msg: str = "Please try again after you have command line tools installed."
        display_message_in_panel(title=title, body=msg)


class OdyseusCompareTwoFilesCommand(sublime_plugin.ApplicationCommand):
    """Command to compare two files."""

    def run(self) -> None:
        """Called when the command is run."""
        run_comparison(three_way_comparison=False, cls_name=self.__class__.__name__)


class OdyseusCompareThreeFilesCommand(sublime_plugin.ApplicationCommand):
    """Command to compare three files."""

    def run(self) -> None:
        """Called when the command is run."""
        run_comparison(three_way_comparison=True, cls_name=self.__class__.__name__)


class OdyseusCompareFileListener(sublime_plugin.EventListener):
    """Event listener for when a view is activated."""

    def on_activated(self, view: sublime.View) -> None:
        """Called when the view gains input focus.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        filename: str = utils.get_file_path(view)

        if filename and filename != Storage.file_a:
            Storage.update(view.file_name())


if __name__ == "__main__":
    pass
