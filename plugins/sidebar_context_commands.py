# -*- coding: utf-8 -*-
"""Sidebar context commands plugin.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from collections.abc import Callable
    from typing import Any

import json
import operator
import os

from multiprocessing import Process
from threading import Thread

import sublime
import sublime_plugin

from . import display_message_in_panel
from . import logger
from . import settings
from . import sphinx_building_docs
from python_utils.lazy_import import lazy_module

if sphinx_building_docs:
    from python_utils import cmd_utils
    from python_utils import misc_utils
    from python_utils.sublime_text_utils import sublime_lib
    from python_utils.sublime_text_utils import utils
else:
    cmd_utils: object = lazy_module("python_utils.cmd_utils")
    misc_utils: object = lazy_module("python_utils.misc_utils")
    sublime_lib: object = lazy_module("python_utils.sublime_text_utils.sublime_lib")
    utils: object = lazy_module("python_utils.sublime_text_utils.utils")


__all__: list[str] = ["OdyseusExecCommandOnSidebarSelectionCommand"]

_msg_template: str = """Working directory: `{cwd}`
Command: `{cmd}`
Command output:
```{lang}
{cmd_output}
```"""

_error_template: str = """Working directory: `{cwd}`
Command `{cmd}`
```
{stderr}
```"""
_operations: list[str] = ["<=", ">=", "!="]
_all_operations: list[str] = ["<", "=", ">"] + _operations
_operations_map: dict[str, Callable[..., bool]] = {
    "<=": operator.le,
    ">=": operator.ge,
    "!=": operator.ne,
}
_all_operations_map: dict[str, Callable[..., bool]] = misc_utils.merge_dict(
    {"<": operator.lt, "=": operator.eq, ">": operator.gt}, _operations_map
)


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"sidebar_context_commands.{s}", default)


class SidebarContextCommandsThread(Thread):
    """Summary

    Attributes
    ----------
    command_output : str | None
        Description
    error : str
        Description
    """

    def __init__(
        self,
        cmd: list[str] = [],
        cwd: str | None = None,
        cmd_settings: dict[str, object] | None = None,
    ):
        """Summary

        Parameters
        ----------
        cmd : list[str], optional
            Description
        cwd : str | None, optional
            Description
        cmd_settings : dict[str, object] | None, optional
            Description
        """
        self._cmd: list[str] = cmd
        self._cwd: str | None = cwd
        self._cmd_settings: dict[str, object] | None = cmd_settings
        self.command_output: str | None = None
        self.error: str = ""
        Thread.__init__(self)

    def read_output(self, output: bytes) -> str:
        """Summary

        Parameters
        ----------
        output : bytes
            Description

        Returns
        -------
        str
            Description
        """
        return str(output, encoding="utf-8")

    def run(self) -> None:
        """Run thread."""
        try:
            with cmd_utils.popen(self._cmd, logger=logger, cwd=None) as proc:
                stdout, stderr = proc.communicate()

                if stderr:
                    self.command_output = None
                    self.error = _error_template.format(
                        cwd=str(self._cwd),
                        cmd=" ".join(self._cmd),
                        stderr=self.read_output(stderr),
                    )
                else:
                    self.command_output = _msg_template.format(
                        cwd=str(self._cwd),
                        lang="diff"
                        if self._cmd_settings.get("report_stdout_diff_syntax", False)
                        else "shell",
                        cmd=" ".join(self._cmd),
                        cmd_output=self.read_output(stdout),
                    )
        except Exception as err:
            self.command_output = None
            self.error = _error_template.format(
                cwd=str(self._cwd), cmd=" ".join(self._cmd), stderr=str(err)
            )


class OdyseusExecCommandOnSidebarSelectionCommand(sublime_plugin.WindowCommand):
    """Execute a command on the sidebar."""

    def run(
        self,
        cmd_id: str | None = None,
        ask_confirmation: bool | dict = False,
        dirs: list[str] = [],
        files: list[str] = [],
        paths: list[str] = [],
    ) -> None:
        """Called when the command is run.

        Parameters
        ----------
        cmd_id : str | None, optional
            Description
        ask_confirmation : bool | dict, optional
            Description
        dirs : list[str], optional
            Description
        files : list[str], optional
            Description
        paths : list[str], optional
            Description

        Returns
        -------
        None
            Description
        """
        cmd_settings: dict | None = self._ody_get_setting(cmd_id)

        if not cmd_settings or not cmd_settings["cmd"]:
            title: str = "%s: No command set or found." % self.name()
            msg: str = "# cmd_id: %s" % str(cmd_id)
            display_message_in_panel(self.window, title=title, body=msg)
            return

        cmd: list[str] = [
            utils.substitute_variables(utils.get_view_context(None), cmd_settings["cmd"])
        ]
        args: list[str] = cmd_settings["args"]
        report_stdout: bool = cmd_settings["report_stdout"]
        pass_path_to_cmd: bool = cmd_settings["pass_path_to_cmd"]
        allow_multiple: bool = cmd_settings["allow_multiple"]
        path_type: str = cmd_settings["path_type"]
        exec_once: bool = cmd_settings["exec_once"]
        working_directory: str | None = (
            cmd_settings["working_directory"]
            if os.path.isdir(cmd_settings["working_directory"])
            else None
        )
        self._ody_stdout_storage: list[str] = []
        selected_paths: list[str] = paths

        if path_type == "folder":
            selected_paths = dirs
        elif path_type == "file":
            selected_paths = files

        if selected_paths:
            if isinstance(ask_confirmation, dict) and not sublime.ok_cancel_dialog(
                ask_confirmation.get("message", ""),
                ask_confirmation.get("ok_title", ""),
            ):
                sublime.status_message("Sidebar operation canceled.")
                return

            threads: list[SidebarContextCommandsThread] = []
            # NOTE: Make a copy of original. It doesn't matter if a change in the original modifies the copy,
            # what matters is that a change in the copy doesn't change the original.
            command: list[str] = cmd[:]
            command.extend(args)

            for path in selected_paths:
                # Build a single command with all selected paths to run once.
                if exec_once and allow_multiple and pass_path_to_cmd:
                    if isinstance(pass_path_to_cmd, str):
                        command.append(pass_path_to_cmd + path)
                    else:
                        command.append(path)
                else:  # Run command for each selected path.
                    arguments: list[str] = utils.substitute_variables(
                        utils.get_view_context(
                            None, additional_context={"selected_sidebar_path": path}
                        ),
                        args,
                    )

                    if pass_path_to_cmd is True:
                        arguments.append(path)

                    if report_stdout:
                        thread: SidebarContextCommandsThread = SidebarContextCommandsThread(
                            cmd=cmd + arguments,
                            cwd=working_directory,
                            cmd_settings=cmd_settings,
                        )
                        threads.append(thread)
                        thread.start()

                        self._ody_handle_threads(
                            threads,
                            lambda process, last_error: self._ody_handle_output(
                                process, last_error
                            ),
                        )
                    else:
                        proc: Process = Process(
                            target=self._ody_proc_exec,
                            args=(cmd + arguments,),
                            kwargs={
                                "cmd_id": str(cmd_id),
                                "cmd_settings": cmd_settings,
                                "cwd": path if os.path.isdir(path) else os.path.dirname(path),
                            },
                        )
                        proc.start()

            if exec_once and command:
                if report_stdout:
                    thread: SidebarContextCommandsThread = SidebarContextCommandsThread(
                        cmd=command,
                        cwd=working_directory,
                        cmd_settings=cmd_settings,
                    )
                    threads.append(thread)
                    thread.start()

                    self._ody_handle_threads(
                        threads,
                        lambda process, last_error: self._ody_handle_output(process, last_error),
                    )
                else:
                    proc: Process = Process(
                        target=self._ody_proc_exec,
                        args=(command,),
                        kwargs={
                            "cmd_id": str(cmd_id),
                            "cmd_settings": cmd_settings,
                            "cwd": working_directory,
                        },
                    )

                    proc.start()
        else:
            sublime.status_message("No valid path/s selected.")

    def _ody_handle_threads(
        self,
        threads: list[SidebarContextCommandsThread],
        callback: Callable[..., None],
        process: list[SidebarContextCommandsThread] | None = None,
        last_error: str | None = None,
    ) -> None:
        """Handle threads.

        Parameters
        ----------
        threads : list[SidebarContextCommandsThread]
            List of threads.
        callback : Callable[..., None]
            Method to call if a thread execution was successful.
        process : list[SidebarContextCommandsThread] | None, optional
            List of successfully executed threads ready to process.
        last_error : str | None, optional
            Error message.
        """
        next_threads: list[SidebarContextCommandsThread] = []
        if process is None:
            process = []

        for thread in threads:
            if thread.is_alive():
                next_threads.append(thread)
                continue

            if thread.command_output is None:
                # This thread failed
                last_error = thread.error
                continue

            # Thread completed correctly
            process.append(thread)

        if len(next_threads):
            # Some more threads to wait
            sublime.set_timeout(
                lambda: self._ody_handle_threads(next_threads, callback, process, last_error),
                100,
            )
        else:
            callback(process, last_error)

    def _ody_handle_output(
        self, threads: list[SidebarContextCommandsThread], last_error: str | None
    ) -> None:
        """Replace the content of a list of selections.

        This is called when there are multiple cursors or a selection of text

        Parameters
        ----------
        threads : list[SidebarContextCommandsThread]
            List of threads.
        last_error : str | None
            Error message.
        """
        if last_error:
            title: str = "%s Error:" % self.__class__.__name__
            display_message_in_panel(self.window, title=title, body=last_error)
        else:
            for thread in threads:
                self._ody_stdout_storage.append(thread.command_output)

        self._ody_display_new_view()

    def _ody_proc_exec(
        self,
        *args,
        cmd_settings: dict[str, object] = {},
        cmd_id: str = "None",
        **kwargs,
    ) -> None:
        """Execute command.

        Parameters
        ----------
        *args
            Arguments.
        cmd_settings : dict[str, object], optional
            Command settings.
        cmd_id : str, optional
            Description
        **kwargs
            Keyword arguments.

        Returns
        -------
        None
            Halt execution.
        """
        if cmd_settings["dry_run"]:
            try:
                print(
                    "\n".join(
                        [
                            self.name(),
                            "Command that will be executed:",
                            " ".join(*args),
                            "Working directory:",
                            kwargs["cwd"],
                            "Command settings:",
                            json.dumps(cmd_settings, indent=4),
                        ]
                    )
                )
            except Exception as err:
                display_message_in_panel(self.window, title=self.name(), body=str(err))

            return

        with cmd_utils.popen(*args, **kwargs) as proc:
            stderr: str = proc.stderr.read().decode("utf-8").strip()

            if stderr:
                title: str = "%s: Error: cmd_id = %s" % (self.name(), cmd_id)
                display_message_in_panel(self.window, title=title, body=stderr)

    def _ody_display_new_view(self) -> None:
        """Display new view."""
        if self._ody_stdout_storage:
            sublime_lib.new_view(
                self.window,
                content="\n".join(self._ody_stdout_storage),
                name=f"{self.name()} report.md",
                read_only=True,
                scratch=True,
                # NOTE: This is the syntax found at plugins/message_panel/OSP-Message
                # Panel.sublime-syntax.
                scope="output.odyseus_plugins_message_panel",
            )

    def _ody_get_defaults(self) -> dict[str, object]:
        """Get default settings.

        Returns
        -------
        dict[str, object]
            Default settings.
        """
        return {
            "cmd": "",
            "args": [],
            "exec_once": False,
            # "auto" is a dummy value that it isn't a bool, list or int used to coerce automatic
            # length calculation.
            "is_visible": "auto",
            "allow_multiple": True,
            "report_stdout": False,
            "pass_path_to_cmd": False,
            "path_type": "both",
            "working_directory": os.path.expanduser("~"),
            "dry_run": False,
        }

    def _ody_get_setting(self, cmd_id: str | None) -> dict[str, object] | None:
        """Set settings.

        Parameters
        ----------
        cmd_id : str | None
            Command ID.

        Returns
        -------
        dict[str, object] | None
            Halt execution.
        """
        if cmd_id is None or get_setting(cmd_id, {}).get("disabled", False):
            return None

        settings_exec_map = get_setting(cmd_id, {}).get("exec_map", {}).get(sublime.platform(), [])

        for _map in settings_exec_map:
            if "cmd" not in _map or _map.get("is_visible", "auto") is False:
                continue

            cmd: str = utils.substitute_variables(utils.get_view_context(None), _map["cmd"])

            if cmd_utils.can_exec(cmd) or cmd_utils.which(cmd):
                return misc_utils.merge_dict(self._ody_get_defaults(), _map)

        return None

    def is_visible(self, **kwargs) -> bool:
        """Set command visibility.

        Parameters
        ----------
        **kwargs
            Keyword arguments.

        Returns
        -------
        bool
            If the command should be visible.
        """
        cmd_settings: dict | None = self._ody_get_setting(kwargs.get("cmd_id", {}))

        if not cmd_settings:
            return False

        path_type: str = cmd_settings["path_type"]
        p: list[str] = kwargs.get("paths", [])

        if path_type == "folder":
            p = kwargs.get("dirs", [])
        elif path_type == "file":
            p = kwargs.get("files", [])

        is_visible: bool | str | list | int = cmd_settings["is_visible"]

        if isinstance(is_visible, bool):
            return is_visible
        elif isinstance(is_visible, (list, int)):
            return is_correct_length(len(p), is_visible)

        return len(p) > 0 if cmd_settings["allow_multiple"] else len(p) == 1


def is_correct_length(val: int, conditions: list[str] | int = []) -> bool:
    """Is correct length.

    Parameters
    ----------
    val : int
        The integer value to check.
    conditions : list[str] | int, optional
        List of conditions or integer to check against.

    Returns
    -------
    bool
        If the value is between the desired parameters.
    """
    cnds = []

    if isinstance(conditions, int):
        return val == conditions

    try:
        for o in _operations:
            for c in conditions:
                if o in c:
                    _, c_val = c.split(o)
                    cnds.append(_operations_map[o](val, int(c_val)))
    except Exception as err:
        logger.exception(err)

    return all(cnds)


if __name__ == "__main__":
    pass
