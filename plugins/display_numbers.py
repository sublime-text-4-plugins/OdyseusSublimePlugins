# -*- coding: utf-8 -*-
"""This plugin converts the selected number in decimal, hexadecimal, binary or octal numeral systems
and displays a popup that shows the result in all four (dec, hex, bin and oct) numeral systems.

Based on: https://github.com/nia40m/sublime-display-nums

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

import re

from functools import partial

import sublime
import sublime_plugin

from . import plugin_name
from . import settings
from . import sphinx_building_docs
from python_utils.lazy_import import lazy_module
from python_utils.sublime_text_utils import settings as settings_utils
from python_utils.sublime_text_utils.queue import Queue

if sphinx_building_docs:
    import json
    import struct

    from python_utils.sublime_text_utils import utils
else:
    json: object = lazy_module("json")
    struct: object = lazy_module("struct")
    utils: object = lazy_module("python_utils.sublime_text_utils.utils")


__all__: list[str] = [
    "OdyseusDnConvertNumberCommand",
    "OdyseusDnDisplayNumbersPopupListener",
    "OdyseusDnChangeBitCommand",
    "OdyseusDnShowNumbersPopupCommand",
    "OdyseusDnSwapEndiannessCommand",
    "OdyseusDnSwapPositionsCommand",
    "OdyseusDnTogglePopupDisplayModeCommand",
    "OdyseusDnCopyRawNumbersCommand",
]

queue: Queue = Queue()
_plugin_id: str = f"{plugin_name}DisplayNumbers-{{}}"
_space: str = "&nbsp;"
_temp_small_space: str = "*"
_small_space: str = "<span class='small-space'>" + _space + "</span>"
_split_re: re.Pattern = re.compile(r"\B_\B", re.I)
_dec_re: re.Pattern = re.compile(r"^(0|[1-9][0-9]*)(u|l|ul|lu|ll|ull|llu)?$", re.I)
_hex_re: re.Pattern = re.compile(r"^0x([0-9a-f]+)(u|l|ul|lu|ll|ull|llu)?$", re.I)
_oct_re: re.Pattern = re.compile(r"^(0[0-7]+)(u|l|ul|lu|ll|ull|llu)?$", re.I)
_bin_re: re.Pattern = re.compile(r"^0b([01]+)(u|l|ul|lu|ll|ull|llu)?$", re.I)
_byte_size_names: list[str] = ["B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]

_str_bytes_size: str = """<div>
<a href='{{"func": "copy", "data": "{data}"}}' title='Copy to clipboard'>C</a>
<span>Size:&nbsp;&nbsp;&nbsp;{data}</span>
</div>"""
_str_float_numbers: str = """<div>
<a href='{{"func": "copy", "data": "{data}"}}' title='Copy to clipboard'>C</a>
<span>Float:&nbsp;&nbsp;{data}</span>
</div>"""
_str_double_numbers: str = """<div>
<a href='{{"func": "copy", "data": "{data}"}}' title='Copy to clipboard'>C</a>
<span>Double:&nbsp;{data}</span>
</div>"""
_str_raw_data: str = """
Hex:    {hex}
Dec:    {dec}
Oct:    {oct}
Bin:    {raw_bin}
Size:   {bytes_size}
Float:  {float_numbers}
Double: {double_numbers}
"""

_popup_css: str = """
    #odyseus-display-numbers-popup {
        margin: 10px;
        font-size: 1.2rem;
    }
    #swap-endianness-options {
        font-size: 1rem;
    }
    .bits {
        color: var(--greenish);
    }
    .number {
        color: var(--redish);
    }
    .hr {
        margin: 5px 0;
    }
    .small-space {
        font-size: 0.35rem;
    }
    div {
        white-space: nowrap;
    }
"""
_popup_html: str = """
<body id="odyseus-display-numbers-popup">
<style>
{popup_css_style}
{popup_custom_css_style}
</style>
<div>
    <a href='{{"func": "copy", "data": "{hex}"}}' title='Copy to clipboard'>C</a>
    <a href='{{"func": "odyseus_dn_convert_number", "data": {{"base":16, "from_popup":true}}}}'
       title='Convert number'>Hex</a>:&nbsp;\
<span class="number">{hex}</span>
</div>
<div>
    <a href='{{"func": "copy", "data": "{dec}"}}' title='Copy to clipboard'>C</a>
    <a href='{{"func": "odyseus_dn_convert_number", "data": {{"base":10, "from_popup":true}}}}'
       title='Convert number'>Dec</a>:&nbsp;\
<span class="number">{dec}</span>
</div>
<div>
    <a href='{{"func": "copy", "data": "{oct}"}}' title='Copy to clipboard'>C</a>
    <a href='{{"func": "odyseus_dn_convert_number", "data": {{"base":8, "from_popup":true}}}}'
       title='Convert number'>Oct</a>:&nbsp;\
<span class="number">{oct}</span>
</div>
<div id="test">
    <a href='{{"func": "copy", "data": "{raw_bin}"}}' title='Copy to clipboard'>C</a>
    <a href='{{"func": "odyseus_dn_convert_number", "data": {{"base":2, "from_popup":true}}}}'
       title='Convert number'>Bin</a>:&nbsp;\
<span class="number">{bin}</span>
</div>
<div class='hr'></div>
<div id="swap-positions">
    <a href='{{"func": "copy", "data": "{raw_pos}"}}' title='Copy to clipboard'>C</a>
    <a href='{{"func": "odyseus_dn_swap_positions", "data": {{"base":{base}, "num":{num}}}}}'
       title='Swap positions'>Swap</a>&nbsp;{pos}
</div>
<div class='hr'></div>
{bytes_size_markup}
{float_numbers_markup}
{double_numbers_markup}
<div class='hr'></div>
<div id="swap-endianness-options">Swap endianness as
    <a href='{{"func": "odyseus_dn_swap_endianness", "data":{{"bits":16}}}}'>16 bit</a>
    <a href='{{"func": "odyseus_dn_swap_endianness", "data":{{"bits":32}}}}'>32 bit</a>
    <a href='{{"func": "odyseus_dn_swap_endianness", "data":{{"bits":64}}}}'>64 bit</a>
</div>
<div>
    <a href='{{"func": "odyseus_dn_copy_raw_numbers"}}'>Copy all data</a>
</div>
</body>
"""


class Storage:
    """Storage class.

    Attributes
    ----------
    last_base : str
        Description
    last_number : str
        Description
    numbers_popup_html : str
        Description
    numbers_raw_data : str
        Storage for the complete data displayed in the numbers popup for later copy to clipboard.
    """

    numbers_raw_data: str = ""
    numbers_popup_html: str = ""
    last_number: str = ""
    last_base: str = ""


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"display_numbers.{s}", default)


def feature_float_numbers(number: int, bits_count: int) -> tuple[str, str]:
    """Summary

    Parameters
    ----------
    number : int
        Description
    bits_count : int
        Description

    Returns
    -------
    tuple[str, str]
        Description
    """
    data: str = ""
    markup: str = ""

    if bits_count / 8 <= 4:
        data = struct.unpack("!f", struct.pack("!I", number))[0]
        markup += _str_float_numbers.format(data=data)

    return (data, markup)


def feature_double_numbers(number: int, bits_count: int) -> tuple[str, str]:
    """Summary

    Parameters
    ----------
    number : int
        Description
    bits_count : int
        Description

    Returns
    -------
    tuple[str, str]
        Description
    """
    data: str = ""
    markup: str = ""

    if bits_count / 8 <= 8:
        data = struct.unpack("!d", struct.pack("!Q", number))[0]
        markup += _str_double_numbers.format(data=data)

    return (data, markup)


def feature_size(number: int) -> tuple[str, str]:
    """Summary

    Parameters
    ----------
    number : int
        Description

    Returns
    -------
    tuple[str, str]
        Description
    """
    index: int = 0

    while number >= 1024 and index < len(_byte_size_names) - 1:
        number /= 1024
        index += 1

    data = "{:.{precision}g} {}".format(
        number,
        _byte_size_names[index],
        precision=get_setting("popup_bytes_size_precision", 7),
    )

    return (data, _str_bytes_size.format(data=data))


def get_bits_in_word() -> int:
    """Summary

    Returns
    -------
    int
        Description
    """
    bytes_in_word: int = get_setting("popup_bytes_in_word", 4)

    if not isinstance(bytes_in_word, int):
        sublime.status_message("'display_numbers.popup_bytes_in_word' setting must be an integer!")
        return 4 * 8

    return bytes_in_word * 8


def format_str(string: str, num: int, separator: str = " ") -> str:
    """Format string.

    Parameters
    ----------
    string : str
        Description
    num : int
        Description
    separator : str, optional
        Description

    Returns
    -------
    str
        Description
    """
    res: str = string[-num:]
    string = string[:-num]
    while len(string):
        res = string[-num:] + separator + res
        string = string[:-num]

    return res


def get_bits_positions(curr_bits_in_word: int, formatted: bool = True) -> str:
    """Summary

    Parameters
    ----------
    curr_bits_in_word : int
        Description
    formatted : bool, optional
        Description

    Returns
    -------
    str
        Description
    """
    positions: str = ""
    start: int = 0

    while start < curr_bits_in_word:
        if get_setting("popup_bit_positions_reversed", False):
            positions += "{: <4}".format(start)
        else:
            positions = "{: >4}".format(start) + positions

        start += 4

    if formatted:
        positions = format_str(positions, 2, _temp_small_space * 3)
        positions = positions.replace(" ", _space).replace(_temp_small_space, _small_space)

    return positions


def prepare_urls(s: str) -> str:
    """Prepare popup binary urls.

    Parameters
    ----------
    s : str
        Description

    Returns
    -------
    str
        Description
    """
    res: str = ""
    offset: int = 0

    bit: str = """<a class='bits' title='Change bit' href='{{ "func":"{func}",
        "data":{{ "offset":{offset} }}
        }}'>{char}</a>"""

    for c in s[::-1]:
        if c.isdigit():
            res = bit.format(func="odyseus_dn_change_bit", offset=offset, char=c) + res

            offset += 1
        else:
            res = c + res

    return res


def get_closer_bits_num(curr_bits: int) -> int:
    """Summary

    Parameters
    ----------
    curr_bits : int
        Description

    Returns
    -------
    int
        Description
    """
    i: int = 8  # bits number in byte
    while i <= curr_bits:
        i <<= 1

    return i


def create_popup_content(number: int, base: int, force: bool = False) -> str:
    """Summary

    Parameters
    ----------
    number : int
        Description
    base : int
        To which base should the number be converted to.
    force : bool, optional
        Description

    Returns
    -------
    str
        HTML markup to populate a popup with.
    """
    if not force and Storage.last_number == number and Storage.last_base == base:
        return Storage.numbers_popup_html

    # select max between (bit_length in settings) and (bit_length of selected number aligned to 4)
    curr_bits_in_word: int = max(
        get_bits_in_word(), number.bit_length() + ((-number.bit_length()) & 0x3)
    )

    hex_num: str = format_str("{:x}".format(number), 2)
    dec_num: str = format_str("{}".format(number), 3, get_setting("popup_decimal_separator", "."))
    oct_num: str = format_str("{:o}".format(number), 3)
    raw_bin: str = format_str("{:0={}b}".format(number, curr_bits_in_word), 4)
    bin_num: str = prepare_urls(
        format_str(
            format_str("{:0={}b}".format(number, curr_bits_in_word), 4, _temp_small_space),
            1,
            _temp_small_space,
        )
    ).replace(_temp_small_space, _small_space)

    # check if number can be negative
    sign_bit: int = get_closer_bits_num((number // 2).bit_length()) - 1

    if number & (1 << sign_bit):
        mask: int = (2 ** (sign_bit + 1)) - number

        dec_num += " (-{})".format(mask)

    bytes_size, bytes_size_markup = feature_size(number)
    float_numbers, float_numbers_markup = (
        feature_float_numbers(number, curr_bits_in_word) if int(base) == 16 else ("-", "")
    )
    double_numbers, double_numbers_markup = (
        feature_double_numbers(number, curr_bits_in_word) if int(base) == 16 else ("-", "")
    )

    Storage.numbers_raw_data = _str_raw_data.format(
        hex=hex_num,
        dec=dec_num,
        oct=oct_num,
        raw_bin=raw_bin,
        float_numbers=float_numbers,
        double_numbers=double_numbers,
        bytes_size=bytes_size,
    )

    Storage.numbers_popup_html = _popup_html.format(
        num=number,
        base=base,
        hex=hex_num,
        dec=dec_num,
        oct=oct_num,
        raw_bin=raw_bin,
        bin=bin_num,
        raw_pos=get_bits_positions(curr_bits_in_word, formatted=False),
        pos=get_bits_positions(curr_bits_in_word),
        float_numbers_markup=float_numbers_markup,
        double_numbers_markup=double_numbers_markup,
        bytes_size_markup=bytes_size_markup,
        popup_css_style=_popup_css,
        popup_custom_css_style=get_setting("popup_custom_css_style", ""),
    )

    Storage.last_number = number
    Storage.last_base = base

    return Storage.numbers_popup_html


def parse_number(text: str) -> dict[str, object]:
    """Parse number.

    Parameters
    ----------
    text : str
        Number to parse.

    Returns
    -------
    dict[str, object]
        Parsed number data.
    """
    if "_" in text:
        underscores = "_"
    else:
        underscores = ""

    # remove underscores in the number
    text = "".join(_split_re.split(text))

    for reg, base in [(_dec_re, 10), (_hex_re, 16), (_oct_re, 8), (_bin_re, 2)]:
        match = reg.match(text)

        if match:
            return {
                "number": int(match.group(1), base),
                "base": base,
                "suffix": match.group(2) or "",
                "underscores": underscores,
                "length": len(match.group(1)),
            }


def convert_number(parsed: dict[str, object], width: int = 0) -> str:
    """Convert number.

    Parameters
    ----------
    parsed : dict[str, object]
        Parsed number data.
    width : int, optional
        Character width (as in ``unsigned``, ``long``, etc.).

    Returns
    -------
    str
        Converted number.
    """
    if parsed["base"] == 10:
        prefix = ""
        num = "{:d}".format(parsed["number"])
        num = format_str(num, 3, parsed["underscores"])
    elif parsed["base"] == 16:
        prefix = "0x"
        num = "{:0{width}x}".format(parsed["number"], width=width)
        num = format_str(num, 4, parsed["underscores"])
    elif parsed["base"] == 2:
        prefix = "0b"
        num = "{:0{width}b}".format(parsed["number"], width=width)
        num = format_str(num, 4, parsed["underscores"])
    else:
        prefix = "0"
        num = "{:o}".format(parsed["number"])
        num = format_str(num, 4, parsed["underscores"])

    return "{}{}{}".format(prefix, num, parsed["suffix"])


class OdyseusDnShowNumbersPopupCommand(sublime_plugin.TextCommand):
    """Show numbers popup command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.

        Returns
        -------
        None
            Halt execution.
        """
        if not self.view:
            return

        all_selections: list[sublime.Region] = utils.get_selections(
            self.view, return_whole_file=False, extract_words=True
        )

        if not len(all_selections):
            return

        parsed: dict[str, object] = parse_number(self.view.substr(all_selections[0]).strip())

        if parsed is None:
            return

        html: str = create_popup_content(parsed["number"], parsed["base"])

        def select_function(href: str) -> None:
            """Select function to execute from an anchor's ``href`` attribute.

            Parameters
            ----------
            href : str
                Anchor's ``href`` attribute.
            """
            data: dict[str, object] = json.loads(href)

            if data.get("func") is not None:
                if data.get("func") == "copy":
                    sublime.set_clipboard(data.get("data"))
                    sublime.status_message("Data copied to clipboard")
                    self.view.hide_popup()
                else:
                    self.view.run_command(data.get("func"), data.get("data"))

        self.view.show_popup(
            html,
            flags=sublime.HIDE_ON_MOUSE_MOVE_AWAY
            if get_setting("popup_hide_on_mouse_move_away", True)
            else 0,
            max_width=1024,
            max_height=768,
            location=all_selections[0].begin(),
            on_navigate=select_function,
        )


class OdyseusDnDisplayNumbersPopupListener(sublime_plugin.EventListener):
    """Display numbers popup listener."""

    def on_selection_modified_async(self, view: sublime.View) -> None:
        """Called after the selection has been modified in the view.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.

        Returns
        -------
        None
            Hide popup and halt execution.
        """
        if get_setting("popup_display_mode", False) == "selection":
            # if more then one select close popup
            if len(view.sel()) > 1:
                return view.hide_popup()

            queue.debounce(
                partial(view.run_command, "odyseus_dn_show_numbers_popup"),
                delay=100,
                key=_plugin_id.format("debounce-show-numbers-popup"),
            )

    def on_text_command(
        self, view: sublime.View, command_name: str, args: dict[str, object]
    ) -> None:
        """Called when a text command is issued.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        command_name : str
            Name of the executed Sublime Text command.
        args : dict[str, object]
            The arguments with which ``command_name`` was executed.
        """
        if get_setting("popup_display_mode", False) == "double_click":
            double_click = (
                command_name == "drag_select"
                and "by" in args
                and args["by"] == "words"
                and view
                and len(view.sel())
            )

            if double_click:
                queue.debounce(
                    partial(view.run_command, "odyseus_dn_show_numbers_popup"),
                    delay=100,
                    key=_plugin_id.format("debounce-show-numbers-popup"),
                )


class OdyseusDnConvertNumberCommand(sublime_plugin.TextCommand):
    """Convert number command."""

    def run(self, edit: sublime.Edit, base: inr = 10, from_popup: bool = False) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        base : inr, optional
            To which base should the number be converted to.
        from_popup : bool, optional
            If the command was executed from the popup.

        Returns
        -------
        None
            Halt execution.
        """
        if not self.view:
            return

        all_selections: list[sublime.Region] = utils.get_selections(
            self.view, return_whole_file=False, extract_words=True
        )

        if not len(all_selections):
            return

        if len(all_selections) > 1 and from_popup:
            self.view.hide_popup()
            return

        selections: list[sublime.Region] = [all_selections[0]] if from_popup else all_selections
        replacement_data = []

        for region in selections:
            selected_number: str = self.view.substr(region)

            parsed: dict[str, object] = parse_number(selected_number)

            if parsed is None:
                self.view.hide_popup()
                continue

            parsed["base"] = base

            replacement_data.append((region, convert_number(parsed)))

        if replacement_data:
            utils.replace_all_selections(self.view, edit, replacement_data)

        if from_popup:
            queue.debounce(
                partial(self.view.run_command, "odyseus_dn_show_numbers_popup"),
                delay=100,
                key=_plugin_id.format("debounce-show-numbers-popup"),
            )


class OdyseusDnChangeBitCommand(sublime_plugin.TextCommand):
    """Change bit command."""

    def run(self, edit: sublime.Edit, offset: int) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        offset : int
            Offset(?). Too advanced for me to explain.

        Returns
        -------
        None
            Hide popup and halt execution.
        """
        selected_range: sublime.Region = self.view.sel()[0]
        selected_number: str = self.view.substr(selected_range).strip()

        parsed: dict[str, object] = parse_number(selected_number)
        if parsed is None:
            return self.view.hide_popup()

        parsed["number"] = parsed["number"] ^ (1 << offset)

        self.view.replace(edit, selected_range, convert_number(parsed, width=parsed["length"]))

        queue.debounce(
            partial(self.view.run_command, "odyseus_dn_show_numbers_popup"),
            delay=100,
            key=_plugin_id.format("debounce-show-numbers-popup"),
        )


class OdyseusDnSwapPositionsCommand(
    settings_utils.SettingsToggleBoolean, sublime_plugin.TextCommand
):
    """Swap bit positions command."""

    def __init__(self, *args, **kwargs) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        sublime_plugin.TextCommand.__init__(self, *args, **kwargs)
        settings_utils.SettingsToggleBoolean.__init__(
            self,
            key="display_numbers.popup_bit_positions_reversed",
            settings=settings,
            description="Bit positions reversed - {}",
            true_label="True",
            false_label="False",
        )

    def run(self, edit: sublime.Edit, base: int, num: int) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            Description
        base : int
            To which base should the number be converted to.
        num : int
            Description

        Returns
        -------
        None
            Description
        """
        settings_utils.SettingsToggleBoolean.run(self)

        view: sublime.View = self.view

        if not view:
            return

        view.update_popup(create_popup_content(num, base, True))


class OdyseusDnTogglePopupDisplayModeCommand(
    settings_utils.SettingsToggleList, sublime_plugin.ApplicationCommand
):
    """Toggle popup display mode command."""

    def __init__(self, *args, **kwargs) -> None:
        """Summary

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        sublime_plugin.ApplicationCommand.__init__(self, *args, **kwargs)
        settings_utils.SettingsToggleList.__init__(
            self,
            key="display_numbers.popup_display_mode",
            settings=settings,
            description="Display Numbers - Popup display mode - {}",
            values_list=["selection", "double_click", "none"],
        )


class OdyseusDnSwapEndiannessCommand(sublime_plugin.TextCommand):
    """Swap endianness command."""

    def run(self, edit: sublime.Edit, bits: int) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        bits : int
            Description

        Returns
        -------
        None
            Description
        """
        if len(self.view.sel()) > 1:
            return self.view.hide_popup()

        selected_range: sublime.Region = self.view.sel()[0]
        selected_number: str = self.view.substr(selected_range).strip()

        parsed: dict[str, object] = parse_number(selected_number)
        if parsed is None:
            return self.view.hide_popup()

        bit_len: int = parsed["number"].bit_length()
        # align bit length to bits
        bit_len: int = bit_len + ((-bit_len) & (bits - 1))

        bytes_len: int = bit_len // 8

        number: bytes = parsed["number"].to_bytes(bytes_len, byteorder="big")

        bytes_word: int = bits // 8

        result: list[int] = []

        for i in range(bytes_word, bytes_len + 1, bytes_word):
            for j in range(0, bytes_word):
                result.append(number[i - j - 1])

        result: int = int.from_bytes(bytes(result), byteorder="big")

        parsed["number"] = result

        self.view.replace(edit, selected_range, convert_number(parsed))


class OdyseusDnCopyRawNumbersCommand(sublime_plugin.TextCommand):
    """Summary"""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        if Storage.numbers_raw_data:
            sublime.set_clipboard(Storage.numbers_raw_data)
            sublime.status_message("Data copied to clipboard")
            self.view.hide_popup()
        else:
            sublime.status_message("No data to copy")


if __name__ == "__main__":
    pass
