# -*- coding: utf-8 -*-
"""Packages UI.

Attributes
----------
queue : Queue
    Queue manager.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

import html
import itertools
import re
import webbrowser

from functools import partial

import sublime
import sublime_plugin

from .. import events
from .. import logger
from .. import plugin_name
from .. import settings
from .. import sphinx_building_docs
from python_utils.lazy_import import lazy_module
from python_utils.sublime_text_utils import repeating_timer
from python_utils.sublime_text_utils import settings as settings_utils
from python_utils.sublime_text_utils import weakmethod
from python_utils.sublime_text_utils.queue import Queue

if sphinx_building_docs:
    from . import data
    from . import package_manager
    from python_utils.sublime_text_utils import sublime_lib
    from python_utils.sublime_text_utils import utils
else:
    data: object = lazy_module(f"{plugin_name}.plugins.packages_ui.data")
    package_manager: object = lazy_module(f"{plugin_name}.plugins.packages_ui.package_manager")
    sublime_lib: object = lazy_module("python_utils.sublime_text_utils.sublime_lib")
    utils: object = lazy_module("python_utils.sublime_text_utils.utils")


queue: Queue = Queue()


__all__: list[str] = [
    "OdyseusPuiUpdatePackagesStorageCommand",
    "OdyseusPuiUpdatePhantomsCommand",
    "OdyseusPuiToggleDefaultsVisibilityCommand",
    "OdyseusPuiChangeFontSizeCommand",
    "OdyseusPuiCommand",
    "OdyseusPuiRenderListCommand",
    "OdyseusPuiTogglePackageCommand",
    "OdyseusPuiOpenHomepageCommand",
    "OdyseusPuiShowInfoCommand",
    "OdyseusPuiViewEventListener",
]

_plugin_id: str = f"{plugin_name}PackagesUI-{{}}"
_status_message_temp: str = "Packages UI: {}"
_custom_syntax: str = "Packages/%s/plugins/packages_ui/OSP-Packages UI.sublime-syntax" % plugin_name
_pack_re: re.Pattern = re.compile(r"(?<=​)(.*?)(?=⁣)")


class Storage:
    """Storage class.

    Attributes
    ----------
    all_packages : list[str]
        List of all Sublime Text packages.
    default_packages : list[str]
        List of all Sublime Text default packages.
    ignored_packages : list[str]
        List of all Sublime Text ignored packages.
    installed_packages : list[str]
        List of all Sublime Text installed packages.
    package_manager : package_manager.OdyseusPuiPackageManager
        An instance of :any:`package_manager.OdyseusPuiPackageManager` with which to get
        lists of Sublime Text packages.
    phantom_set : sublime.PhantomSet
        A collection that manages ``sublime.Phantom`` and the process of adding them, updating
        them and removing them from the View.
    pui_phantoms_thread : PhantomsRendererThread
        An instance of :any:`repeating_timer.RepeatingTimer` to keep ``Phantoms`` updated.
    stored_selections : list[sublime.Region]
        Keep all selections stored to be able to restore them after each ``Phantoms`` update.
    """

    phantom_set: sublime.PhantomSet = None
    stored_selections: list[sublime.Region] = []
    pui_phantoms_thread: PhantomsRendererThread = None
    package_manager: package_manager.OdyseusPuiPackageManager = None
    all_packages: list[str] = []
    installed_packages: list[str] = []
    default_packages: list[str] = []
    ignored_packages: list[str] = []

    @classmethod
    def initialize_package_manager(cls) -> None:
        """Initialize package manager."""
        cls.package_manager = None

        try:
            cls.package_manager = package_manager.OdyseusPuiPackageManager()
        except Exception as err:
            logger.error(err)

    @classmethod
    def store_selection(cls, view: sublime.View) -> None:
        """Store selection.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        cls.stored_selections = list(view.sel())

    @classmethod
    def init_phantoms_thread(cls) -> None:
        """Initialize :any:`PhantomsRendererThread`."""
        if not cls.pui_phantoms_thread:
            cls.pui_phantoms_thread = PhantomsRendererThread()
            cls.pui_phantoms_thread.start()

    @classmethod
    def init_phantom_set(cls, view: sublime.View) -> None:
        """Initialize ``Phantoms`` set.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if not cls.phantom_set:
            cls.phantom_set = sublime.PhantomSet(view, "packages-ui-phantoms")

    @classmethod
    def cancel_phantoms_thread(cls) -> None:
        """Cancel :any:`PhantomsRendererThread`."""
        if cls.pui_phantoms_thread:
            cls.pui_phantoms_thread.cancel()

    @classmethod
    def toggle_phantoms_thread(cls, start: bool = True) -> None:
        """Toggle :any:`PhantomsRendererThread`.

        Parameters
        ----------
        start : bool, optional
            Force start if not running.
        """
        cls.init_phantoms_thread()

        if cls.pui_phantoms_thread:
            if not cls.pui_phantoms_thread.is_running and start:
                cls.pui_phantoms_thread.start()
            else:
                cls.pui_phantoms_thread.cancel()

    @classmethod
    def update_packages_storage(
        cls, target: sublime.View | sublime.Window, force: bool = False
    ) -> None:
        """Update packages storage.

        Parameters
        ----------
        target : sublime.View | sublime.Window
            Target for the ``ActivityIndicator``.
        force : bool, optional
            Force update.
        """
        sublime_lib.ActivityIndicator.width: int = 5
        with sublime_lib.ActivityIndicator(target, "Updating packages storage"):
            if force:
                cls.initialize_package_manager()
                cls.all_packages.clear()
                cls.installed_packages.clear()
                cls.default_packages.clear()
                cls.ignored_packages.clear()

            try:
                if cls.package_manager and not cls.all_packages:
                    cls.all_packages = list(cls.package_manager.list_all_packages())
            except Exception as err:
                cls.all_packages = []
                logger.error(err)

            try:
                if cls.package_manager and not cls.installed_packages:
                    cls.installed_packages = list(cls.package_manager.list_packages())
            except Exception as err:
                cls.installed_packages = []
                logger.error(err)

            try:
                if cls.package_manager and not cls.default_packages:
                    cls.default_packages = list(cls.package_manager.list_default_packages())
            except Exception as err:
                cls.default_packages = []
                logger.error(err)

            try:
                user_settings = sublime.load_settings("Preferences.sublime-settings")
                cls.ignored_packages = user_settings.get("ignored_packages", [])
            except Exception as err:
                cls.ignored_packages = []
                logger.error(err)

            if force:
                status_message("Packages storage updated.")


def get_setting(s: str, default: Any) -> Any:
    """Get settings.

    Parameters
    ----------
    s : str
        Part of the key in the settings file that stores the settings for a plugin.
    default : Any
        Default value in case the key storing a value isn't found.

    Returns
    -------
    Any
        A setting value if found or a predefined default one.
    """
    return settings.get(f"packages_ui.{s}", default)


@events.on("plugin_loaded")
def on_packages_ui_plugin_loaded() -> None:
    """Called on plugin loaded."""
    queue.debounce(
        Storage.initialize_package_manager,
        delay=500,
        key=_plugin_id.format("debounce-initialize-package-manager"),
    )


@events.on("plugin_unloaded")
def on_packages_ui_plugin_unloaded() -> None:
    """Called on plugin unloaded."""
    Storage.cancel_phantoms_thread()
    Storage.pui_phantoms_thread = None


def debounce_phantoms_update(view: sublime.View) -> None:
    """Debounce ``Phantoms`` update.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    """
    queue.debounce(
        lambda: sublime.set_timeout_async(view.run_command("odyseus_pui_update_phantoms"), 100),
        delay=500,
        key=_plugin_id.format("debounce-update-phantoms"),
    )


def debounce_store_selection(view: sublime.View) -> None:
    """Debounce store selection.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    """
    queue.debounce(
        partial(Storage.store_selection, view),
        delay=250,
        key=_plugin_id.format("debounce-store-selection"),
    )


def status_message(msg: str) -> None:
    """Status message.

    Parameters
    ----------
    msg : str
        Message.
    """
    sublime.status_message(_status_message_temp.format(msg))


def split_into_columns(lst: list[str], num_cols: int = 3) -> list[str]:
    """Split list into collumns.

    Parameters
    ----------
    lst : list[str]
        The list to split into columns.
    num_cols : int, optional
        The number of collumns to split into.

    Returns
    -------
    list[str]
        A list of lists with a maximum length of ``num_cols``.
    """
    return list(itertools.zip_longest(*[iter(lst)] * num_cols))


def get_help_table() -> str:
    """Get help keybindings table.

    Returns
    -------
    str
        The table of defined keybindings.
    """
    table: str = ""
    keybindings_help: list[str] | None = None
    col_offset: int = 0

    try:
        keybindings_help = utils.generate_keybindings_help_data(
            data.help_data, "[{key}] {description}", spacer=""
        ).split("\n")
        col_offset = len(max(keybindings_help, key=len))
        keybindings_help = split_into_columns(keybindings_help)
    except Exception:
        keybindings_help = None

    if not keybindings_help:
        return "No keybindings set"

    for row_def in keybindings_help:
        row_str: str = ""
        for col_def in row_def:
            if col_def:
                row_str += " " + col_def.ljust(col_offset, " ")

        if row_str:
            table += row_str + "\n"

    return table


def is_packages_ui_view(view: sublime.View) -> bool:
    """Is Packages UI view.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.

    Returns
    -------
    bool
        If ``view`` is the Packages UI view.
    """
    return view and view.settings().get("packages_ui.interface") == "packages_ui"


def toggle_packages(view: sublime.View, packs: list[str]) -> None:
    """Toggle packages enabled state.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    packs : list[str]
        List of packages names.
    """
    view.run_command("odyseus_pui_update_phantoms", {"clear_only": True})
    set_phantoms_thread_rendering(True)

    user_settings: sublime.Settings = sublime.load_settings("Preferences.sublime-settings")
    ignored_packages: list[str] = user_settings.get("ignored_packages", [])

    for pack in packs:
        if pack in ignored_packages:
            ignored_packages.remove(pack)
        else:
            ignored_packages.append(pack)

    new_value: list[str] = list(set(ignored_packages))
    new_value = sorted(new_value, key=lambda s: s.lower())

    user_settings.set("ignored_packages", new_value)
    sublime.save_settings("Preferences.sublime-settings")

    Storage.update_packages_storage(sublime.active_window(), force=True)
    set_phantoms_thread_rendering(False)


def set_phantoms_thread_rendering(flag: bool) -> None:
    """Set the ``is_rendering`` flag to the :any:`PhantomsRendererThread` instance.

    Parameters
    ----------
    flag : bool
        Boolean flag.
    """
    if Storage.pui_phantoms_thread:
        Storage.pui_phantoms_thread.is_rendering = flag


def get_packages_from_selections(view: sublime.View) -> list[str]:
    """Get the list of packages names from the ``view`` selections.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.

    Returns
    -------
    list[str]
        List of packages names.
    """
    packages: set[str] = set()

    for region in view.sel():
        string = view.substr(region)
        # Although _pack_re has three groups, it will only match the middle one and
        # _pack_re.findall() will return a list of strings and not a list of tuples.
        # If I'm not mistaken, this is due to using lookbehind/lookahead groups.
        matches: list[str] = _pack_re.findall(string)
        packages.update(set(matches))

        # Search the beginning and end of a selection for the desired scope
        # to expand package selection.
        for sel in (region.begin(), region.end()):
            if view.match_selector(sel, data.phantom_regions_scope):
                packages.add(view.substr(view.extract_scope(sel)))

    # Cleanup pack_wraps just in case.
    return list(packages.difference(data.pack_wraps))


def get_info_popup_content(pack: str) -> str:
    """Generate package information for use in a popup.

    Parameters
    ----------
    pack : str
        Package name.

    Returns
    -------
    str
        HTML markup.
    """
    try:
        info: dict[str, object] = Storage.package_manager.get_metadata(pack)
        version: str | bool = info.get("version", False)
        url: str | bool = info.get("url", False)
        return data.info_popup_content.format(
            pack=data.info_popup_package_url.format(
                url=url,
                pack=pack,
            )
            if url
            else pack,
            version=data.info_popup_package_version.format(version=version) if version else "",
            desc=info.get("description", "No description provided."),
            default=" (default package)" if pack in Storage.default_packages else "",
        )
    except Exception as err:
        logger.error(err)

    return ""


def open_info_popup(view: sublime.View, content: str, location: int = -1) -> None:
    """Open a popup with information about packages.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    content : str
        Popup content.
    location : int, optional
        A text point to use as an anchor for the popup.
    """
    html: str = data.info_popup.format(content=content)
    view.show_popup(
        html,
        flags=0,
        location=location,
        max_width=1024,
        max_height=768,
        on_navigate=webbrowser.open_new_tab,
    )


class PhantomsRendererThread(repeating_timer.RepeatingTimer):
    """Phantoms renderer thread.

    Attributes
    ----------
    is_rendering : bool
        Panthoms are being rendered.
    """

    def __init__(self, interval_ms: int = 1000) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        interval_ms : int, optional
            Interval in milliseconds.
        """
        super().__init__(interval_ms, self._check_current_view)

        # to prevent from overlapped processes when using a low interval
        self.is_rendering: bool = False

    def _check_current_view(self):
        """Check current view.

        Returns
        -------
        None
            Halt execution.
        """
        if self.is_rendering:
            return

        view: sublime.View = sublime.active_window().active_view()

        if not is_packages_ui_view(view) or not Storage.all_packages:
            return

        try:
            debounce_phantoms_update(view)
        except Exception as err:
            logger.error(err)


class OdyseusPuiUpdatePackagesStorageCommand(sublime_plugin.WindowCommand):
    """Update packages storage command."""

    def run(self) -> None:
        """Called when the command is run."""
        Storage.update_packages_storage(self.window, force=True)


class OdyseusPuiToggleDefaultsVisibilityCommand(
    settings_utils.SettingsToggleBoolean, sublime_plugin.TextCommand
):
    """Toggle default packages visibility command."""

    def __init__(self, *args, **kwargs) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        sublime_plugin.TextCommand.__init__(self, *args, **kwargs)
        settings_utils.SettingsToggleBoolean.__init__(
            self,
            key="packages_ui.display_default_packages",
            settings=settings,
        )

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            Description

        Returns
        -------
        None
            Halt execution.
        """
        settings_utils.SettingsToggleBoolean.run(self)

        view: sublime.View = self.view

        if not view:
            return

        if is_packages_ui_view(view):
            sublime.set_timeout_async(lambda: view.run_command("odyseus_pui_render_list"), 0)


class OdyseusPuiChangeFontSizeCommand(sublime_plugin.WindowCommand):
    """Change font size command."""

    def run(self, increment: bool | None = None):
        """Called when the command is run.

        Parameters
        ----------
        increment : bool | None, optional
            Increment font size.

        Returns
        -------
        None
            Halt execution.
        """
        plist_view = None

        for view in self.window.views():
            if is_packages_ui_view(view):
                plist_view = view
                break

        if plist_view:
            view_settings: sublime.Settings = plist_view.settings()

            if increment is None:
                return view_settings.set("font_size", 12)

            old_size: int = view_settings.get("font_size")
            new_size: int = old_size + (1 if increment else -1)

            if new_size < 4 or new_size > 20:
                return

            view_settings.set("font_size", new_size)


class OdyseusPuiCommand(sublime_plugin.WindowCommand):
    """Open or focus the Packages UI view command."""

    def run(self) -> None:
        """Called when the command is run."""
        set_phantoms_thread_rendering(True)
        plist_view: sublime.View | None = None

        for view in self.window.views():
            if is_packages_ui_view(view):
                plist_view = view
                break

        if plist_view:
            self.window.focus_view(plist_view)
        else:
            plist_view = self.create_view()

        plist_view.run_command("odyseus_pui_render_list")

    def create_view(self) -> sublime.View:
        """Create Packages UI view.

        Returns
        -------
        view : sublime.View
            A Sublime Text ``View`` object.
        """

        view: sublime.View = sublime_lib.new_view(
            self.window,
            name="✔ Packages UI",
            read_only=True,
            scratch=True,
            syntax=_custom_syntax,
            settings={
                "packages_ui.interface": "packages_ui",
                "packages_ui.phantoms_need_update": True,
            },
        )

        self.disable_other_plugins(view)

        return view

    def disable_other_plugins(self, view: sublime.View) -> None:
        """Set flags to disable other plugins functionality on Packages UI view.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if not get_setting("vintageous_friendly", False):
            view.settings().set("__vi_external_disable", False)


class OdyseusPuiUpdatePhantomsCommand(sublime_plugin.TextCommand):
    """Update ``Phantoms`` command."""

    def run(self, edit: sublime.Edit, clear_only: bool = False) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        clear_only : bool, optional
            Clear all ``Phantoms`` without updating them.

        Returns
        -------
        None
            Halt execution.
        """
        if not is_packages_ui_view(self.view):
            return

        Storage.init_phantom_set(self.view)

        if clear_only and Storage.phantom_set:
            Storage.phantom_set.update([])
            return

        set_phantoms_thread_rendering(True)

        logger.debug("Building phantoms set")
        phantoms: list[sublime.Phantom] = []
        info_img: str = data.phantom_images["info"]

        for region in self.view.find_by_selector(data.phantom_regions_scope):
            pack_name: str = self.view.substr(region)
            pack_enabled: bool = pack_name not in Storage.ignored_packages
            pack_is_default: bool = pack_name in Storage.default_packages

            check_name: str = "e" if pack_enabled else "d"
            check_name += "d" if pack_is_default else ""
            check_img: str = data.phantom_images[check_name]

            check_class: str = "enabled-link" if pack_enabled else "disabled-link"

            phantom_point: int = region.begin() - 1
            phantom_region: sublime.Region = sublime.Region(phantom_point)
            phantom_anchor_html: str = data.phantom_anchor_template.format(
                pack=html.escape(pack_name),
                phantom_point=phantom_point,
                check_class=check_class,
                check_image=check_img,
                info_image=info_img,
            )
            phantoms.append(
                sublime.Phantom(
                    region=phantom_region,
                    content=data.phantom_template.format(
                        content=phantom_anchor_html,
                        info_links_color=get_setting("info_links_color", "var(--bluish)"),
                        enabled_links_color=get_setting("enabled_links_color", "var(--greenish)"),
                        disabled_links_color=get_setting(
                            "disabled_links_color",
                            "color(var(--foreground) alpha(0.3))",
                        ),
                    ),
                    layout=sublime.LAYOUT_INLINE,
                    # use weak reference for callback
                    # to allow for phantoms to be cleaned up in __del__
                    on_navigate=weakmethod.WeakMethodProxy(self.on_navigate),
                )
            )

        logger.debug("Made %d phantoms" % len(phantoms))
        Storage.phantom_set.update(phantoms)

        set_phantoms_thread_rendering(False)

    def on_navigate(self, href: str) -> None:
        """Popup navigation event handler.

        Parameters
        ----------
        href : str
            ``href`` of a link to perform an action.
        """
        action, phantom_point, package = href.split(":")
        phantom_point: int = int(phantom_point) + 2

        if action == "toggle":
            sublime.set_timeout_async(lambda: toggle_packages(self.view, [package]), 0)
        elif action == "info":
            sublime.set_timeout_async(
                lambda: open_info_popup(
                    self.view,
                    get_info_popup_content(package),
                    location=phantom_point,
                ),
                0,
            )


class OdyseusPuiRenderListCommand(sublime_plugin.TextCommand):
    """Render Packages UI view command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.

        Returns
        -------
        None
            Halt execution.
        """
        if not is_packages_ui_view(self.view):
            return

        logger.debug("Rendering packages list")

        set_phantoms_thread_rendering(True)
        view: sublime.View = self.view
        Storage.update_packages_storage(self.view)

        view.set_read_only(False)
        full_region: sublime.Region = sublime.Region(0, view.size())

        installed_packages: list[str] = (
            Storage.all_packages
            if get_setting("display_default_packages", True)
            else Storage.installed_packages
        )
        installed_packages_count: int = len(installed_packages)

        if not installed_packages_count:
            warning_msg: str = "Refresh needed"
            view.replace(edit, full_region, "")
            view.insert(edit, 0, warning_msg)
            return

        # NOTE: The +2 is to force the left list to be longer than the right one.
        left_list_length: int = (installed_packages_count + 2 // 2) // 2
        left_list: list[str] = installed_packages[:left_list_length]
        right_list: list[str] = installed_packages[left_list_length:]

        # Make the two lists the same length.
        if len(left_list) > len(right_list):
            right_list.append("")

        # Merge the left/right lists into a list of two elements tuples.
        packages_map: list[tuple[str, str]] = list(zip(left_list, right_list))

        # A minimum of 42 characters and a maximum of the longest package name length.
        offset: int = max([len(max(installed_packages, key=len)), 42]) + 2
        rows_list: list[str] = []

        # NOTE:
        # lp and rp stands for left package and right package respectively.
        # lpo and rpo stands for left package offset and right package offset respectively.
        for lp, rp in packages_map:
            lp: str = f"{data.pack_wrap_start}{lp}{data.pack_wrap_end}"
            rp: str = f"{data.pack_wrap_start}{rp}{data.pack_wrap_end}" if rp else ""
            lpo: str = lp.ljust(offset, " ") + (" | " if rp else "")
            rpo: str = rp.ljust(offset, " ") if rp else ""
            left_pack_bulleted: str = f" {lpo}" if lp else ""
            right_pack_bulleted: str = f" {rpo}" if rp else ""
            rows_list.append(left_pack_bulleted + right_pack_bulleted)

        help_table: str = get_help_table()
        separator: str = "#" * (offset * 2 + 10) + "\n"
        title: str = "INSTALLED PACKAGES".center(offset * 2 + 10, " ") + "\n"
        header: str = f"{separator}{title}{separator}"

        if help_table:
            header = f"{separator}{title}{separator}{help_table}{separator}"

        checkbox_note: str = (
            "Default packages are identified with rounded checkboxes".center(offset * 2 + 10, " ")
            + "\n"
        )
        footer: str = f"\n{separator}{checkbox_note}{separator}"

        view.replace(edit, full_region, "")
        view.insert(edit, 0, footer)
        view.insert(edit, 0, "\n".join(rows_list))
        view.insert(edit, 0, header)
        # NOTE: Do not call debounced to avoid some of the "jumpyness".
        view.run_command("odyseus_pui_update_phantoms")

        if Storage.stored_selections:
            view.sel().clear()
            view.sel().add_all(Storage.stored_selections)

        set_phantoms_thread_rendering(False)
        view.set_read_only(True)


class OdyseusPuiTogglePackageCommand(sublime_plugin.TextCommand):
    """Toggle selected packages command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.

        Returns
        -------
        None
            Halt execution.
        """
        if not is_packages_ui_view(self.view):
            return

        set_phantoms_thread_rendering(True)
        Storage.update_packages_storage(self.view)
        view: sublime.View = self.view
        packs: list[str] = get_packages_from_selections(view)

        if packs:
            sublime.set_timeout_async(lambda: toggle_packages(view, packs), 0)
            sublime.set_timeout_async(lambda: view.run_command("odyseus_pui_update_phantoms"), 0)
            sublime.set_timeout_async(lambda: set_phantoms_thread_rendering(False), 0)


class OdyseusPuiOpenHomepageCommand(sublime_plugin.TextCommand):
    """Open packages home pages command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        Storage.update_packages_storage(self.view)
        view: sublime.View = self.view
        urls: list[str] = []
        packs: list[str] = get_packages_from_selections(view)

        for pack in packs:
            if pack:
                try:
                    info: dict[str, object] = Storage.package_manager.get_metadata(pack)
                    url: str | bool = info.get("url", False)

                    if url:
                        urls.append(url)
                except Exception as err:
                    logger.error(err)

        if urls:
            self.open_homepages(urls)

    def open_homepages(self, urls: list[str]) -> None:
        """Open home pages.

        Parameters
        ----------
        urls : list[str]
            List of URLs.
        """
        urls_count: int = len(urls)
        go_ahead: bool = urls_count < 5 or sublime.ok_cancel_dialog(
            "You are about to open %d web pages. Proceed?" % urls_count,
            ok_title="Yes",
        )

        if go_ahead:
            for url in urls:
                webbrowser.open_new_tab(url)


class OdyseusPuiShowInfoCommand(sublime_plugin.TextCommand):
    """Show packages information popup command."""

    def run(self, edit: sublime.Edit) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.

        Returns
        -------
        None
            Halt execution.
        """
        if not is_packages_ui_view(self.view):
            return

        Storage.update_packages_storage(self.view)
        popup_content: list[str] = []
        view: sublime.View = self.view
        packs: list[str] = get_packages_from_selections(view)

        for pack in packs:
            popup_content.append(get_info_popup_content(pack))

        if popup_content:
            open_info_popup(view, "<br>".join(popup_content))


class OdyseusPuiViewEventListener(sublime_plugin.ViewEventListener):
    """Packages UI listener."""

    def on_pre_close(self) -> None:
        """Called when a view is about to be closed.

        Returns
        -------
        None
            Halt execution.
        """
        if self.ignore_event():
            return

        Storage.cancel_phantoms_thread()
        Storage.phantom_set = None
        Storage.stored_selections = None

    def on_load_async(self) -> None:
        """Called when the file is finished loading.

        Returns
        -------
        None
            Halt execution.
        """
        if self.ignore_event():
            return

        Storage.toggle_phantoms_thread()
        sublime.set_timeout_async(lambda: self.view.run_command("odyseus_pui_render_list"), 10)

    def on_selection_modified_async(self) -> None:
        """Called after the selection has been modified in the view.

        Returns
        -------
        None
            Halt execution.
        """
        if self.ignore_event():
            return

        debounce_store_selection(self.view)

    def on_modified_async(self) -> None:
        """Called after changes have been made to the view.

        Returns
        -------
        None
            Halt execution.
        """
        if self.ignore_event():
            return

        Storage.toggle_phantoms_thread()

    def on_activated_async(self) -> None:
        """Called when the view gains input focus.

        Returns
        -------
        None
            Halt execution.
        """
        if self.ignore_event():
            return

        Storage.toggle_phantoms_thread()
        sublime.set_timeout_async(lambda: self.view.run_command("odyseus_pui_render_list"), 0)

    def ignore_event(self) -> bool:
        """Ignore events if not triggered on the Packages UI view.

        Returns
        -------
        bool
            If it is the Packages UI view.
        """
        return not is_packages_ui_view(self.view)


if __name__ == "__main__":
    pass
