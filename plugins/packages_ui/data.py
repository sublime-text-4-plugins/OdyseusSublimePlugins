# -*- coding: utf-8 -*-
"""Summary

Attributes
----------
help_data : dict[str, dict]
    Description
info_popup : str
    Description
info_popup_content : str
    Description
info_popup_package_url : str
    Description
info_popup_package_version : str
    Description
pack_wrap_end : str
    Description
pack_wrap_start : str
    Description
pack_wraps : set[str]
    Description
phantom_anchor_template : str
    Description
phantom_images : dict[str, str]
    Description
phantom_regions_scope : str
    Description
phantom_template : str
    Description
"""
from __future__ import annotations

# NOTE: Invisible character. Boooo.
# I can't figure out how to make it work with '\u200b'/'\u2063'.
pack_wrap_start: str = "​"
pack_wrap_end: str = "⁣"
pack_wraps: set[str] = set((pack_wrap_start, pack_wrap_end))
info_popup_content: str = """
<p class="row title"><span class="val">• {pack}{version}</span>{default}</p>
<p class="desc">{desc}</p>
"""
info_popup_package_version: str = ' - <span class="val version">{version}</span>'
info_popup_package_url: str = '<a title="Package homepage" href="{url}">{pack}</a>'
info_popup: str = """<html>
    <style>
        .main-title {{
            color: var(--orangish);
        }}
        .row {{
            margin: 0;
            padding: 0;
        }}
        .title {{
            font-weight: bold;
        }}
        .desc {{
        }}
        .val {{
        }}
        .version {{
        }}
        .help {{
            color: var(--greenish);
            font-style: italic;
        }}
    </style>
    <body>
        <h2 class="main-title">Packages information</h2>
        <p class="help">Click on package name to open home page</p>
        {content}
    </body>
    </html>
"""
help_data: dict[str, dict] = {
    "Toggle package": {
        "command": "odyseus_pui_toggle_package",
    },
    "Show package info": {
        "command": "odyseus_pui_show_info",
    },
    "Open package home page": {
        "command": "odyseus_pui_open_homepage",
    },
    "Toggle defaults display": {
        "command": "odyseus_pui_toggle_defaults_visibility",
    },
    "Refresh view": {
        "command": "odyseus_pui_render_list",
    },
    "Update packages storage": {
        "command": "odyseus_pui_update_packages_storage",
    },
    "Increase font size": {
        "command": "odyseus_pui_change_font_size",
        "args": {"plus": True},
    },
    "Decrease font size": {
        "command": "odyseus_pui_change_font_size",
        "args": {"plus": False},
    },
    "Reset font size": {
        "command": "odyseus_pui_change_font_size",
        "args": {"plus": "reset"},
    },
}
phantom_regions_scope: str = (
    "meta.item.packages-ui.package-item meta.item.packages-ui.package-item.name"
)
phantom_anchor_template: str = """<a title="Toggle package" class="{check_class}" href="toggle:{phantom_point}:{pack}">{check_image}</a> \
<a title="Package information" class="info-link" href="info:{phantom_point}:{pack}">{info_image}</a>"""
phantom_template: str = """
<body id="packages-ui">
<style>
    html, body {{
        margin: 0;
        padding: 0;
        background-color: transparent;
    }}
    a {{
        line-height: 0;
        text-decoration: none;
    }}
    a.info-link {{
        font-weight: bold;
        color: {info_links_color};
    }}
    a.enabled-link {{
        color: {enabled_links_color};
    }}
    a.disabled-link {{
        color: {disabled_links_color};
    }}
    a.disabled-link .disabled-check {{
        color: transparent;
    }}
</style>
{content}
</body>
"""
phantom_images: dict[str, str] = {
    "e": "[✔]",
    "d": """[<span class="disabled-check">✔</span>]""",
    "ed": "(✔)",
    "dd": """(<span class="disabled-check">✔</span>)""",
    "info": "ℹℹ",
}


if __name__ == "__main__":
    pass
