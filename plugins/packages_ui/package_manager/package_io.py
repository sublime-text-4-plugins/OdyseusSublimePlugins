# -*- coding: utf-8 -*-
"""Summary
"""
from __future__ import annotations

import os
import zipfile

import sublime

from .. import logger


def read_package_file(
    package: str, relative_path: str, binary: bool = False
) -> str | bytes | bool:
    """Reads the contents of a file that is part of a package

    Parameters
    ----------
    package : str
        The name of the package to read from.
    relative_path : str
        The path to the file, relative to the package root.
    binary : bool, optional
        If the contents should be read as a byte string instead of a unicode string.

    Returns
    -------
    str | bytes | bool
    bool
        A unicode or byte string (depending on value if binary param) or False on error.
    """

    if relative_path is None:
        return False

    package_dir: str = _get_package_dir(package)

    if os.path.exists(package_dir) and _regular_file_exists(package, relative_path):
        return _read_regular_file(package, relative_path, binary)

    if int(sublime.version()) >= 3000:
        result: str | bytes | bool = _read_zip_file(package, relative_path, binary)
        if result is not False:
            return result

    return False


def package_file_exists(package: str, relative_path: str) -> bool:
    """Determines if a file exists inside of the package specified. Handles both
    packed and unpacked packages.

    Parameters
    ----------
    package : str
        The name of the package to look in.
    relative_path : str
        The path to the file, relative to the package root.

    Returns
    -------
    bool
        If the file exists.
    """

    if relative_path is None:
        return False

    package_dir: str = _get_package_dir(package)

    if os.path.exists(package_dir):
        result: bool = _regular_file_exists(package, relative_path)
        if result:
            return result

    if int(sublime.version()) >= 3000:
        return _zip_file_exists(package, relative_path)

    return False


def _get_package_dir(package: str) -> str:
    """Get package directory.

    Parameters
    ----------
    package : str
        Package name.

    Returns
    -------
    str
        The full filesystem path to the package directory.
    """

    return os.path.join(sublime.packages_path(), package)


def _read_regular_file(
    package: str, relative_path: str, binary: bool = False
) -> str | bytes:
    """Summary

    Parameters
    ----------
    package : str
        Description
    relative_path : str
        Description
    binary : bool, optional
        Description

    Returns
    -------
    str | bytes
        Description
    """
    package_dir: str = _get_package_dir(package)
    file_path: str = os.path.join(package_dir, relative_path)
    mode: str = "rb" if binary else "r"
    encoding: str | None = None if binary else "utf-8"
    errors: str | None = None if binary else "replace"

    with open(file_path, mode, encoding=encoding, errors=errors) as f:
        return f.read()


def _read_zip_file(
    package: str, relative_path: str, binary: bool = False
) -> str | bytes | bool:
    """Summary

    Parameters
    ----------
    package : str
        Description
    relative_path : str
        Description
    binary : bool, optional
        Description

    Returns
    -------
    str | bytes | bool
        Description
    """
    zip_path: str = os.path.join(
        sublime.installed_packages_path(), package + ".sublime-package"
    )

    if not os.path.exists(zip_path):
        return False

    try:
        package_zip: zipfile.ZipFile = zipfile.ZipFile(zip_path, "r")

    except (zipfile.BadZipfile):
        logger.error(
            """
An error occurred while trying to unzip the sublime-package file
for %s.
"""
            % package
        )
        return False

    try:
        contents: bytes = package_zip.read(relative_path)
        if not binary:
            contents = contents.decode("utf-8")
        return contents

    except (KeyError):
        pass

    except (zipfile.BadZipfile):
        logger.error(
            """
Unable to read file from sublime-package file for %s due to the
package file being corrupt
"""
            % package
        )

    except (IOError):
        logger.error(
            """
Unable to read file from sublime-package file for %s due to an
invalid filename
"""
            % package
        )

    except (UnicodeDecodeError):
        logger.error(
            """
Unable to read file from sublime-package file for %s due to an
invalid filename or character encoding issue
"""
            % package
        )

    return False


def _regular_file_exists(package: str, relative_path: str) -> bool:
    """Summary

    Parameters
    ----------
    package : str
        Description
    relative_path : str
        Description

    Returns
    -------
    bool
        Description
    """
    package_dir: str = _get_package_dir(package)
    file_path: str = os.path.join(package_dir, relative_path)
    return os.path.exists(file_path)


def _zip_file_exists(package: str, relative_path: str) -> bool:
    """Summary

    Parameters
    ----------
    package : str
        Description
    relative_path : str
        Description

    Returns
    -------
    bool
        Description
    """
    zip_path: str = os.path.join(
        sublime.installed_packages_path(), package + ".sublime-package"
    )

    if not os.path.exists(zip_path):
        return False

    try:
        package_zip: zipfile.ZipFile = zipfile.ZipFile(zip_path, "r")

    except (zipfile.BadZipfile):
        logger.error(
            """
An error occurred while trying to unzip the sublime-package file
for %s.
"""
            % package
        )
        return False

    try:
        package_zip.getinfo(relative_path)
        return True

    except (KeyError):
        return False
