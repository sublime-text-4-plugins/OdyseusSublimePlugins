# -*- coding: utf-8 -*-
"""Summary
"""
from __future__ import annotations

import json
import os
import re
import sys

# NOTE: To prevent import errors in thread with datetime
import locale  # noqa

import sublime

from .. import logger
from .package_io import package_file_exists
from .package_io import read_package_file


class OdyseusPuiPackageManager:
    """Allows downloading, creating, installing, upgrading, and deleting packages.

    Delegates metadata retrieval to the CHANNEL_PROVIDERS classes.
    Uses VcsUpgrader-based classes for handling git and hg repositories in the
    Packages folder. Downloader classes are utilized to fetch contents of URLs.

    Also handles displaying package messaging, and sending usage information to
    the usage server.

    Attributes
    ----------
    platform_selectors : list[str]
        Description
    settings : dict
        Cached settings.
    """

    def __init__(self) -> None:
        """Summary"""
        self.settings: dict[str, object] = {
            "platform": sublime.platform(),
            "arch": sublime.arch(),
            "version": int(sublime.version()),
            "packages_path": sublime.packages_path(),
            "installed_packages_path": sublime.installed_packages_path(),
        }

        self.platform_selectors: list[str] = [
            self.settings["platform"] + "-" + self.settings["arch"],
            self.settings["platform"],
            "*",
        ]

    def get_metadata(
        self, package: str, is_dependency: bool = False
    ) -> dict[str, object]:
        """Returns the package metadata for an installed package

        Parameters
        ----------
        package : str
            The name of the package.
        is_dependency : bool, optional
            If the metadata is for a dependency.

        Returns
        -------
        dict[str, object]
            A dict with the keys ``version``, ``url`` and ``description`` or an empty dict on error.
        """

        metadata_filename: str = "package-metadata.json"
        if is_dependency:
            metadata_filename = "dependency-metadata.json"

        if package_file_exists(package, metadata_filename):
            metadata_json: str = read_package_file(package, metadata_filename)
            if metadata_json:
                try:
                    return json.loads(metadata_json)
                except (ValueError):
                    logger.error(
                        """
An error occurred while trying to parse the package
metadata for %s.
"""
                        % package
                    )

        return {}

    def get_dependencies(self, package: str) -> list[str]:
        """Returns a list of dependencies for the specified package on the
        current machine

        Parameters
        ----------
        package : str
            The name of the package.

        Returns
        -------
        list[str]
            A list of dependency names.
        """

        if package_file_exists(package, "dependencies.json"):
            dep_info_json: str = read_package_file(package, "dependencies.json")
            if dep_info_json:
                try:
                    return self.select_dependencies(json.loads(dep_info_json))
                except (ValueError):
                    logger.error(
                        """
An error occurred while trying to parse the
dependencies.json for %s.
"""
                        % package
                    )

        metadata: dict[str, object] = self.get_metadata(package)
        if metadata:
            return metadata.get("dependencies", [])

        return []

    def _is_git_package(self, package: str) -> bool:
        """Is Git package.

        Parameters
        ----------
        package : str
            The package name.

        Returns
        -------
        bool
            If the package is installed via git.
        """

        git_dir: str = os.path.join(self.get_package_dir(package), ".git")
        return os.path.exists(git_dir) and (
            os.path.isdir(git_dir) or os.path.isfile(git_dir)
        )

    def _is_hg_package(self, package: str) -> bool:
        """Is hg package.

        Parameters
        ----------
        package : str
            The package name

        Returns
        -------
        bool
            If the package is installed via hg.
        """

        hg_dir: str = os.path.join(self.get_package_dir(package), ".hg")
        return os.path.exists(hg_dir) and os.path.isdir(hg_dir)

    def select_dependencies(self, dependency_info: dict[str, object]) -> list[str]:
        """Takes the a dict from a dependencies.json file and returns the
        dependency names that are applicable to the current machine.

        Parameters
        ----------
        dependency_info : dict[str, object]
            A dict from a dependencies.json file.

        Returns
        -------
        list[str]
            A list of dependency names.
        """
        for platform_selector in self.platform_selectors:
            if platform_selector not in dependency_info:
                continue

            platform_dependency: dict[str, object] = dependency_info[platform_selector]
            versions: list[str] = list(platform_dependency.keys())

            # Sorting reverse will give us >, < then *
            for version_selector in sorted(versions, reverse=True):
                if not is_compatible_version(version_selector):
                    continue
                return platform_dependency[version_selector]

        # If there were no matches in the info, but there also weren't any
        # errors, then it just means there are not dependencies for this machine
        return []

    def list_packages(self, unpacked_only: bool = False) -> set[str]:
        """List packages.

        Parameters
        ----------
        unpacked_only : bool, optional
            Only list packages that are not inside of .sublime-package files.

        Returns
        -------
        set[str]
            A list of all installed, non-default, non-dependency, package names.
        """

        packages = self._list_visible_dirs(self.settings["packages_path"])

        if unpacked_only is False:
            packages |= self._list_sublime_package_files(
                self.settings["installed_packages_path"]
            )

        packages -= set(self.list_default_packages())
        packages -= set(self.list_dependencies())
        packages -= set(["User", "Default"])
        return sorted(packages, key=lambda s: s.lower())

    def list_dependencies(self) -> list[str]:
        """List dependencies.

        Returns
        -------
        list[str]
            A list of all installed dependency names
        """

        output: list[str] = []

        # This is seeded since it is in a .sublime-package with ST3
        if sys.version_info >= (3,):
            output.append("0_package_control_loader")

        for name in self._list_visible_dirs(self.settings["packages_path"]):
            if not self._is_dependency(name):
                continue
            output.append(name)

        return sorted(output, key=lambda s: s.lower())

    def list_all_packages(self) -> set[str]:
        """Lists all packages on the machine.

        Returns
        -------
        set[str]
            A list of all installed package names, including default packages.
        """

        packages: set[str] = self.list_default_packages() + self.list_packages()
        return sorted(packages, key=lambda s: s.lower())

    def list_default_packages(self) -> set[str]:
        """List default packages.

        Returns
        -------
        set[str]
            Description
        """

        app_dir: str = os.path.dirname(sublime.executable_path())
        packages: set[str] = self._list_sublime_package_files(
            os.path.join(app_dir, "Packages")
        )

        packages -= set(["User", "Default"])
        return sorted(packages, key=lambda s: s.lower())

    def _list_visible_dirs(self, path: str) -> set[str]:
        """Return a set of directories in the folder specified that are not
        hidden and are not marked to be removed

        Parameters
        ----------
        path : str
            The folder to list the directories inside of.

        Returns
        -------
        set[str]
            A set of directory names.
        """

        output: set[str] = set()
        for filename in os.listdir(path):
            if filename[0] == ".":
                continue
            file_path: str = os.path.join(path, filename)
            if not os.path.isdir(file_path):
                continue
            # Don't include a dir if it is going to be cleaned up
            if os.path.exists(os.path.join(file_path, "package-control.cleanup")):
                continue
            output.add(filename)
        return output

    def _list_sublime_package_files(self, path: str) -> set[str]:
        """Return a set of all .sublime-package files in a folder

        Parameters
        ----------
        path : str
            The directory to look in for .sublime-package files.

        Returns
        -------
        set[str]
            A set of the package names - i.e. with the .sublime-package suffix removed.
        """

        output: set[str] = set()
        if not os.path.exists(path):
            return output
        for filename in os.listdir(path):
            if not re.search(r"\.sublime-package$", filename):
                continue
            output.add(filename.replace(".sublime-package", ""))
        return output

    def _is_dependency(self, name: str) -> bool:
        """Is dependency.

        Checks if a package specified is a dependency

        Parameters
        ----------
        name : str
            The name of the package to check if it is a dependency.

        Returns
        -------
        bool
            If the package is a dependency.
        """

        metadata_path: str = os.path.join(
            self.settings["packages_path"], name, "dependency-metadata.json"
        )
        hidden_path: str = os.path.join(
            self.settings["packages_path"], name, ".sublime-dependency"
        )
        return os.path.exists(metadata_path) or os.path.exists(hidden_path)

    def get_package_dir(self, package: str) -> str:
        """Get package directory.

        Parameters
        ----------
        package : str
            The package name.

        Returns
        -------
        str
            The full filesystem path to the package directory.
        """

        return os.path.join(self.settings["packages_path"], package)


def is_compatible_version(version_range: str) -> bool | None:
    """Summary

    Parameters
    ----------
    version_range : str
        Description

    Returns
    -------
    bool | None
        Description
    """
    min_version = float("-inf")
    max_version = float("inf")

    if version_range == "*":
        return True

    gt_match = re.match(r">(\d+)$", version_range)
    ge_match = re.match(r">=(\d+)$", version_range)
    lt_match = re.match(r"<(\d+)$", version_range)
    le_match = re.match(r"<=(\d+)$", version_range)
    range_match = re.match(r"(\d+) - (\d+)$", version_range)

    if gt_match:
        min_version = int(gt_match.group(1)) + 1
    elif ge_match:
        min_version = int(ge_match.group(1))
    elif lt_match:
        max_version = int(lt_match.group(1)) - 1
    elif le_match:
        max_version = int(le_match.group(1))
    elif range_match:
        min_version = int(range_match.group(1))
        max_version = int(range_match.group(2))
    else:
        return None

    if min_version > int(sublime.version()):
        return False
    if max_version < int(sublime.version()):
        return False

    return True


if __name__ == "__main__":
    pass
