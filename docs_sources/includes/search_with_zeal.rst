
***********************
Search with Zeal plugin
***********************

Plugin that makes possible to search on Zeal from Sublime Text.

.. contextual-admonition::
    :context: warning
    :title: Dependencies

    - Zeal: An off-line documentation browser.

        + `Download instructions <https://zealdocs.org>`__.
        + `GitHub repository <https://github.com/zealdocs/zeal>`__.

.. contextual-admonition::
    :context: info
    :title: Mentions

    Plugin based on `Zeal for Sublime Text <https://github.com/vaanwd/Zeal>`__.

.. contextual-admonition::
    :context: info
    :title: Available commands

    - ``odyseus_search_with_zeal_selection``: Command to search the current file view's selection on Zeal.
    - ``odyseus_search_with_zeal``: This command will display a Sublime Text quick panel from which one can type queries to search on Zeal.

Plugin settings
===============

Settings for this plugin are prefixed with ``search_with_zeal.``. Possible options:

- ``exec_map`` (:py:class:`dict`): See :ref:`command-executables-note-reference` and :ref:`common-variables-substitution-reference`.
- ``fallback`` (Default: ``none``): Configure behavior for when none of the docsets match.

    + ``stop``: Do nothing.
    + ``none``: Search for the selected word without a Zeal namespace.
    + ``guess``: Try to guess the docset namespace.

- ``multi_match`` (Default: ``select``): Configure behavior for when multiple docsets were matched.

    + ``select``: Open a list of all matching docsets to choose from.
    + ``join``: Join all matching docsets for the query.

- ``docsets`` (:py:class:`list`): Default set of docsets configuration that help will be offered for, based on the syntax of the current view. You can add docsets to this list via ``user_docsets``.

    + ``name``: The name shown in the popup and used as the default for the others fields if not specified.
    + ``namespace``: Namespace to use for searching in Zeal. If not specified, ``name`` lower cased will be used.
    + ``selector``: A scope selector used to determine whether a docset is relevant and should be displayed for the current view. If not specified, the selector will be composed of ``source.`` plus ``name`` lower cased.

- ``user_docsets`` (:py:class:`list`): Docsets defined here will be added to the ones definied in ``docsets``. To override default ones, just define new docsets in ``docsets``.
