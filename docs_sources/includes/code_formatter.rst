
*********************
Code formatter plugin
*********************

Plugin to format code using external |CLI| tools.

.. contextual-admonition::
    :context: warning
    :title: Dependencies

    This plugin uses external |CLI| tools that can accept text read from **STDIN** and can output text to **STDOUT**.

.. contextual-admonition::
    :context: info
    :title: Mentions

    This plugin uses parts of several other plugins:

    - `Sublime EsFormatter <https://github.com/piuccio/sublime-esformatter>`__: |CLI| threaded execution is largely based on this plugin.
    - `JsFormat <https://github.com/jdavisclark/JsFormat>`__: Code merging is slightly based on this plugin.

.. contextual-admonition::
    :context: info
    :title: Available commands

    - ``odyseus_code_formatter``: The main command used to format code. Possible arguments:

        + ``cmd_id`` (:py:class:`str` or :py:class:`list`) (**Required**): A unique ID for a command definied in the settings (e.g.: for a command definition named ``code_formatter.odyseus_js_beautify``, the value for the ``cmd_id`` parameter should be ``odyseus_js_beautify``). It can also be a list of commands IDs to be executed in sequence (the ``STDOUT`` of each command will be used as ``STDIN`` of the next command in the chain).
        + ``diff`` (:py:class:`bool`): Do not perform changes to the code that's being formatted. Instead, show the differences between the unformatted and formatted text, if any, into a new view.
        + ``save`` (:py:class:`bool`): Save file after formatting it. Only taken into account if the whole file is formatted, not individual selections.
        + ``ignore_selection`` (:py:class:`bool`): If set to ``True``, the entire file will be formatted whether there are selections or not in the current file's view.

    - ``odyseus_code_formatter_overlay``: The sole purpose of this command is to make available all commands defined in the settings in the command palette. All command definitions found in the ``command_definitions``, ``command_definitions_user``, ``chain_definitions`` and ``chain_definitions_user`` will be used.

Plugin settings
===============

Settings for this plugin are prefixed with ``code_formatter.``. All settings of each command definition are **optional** except when stated otherwise:

- ``command_definitions`` (:py:class:`dict`): A dictionary of dictionaries containing the default command definitions. Each key in the ``command_definitions`` dictionary is what will be used as the ``cmd_id`` parameter for the ``odyseus_code_formatter`` and ``odyseus_code_formatter_overlay`` commands.

    + ``name`` (:py:class:`str`): A string that will be used when the command definition is represented in the command palette. If not specified the command definition key will be used.
    + ``exec_map`` (:py:class:`dict`) (**Required**): A dictionary with only three (3) keys (``linux``, ``osx``, and ``windows`` - see :ref:`command-executables-note-reference`); all of which are optional but at least one should exist. Each of these platform keys should contain a list of dictionaries which can accept the following options.

        * ``cmd`` (:py:class:`str`) (**Required**): The command to run. It can be the name of an executable or the full path to an executable. See :ref:`common-variables-substitution-reference`.
        * ``args`` (:py:class:`list`): Arguments to pass to ``cmd``. See :ref:`common-variables-substitution-reference`.
        * ``enabled`` (:py:class:`bool`): Setting this option to ``False`` will prevent the command from being looked up and be ignored.

        .. contextual-admonition::
            :context: note
            :title: Commands lookup

            A command lookup will be performed for each defined command and the first that can be executed will be chosen. Commands do not necessarily need to be available in ``PATH``; any executable can be set as a command.

    + ``enabled`` (:py:class:`bool`): This is a convenience setting to avoid having to completely delete a command definition. If will hide the command definition from being processed to be displayed with the ``odyseus_code_formatter_overlay`` command and also prevent the command from being executed with the ``odyseus_code_formatter`` command.
    + ``base`` (:py:class:`str`): This should be the ID of an existent command definition. It's a convenient way of defining commands without having to repeat values. The base command cannot be based on another command. The base command can have ``enabled`` set to ``False``.

    + ``to_new_file`` (:py:class:`dict`, :py:class:`bool`): Save the formatted text into a new file. The new file name will be based on the file name of the source file. This setting can be a :py:class:`dict` with the following options:

        * ``output_suffix`` (:py:class:`str`): A string that will be inserted between the source file name and the source file extension name. e.g.: With source file named ``style.css`` and an ``output_suffix`` value of ``.min``, the new file will be named ``style.min.css``. If not specified, it will default to an auto-generated suffix will be used in the form ``"YYYY-MM-DD_HH.MM.SS.MS"`` (the ``MS`` at the end are microseconds as a decimal number, zero-padded to 6 digits).
        * ``output_extension`` (:py:class:`str`): A string that will be used as the file extension for the new file.
        * ``overwrite_existing`` (:py:class:`bool`, :py:class:`str`): Whether to overwrite an existent file. If ``True``, existent files will be overwritten without confirmation. If ``False``, cancel file creation. If not specified, the default is to a confirmation dialog that which one can opt to overwrite an existent file or backup an existent file before overwriting it.

    + ``is_visible`` (:py:class:`bool`, :py:class:`str` or :py:class:`list`): Whether this command should be displayed in the command palette, a menu, etc. or allowed to be executed in a view. Possible values:

        * A boolean: If ``True``, the command is always visible and able to be executed. If set to ``False``, neither will be visible nor be allowed to be executed. See :ref:`commands-visibility-note-reference`.
        * A string: A scope selector.
        * A list of strings: A list of scope selectors. This is a convenient way of defining scope selectors. To avoid having to define very complex and long selectors inside a single line string, one can define them as a list that will later be joined by commas.

    + ``whole_file_not_allowed`` (:py:class:`bool`, :py:class:`str` or :py:class:`list`): This setting is used to prevent formatting an entire file. Same concept and possible values as the ``is_visible`` option.

- ``command_definitions_user``: Commands defined here will be merged into the commands defined in ``command_definitions``. To override default ones, new definitions can be created in ``command_definitions``.
- ``chain_definitions``:
- ``chain_definitions_user``:

Usage
=====

This plugin is meant to be used from anywhere in Sublime Text where a command can be defined and be *paired* with settings defined in the **OdyseusSublimePlugins.sublime-settings** settings file. Example:

.. code-block:: javascript

    {
        "code_formatter.command_definitions": {
            "odyseus_js_beautify": {
                "name": "Prettify JavaScript (JSBeautify)",
                "exec_map": {
                    "linux": [{
                        "cmd": "~/.local/bin/js-beautify",
                        "args": [
                            "--stdin",
                            "--operator-position=before-newline",
                            "--max-preserve-newlines=2",
                            "--brace-style=collapse",
                            "-"
                        ]
                    }],
                    "osx": [],
                    "windows": []
                },
                "whole_file_not_allowed": "text.html",
                "is_visible": "source.json, source.js, text.html"
            }
        }
    }

The previous command definition can be used in a keyboard shortcut as follows:

.. code-block:: javascript

    // Command defined as a keyboard shortcut.
    [{
        "keys": ["ctrl+alt+f"],
        "command": "odyseus_code_formatter",
        "args": {
            // Note that the value of "cmd_id" is used as part of the setting definition key
            // ("code_formatter.command_definitions" > "odyseus_js_beautify").
            "cmd_id": "odyseus_js_beautify",
            "save": true,                    // If one would want to save the file after formatting.
            "ignore_selection": true         // If one would want to always format the entire file.
        },
        // The context is only needed for the benefit of using the same key combination to
        // trigger different commands. The "is_visible" option of a command definition already
        // handles command execution "permissions".
        "context": [{
            "key": "selector",
            "operator": "equal",
            "operand": "source.js"
        }, {
            "key": "selector",
            "operator": "equal",
            "operand": "source.json"
        }, {
            "key": "selector",
            "operator": "equal",
            "operand": "text.html"
        }]
    }]

Examples
========

Example of ``base`` command definition option usage.

.. code-block:: javascript

    {
        "code_formatter.command_definitions": {
            "css_minifier_inline": {
                "name": "CSS Minifier (inline)",
                "exec_map": {
                    "linux": [{
                        "cmd": "command",
                        "args": [
                            "--arg-1",
                            "--arg-2",
                            "--arg-n",
                        ]
                    }],
                },
                "is_visible": "source.css"
            },
            "css_minifier_to_new_file": {
                "name": "CSS Minifier (to new file)",
                "base": "css_minifier_inline",
                "to_new_file": {
                    "output_suffix": ".min"
                }
            }
        }
    }

Another example of ``base`` command definition option usage. A definition used as base for other commands can be disabled (``enabled`` set to ``False``).

.. code-block:: javascript

    {
        "code_formatter.command_definitions": {
            "base_command": {
                "enabled": false,
                "exec_map": {
                    "linux": [{
                        "cmd": "command",
                        "args": [
                            "--arg-1",
                            "--arg-2",
                            "--arg-n",
                        ]
                    }],
                },
                "is_visible": "source.css"
            },
            "command_to_new_file": {
                "name": "Base command (to new file)",
                "base": "base_command",
                "to_new_file": true
            },
            "command_to_diff_only": {
                "name": "Base command (diff only)",
                "base": "base_command",
                "diff": true
            },
            "command_format_and_save": {
                "name": "Base command (format and save)",
                "base": "base_command",
                "save": true
            }
        }
    }

Example of a ``chain_definitions`` usage.

.. code-block:: javascript

    {
        "code_formatter.command_definitions": {
            "odyseus_es_formatter": {/* ... */},
            "odyseus_js_beautify": {/* ... */}
        },
        "code_formatter.chain_definitions": {
            "odyseus_es_formatter_and_js_beautify": {
                "name": "Prettify JavaScript (ESFormatter and JSBeautify)",
                "chain": [
                    "odyseus_es_formatter",
                    "odyseus_js_beautify"
                ]
            }
        }
    }
