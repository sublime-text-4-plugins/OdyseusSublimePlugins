
*******************************
Sidebar context commands plugin
*******************************

Plugin to create Sublime Text's sidebar context menu items that execute commands using the selected paths in the sidebar.

.. contextual-admonition::
    :context: info
    :title: Available command

    - ``odyseus_exec_command_on_sidebar_selection``: Possible arguments:

        + ``cmd_id`` (:py:class:`str`) (**Required**): A unique ID for the command. This ID will be used to locate the settings for the command.
        + ``ask_confirmation`` (:py:class:`dict`): Ask for confirmation before executing a command. This is a dictionary with only two keys:

            * ``message`` (:py:class:`str`) (**Required**): The message that the confirmation dialog will display.
            * ``ok_title`` (:py:class:`str`): The title for the button on the dialog that will confirm the execution when pressed.

        + ``dirs`` (:py:class:`list`): The list of selected folders in the sidebar.
        + ``files`` (:py:class:`list`): The list of selected files in the sidebar.
        + ``paths`` (:py:class:`list`): The list of selected files and folders in the sidebar.

            * The ``dirs``, ``files`` and ``paths`` arguments are populated by Sublime Text when one selects files/folders in the sidebar. Which of the selected paths are used when executing the command depends on the command setting ``path_type``.
            * There is no need to specify the three arguments, just the one that is needed.

Plugin settings
===============

Settings for this plugin are prefixed with ``sidebar_context_commands.`` and all settings are **optional** except when stated otherwise:

- ``disabled`` (:py:class:`bool`): This is a convenience setting. If one has several commands defined in ``exec_map``, setting the ``is_visible`` for each command is just annoying. Setting ``disabled`` to **true** is the same as setting ``is_visible`` to **false** on each defined command.
- ``exec_map`` (:py:class:`dict`) (**Required**): A dictionary with only three (3) keys (``linux``, ``osx``, and ``windows`` - see :ref:`command-executables-note-reference`); all of which are optional. Each of these keys should contain a list of dictionaries which can accept the following options.

    + ``cmd`` (:py:class:`str`) (**Required**): The command to run. It can be an executable name or the full path to an executable. See :ref:`common-variables-substitution-reference`.
    + ``args`` (:py:class:`list`): Arguments to pass to ``cmd``. Special variable ``${selected_sidebar_path}`` will be replaced with path of selected file/folder in sidebar. If ``exec_once`` is **true**, ``${selected_sidebar_path}`` variables defined here will be ignored. See :ref:`common-variables-substitution-reference`.
    + ``exec_once`` (:py:class:`bool`): Whether to execute the command once with all selected paths passed as arguments (if ``pass_path_to_cmd`` is set accordingly to **true** or a string), or run the command for each selected path. If not specified, **false**.
    + ``is_visible`` (:py:class:`bool`, :py:class:`list` or :py:class:`int`): Possible values:

        * A boolean: See :ref:`commands-visibility-note-reference`.
        * A list: A list of strings representing comparisons. Possible comparisons are ``<=`` (less than or equal to), ``>=`` (greater than or equal to), and ``!=`` (not equal to). Example: ``[">=2", "<=3"]`` (the command will be visible only if there are 2 or 3 files/folders selected).

            .. note::

                I purposely left out the  ``<``, ``=``, and ``>`` comparisons for programmatic reasons (mostly laziness). But either operation can be achieved with the available operations.

        * An integer: The exact number of selected paths on the sidebar that should be selected for the command to be displayed.
        * If not specified: The command will be visible if the option ``allow_multiple`` is set to **true** and any amount of paths are selected in the sidebar. If ``allow_multiple`` is set to **false**, the command will be visible only if one (1) path is selected.

    + ``allow_multiple`` (:py:class:`bool`): Allow the command to displayed when multiple paths are selected in the sidebar. If not specified, **true**.
    + ``report_stdout`` (:py:class:`bool`): After executing the command defined in ``cmd`` on all selected paths, display a file in a new view with the resulting output, if any. If not specified, **false**.
    + ``report_stdout_diff_syntax`` (:py:class:`bool`): The output displayed when the ``report_stdout`` option is used will be surrounded with fenced code markup. By default the ``shell`` language is used. With this options set to **true** the ``diff`` language will be used.
    + ``pass_path_to_cmd`` (:py:class:`bool`): Whether to pass path of selected file/folder as an argument to ``cmd``. If not specified, **false**.
    + ``path_type`` (:py:class:`str`): One of **folder**, **file** or **both**. Used to decide which of the selected items in the sidebar will be used to execute the command defined in ``cmd``. If not specified, **both**.

        * ``folder``: If there are files and folders selected in the sidebar, the **files** will be ignored.
        * ``file``: If there are files and folders selected in the sidebar, the **folders** will be ignored.
        * ``both``: **All** selected files and folders will be used.

    + ``dry_run`` (:py:class:`bool`): Do not execute the command/s, log it/them to console. If not specified, **false**. This will print to Sublime's console the full command/s that will be executed, the directory that will be used as working directory when the command/s is/are executed and the settings that were used to construct the command/s.

Usage
=====

This command is meant to be used only from the sidebar context menu and should be used in menu items definitions in a **Side Bar.sublime-menu** file and *paired* with settings defined in the **OdyseusSublimePlugins.sublime-settings** settings file. They work and make use of the sidebar selected paths whether they are files, folders or both depending on configuration. The following is a practical example that will open the selected folder on the sidebar in Git GUI.

#. Add the command definition in the **OdyseusSublimePlugins.sublime-settings** settings file.

    .. code-block:: javascript

        {
            "sidebar_context_commands.odyseus_open_git_gui_sidebar_context": {
                "exec_map": {
                    "linux": [{
                        "cmd": "/usr/bin/git",
                        "args": ["gui"],
                        "exec_once": false,
                        "path_type": "folder"
                    }],
                    "osx": [],
                    "windows": []
                }
            }
        }

#. Define a new menu entry in the **Side Bar.sublime-menu** file.

    .. code-block:: javascript

        [{
            "caption": "Git Gui",
            "command": "odyseus_exec_command_on_sidebar_selection",
            "args": {
                // Note that the value of "cmd_id" is used as part of the setting definition key
                // ("sidebar_context_commands.odyseus_open_git_gui_sidebar_context").
                "cmd_id": "odyseus_open_git_gui_sidebar_context",
                "dirs": []
            }
        }]
