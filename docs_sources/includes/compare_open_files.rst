
*************************
Compare open files plugin
*************************

Plugin to compare opened files with an external program.

.. contextual-admonition::
    :context: info
    :title: Available commands

    - ``odyseus_compare_two_files``: Compare two opened files.
    - ``odyseus_compare_three_files``: Compare three opened files.

- The files selection is automatic. Just focus the tabs of the 2/3 files that one wants to compare and immediately execute the proper command from the command palette (or a keyboard shortcut).
- Settings for this plugin are prefixed with ``compare_open_files.``. Available option:

    + ``exec_map`` (:py:class:`dict`): See :ref:`command-executables-note-reference`.
