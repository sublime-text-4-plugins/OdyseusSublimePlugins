
*********************
Keyword search plugin
*********************

Adds a couple of missing features to Sublime Text Find Results buffer.

.. contextual-admonition::
    :context: info
    :title: Mentions

    Plugin based on `Bang search <https://github.com/bsoun/keyword-search>`__.

.. contextual-admonition::
    :context: info
    :title: Available commands

    - ``odyseus_keyword_search``:
    - ``odyseus_keyword_search_input``:

Plugin settings
===============


Usage
=====
