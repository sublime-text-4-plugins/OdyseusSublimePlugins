
***********************
Smart keystrokes plugin
***********************

A series of commands that are meant to replace or extend certain keystrokes' default behaviors. All of these commands are meant to be used from keybindings definitions.

.. contextual-admonition::
    :context: info
    :title: Mentions

    - ``odyseus_smart_delete`` command based on `Sublime Text Smart Delete Plugin <https://github.com/mac2000/sublime-smart-delete>`__.
    - ``odyseus_deselect`` command based on `Deselect <https://github.com/glutanimate/sublime-deselect>`__.
    - ``odyseus_smart_backspace`` and ``odyseus_normal_backspace`` commands based on `Smart Backspace <https://github.com/awhite/sublime-smart-backspace>`__.

.. contextual-admonition::
    :context: info
    :title: Available commands

    - ``odyseus_deselect``: Deselect selections.
    - ``odyseus_normal_backspace``: Original functionality of backspace.
    - ``odyseus_smart_backspace``: Adds smart backspace capability found in JetBrains IDEs to Sublime Text.
    - ``odyseus_smart_delete``: Smart delete all spaces before or after caret.
