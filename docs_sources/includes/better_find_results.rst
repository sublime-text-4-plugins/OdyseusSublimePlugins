
**************************
Better find results plugin
**************************

Adds a couple of missing features to Sublime Text Find Results buffer.

.. contextual-admonition::
    :context: info
    :title: Mentions

    Plugin based on `BetterFindBuffer for SublimeText 3 <https://github.com/aziz/BetterFindBuffer>`__.

.. contextual-admonition::
    :context: info
    :title: Available commands

    All commands are triggered by the defined keyboard shortcuts.

    - ``odyseus_bfr_find_in_files_open_file``
    - ``odyseus_bfr_find_in_files_open_all_files``
    - ``odyseus_bfr_find_in_files_jump``
    - ``odyseus_bfr_find_in_files_jump_file``
    - ``odyseus_bfr_find_in_files_jump_match``
    - ``odyseus_bfr_toggle_popup_help``
    - ``odyseus_bfr_fold_and_move_to_file``
    - ``odyseus_bfr_copy_from_find_results``
    - ``odyseus_bfr_toggle_custom_syntax``: This command can be accessed from the **Command Palette**.

Plugin settings
===============

This plugin has only syntax settings that can be accessed from **Preferences** > **Package Settings** > **OdyseusSublimePlugins** > **Better Find Results Syntax Settings**. In addition to be able to set common Sublime Text syntax settings, the ``better_find_results.keymap_disable_all`` setting can be set to ``true`` and will disable all keybindings defined by this plugin.
