
********************
Run commands plugins
********************

Plugin that allows to execute any Sublime Text commands (or those provided by any Sublime Text plugin) on all opened file views.

.. contextual-admonition::
    :context: info
    :title: Available commands

    - ``odyseus_run_cmd_in_all_views``: Command that allows to execute any Sublime Text commands (or those provided by any Sublime Text plugin) on all opened file views. Available arguments:

        + ``cmd`` (:py:class:`str`) (**Required**): The name of a Sublime Text command or that of a command provided by a Sublime Text plugin.
        + ``cmd_args`` (:py:class:`dict`): Arguments passed to the command defined in ``cmd``.
        + ``file_extensions`` (:py:class:`list`): A list of file extensions. The command defined in ``cmd`` will only execute on those opened files that matches these file extensions.
        + ``view_syntaxes`` (:py:class:`list`): A list of file syntaxes. The command defined in ``cmd`` will only execute on those opened files that matches these file syntaxes.
        + ``strict_syntax`` (:py:class:`bool`): If **true**, it will compare exact matches of file syntaxes. If not specified, **false**.

            .. note::

                The arguments ``file_extensions`` and ``view_syntaxes`` are mutually exclusive. Either one or the other can be specified, but not both. If both are specified, only ``file_extensions`` will be used. If neither is specified, a command will run in **all opened files without exceptions**.

    - ``odyseus_run_multiple_cmds``: Command that allows to execute multiple Sublime Text commands. Available arguments:

        + ``commands`` (:py:class:`list`) (**Required**): A list of command definitions just like one would use in Sublime Text's command/menu/keybinding files. Possible keys:

            * ``command`` (:py:class:`str`) (**Required**): The command to execute.
            * ``args`` (:py:class:`dict`): The arguments to pass to the executed command.
            * ``context`` (:py:class:`str`): By default all commands will be executed using a ``sublime.Window`` as context. Here one can specify ``view`` to change the context to a ``sublime.View`` or ``app`` to change the context to the ``sublime`` module.

Run command in all views
========================

- This command should not be used to create commands that can be executed from Sublime's command palette or from keyboard shortcuts. Imagine accidentally executing a command that can modify hundreds of opened files?!
- This command doesn't have configurable settings and can only be used when defined in Sublime menu/command files. Example:

    .. code-block:: javascript

        [{
            "caption": "Remove trailing spaces from all opened files",
            "command": "odyseus_run_cmd_in_all_views",
            "args": {
                "cmd": "delete_trailing_spaces"
            }
        }]

Run multiple commands
=====================


.. code-block:: javascript

    [{
        "caption": "Format Python files",
        "command": "odyseus_run_multiple_cmds",
        "args": {
            "commands": [{
                "command": "odyseus_fix_python_imports"
            }, {
                "command": "odyseus_code_formatter",
                "args": {
                    "cmd_id": "odyseus_autopep8"
                }
            }]
        }
    }]

.. code-block:: javascript

    // NOTE: this command will not work with Sublime Text's ``chain`` command.
    [{
        "caption": "OdyseusPlugins: Test chain command",
        "command": "odyseus_run_multiple_cmds",
        "args": {
            "commands": [{
                "command": "select_all"
            }, {
                "command": "copy"
            }, {
                "command": "new_file"
            }, {
                "command": "paste"
            }, {
                "command": "save"
            }]
        }
    }]
