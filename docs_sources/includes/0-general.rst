
*************
General notes
*************

Commands added by plugins
=========================

All internal command's classes are prefixed with my nickname (Odyseus) or a variation of it to prevent clashes with other plugins or with Sublime's own Python code.

Plugin settings
===============

All plugin's settings can be overridden at project level. Plugin settings should be stored in a project settings inside a key called ``OdyseusSublimePlugins``. Example:

.. code-block:: javascript

    "folders": [],
    "settings": {
        "OdyseusSublimePlugins": {
            // Plugin settings as found in OdyseusSublimePlugins.sublime-settings.
        }
    }

.. contextual-admonition::
    :context: warning
    :title: Project settings limitations

    - Changing settings at a project level can only be done by editing the project settings file.
    - If a setting is tied to a Sublime Text menu item, attempting to change the setting from it will have no effect once the setting is defined inside the project settings file. Menu items will still reflect the setting value set inside the project settings file.

.. _commands-visibility-note-reference:

Commands visibility options
===========================

These options (normally called simply ``is_visible``) set the visibility of the commands (context menu item, command palette, etc.). It could be a boolean or a string.

- ``false``: Never display the command. Useful to hide default commands that one might not want to use nor to occupy space in a menu.
- ``true``: Ignore any command logic and display the command.
- Any other value of any type: Display command depending on the command's logic. How the value of the setting will be used to set a command visibility differs from command to command. Read each commands documentations for details.

.. _command-executables-note-reference:

Command executables settings
============================

Settings storing executables (normally called simply ``exec_map``) are defined as dictionaries containing platform names (``linux``, ``osx``, and ``windows``). Example:

.. code-block:: javascript

    "search_with_zeal.exec_map": {
        "linux": [
            "/usr/bin/zeal"
        ],
        "osx": [],
        "windows": [
            "C:\\Program Files\\Zeal\\zeal.exe"
        ]
    }

- The command that will be executed will depend on the platform in which Sublime Text is used.
- Each of these platform settings is a list that can contain one or more command definitions. Depending on the plugin, commands could be defined as simple strings (a command name or path), or more complex definitions.
- Upon execution, a plugin will check for all commands existence in a system and execute the first one that checked as existent.

.. _common-variables-substitution-reference:

Variables substitution
======================

Some plugins' settings might make use of certain variables that can be replaced at run time. This can be very convenient for defining relative paths to executables, relative paths to configuration files, etc.

In addition to the following common variables set by Sublime Text, system environment variables are also expanded (see :py:meth:`os.path.expandvars` and :py:meth:`os.path.expanduser`).

A plugin can also define and use extra variables. See each plugin's documentation for details.

- ``${packages}``: The absolute path of the **Packages** folder inside a Sublime Text configuration directory.
- ``${file}``: The absolute path of a file's view. **(*)**
- ``${file_path}``: The absolute path of the folder containing a file's view. **(*)**
- ``${folder}``: A project's root folder. **(*)**
- ``${file_name}``: The full name of a file. **(*)**
- ``${file_base_name}``: The file name without its extension name. **(*)**
- ``${file_extension}``: The file extension name. **(*)**
- ``${canonical_filename}``: Same as ``file_name`` or an auto-generated name ``<untitled {buffer_id}>``.

.. warning::

    **(*)** If a suitable path can't be ascertained, the variables will not be replaced.
