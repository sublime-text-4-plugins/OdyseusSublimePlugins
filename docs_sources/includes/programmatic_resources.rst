
*****************************
Programmatic resources plugin
*****************************

Keybindings and commands definitions using Python.

.. contextual-admonition::
    :context: info
    :title: Mentions

    - Based on `Sublime Text Programmatic Key Bindings <https://github.com/VonHeikemen/sublime-pro-key-bindings>`__.

.. contextual-admonition::
    :context: info
    :title: Available command

    - ``odyseus_programmatic_resources_compiler``: Deselect selections.

.. note::

    I implemented this plugin mainly for two reasons.

    #. I don't like bloatware. This plugin adds more than 30 commands, which would require to define about 30 commands in a ``sublime-commands`` file and also about 30 keybindings in a ``sublime-keymap`` file. Adding that many commands/keybindings would be the definition of bloatware; and trying to hide them or override them if I add them in a standard way would be a chore.
    #. I despise editing JSON files; even the JSON implementation used by Sublime Text which is more permissive/less *bitchy*.
