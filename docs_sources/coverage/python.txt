Undocumented Python objects
===========================
plugins.characters_table
------------------------
Functions:
 * insert_char

plugins.run_commands
--------------------
Classes:
 * OdyseusRunMultipleCmdsOverlayCommand -- missing methods:

   - run

plugins.strings_replacer
------------------------
Classes:
 * OperationsTextInputHandler -- missing methods:

   - initial_text
   - preview

python_utils.colour.future_colour
---------------------------------
Classes:
 * Format

python_utils.sublime_text_utils.completions
-------------------------------------------
Functions:
 * get_kind

