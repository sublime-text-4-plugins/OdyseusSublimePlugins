python\_utils.sublime\_text\_utils.weakmethod module
====================================================

.. automodule:: python_utils.sublime_text_utils.weakmethod
   :members:
   :private-members:
