python\_utils.parso.python package
==================================

.. automodule:: python_utils.parso.python
   :members:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   python_utils.parso.python.diff
   python_utils.parso.python.errors
   python_utils.parso.python.parser
   python_utils.parso.python.pep8
   python_utils.parso.python.prefix
   python_utils.parso.python.token
   python_utils.parso.python.tokenize
   python_utils.parso.python.tree
