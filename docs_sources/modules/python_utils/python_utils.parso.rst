python\_utils.parso package
===========================

.. automodule:: python_utils.parso
   :members:
   :private-members:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   python_utils.parso.pgen2
   python_utils.parso.python

Submodules
----------

.. toctree::
   :maxdepth: 4

   python_utils.parso._compatibility
   python_utils.parso.cache
   python_utils.parso.file_io
   python_utils.parso.grammar
   python_utils.parso.normalizer
   python_utils.parso.parser
   python_utils.parso.tree
   python_utils.parso.utils
