python\_utils.mistune.plugins.footnotes module
==============================================

.. automodule:: python_utils.mistune.plugins.footnotes
   :members:
   :private-members:
