python\_utils.schemas package
=============================

.. automodule:: python_utils.schemas
   :members:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   python_utils.schemas.mail_system_schema
