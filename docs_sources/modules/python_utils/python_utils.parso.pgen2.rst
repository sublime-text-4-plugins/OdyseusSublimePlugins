python\_utils.parso.pgen2 package
=================================

.. automodule:: python_utils.parso.pgen2
   :members:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   python_utils.parso.pgen2.generator
   python_utils.parso.pgen2.grammar_parser
