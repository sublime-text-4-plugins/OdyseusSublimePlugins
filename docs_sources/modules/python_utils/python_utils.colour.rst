python\_utils.colour package
============================

.. automodule:: python_utils.colour
   :members:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   python_utils.colour.color_maps
   python_utils.colour.constants
   python_utils.colour.future_colour
   python_utils.colour.utils
