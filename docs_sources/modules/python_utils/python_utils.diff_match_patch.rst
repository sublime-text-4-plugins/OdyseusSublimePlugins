python\_utils.diff\_match\_patch package
========================================

.. automodule:: python_utils.diff_match_patch
   :members:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   python_utils.diff_match_patch.diff_match_patch
