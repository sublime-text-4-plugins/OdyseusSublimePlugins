plugins.console\_wrap package
=============================

.. automodule:: plugins.console_wrap
   :members:
   :private-members:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   plugins.console_wrap.wrappers
