plugins package
===============

.. automodule:: plugins
   :members:
   :private-members:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   plugins.better_find_results
   plugins.characters_table
   plugins.cheatsheets
   plugins.code_formatter
   plugins.console_wrap
   plugins.message_panel
   plugins.packages_ui

Submodules
----------

.. toctree::
   :maxdepth: 4

   plugins.better_bookmarks
   plugins.case_conversion
   plugins.center_comment
   plugins.compare_open_files
   plugins.data_generation
   plugins.delete_blank_lines
   plugins.display_numbers
   plugins.escape_regex
   plugins.file_size
   plugins.find_plus_plus
   plugins.html_to_text
   plugins.json_to_from_yaml
   plugins.keyword_search
   plugins.programmatic_resources
   plugins.python_utilities
   plugins.run_commands
   plugins.search_with_zeal
   plugins.sidebar_context_commands
   plugins.smart_keystrokes
   plugins.strings_replacer
   plugins.toggle_quotes
   plugins.toggle_words
   plugins.trailing_spaces
   plugins.word_highlight
