plugins.delete\_blank\_lines module
===================================

.. automodule:: plugins.delete_blank_lines
   :members:
   :private-members:
