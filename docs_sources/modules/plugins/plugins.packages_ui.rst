plugins.packages\_ui package
============================

.. automodule:: plugins.packages_ui
   :members:
   :private-members:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   plugins.packages_ui.package_manager

Submodules
----------

.. toctree::
   :maxdepth: 4

   plugins.packages_ui.data
