plugins.console\_wrap.wrappers package
======================================

.. automodule:: plugins.console_wrap.wrappers
   :members:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   plugins.console_wrap.wrappers.go
   plugins.console_wrap.wrappers.js
   plugins.console_wrap.wrappers.php
   plugins.console_wrap.wrappers.py
