# -*- coding: utf-8 -*-
from __future__ import annotations

import os

import sublime
import sublime_plugin

from .plugins import OdyseusPluginsToggleConsolePersistenceCommand  # noqa
from .plugins import OdyseusPluginsToggleLoggingLevelCommand  # noqa
from .plugins import display_message_in_panel
from .plugins import events
from .plugins import logger
from .plugins import root_folder
from .plugins import settings
from python_utils.sublime_text_utils import settings as settings_utils

all_plugins: list[str] = sorted(
    [
        entry.name.replace(".py", "")
        for entry in os.scandir(os.path.join(root_folder, "plugins"))
        if all(
            (
                entry.name.endswith(".py") or entry.is_dir(),
                not entry.name.endswith("~"),
                not entry.name.startswith("_"),
                not entry.name.endswith(".md"),
            )
        )
    ]
)

# CRITICAL: Do not try again any other method to load plugins. I tried several methods to load them
# dynamically to be able to reload them on demand whitout the need to restart the editor.
# None of them work 100%; so, restart the f*cking editor and move on.
for plugin in all_plugins:
    module: str = f".plugins.{plugin}"

    try:
        exec(f"from {module} import *")
    except Exception as err:
        logger.error(err)
        display_message_in_panel(
            title=f"OdyseusPlugins: Error loading {module} module", body=str(err)
        )


class OdyseusSublimePluginsProjectSettingsController(settings_utils.ProjectSettingsController):
    """Project settings controller.
    """
    @settings_utils.distinct_until_buffer_changed
    def on_post_save_async(self, view: sublime.View) -> None:
        """Called after a view has been saved.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        self._on_post_save_async(view, settings)


class SublimeConsoleController(sublime_plugin.EventListener):
    """Keep Sublime Text console always open on debug mode."""

    def _ody_display_console(self, view: sublime.View) -> None:
        if (
            view
            and view.is_valid()
            and settings.get("general.persist_console")
            and not view.settings().get("is_widget")
            and not view.settings().get("edit_settings_view")
            and view.window()
            and view.window().active_panel() != "console"
        ):
            view.window().run_command("show_panel", {"panel": "console"})

    def on_activated_async(self, view: sublime.View) -> None:
        self._ody_display_console(view)

    def on_new_async(self, view: sublime.View) -> None:
        self._ody_display_console(view)


def plugin_loaded() -> None:
    """On plugin loaded callback."""
    events.broadcast("plugin_loaded")


def plugin_unloaded() -> None:
    """On plugin unloaded."""
    events.broadcast("plugin_unloaded")


if __name__ == "__main__":
    pass
