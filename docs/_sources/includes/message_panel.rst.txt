
********************
Message panel plugin
********************

Plugin to display messages in a Sublime Text panel.

.. contextual-admonition::
    :context: info
    :title: Mentions

    Plugin based on `SublimeLinter <https://github.com/SublimeLinter/SublimeLinter>`__.

.. contextual-admonition::
    :context: info
    :title: Available commands

    - ``odyseus_display_message_in_panel``: Possible arguments:

        + ``msg`` (:py:class:`str`): The message to display in the panel.

    - ``odyseus_clear_message_panel``: Clear the message panel.
    - ``odyseus_remove_message_panel``: Destroy the message panel.
    - ``odyseus_toggle_message_panel``: Open/Close the message panel.

- A custom syntax is used (basically, a Markdown syntax with a limited set of Markdown markup) to highlight the panel view. The syntax is accompanied by a settings file that disables certain features to make the panel view *light on the eyes*.
- The messages in the panel are time stamped and persistent for the life time of the panel (not across Sublime Text sessions).
- Certain paths (marked up like Markdown links. e.g. ``[File](/path/to/file)``) are specially handled. They can be opened in Sublime Text by double clicking on them. They will also have a tooltip with the options **Edit file** (same as double clicking on it) and **Launch file** (to open the file in the system's file handler for that file).
