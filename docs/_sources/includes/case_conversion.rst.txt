
**********************
Case conversion plugin
**********************

Case conversion plugin.

.. contextual-admonition::
    :context: info
    :title: Mentions

    Plugin based on `Case Conversion <https://github.com/jdavisclark/CaseConversion>`__.

.. contextual-admonition::
    :context: info
    :title: Available commands

    - ``odyseus_convert_case``: Convert selections case. Possible arguments:

        + ``case_func`` (:py:class:`str`) (**Required**): A function name. It must be the name of an existent function on the ``case_conversion`` module. See :ref:`Available case conversion functions <available-case-conversion-functions-reference>`.
        + ``acronyms``(:py:class:`bool`, :py:class:`list`): List of acronyms. If :py:class:`None` or :py:class:`False`, don't use acronnyms. If True, use the value of the ``case_conversion.acronyms`` setting. If :py:class:`list`, use the specified acronyms. See :ref:`plugin settings <case-convertion-setting-reference>`.

    - ``odyseus_toggle_snake_camel_pascal``: Toggle selections case between snake/camel/pascal case. Possible arguments:

        + ``acronyms``: Same as ``odyseus_convert_case > acronyms``.

.. _case-convertion-setting-reference:

Plugin settings
===============

Settings for this plugin are prefixed with ``case_conversion.``. All of these settings can be overridden when defining commands/menus by specifying command arguments with the same name as a setting. Possible options:

- ``acronyms`` (:py:class:`list`): Is a list of words that are to be considered acronyms. Valid acronyms contain only upper and lower-case letters, and digits. Invalid words are ignored, and valid words are converted to upper-case. Order matters; words earlier in the list will be selected before words later in the list. For example, if "UI" were to be put before "GUI", then "GUI" would never be selected, because the "UI" in "GUI" would always be selected first.

Usage
=====

- Defining commands/menus in **.sublime-menu**, **.sublime-commands** or **.sublime-keymap** files:

    + Toggle snake/camel/pascal case keyboard shortcut:

        .. code-block:: javascript

            [{
                "keys": ["ctrl+k", "ctrl+s"],
                "command": "odyseus_toggle_snake_camel_pascal"
            }]

    + Convert to dash case command/menu definition:

        .. code-block:: javascript

            [{
                "caption": "Convert to dash case",
                "command": "odyseus_convert_case",
                "args": {
                    "case_func": "dash"
                }
            }]

.. _available-case-conversion-functions-reference:

.. contextual-admonition::
    :context: info
    :title: Available case conversion functions

    - ``ada``: Return text in Ada_Case style.
    - ``backslash``: Return text in backslash\\case style.
    - ``camel``: Return text in camelCase style.
    - ``capital``: Return text in Capital case style.
    - ``const``: Return text in CONST_CASE style (aka SCREAMING_SNAKE_CASE).
    - ``dash``: Return text in dash-case style (aka kebab-case, spinal-case).
    - ``dot``: Return text in dot.case style.
    - ``http_header``: Return text in Http-Header-Case style.
    - ``lower``: Return text in lowercase style.
    - ``pascal``: Return text in PascalCase style (a.k.a. MixedCase).
    - ``separate_words``: Return text in "separate words" style.
    - ``slash``: Return text in slash/case style.
    - ``snake``: Return text in snake_case style.
    - ``title``: Return text in Title_case style.
    - ``upper``: Return text in UPPERCASE style.
