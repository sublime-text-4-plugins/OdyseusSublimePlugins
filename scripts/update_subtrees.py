#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from subprocess import run

root_folder = os.path.realpath(
    os.path.abspath(
        os.path.join(
            os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir))
        )
    )
)


if __name__ == "__main__":
    cmd1 = "git subtree pull --prefix docs_sources/common_rest_abbreviations git@gitlab.com:Odyseus/common_rest_abbreviations.git master --squash"
    cmd2 = "git subtree pull --prefix docs_sources/python_mocks git@gitlab.com:Odyseus/python_mocks.git main --squash"
    cmd3 = "git subtree pull --prefix docs_sources/sphinx_extensions git@gitlab.com:Odyseus/sphinx_extensions.git master --squash"
    cmd4 = "git subtree pull --prefix docs_sources/sphinx_rtd_theme_mod git@gitlab.com:Odyseus/sphinx_rtd_theme_mod.git master --squash"

    os.chdir(root_folder)
    run(cmd1, shell=True)
    run(cmd2, shell=True)
    run(cmd3, shell=True)
    run(cmd4, shell=True)
