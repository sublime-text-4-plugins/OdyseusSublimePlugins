#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Summary

Attributes
----------
docopt_doc : TYPE
    Description
root_folder : TYPE
    Description
sublime_text_config_folder : TYPE
    Description
sublime_text_lib_folder : TYPE
    Description
"""
import os
import sys

root_folder = os.path.realpath(
    os.path.abspath(
        os.path.join(os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir)))
    )
)
sublime_text_config_folder = os.path.normpath(os.path.join(root_folder, *([os.pardir] * 2)))
sublime_text_lib_folder = os.path.join(sublime_text_config_folder, "Lib", "python38")

sys.path.insert(0, sublime_text_lib_folder)

from python_utils import logging_system
from python_utils import sphinx_docs_utils
from python_utils.docopt import docopt


def generate_docs(
    build_coverage=False,
    build_doctest=False,
    generate_api_docs=False,
    force_clean_build=False,
    generate_html=False,
):
    """See :any:`sphinx_docs_utils.generate_docs`

    Parameters
    ----------
    build_coverage : bool, optional
        Description
    generate_api_docs : bool, optional
        See :any:`sphinx_docs_utils.generate_docs`.
    force_clean_build : bool, optional
        See :any:`sphinx_docs_utils.generate_docs`.
    generate_html : bool, optional
        Description

    Done
    ----
    Investigate if I can *fake* the ``sublime`` and ``sublime_plugin`` modules so I can document the
    plugin APIs inside the plugins folder.
    """
    prefix = "generate_docs-CLI"
    logger = logging_system.Logger(
        logger_name=prefix,
        stream_handler_formatter_options={"format_str": "%(message)s"},
        use_file_handler=os.path.join(root_folder, "tmp", "logs"),
    )

    ignored_apidoc_modules = [
        os.path.join(root_folder, "__init__.py"),
        os.path.join(root_folder, "plugins", "_base_plugin.py"),
        os.path.join(sublime_text_lib_folder, "python_utils", "bottle.py"),
        os.path.join(sublime_text_lib_folder, "python_utils", "demjson3"),
        os.path.join(sublime_text_lib_folder, "python_utils", "lazy_import"),
        os.path.join(sublime_text_lib_folder, "python_utils", "polib.py"),
        os.path.join(sublime_text_lib_folder, "python_utils", "pyperclip"),
        os.path.join(sublime_text_lib_folder, "python_utils", "pypng.py"),
        os.path.join(sublime_text_lib_folder, "python_utils", "sublime_text_utils", "sublime_lib"),
        os.path.join(sublime_text_lib_folder, "python_utils", "svgelements"),
        os.path.join(sublime_text_lib_folder, "python_utils", "tqdm"),
        os.path.join(sublime_text_lib_folder, "python_utils", "yaml"),
        # The following module has perfectly valid docstrings, but Sphinx is being a
        # b*tch and throws a million warnings for no reason.
        # Ignore it until Sphinx gets its sh*t together.
        os.path.join(sublime_text_lib_folder, "python_utils", "tqdm_wget.py"),
    ]

    base_apidoc_dest_path_rel_to_root = os.path.join("docs_sources", "modules")

    apidoc_paths_rel_to_root = [
        (
            "plugins",
            os.path.join(base_apidoc_dest_path_rel_to_root, "plugins"),
        ),
        (
            os.path.join(sublime_text_lib_folder, "python_utils"),
            os.path.join(base_apidoc_dest_path_rel_to_root, "python_utils"),
        ),
        (
            os.path.join(root_folder, "docs_sources", "python_mocks", "sublime_text", "sublime"),
            os.path.join(base_apidoc_dest_path_rel_to_root, "sublime"),
        ),
        (
            os.path.join(
                root_folder, "docs_sources", "python_mocks", "sublime_text", "sublime_plugin"
            ),
            os.path.join(base_apidoc_dest_path_rel_to_root, "sublime_plugin"),
        ),
    ]

    sphinx_docs_utils.generate_docs(
        root_folder=root_folder,
        docs_src_path_rel_to_root="docs_sources",
        docs_dest_path_rel_to_root="docs",
        apidoc_paths_rel_to_root=apidoc_paths_rel_to_root,
        ignored_modules=ignored_apidoc_modules,
        doctree_temp_location_rel_to_sys_temp="OdyseusSublimePlugins-doctrees",
        update_inventories=False,
        generate_html=generate_html,
        build_coverage=build_coverage,
        build_doctest=build_doctest,
        generate_api_docs=generate_api_docs,
        force_clean_build=force_clean_build,
        logger=logger,
    )


docopt_doc = """Development utilities

Usage:
<usage>
    app.py (-h | --help | --version)
    app.py (generate_all | generate_api_docs |
            build_coverage | generate_html |
            build_doctest) [-f | --force-clean-build]
</usage>

Options:

<options>
-h, --help
    Show this application basic help.

--version
    Show application version.

-f, --force-clean-build
    Force clean build by removing all generated and cached data
    of previous builds.
</options>

Commands:

generate_all
    Build all except doctest.

generate_api_docs
    Generate API docs.

build_coverage
    Build coverage.

generate_html
    Generate HTML.

build_doctest
    Build doctest.

"""


if __name__ == "__main__":
    if len(sys.argv) == 1:
        sys.argv.append("--help")

    args = docopt(docopt_doc, version="Ω")

    if args["generate_all"]:
        generate_docs(
            generate_api_docs=True,
            build_coverage=True,
            generate_html=True,
            force_clean_build=args["--force-clean-build"],
        )

    if args["generate_api_docs"]:
        generate_docs(generate_api_docs=True)

    if args["build_coverage"]:
        generate_docs(build_coverage=True, force_clean_build=args["--force-clean-build"])

    if args["generate_html"]:
        generate_docs(generate_html=True, force_clean_build=args["--force-clean-build"])

    if args["build_doctest"]:
        generate_docs(build_doctest=True, force_clean_build=args["--force-clean-build"])
