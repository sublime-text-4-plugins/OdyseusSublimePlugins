# -*- coding: utf-8 -*-


def keymap(bind, cmd):
    # Sublime Text native commands.
    bind(["f7"], "toggle_minimap")
    bind(["f4"], "toggle_side_bar")
    bind(["f5"], "toggle_show_open_files")
    bind(["ctrl+shift+c"], "toggle_comment", block=False)
    bind(["ctrl+alt+c"], "toggle_comment", block=True)
    bind(["ctrl+'"], "show_panel", panel="console", toggle=True)
    bind(["ctrl+k", "ctrl+t"], "title_case")

    # SublimeLinter plugin.
    bind(["ctrl+shift+alt+l"], "sublimelinter_lint")
    bind(["super+n"], "sublime_linter_goto_error", direction="next", wrap=True)
    bind(["ctrl+k", "a"], "sublime_linter_panel_toggle")

    # StringEncode plugin.
    bind(["ctrl+alt+shift+h"], "html_entitize")
    bind(["ctrl+alt+shift+x"], "xml_entitize")
    bind(["ctrl+alt+e"], "base64_encode")
    bind(["ctrl+alt+d"], "base64_decode")

    # ProjectManager plugin.
    bind(["ctrl+alt+p"], "project_manager", action="new")

    # OdyseusSublimePlugins.plugins.better_bookmarks.py plugin.
    bind(["ctrl+f2"], "odyseus_better_bookmarks", subcommand="mark_line")
    bind(["f2"], "odyseus_better_bookmarks", subcommand="cycle_mark", direction="next")
    bind(["ctrl+f3"], "odyseus_better_bookmarks", subcommand="layer_swap", direction="next")
    bind(["ctrl+shift+f3"], "odyseus_better_bookmarks", subcommand="clear_marks")

    # OdyseusSublimePlugins.plugins.characters_table.py plugin.
    bind(["ctrl+k", "ctrl+k", "ctrl+k"], "odyseus_ct_toggle_digraph_keymap")
    bind(["ctrl+k", "ctrl+n"], "odyseus_ct_characters_table_menu", mode="unicode_lookup")
    bind(["ctrl+k", "ctrl+k", "ctrl+n"], "odyseus_ct_characters_table_menu", mode="unicode_input")
    bind(["ctrl+k", "ctrl+j"], "odyseus_ct_characters_table_menu", mode="win_alt_code_lookup")
    bind(["ctrl+k", "ctrl+k", "ctrl+j"], "odyseus_ct_characters_table_menu", mode="win_alt_code_input")

    # OdyseusSublimePlugins.plugins.console_wrap.py plugin.
    bind(["ctrl+shift+q"], "odyseus_console_wrap", insert_before=False)
    bind(["ctrl+shift+alt+q"], "odyseus_console_wrap", insert_before=True)

    # OdyseusSublimePlugins.plugins.toggle_quotes.py plugin.
    # NOTE: This key binding overrides default key binding to quit application.
    # I always f*cking hated that!! So, good riddance!!
    bind(["ctrl+q"], "odyseus_toggle_quotes")

    # OdyseusSublimePlugins.plugins.escape_regex.py plugin.
    bind(["ctrl+shift+e"], "odyseus_regex_escape")

    # OdyseusSublimePlugins.plugins.strings_replacer.py plugin.
    bind(["ctrl+,"], "odyseus_strings_replacer_menu", mode="lookup")
    bind(["ctrl+."], "odyseus_strings_replacer_menu", mode="input")

    # OdyseusSublimePlugins.plugins.toggle_words.py plugin.
    bind(["ctrl+t"], "odyseus_toggle_word")

    # OdyseusSublimePlugins.plugins.text_debugging.py plugin.
    bind(["alt+d"], "odyseus_text_debugging_python", selector_python)

    # OdyseusSublimePlugins.plugins.trailing_spaces.py plugin.
    bind(["ctrl+shift+t"], "odyseus_ts_delete_trailing_spaces")

    # OdyseusSublimePlugins.plugins.case_conversion.py plugin.
    bind(["ctrl+k", "ctrl+d"], "odyseus_convert_case", case_func="dash", acronyms=True)
    bind(["ctrl+k", "ctrl+s"], "odyseus_toggle_snake_camel_pascal")

    # OdyseusSublimePlugins.plugins.keyword_search.py plugin.
    bind(["alt+super+b"], "odyseus_keyword_search")
    bind(["ctrl+super+b"], "odyseus_keyword_search_input")
    bind(
        ["super+d", "super+d"],
        "odyseus_keyword_search",
        selector_python,
        search_method="!py3local",
    )

    # OdyseusSublimePlugins.plugins.smart_keystrokes.py plugin.
    bind(["backspace"], "odyseus_smart_backspace")
    bind(["shift+backspace"], "odyseus_normal_backspace")
    bind(["delete"], "odyseus_smart_delete")
    bind(["escape"], "odyseus_deselect", selection_not_empty)

    # OdyseusSublimePlugins.plugins.compare_open_files.py plugin.
    bind(["super+c"], "odyseus_compare_two_files")
    bind(["super+ctrl+c"], "odyseus_compare_three_files")

    # plugins.search_with_zeal.py plugin.
    # bind(["super+z"], "odyseus_search_with_zeal_selection")
    # bind(["super+ctrl+z"], "odyseus_search_with_zeal")
    bind(
        ["super+z"],
        "show_overlay",
        overlay="command_palette",
        command="odyseus_zeal_search_selection",
    )
    bind(
        ["super+ctrl+z"],
        "show_overlay",
        overlay="command_palette",
        command="odyseus_zeal_search",
    )

    # OdyseusSublimePlugins.plugins.message_panel plugin.
    bind(["ctrl+k", "ctrl+m"], "odyseus_toggle_message_panel")
    bind(["ctrl+k", "ctrl+c"], "odyseus_clear_message_panel")

    # OdyseusSublimePlugins.plugins.file_size.py plugin.
    bind(["ctrl+alt+s"], "odyseus_update_file_size_status")

    # OdyseusSublimePlugins.plugins.packages_ui plugin.
    bind(["t"], "odyseus_pui_toggle_package", selector_pui)
    bind(["r"], "odyseus_pui_render_list", selector_pui)
    bind(["u"], "odyseus_pui_update_packages_storage", selector_pui)
    bind(["i"], "odyseus_pui_show_info", selector_pui)
    bind(["+"], "odyseus_pui_change_font_size", selector_pui, increment=True)
    bind(["-"], "odyseus_pui_change_font_size", selector_pui, increment=False)
    bind(["0"], "odyseus_pui_change_font_size", selector_pui)
    bind(["h"], "odyseus_pui_open_homepage", selector_pui)
    bind(["d"], "odyseus_pui_toggle_defaults_visibility", selector_pui)

    # OdyseusSublimePlugins.plugins.better_find_results plugin.
    # NOTE: The setting better_find_results.keymap_disable_all can be set in the
    # syntax settings file plugins/better_find_results/Find Results.sublime-settings.
    # It will disable all keybindings without the need to modify the keybindings themselves.
    bind(["ctrl+c"], "odyseus_bfr_copy_from_find_results", selector_bfr)
    bind(["enter"], "odyseus_bfr_find_in_files_open_file", selector_bfr)
    bind(["ctrl+a"], "odyseus_bfr_find_in_files_open_all_files", selector_bfr)
    bind(["down"], "odyseus_bfr_find_in_files_jump_file", selector_bfr)
    bind(["up"], "odyseus_bfr_find_in_files_jump_file", selector_bfr, forward=False)
    bind(["right"], "odyseus_bfr_find_in_files_jump_match", selector_bfr)
    bind(["left"], "odyseus_bfr_find_in_files_jump_match", selector_bfr, forward=False)
    bind(["ctrl+right"], "odyseus_bfr_fold_and_move_to_file", selector_bfr)
    bind(["ctrl+left"], "odyseus_bfr_fold_and_move_to_file", selector_bfr, forward=False)
    bind(["?"], "odyseus_bfr_open_popup_help", selector_bfr)

    # Various.
    bind(["super+v"], [cmd("odyseus_html_to_text"), cmd("html_entitize")])

    # Formatters
    formatters_data = [
        ("odyseus_code_formatter", selector_c, {"cmd_id": "odyseus_clang_format"}),
        (
            "odyseus_code_formatter",
            selector_php,
            {"cmd_id": "php_code_beautifier", "ignore_selection": True},
        ),
        (
            "odyseus_code_formatter",
            selector_css,
            {"cmd_id": "odyseus_css_prettier"},
        ),
        (
            "odyseus_code_formatter",
            selector_js,
            {"cmd_id": ["odyseus_js_es_formatter", "odyseus_js_beautify"]},
        ),
        (
            "odyseus_code_formatter",
            selector_json,
            {"cmd_id": "odyseus_js_beautify"},
        ),
        (
            "odyseus_code_formatter",
            selector_lua,
            {"cmd_id": "lua_formatter", "ignore_selection": True},
        ),
        (
            "odyseus_code_formatter",
            selector_yaml,
            {"cmd_id": "odyseus_yaml_prettier"},
        ),
        (
            "odyseus_code_formatter",
            selector_html,
            {"cmd_id": "odyseus_html_prettifier_prettier"},
        ),
    ]
    for c, s, k in formatters_data:
        bind(
            ["ctrl+alt+f"],
            c,
            s,
            **k,
        )
        k["diff"] = True
        bind(
            ["ctrl+shift+alt+f"],
            c,
            s,
            **k,
        )
    bind(
        ["ctrl+alt+g"],
        "odyseus_code_formatter",
        selector_js,
        cmd_id="odyseus_js_es_formatter",
    )
    bind(
        ["ctrl+alt+g"],
        "odyseus_code_formatter",
        selector_css,
        cmd_id="odyseus_css_autoprefixer",
    )
    # NOTE: I use both formatters chained (Black and Autopep8) because I don't trust Black nor any
    # other opinionated code formatter. But I'm forced to tolerate it because Autopep8 doesn't provide
    # certain features like double quotes enforcing, etc. I'm still struggling with Black's
    # expansion to f*cking infinity of objects, compression of expanded objects, the f*cking magic
    # comma, etc.
    bind(
        ["ctrl+alt+f"],
        [
            # cmd("odyseus_fix_python_imports"),
            cmd(
                "odyseus_code_formatter",
                cmd_id=["odyseus_fix_python_imports", "odyseus_black_formatter", "odyseus_autopep8"],
            ),
        ],
        selector_python,
    )
    bind(
        ["ctrl+shift+alt+f"],
        [
            # cmd("odyseus_fix_python_imports"),
            cmd(
                "odyseus_code_formatter",
                cmd_id=["odyseus_fix_python_imports", "odyseus_black_formatter", "odyseus_autopep8"],
                diff=True,
            ),
        ],
        selector_python,
    )


# Contexts
overlay_visible = {"key": "overlay_visible", "operator": "equal", "operand": True}
selection_not_empty = {
    "key": "selection_empty",
    "operator": "equal",
    "operand": False,
    "match_all": True,
}
selector_bfr = [
    {"key": "selector", "operator": "equal", "operand": "text.find-in-files"},
    {
        "key": "setting.better_find_results.keymap_disable_all",
        "operator": "not_equal",
        "operand": True,
    },
]
selector_pui = [
    {"key": "selector", "operator": "equal", "operand": "text.plist"},
    {
        "key": "setting.packages_ui.keymap_disable_all",
        "operator": "not_equal",
        "operand": True,
    },
]
selector_c = {
    "key": "selector",
    "operator": "equal",
    "operand": "source.glsl | source.c | source.c++",
}
selector_php = {"key": "selector", "operator": "equal", "operand": "source.php"}
selector_css = {
    "key": "selector",
    "operator": "equal",
    "operand": "source.scss | source.sass | source.css",
}
selector_js = {
    "key": "selector",
    "operator": "equal",
    "operand": "(source.js | source.html) - source.json",
}
selector_json = {
    "key": "selector",
    "operator": "equal",
    "operand": "source.json - (source.js | source.html)",
}
selector_lua = {
    "key": "selector",
    "operator": "equal",
    "operand": "source.luae | source.lua | source.menu",
}
selector_yaml = {"key": "selector", "operator": "equal", "operand": "source.yaml"}
selector_html = {
    "key": "selector",
    "operator": "equal",
    "operand": "(text.xml | text.html) - source.php",
}
selector_python = {"key": "selector", "operator": "equal", "operand": "source.python"}
