# -*- coding: utf-8 -*-
p = "OP: "


def commands(bind, cmd):
    # OdyseusSublimePlugins.plugins.packages_ui.py plugin.
    bind(p + "Packages UI", "odyseus_pui")

    # OdyseusSublimePlugins.plugins.code_formatter.py plugin.
    bind(
        p + "Convert to HTML5",
        "odyseus_code_formatter",
        cmd_id="odyseus_convert_to_html5",
    )

    # OdyseusSublimePlugins.plugins.fold_python_comments.py plugin.
    bind(p + "Fold Python docstrings", "odyseus_fold_python_docstrings")
    bind(p + "Unfold Python docstrings", "odyseus_unfold_python_docstrings")
    bind(p + "Toggle Python docstrings auto fold", "odyseus_toggle_python_docstrings_auto_fold")

    # OdyseusSublimePlugins.plugins.html_to_text.py plugin.
    bind(
        p + "Convert HTML to text whole file",
        "odyseus_convert_html_to_text",
        whole_file=True,
    )
    bind(
        p + "Convert HTML to text whole file (minimal markup)",
        "odyseus_convert_html_to_text",
        whole_file=True,
        parser_options="minimal_markup",
    )
    bind(p + "Convert HTML to text selection", "odyseus_convert_html_to_text")

    # OdyseusSublimePlugins.plugins.json_to_from_yam.py plugin.
    bind(
        p + "Convert JSON to YAML",
        "odyseus_convert_json_to_from_yaml",
        target_format="yaml",
    )
    bind(
        p + "Convert YAML to JSON",
        "odyseus_convert_json_to_from_yaml",
        target_format="json",
    )

    # OdyseusSublimePlugins.plugins.escape_regex.py plugin.
    bind(p + "Replace selection with regex escaped equivalent", "odyseus_regex_escape")

    # OdyseusSublimePlugins.plugins.strings_replacer.py plugin.
    bind(p + "Strings Replacer input", "odyseus_strings_replacer_menu", mode="input")

    # OdyseusSublimePlugins.plugins.case_conversion.py plugin.
    bind(p + "Toggle snake/camel/pascal case", "odyseus_toggle_snake_camel_pascal")

    # OdyseusSublimePlugins.plugins.trailing_spaces.py plugin.
    bind(p + "Delete trailing spaces", "odyseus_ts_delete_trailing_spaces")

    # OdyseusSublimePlugins.plugins.toggle_quotes.py plugin.
    bind(p + "Toggle quotes", "odyseus_toggle_quotes")

    # OdyseusSublimePlugins.plugins.toggle_words.py plugin.
    bind(p + "Toggle words", "odyseus_toggle_word")

    # OdyseusSublimePlugins.plugins.text_debugging.py plugin.
    bind(p + "Text debugging", "odyseus_text_debugging")

    # OdyseusSublimePlugins.plugins.fix_python_imports.py plugin.
    bind(p + "Fix Python imports", "odyseus_fix_python_imports")

    # OdyseusSublimePlugins.plugins.display_numbers.py plugin.
    bind(p + "Display numbers popup", "odyseus_dn_show_numbers_popup")
    bind(p + "Convert number to hexadecimal", "odyseus_dn_convert_number", base=16)
    bind(p + "Convert number to decimal", "odyseus_dn_convert_number", base=10)
    bind(p + "Convert number to octal", "odyseus_dn_convert_number", base=8)
    bind(p + "Convert number to binary", "odyseus_dn_convert_number", base=2)

    # OdyseusSublimePlugins.plugins.delete_blank_lines.py plugin.
    bind(p + "Delete blank lines", "odyseus_delete_blank_lines", surplus=False)
    bind(p + "Delete Surplus blank lines", "odyseus_delete_blank_lines", surplus=True)
    bind(
        p + "Delete blank lines (whole file)",
        "odyseus_delete_blank_lines",
        surplus=False,
        whole_file=True,
    )
    bind(
        p + "Delete Surplus blank lines (whole file)",
        "odyseus_delete_blank_lines",
        surplus=True,
        whole_file=True,
    )

    # OdyseusSublimePlugins.plugins.data_generation.py plugin.
    # Passwords generator.
    bind(p + "Generate alphanumeric password", "odyseus_password_generator_overlay")
    bind(
        p + "Generate secure password",
        "odyseus_password_generator_overlay",
        symbols=True,
    )

    # OdyseusSublimePlugins.plugins.data_generation.py plugin.
    # UUID generation.
    uuid_data = (
        # (label, single, short, uppercase)
        (" unique", 1, 0, 0),
        (" unique, without dashes", 1, 1, 0),
        (" unique, uppercase", 1, 0, 1),
        (" unique, uppercase, without dashes", 1, 1, 1),
        (" multiple", 0, 0, 0),
        (" multiple, without dashes", 0, 1, 0),
        (" multiple, uppercase", 0, 0, 1),
        (" multiple, uppercase, without dashes", 0, 1, 1),
    )
    for label, single, short, uppercase in uuid_data:
        bind(
            p + "Generate UUID v4" + label,
            "odyseus_generate_uuid",
            short=bool(short),
            single=bool(single),
            uppercase=bool(uppercase),
        )

    # OdyseusSublimePlugins.plugins.keyword_search.py plugin.
    bind(p + "Keyword Search", "odyseus_keyword_search")
    bind(p + "Keyword Search Input", "odyseus_keyword_search_input")

    # OdyseusSublimePlugins.plugins.find_plus_plus.py plugin.
    bind(p + "Find++ In Current File", "odyseus_fpp_find_in_current_file")
    bind(p + "Find++ In Current Folder", "odyseus_fpp_find_in_current_folder")
    bind(p + "Find++ In Open Files", "odyseus_fpp_find_in_open_files")
    bind(p + "Find++ In Project", "odyseus_fpp_find_in_project")
    bind(p + "Find++ In...", "odyseus_fpp_find_in_panel")

    # OdyseusSublimePlugins.plugins.console_wrap.py plugin.
    bind(p + "Console Wrap - Create logs", "odyseus_console_wrap")
    bind(
        p + "Console Wrap - Remove logs", "odyseus_console_wrap_action", action="remove"
    )
    bind(
        p + "Console Wrap - Remove Commented logs",
        "odyseus_console_wrap_action",
        action="remove_commented",
    )
    bind(
        p + "Console Wrap - Comment logs",
        "odyseus_console_wrap_action",
        action="comment",
    )
    bind(
        p + "Console Wrap - Show all logs",
        "odyseus_console_wrap_action",
        action="show_quick_nav",
    )

    # OdyseusSublimePlugins.plugins.characters_table.py plugin.
    bind(
        p + "CharacterTable - Toggle mnemonics keymap",
        "odyseus_ct_toggle_mnemonics_keymap",
    )
    bind(
        p + "CharacterTable - Toggle digraph keymap",
        "odyseus_ct_toggle_digraph_keymap",
    )
    bind(
        p + "CharacterTable -  Unicode input",
        "odyseus_ct_characters_table_menu",
        mode="unicode_input",
    )
    bind(
        p + "CharacterTable -  Windows alt code lookup",
        "odyseus_ct_characters_table_menu",
        mode="win_alt_code_lookup",
    )
    bind(
        p + "CharacterTable -  Windows alt code input",
        "odyseus_ct_characters_table_menu",
        mode="win_alt_code_input",
    )

    # OdyseusSublimePlugins.plugins.cheatsheets.py plugin.
    bind(
        p + "Cheatsheets",
        "odyseus_cheatsheets_overlay",
        category="*",
    )

    # OdyseusSublimePlugins.plugins.better_bookmarks.py plugin.
    bind(
        p + "BetterBookmarks - Previous Layer",
        "odyseus_better_bookmarks",
        direction="prev",
        subcommand="layer_swap",
    )
    bind(
        p + "BetterBookmarks - Next Layer",
        "odyseus_better_bookmarks",
        direction="next",
        subcommand="layer_swap",
    )
    bind(
        p + "BetterBookmarks - Previous Bookmark",
        "prev_bookmark",
    )
    bind(
        p + "BetterBookmarks - Next Bookmark",
        "next_bookmark",
    )
    bind(
        p + "BetterBookmarks - Add Bookmark",
        "odyseus_better_bookmarks",
        subcommand="mark_line",
    )
    bind(
        p + "BetterBookmarks - Clear Layer",
        "odyseus_better_bookmarks",
        subcommand="clear_marks",
    )
    bind(
        p + "BetterBookmarks - Clear All Bookmarks",
        "odyseus_better_bookmarks",
        subcommand="clear_all",
    )
