#!/usr/bin/env python3.10
# -*- coding: utf-8 -*-
"""Summary
"""
import os
import re
import sys


root_folder = os.path.realpath(
    os.path.abspath(
        os.path.join(os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir)))
    )
)
sublime_text_config_folder = os.path.normpath(os.path.join(root_folder, *([os.pardir] * 2)))
sublime_text_lib_folder = os.path.join(sublime_text_config_folder, "Lib", "python38")

sys.path.insert(0, sublime_text_lib_folder)

from python_utils import fix_imports

if __name__ == "__main__":
    sys.argv[0] = re.sub(r"(-script\.pyw|\.exe)?$", "", sys.argv[0])
    fix_imports.main()
