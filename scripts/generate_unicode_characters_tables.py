#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import csv
import json
import os
import sys

root_folder = os.path.realpath(
    os.path.abspath(
        os.path.join(
            os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir))
        )
    )
)
sublime_text_config_folder = os.path.normpath(
    os.path.join(root_folder, *([os.pardir] * 2))
)
sublime_text_lib_folder = os.path.join(sublime_text_config_folder, "Lib", "python38")

sys.path.insert(0, sublime_text_lib_folder)

ignored_char_template = """Unicode code: {code}
Unicode name: {name}
Unicode category: {category}
{win_alt_codes}
"""
json_separators = (",", ":")
json_dump_kwargs_indented = dict(indent=4, ensure_ascii=True, sort_keys=True)
json_dump_kwargs = dict(separators=json_separators, ensure_ascii=True, sort_keys=True)
data_folder = os.path.join(root_folder, "plugins", "characters_table", "data")
unicode_data_file = os.path.join(data_folder, "source_data", "UnicodeData.txt")
rfc1345_mnemonics_file = os.path.join(
    data_folder, "source_data", "rfc1345-mnemonics.txt"
)
unicode_to_win_alt_file = os.path.join(
    data_folder, "source_data", "unicode_to_win_alt_map.json"
)
character_to_mnemonic_indented_file = os.path.join(
    data_folder, "indented", "character_to_mnemonic_map_indented.json"
)
character_to_mnemonic_file = os.path.join(data_folder, "character_to_mnemonic_map.json")
code_to_character_map_indented_file = os.path.join(
    data_folder, "indented", "code_to_character_map_indented.json"
)
code_to_character_map_file = os.path.join(data_folder, "code_to_character_map.json")
full_unicode_map_indented_file = os.path.join(
    data_folder, "indented", "full_unicode_map_indented.json"
)
full_unicode_map_file = os.path.join(data_folder, "full_unicode_map.json")
windows_alt_codes_map_indented_file = os.path.join(
    data_folder, "indented", "windows_alt_codes_map_indented.json"
)
windows_alt_codes_map_file = os.path.join(data_folder, "windows_alt_codes_map.json")
ignored_characters_file = os.path.join(
    data_folder, "source_data", "IgnoredCharacters.txt"
)


def generate_tables():
    full_map = {}
    code_to_character_map = {}
    character_to_mnemonic_map = {}
    windows_alt_codes_map = {}
    ignored_characters_map = {}

    with open(rfc1345_mnemonics_file, "r") as f:
        for line in f.readlines():
            if len(line) < 4:
                continue
            if line[0] != " ":
                continue
            if line[1] == " ":
                continue

            mnemonic, code, iso_name = line[1:].split(None, 2)

            if len(mnemonic) == 1:
                mnemonic += " "

            try:
                char = eval(('u"\\U%08s"' % code).replace(" ", "0"))
            except BaseException:
                print("Error decoding: %s, %s\n" % (code, iso_name))
                continue

            character_to_mnemonic_map[char] = mnemonic

    with open(unicode_data_file, "r") as f:
        unicodedata_reader = csv.reader(f, delimiter=";")

        for row in unicodedata_reader:
            code = ("%08s" % row[0]).replace(" ", "0")
            char = eval(f'u"\\U{code}"')
            iso_name = row[1]

            # skip chars from "Other" Category
            if row[2].startswith("C"):
                ignored_characters_map[code] = {
                    "name": iso_name,
                    "category": row[2],
                    "win_alt_codes": "",
                }
                continue

            full_map[code] = {
                "n": iso_name,
                "m": character_to_mnemonic_map.get(char, ""),
            }
            code_to_character_map[code] = char

    with open(unicode_to_win_alt_file, "r") as f:
        unicode_to_win_alt_map = json.load(f)

        for code, win_alt_codes in unicode_to_win_alt_map.items():
            if code in full_map:
                for alt_code in win_alt_codes:
                    windows_alt_codes_map[alt_code] = {
                        "n": full_map[code]["n"],
                        "u": code,
                    }
            else:
                ignored_characters_map[code][
                    "win_alt_codes"
                ] = f"Windows alt codes: {', '.join(win_alt_codes)}\n"

    with open(code_to_character_map_indented_file, "w") as f:
        json.dump(code_to_character_map, f, **json_dump_kwargs_indented)

    with open(code_to_character_map_file, "w") as f:
        json.dump(code_to_character_map, f, **json_dump_kwargs)

    with open(full_unicode_map_indented_file, "w") as f:
        json.dump(full_map, f, **json_dump_kwargs_indented)

    with open(full_unicode_map_file, "w") as f:
        json.dump(full_map, f, **json_dump_kwargs)

    with open(character_to_mnemonic_indented_file, "w") as f:
        json.dump(character_to_mnemonic_map, f, **json_dump_kwargs_indented)

    with open(character_to_mnemonic_file, "w") as f:
        json.dump(character_to_mnemonic_map, f, **json_dump_kwargs)

    with open(windows_alt_codes_map_indented_file, "w") as f:
        json.dump(windows_alt_codes_map, f, **json_dump_kwargs_indented)

    with open(windows_alt_codes_map_file, "w") as f:
        json.dump(windows_alt_codes_map, f, **json_dump_kwargs)

    with open(ignored_characters_file, "w") as f:
        f.write(
            "".join(
                [
                    ignored_char_template.format(
                        code=code,
                        name=char_def["name"],
                        category=char_def["category"],
                        win_alt_codes=char_def["win_alt_codes"],
                    )
                    for code, char_def in ignored_characters_map.items()
                ]
            )
        )


if __name__ == "__main__":
    generate_tables()
