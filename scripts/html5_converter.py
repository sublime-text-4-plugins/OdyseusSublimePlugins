#!/usr/bin/env python3.10
# -*- coding: utf-8 -*-
"""Script to convert HTML markup into HTML5.

I use it mainly to convert HTML4 to HTML5 and to correctly mark up HTML that do not use closing
tags. I would like to "meet" the ones that decided that it's OK to use the latter aberration!!!

It's a script because it uses a third-party Python modules and because who in the f*ck uses Python 3.3!?!?!?
Only Sublime Text, that's who!!!! FFS!!!
"""
try:
    import html5lib
except (ImportError, SystemError):
    raise SystemExit("Required <html5lib> Python module not found.")

__version__ = "Ω"

import argparse
import sys

_args_map = [
    ("--doctype-declaration", "doctype_declaration", "str", "<!DOCTYPE html>",
        "Doctype declaration. [Default: <!DOCTYPE html>]"),
    ("--quote-attr-values", "quote_attr_values", "str", "legacy",
        "Whether to quote attribute values that don't require quoting per legacy browser behavior "
        "(legacy), when required by the standard (spec), or always (always). [Default: legacy]"),
    ("--quote-char", "quote_char", "str", "\"",
        "Use given quote character for attribute quoting. Defaults to '\"' which will use double "
        "quotes unless attribute value contains a double quote, in which case single quotes are used. "
        "[Default: \"]"),
    ("--omit-optional-tags", "omit_optional_tags", "bool", False,
        "Omit start/end tags that are optional."),
    ("--minimize-boolean-attributes", "minimize_boolean_attributes", "bool", False,
        "Shortens boolean attributes to give just the attribute value, for example: "
        "**<input disabled=\"disabled\">** becomes **<input disabled>**."),
    ("--use-trailing-solidus", "use_trailing_solidus", "bool", False,
        "Includes a close-tag slash at the end of the start tag of void elements (empty elements "
        "whose end tag is forbidden). E.g. <hr/>."),
    ("--space-before-trailing-solidus", "space_before_trailing_solidus", "bool", False,
        "Places a space immediately before the closing slash in a tag using a trailing solidus. "
        "E.g. <hr />. Requires --use-trailing-solidus."),
    ("--escape-lt-in-attrs", "escape_lt_in_attrs", "bool", False,
        "Whether or not to escape '<'' in attribute values."),
    ("--escape-rcdata", "escape_rcdata", "bool", False,
        "Whether to escape characters that need to be escaped within normal elements within rcdata "
        "elements such as style."),
    ("--resolve-entities", "resolve_entities", "bool", False,
        "Whether to resolve named character entities that appear in the source tree. The XML "
        "predefined entities &lt; &gt; &amp; &quot; &apos; are unaffected by this setting."),
    ("--alphabetical-attributes", "alphabetical_attributes", "bool", False,
        "Reorder attributes to be in alphabetical order."),
    ("--inject-meta-charset", "inject_meta_charset", "bool", False,
        "Whether or not to inject the meta charset."),
    ("--strip-whitespace", "strip_whitespace", "bool", False,
        "Whether to remove semantically meaningless whitespace. (This compresses all whitespace "
        "to a single space except within 'pre'.)"),
    ("--sanitize", "sanitize", "bool", False,
        "Strip all unsafe or unknown constructs from output. "
        "See :py:class:`html5lib.filters.sanitizer.Filter`.")
]
_ignored_serializer_args = ("doctype_declaration")
_description = """Convert HTML markup into HTML5.
It only accepts input from STDIN and outputs into STDOUT. I use it mainly to convert HTML4 to HTML5
and to correctly mark up HTML that do not use closing tags.

Note: All boolean parameters passed to :py:class:`html5lib.serializer.HTMLSerializer` class
are overridden to False."
"""

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=_description
    )

    for cli_arg, cls_arg, arg_type, default, help_txt in _args_map:
        if arg_type == "bool":
            parser.add_argument(
                cli_arg,
                action="store_true",
                default=default,
                dest=cls_arg,
                help=help_txt
            )
        elif arg_type == "str":
            parser.add_argument(
                cli_arg,
                nargs="?",
                const=default,
                default=default,
                dest=cls_arg,
                help=help_txt
            )

    args = parser.parse_args()
    kwargs = vars(args)
    serializer_args = {key: val for key, val in kwargs.items()
                       if key not in _ignored_serializer_args}

    html_output = []

    try:
        if sys.stdin.isatty():
            raise RuntimeError("No STDIN passed.")

        html_data = sys.stdin.read().strip()

        if not html_data:
            raise RuntimeError("STDIN is empty.")

        element = html5lib.parse(html_data)
        walker = html5lib.getTreeWalker("etree")
        stream = walker(element)
        serializer = html5lib.serializer.HTMLSerializer(**serializer_args)
        output = serializer.serialize(stream)

        for item in output:
            html_output.append(item)
    except Exception as err:
        sys.stdout.write(err)
        raise SystemExit(err)

    if html_output:
        if kwargs["doctype_declaration"]:
            html_output.insert(0, kwargs["doctype_declaration"])
            html_output.insert(1, "\n")

        sys.stdout.write("".join(html_output))

    raise SystemExit(0)
